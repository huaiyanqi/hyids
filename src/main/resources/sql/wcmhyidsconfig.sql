/*
Navicat MySQL Data Transfer

Source Server         : localhost_admin
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : mty_wcm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-08-03 16:23:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wcmhyidsconfig
-- ----------------------------
DROP TABLE IF EXISTS `wcmhyidsconfig`;
CREATE TABLE `wcmhyidsconfig` (
  `configid` int(11) NOT NULL,
  `ctype` smallint(6) NOT NULL DEFAULT 0,
  `ckey` varchar(300) NOT NULL,
  `cvalue` varchar(500) DEFAULT NULL,
  `cdesc` varchar(300) DEFAULT NULL,
  `characteristic` varchar(300) NOT NULL,
  PRIMARY KEY (`configid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- 微信预览时 需要的微信账号
INSERT INTO `mty_wcm`.`wcmhyidsconfig` (`configid`, `ctype`, `ckey`, `cvalue`, `cdesc`, `characteristic`) VALUES ('1', '1', 'wxnumber', 'watnry7', '微信预览时需要的微信公众号', 'wx');

