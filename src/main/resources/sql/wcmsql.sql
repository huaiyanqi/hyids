-- ----------------------------
-- // 添加纸媒  1 同意  领导是否审核标识 
-- ----------------------------
ALTER TABLE WCMChnlDoc  ADD   Alreadyauditing  varchar(10)  null  DEFAULT null
-- ----------------------------
-- // 添加稿件oa送审状态    1 = 已上传   2=成功  3=终止 4=回退
-- ----------------------------
ALTER TABLE WCMChnlDoc  ADD   UnderReview  varchar(10)  null  DEFAULT null
