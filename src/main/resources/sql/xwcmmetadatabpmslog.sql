/*
Navicat MySQL Data Transfer

Source Server         : localhost_admin
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : mty_wcm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-07-12 17:36:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for xwcmmetadatabpmslog
-- ----------------------------
DROP TABLE IF EXISTS `xwcmmetadatabpmslog`;
CREATE TABLE `xwcmmetadatabpmslog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recid` int(11) DEFAULT NULL COMMENT '海融wcmchnldocid',
  `channel` varchar(50) DEFAULT NULL COMMENT '推送的渠道（wx，wz，zm，gr）',
  `pushtime` datetime DEFAULT NULL,
  `pushtitle` varchar(500) DEFAULT '' COMMENT '标题',
  `pushsig` int(5) DEFAULT NULL COMMENT '1 标识bpms没有返回结果   2  标识返回结果了',
  `returnjson` varchar(3000) DEFAULT NULL COMMENT '推送给bpms时返回的最终json',
  `bpmsopinion` varchar(3000) DEFAULT NULL COMMENT 'bpms返回结果内容  ',
  `bpmspushtime` datetime DEFAULT NULL COMMENT 'bpms 返回时间   ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
