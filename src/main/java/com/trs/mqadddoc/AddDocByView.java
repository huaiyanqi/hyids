package com.trs.mqadddoc;

import com.trs.addhytokpsdzc.AddDocumentControllerLSGB;
import com.trs.addhytokpsdzc.AddDocumentControllerQLQD;
import com.trs.addhytokpsdzc.AddDocumentControllerQXZMCX;
import com.trs.addhytokpsdzc.AddDocumentControllerZCJD;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpsdzc.AddDocumentControllerZWMC;
import com.trs.addhytokpsdzc.AddDocumentControllerZWMCDBP;
import com.trs.addhytokpsdzc.AddDocumentControllerBMWD;
import com.trs.addhytokpsdzc.AddDocumentControllerCZXX;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWHDJLZXFT;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWJZFGB;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZCZ;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZGH;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZHDJLYJZJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZHDJLZXDC;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZHDJLZXJYTS;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZHY;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZJGGS;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZLDJL;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZRQGS;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZRSRM;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZZCJD;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.ResResource;
import com.trs.hy.Reslibrary;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;

public class AddDocByView {
	
	
	ChannelReceiver cR=new ChannelReceiver();
	Reslibrary  rl= new Reslibrary();
	ResResource  re= new ResResource();
	HttpFileUpload hfl=new HttpFileUpload();
	JDBCIIP iip=new JDBCIIP();
	AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	static AddDocumentControllerCZXX zwgk=new AddDocumentControllerCZXX();
	JDBCIDS  jdbcids=new JDBCIDS();
	
	public static void addDocByView(String docid,String chnlid,String viewName,String SITEID) throws Exception{
		
		//自定义视图开始新建文章
		if("WEBLDJL".equals(viewName)){
			//政府网站-------领导简历  
			AddDocumentControllerZFWZLDJL.addDocumentZFWZLDJL(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================领导简历  视图新增数据结束=========================","addDoc",0);	
		}else  if("WEBRSRM".equals(viewName)){
			//政府网站-------人事任免
			AddDocumentControllerZFWZRSRM.addDocumentZFWZRSRM(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================人事任免视图新增数据结束=========================","addDoc",0);	
		}else if("WEBZCJD".equals(viewName)){
			//政府网站-------政策解读
			AddDocumentControllerZFWZZCJD.addDocumentZFWZZCJD(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================政策解读视图新增数据结束=========================","addDoc",0);	
		}else if("WEBZCWJ".equals(viewName)){
			//政府网站-------政策文件
			AddDocumentControllerZFWZZCWJ.addDocumentZFWZZCWJ(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================政策文件视图新增数据结束=========================","addDoc",0);	
		}else if("WEBCZ".equals(viewName)){
			//政府网站-------财政
			AddDocumentControllerZFWZCZ.addDocumentZFWZCZ(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================财政视图新增数据结束=========================","addDoc",0);	
		}else if("WEBRQGS".equals(viewName)){
			//政府网站-------任前公示
			AddDocumentControllerZFWZRQGS.addDocumentZFWZRQGS(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================任前公示视图新增数据结束=========================","addDoc",0);	
		}else if("WEBGH".equals(viewName)){
			//政府网站-------规划
			AddDocumentControllerZFWZGH.addDocumentZFWZGH(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================规划视图新增数据结束=========================","addDoc",0);	
		}else if("WEBHY".equals(viewName)){
			//政府网站-------会议
			AddDocumentControllerZFWZHY.addDocumentZFWZHY(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================会议视图新增数据结束=========================","addDoc",0);	
		}else if("WEBZFGB".equals(viewName)){
			//政府网站-------政府公报
			AddDocumentControllerZFWJZFGB.addDocumentZFWJZFGB(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================政府公报视图新增数据结束=========================","addDoc",0);	
		}else if("WEBHDJLZXJYTS".equals(viewName)){
			//政府网站-------互动交流咨询建议投诉
			AddDocumentControllerZFWZHDJLZXJYTS.addDocumentZFWZHDJLZXJYTS(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================互动交流咨询建议投诉视图新增数据结束=========================","addDoc",0);	
		}else if("WEBHDJLZXDC".equals(viewName)){
			//政府网站-------互动交流在线调查
			AddDocumentControllerZFWZHDJLZXDC.addDocumentZFWZHDJLZXDC(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================互动交流在线调查视图新增数据结束=========================","addDoc",0);	
		}else if("WEBHDJLZXFT".equals(viewName)){
			//政府网站-------互动交流在线访谈
			AddDocumentControllerZFWHDJLZXFT.addDocumentZFWJHDJLZXFT(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================互动交流在线访谈视图新增数据结束=========================","addDoc",0);	
		}else if("WEBHDJLYJZJ".equals(viewName)){
			//政府网站-------互动交流意见征集
			AddDocumentControllerZFWZHDJLYJZJ.addDocumentZFWZHDJLYJZJ(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================互动交流意见征集视图新增数据结束=========================","addDoc",0);	
		}else if("WEBJGGS".equals(viewName)){
			//政府网站-------结果公示
			AddDocumentControllerZFWZJGGS.addDocumentZFWZJGGS(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================结果公示视图新增数据结束=========================","addDoc",0);	
		}else if("ZCWJ".equals(viewName)){
			// 首都之窗-----政策文件 
			AddDocumentControllerZCWJ.addDocumentZCJDandZCWJ(docid, chnlid, viewName.toUpperCase(),SITEID);
			Util.log("==================================首都之窗 政策文件视图新增数据结束=========================","addDoc",0);	
		}else if("ZCJD".equals(viewName)){
			// 首都之窗----- 政策解读   
			AddDocumentControllerZCJD.addDocumentZCJD(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================首都之窗 政策解读  视图新增数据结束=========================","addDoc",0);	
		}else if("CZXX".equals(viewName)){
			//首都之窗 ------- 财政信息
			AddDocumentControllerCZXX.addDocumentCZXX(docid, chnlid, viewName.toUpperCase(),SITEID);
			Util.log("==================================首都之窗 财政信息视图新增数据结束=========================","addDoc",0);	
		}else if("LSGB".equals(viewName)){
			//首都之窗 ------- 历史公报  
			AddDocumentControllerLSGB.addDocumentLSGB(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================首都之窗 历史公报 视图新增数据结束=========================","addDoc",0);	
		}else if("ZWMC".equals(viewName)){
			//首都之窗 ------- 政务名词   以下视图  不做回推代码
			AddDocumentControllerZWMC.addDocumentZWMC(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================首都之窗 政务名词 视图新增数据结束=========================","addDoc",0);	
		}else if("ZWMCDBP".equals(viewName)){
			//首都之窗 ------- 政务名词大比拼
			AddDocumentControllerZWMCDBP.addDocumentZWMCDBP(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================首都之窗 政务名词大比拼 视图新增数据结束=========================","addDoc",0);	
		}else if("XQZMCX".equals(viewName)){
			//首都之窗 ------- 取消证明查询
			AddDocumentControllerQXZMCX.addDocumentQXZMCX(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================首都之窗 取消证明查询 视图新增数据结束=========================","addDoc",0);	
		}else if("BMWD".equals(viewName)){
			//首都之窗 ------- 便民问答
			AddDocumentControllerBMWD.addDocumentBMWD(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================首都之窗 便民问答 视图新增数据结束=========================","addDoc",0);	
		}else if("QLQD".equals(viewName)){
			//首都之窗 -------权力清单
			AddDocumentControllerQLQD.addDocumentQLQD(docid,chnlid,viewName.toUpperCase(),SITEID);
			Util.log("==================================首都之窗 权力清单 视图新增数据结束=========================","addDoc",0);	
		}
		
	}

}
