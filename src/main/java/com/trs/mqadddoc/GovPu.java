package com.trs.mqadddoc;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResDirectory;
import com.trs.hy.ResResource;
import com.trs.hy.Reslibrary;
import com.trs.ids.SyncIdsObjectUtil;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.middleUtil;
import com.trs.zfwz.zfwzUtil;

public class GovPu {
	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String Z_ID =null;
	ChannelReceiver cR=new ChannelReceiver();
	Reslibrary  rl= new Reslibrary();
	ResResource  re= new ResResource();
	HttpFileUpload hfl=new HttpFileUpload();
	JDBCIIP iip=new JDBCIIP();
	AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	JDBCIDS  jdbcids=new JDBCIDS();
	AddDocByView ac=new AddDocByView();
	/**
	 * 添加栏目到开普
	 * @param jsondata
	 */
	@SuppressWarnings("unchecked")
	public void addChannelHY(JSONObject jsondata){
		
		
		ResourceBundle bundle = ResourceBundle.getBundle("services");
		try {
			Z_ID = new String(bundle.getString("Z_ID").getBytes("ISO-8859-1"), "UTF8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		//MQ消息队列数据获取
		String CHANNELID=jsondata.getString("CHANNELID");//栏目id
		String CHNLNAME=jsondata.getString("CHNLDESC");//栏目名称
		String SITEID=jsondata.getString("SITEID");//站点id
		String TRUENAME=jsondata.getString("TRUENAME");//真实姓名
		String CRUSER=jsondata.getString("CRUSER");//创建人
		String OPERUSER=jsondata.getString("OPERUSER");//操作人
		String CHNLDESC=jsondata.getString("CHNLDESC");//栏目描述
		String PARENTID=jsondata.getString("PARENTID");//父栏目id
		String CRTIME=jsondata.getString("CRTIME");//创建时间
		String CHNLDESCPINYIN=jsondata.getString("CHNLDESCPINYIN");//栏目名称缩写
		
		Util.log("------------新建栏目开始------------","addLMlog"+SITEID,0);
		System.out.println("------------新建栏目开始------------");	
		
		//根据站点id 获取到开普对应的资源库id
		String kaipuid="无数据";
		String kaipuPARENTID="无数据";
		String kaipuidLM=null;
		String ysjjid="";
		String zykid="没有相关匹配资源库id";
		String SITENAME="";
		
		//根据站点id查询站点名称
		String sServiceId="gov_site";
		String sMethodName="queryNavDesc";
		Map savemap = new HashMap();
		savemap.put("SiteId",SITEID);
		savemap.put("CurrUserName", USER); // 当前操作的用户
		String hy11=null;
		try {
			hy11 = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Util.log("查询栏目接口："+hy11,"addLMlog"+SITEID,0);
		JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
		Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
		System.out.println("添加栏目根据站点id查询站点名称--海云返回结果："+jsonObjectHYSE.get("MSG"));
		Util.log("添加栏目根据站点id查询站点名称--海云返回结果："+jsonObjectHYSE.get("MSG"),"addLMlog"+SITEID,0);
		if("true".equals(dataArrHYSave)){
			SITENAME=jsonObjectHYGPSE.getString("NAVDESC");//站点名称
		}
		
		//开始判断资源库id
		String [] zid=Z_ID.split(";");
		for(int z=0;z<zid.length;z++){
			String zid1=zid[z];
			Util.log("第"+z+"组资源库id名称："+zid1,"addLMlog",0);
			System.out.println("第"+z+"组资源库id名称："+zid1);
			
			String [] zsting=zid1.split(",");
			
			Util.log("第"+z+"次资源库得名称："+zsting[1],"addLMlog"+SITEID,0);
			System.out.println("第"+z+"次资源库得名称："+zsting[1]);
			Util.log(zsting[1]+"========================"+SITENAME,"addLMlog"+SITEID,0);
			System.out.println(zsting[1]+"========================"+SITENAME);
			if(SITENAME.equals(zsting[1])){
				zykid=zsting[0];
				try {
					if(!PARENTID.equals("0")){
						kaipuPARENTID=cR.channelIDCheck(PARENTID);
					}
					// 站点对应的开普的id
					kaipuid=cR.channelReceiverCheck(SITEID);
					//查询栏目是否存在开普
					kaipuidLM=cR.LMReceiverCheck(CHANNELID);
					//查询新增栏目的视图id
					String viewid=zfwzUtil.selectViewid(CHANNELID);
					//查询开普元数据集id
					if("".equals(viewid)){
						System.out.println("咨询视图空字符串：："+viewid);
						viewid="5";
					}
					String sql="SELECT * FROM viewid WHERE viewid = '"+viewid+"'";
					ysjjid=JDBC.selectYsjjid(sql);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Util.log("查询栏目上级栏目是否存在开普：：："+kaipuPARENTID+"============================"+"开普是否存在该栏目得站点：：："+kaipuid,"addLMlog"+SITEID,0);
				System.out.println("查询栏目上级栏目是否存在开普：：："+kaipuPARENTID+"============================"+"开普是否存在该栏目得站点：：："+kaipuid);
				if(kaipuPARENTID != null && kaipuid != null){
					
					if(kaipuidLM!=null){
						Util.log("该栏目已经存在与开普资源库。","addLMlog"+SITEID,0);
						System.out.println("该栏目已经存在与开普资源库。");
					}else{
						//存入开普数据库传参数
						JSONObject json= new JSONObject();
				        json.put("dirName", CHNLDESC);//目录名称
				        json.put("dirCode", CHNLDESCPINYIN);//目录代号
				        json.put("seqNum", "1");//排序号
//				        json.put("dirType", "");//目录类型
				        if(!PARENTID.equals("0")){
					        json.put("dirPid", kaipuPARENTID);//父目录id，空表示为根目录
				        }else{
//				        	kaipuid=PARENTID;
				        	json.put("dirPid", kaipuid);//父目录id，空表示为根目录
				        }
				        
				        json.put("metadataIds",ysjjid);//直接挂元数据集id
				        json.put("libId",zykid);//目录所属资源库ID  
				        json.put("isOrig", "0");//dirPid为资源库的目录ID 
				        json.put("memo",CHNLNAME);//栏目描述CHNLNAME
				        json.put("creatorId",OPERUSER);//创建人
				        
				        ResDirectory rd= new ResDirectory();
				        String result=null;
				        try {
				        	result=rd.addResDirectory(json);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Util.log("新增栏目：：："+json,"addLMlog"+SITEID,0);
				        //处理返回结果，拿到开普栏目的id
						JSONObject jsonKp = JSON.parseObject(result);
						JSONObject kpLanmu = jsonKp.getJSONObject("data");
						String resultkp=jsonKp.getString("code");
						Util.log("新增栏目返回结果：：："+result,"addLMlog"+SITEID,0);
						if(resultkp.equals("0")){
							String dirId=kpLanmu.getString("dirId");//站点id
							// 两方id存入到中间表
							JDBC  jdbc=new JDBC();
							Util.log("栏目存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTIDLM+",siteid) VALUES ('"+dirId+"','"+CHANNELID+"','LM','"+PARENTID+"',"+SITEID+")","addLMlog"+SITEID,0);
							System.out.println("栏目存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTIDLM+",siteid) VALUES ('"+dirId+"','"+CHANNELID+"','LM','"+PARENTID+"',"+SITEID+")");
							try {
								Integer jd=jdbc.JDBCDriver("INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTIDLM+",siteid) VALUES ('"+dirId+"','"+CHANNELID+"','LM','"+PARENTID+"',"+SITEID+")");
								if(jd!=-1){
									Util.log("该栏目中间表存入成功!","addLMlog"+SITEID,0);
									System.out.println("栏目中间表存入成功!");
								}else{
									Util.log("该栏目中间表存入失败!","addLMlog"+SITEID,0);
									System.out.println("栏目中间表存入失败!");
								}
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}else{
							Util.log("栏目同步开普失败"+jsonKp.getString("msg"),"addLMlog"+SITEID,0);
							System.out.println("栏目同步开普失败"+jsonKp.getString("msg"));
						}
						
						
					}	
				}else{
					Util.log("第"+z+"次资源库得名称："+zsting[1],"addLMlog"+SITEID,0);
					System.out.println("中间表中没有该栏目父栏目或站点数据，不做新建");
				}
				Util.log("资源库id结果"+zykid,"addLMlog"+SITEID,0);
				Util.log("站点名称"+SITENAME,"addLMlog"+SITEID,0);
				System.out.println("资源库id结果"+zykid);
				System.out.println("站点名称"+SITENAME);
			}else{
				Util.log("资源库没有相关站点id："+SITENAME,"addLMlog"+SITEID,0);
				System.out.println("资源库没有相关站点id："+SITENAME);
			}
		}
		
		Util.log("------------新建栏目结束------------","addLMlog"+SITEID,0);
		System.out.println("------------新建栏目结束------------");
		
	}
	
	/**
	 * 修改开普栏目
	 * @param jsondata
	 */
	public  void  upChannelHY(JSONObject jsondata){
		

		//MQ消息队列数据获取
		
		//MQ消息队列数据获取
		String CHANNELID=jsondata.getString("CHANNELID");//栏目id
		String OPERUSER=jsondata.getString("OPERUSER");//操作人
		String CHNLDESC=jsondata.getString("CHNLDESC");//栏目描述
		String CHNLNAME=jsondata.getString("CHNLNAME");//栏目名称
		String SITEID=jsondata.getString("SITEID");//站点id
		
		Util.log("------------修改栏目结束------------","updateLMlog"+SITEID,0);
		System.out.println("------------修改栏目开始------------");
		
		//根据站点id 获取到开普对应的资源库id
		String kaipuidLM=null;
		String ysjjid="";
		try {
			//查询栏目是否存在开普
			kaipuidLM=cR.LMReceiverCheck(CHANNELID);
			//查询新增栏目的视图id
			String viewid=zfwzUtil.selectViewid(CHANNELID);
			//查询开普元数据集id
			if("".equals(viewid)){
				System.out.println("咨询视图空字符串：："+viewid);
				viewid="5";
			}
			String sql="SELECT * FROM viewid WHERE viewid = '"+viewid+"'";
			ysjjid=JDBC.selectYsjjid(sql);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(kaipuidLM == null){
			Util.log("该栏目开普资源库中不存在。","updateLMlog"+SITEID,0);
			System.out.println("该栏目开普资源库中不存在。");
		}else{
			//存入开普数据库传参数
			JSONObject json= new JSONObject();
	        json.put("dirName", CHNLDESC);//目录名称
	        json.put("dirId", kaipuidLM);//目录id  开普的id
	        json.put("isOrig", "0");//dirPid为资源库的目录ID 
	        json.put("memo",CHNLNAME);//栏目描述
	        json.put("modifierId",OPERUSER);//修改人
	        json.put("metadataIds",ysjjid);//直接挂元数据集id
	        
	        ResDirectory rd= new ResDirectory();
	        String result=null;
	        try {
	        	result=rd.updateResDirectory(json);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        //处理返回结果，拿到开普栏目的id
			JSONObject jsonKp = JSON.parseObject(result);
			String resultkp=jsonKp.getString("code");
			if(resultkp.equals("0")){
				Util.log(CHNLNAME+"，栏目修改成功！","updateLMlog"+SITEID,0);
				System.out.println(CHNLNAME+"，栏目修改成功！");
			}else{
				Util.log(CHNLNAME+"，栏目修改失败！："+jsonKp.getString("msg"),"updateLMlog"+SITEID,0);
				System.out.println(CHNLNAME+"，栏目修改失败！："+jsonKp.getString("msg"));
			}
		}
		Util.log("------------修改栏目结束------------","updateLMlog"+SITEID,0);
		System.out.println("------------修改栏目结束------------");
		
	}
	
	
	/**
	 * 删除栏目
	 * @param jsondata
	 */
	public  void  deChannelHY(JSONObject jsondata){

		//MQ消息队列数据获取
		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  站点id SITEID  父栏目id  PARENTID
		String CHANNELID=jsondata.getString("CHANNELID");//栏目id
		String CHNLNAME=jsondata.getString("CHNLNAME");//栏目名称
		String SITEID=jsondata.getString("SITEID");//站点id
		String pid=jsondata.getString("PARENTID");//栏目父id
		
		Util.log("------------删除栏目开始------------","delleteLMlog"+SITEID,0);
		System.out.println("------------删除栏目开始------------");
		
		
		//根据站点id 获取到开普对应的资源库id
		String kaipuidLM=null;
		try {
			//查询栏目是否存在开普
			kaipuidLM=cR.LMReceiverCheck(CHANNELID);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(kaipuidLM == null){
			Util.log(CHANNELID+"======================================"+CHNLNAME,"delleteLMlog"+SITEID,0);
			Util.log("该栏目开普资源库中不存在。","delleteLMlog"+SITEID,0);
			System.out.println("该栏目开普资源库中不存在。");
		}else{
			
			
			try {
				//当前方法删除的是 当前栏目 
				middleUtil.deleteLm(CHANNELID,SITEID ,CHNLNAME);
				//删除当前栏目下的数据
				middleUtil.deleteDocument(CHANNELID,SITEID ,CHNLNAME);
				//删除中间表    删除的是 当前栏目的下级全部栏目及数据
				middleUtil.selectLM(SITEID,CHANNELID, "dev");
			} catch (Exception e) {
				Util.log(CHANNELID+"======================================"+CHNLNAME,"delleteLMlog"+SITEID,0);
				Util.log(CHNLNAME+"，中间表栏目级数据删除失败！","delleteLMlog"+SITEID,0);
				e.printStackTrace();
			}
			
			//存入开普数据库传参数
			JSONObject json= new JSONObject();
	        json.put("dirIds", kaipuidLM);//目录id 开普
	        json.put("isOrig", "0");//isOrig=0时，dirIds为资源库的目录ID
	        
	        ResDirectory rd= new ResDirectory();
	        String result=null;
	        try {
	        	result=rd.delAllDirs(json);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        //处理返回结果，拿到开普栏目的id 
	        //无论开普删除成功与否 都删除中间表
			JSONObject jsonKp = JSON.parseObject(result);
			String resultkp=jsonKp.getString("code");
			if(resultkp.equals("0")){
				Util.log(CHANNELID+"======================================"+CHNLNAME,"delleteLMlog"+SITEID,0);
				Util.log(CHNLNAME+"，栏目删除成功！","delleteLMlog"+SITEID,0);
				System.out.println(CHNLNAME+"，栏目删除成功！");
			}else{
				Util.log(CHNLNAME+"，栏目删除失败！："+jsonKp.getString("msg"),"delleteLMlog"+SITEID,0);
				System.out.println(CHNLNAME+"，栏目删除失败！："+jsonKp.getString("msg"));
			}
		}
		Util.log("------------删除栏目结束------------","delleteLMlog"+SITEID,0);
		System.out.println("------------删除栏目结束------------");
		
	
	}
	 
	/**
	 * 新建站点
	 * @param jsondata
	 */
	public  void  addSite(JSONObject jsondata){
		
		ResourceBundle bundle = ResourceBundle.getBundle("services");
		try {
			Z_ID = new String(bundle.getString("Z_ID").getBytes("ISO-8859-1"), "UTF8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String zykid="没有相关匹配资源库id";
		Util.log("---------------------新建站点开始---------------------","addLMlog",0);
		System.out.println("---------------------新建站点开始---------------------");
			if(jsondata!=null){
				
//				System.out.println("获取到监听mq的资源库的修改信息"+dataArr.size()+"资源库数据====="+dataArr);
				//对解析出的数组进行遍历
				String SITENAME=jsondata.getString("SITENAME");//站点名称
				String SITEID=jsondata.getString("SITEID");//站点id
				String CRUSER=jsondata.getString("CRUSER");//创建人
				String STATUS=jsondata.getString("STATUS");//站点状态
				String DATAPATH=jsondata.getString("DATAPATH");// url  ==  DATAPATH 数据存放位置
				String SITEDESC=jsondata.getString("SITEDESC");//站点描述
				String CHNLDESCPINYIN=jsondata.getString("SITEDESCPINYIN");//栏目名称缩写
				
				
				//开始判断资源库id
				String [] zid=Z_ID.split(";");
				
				for(int z=0;z<zid.length;z++){
					String zid1=zid[z];
					System.out.println("第"+z+"组资源库id名称："+zid1);
					String [] zsting=zid1.split(",");
					String zname=zsting[1];
					System.out.println("第"+z+"次资源库得名称："+zsting[1]);
					
					if(zname.equals(SITENAME)){
						//获取到得资源库id
						zykid=zsting[0];
						
						//根据站点id 获取到开普对应的资源库id
						String kaipuidZD=null;
						try {
							//查询站点是否存在开普
							kaipuidZD=cR.channelReceiverCheck(SITEID);
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(kaipuidZD!=null){
							Util.log("该站点已经存在于开普资源库。","addLMlog",0);
							System.out.println("该站点已经存在于开普资源库。");
						}else{
							//存入开普数据库传参数
							JSONObject json= new JSONObject();
					        json.put("dirName", SITENAME);//目录名称
					        json.put("dirCode", CHNLDESCPINYIN);//目录代号
					        json.put("seqNum", "1");//排序号
					        json.put("dirPid", "");//父目录id，空表示为根目录
					        json.put("libId",zykid);//目录所属资源库ID  站点id
					        json.put("isOrig", "0");//dirPid为资源库的目录ID 
					        json.put("memo",SITEDESC);//栏目描述
					        json.put("creatorId",CRUSER);//创建人
					        
					        ResDirectory rd= new ResDirectory();
					        String result=null;
					        try {
					        	result=rd.addResDirectory(json);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					        //处理返回结果，拿到开普栏目的id
							JSONObject jsonKp = JSON.parseObject(result);
							JSONObject kpLanmu = jsonKp.getJSONObject("data");
							String resultkp=jsonKp.getString("code");
							String dirId=null;
							if(resultkp.equals("0")){
								Util.log("-----------新建站点成功------------","addLMlog",0);
								System.out.println("-----------新建站点成功------------");
								dirId=kpLanmu.getString("dirId");//站点id
								// 两方id存入到中间表
								JDBC  jdbc=new JDBC();
								Util.log("站点存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTIDLM+") VALUES ('"+dirId+"','"+SITEID+"','ZD','0')","addLMlog",0);
								System.out.println("站点存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTIDLM+") VALUES ('"+dirId+"','"+SITEID+"','ZD','0')");
								try {
									Integer jd=jdbc.JDBCDriver("INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTIDLM+") VALUES ('"+dirId+"','"+SITEID+"','ZD','0')");
									if(jd!=-1){
										Util.log("站点中间表存入成功!","addLMlog",0);
										System.out.println("站点中间表存入成功!");
									}else{
										Util.log("站点中间表存入失败!","addLMlog",0);
										System.out.println("站点中间表存入失败!");
									}
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							}else{
								Util.log("站点同步开普失败"+jsonKp.getString("msg"),"addLMlog",0);
								System.out.println("站点同步开普失败"+jsonKp.getString("msg"));
							}
							
						}
						System.out.println("资源库id结果"+zykid);
						System.out.println("站点名称"+SITENAME);
					}else{
						System.out.println("资源库中没有相关站点id："+SITENAME);
					}
					
				}
				
			}
			Util.log("---------------------新建站点结束---------------------","addLMlog",0);
			System.out.println("---------------------新建站点结束---------------------");
		
		
	}
	
	
	/**
	 * 修改站点
	 * @param jsondata
	 */
	public  void  upSite(JSONObject jsondata){

		 Util.log("------------修改站点开始------------","addLMlog",0);
		 System.out.println("------------修改站点开始------------");
			if(jsondata!=null){
//				System.out.println("获取到监听mq的资源库的修改信息"+dataArr.size()+"资源库数据====="+dataArr);
				//对解析出的数组进行遍历
				String SITENAME=jsondata.getString("SITENAME");//站点名称
				String SITEID=jsondata.getString("SITEID");//站点id
				String CRUSER=jsondata.getString("CRUSER");//创建人
				String SITEDESC=jsondata.getString("SITEDESC");//站点描述
				
				//根据站点id 获取到开普对应的资源库id
				String kaipuidLM=null;
				try {
					//查询栏目是否存在开普
					kaipuidLM=cR.channelReceiverCheck(SITEID);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("修改站点的目录id："+kaipuidLM);
				if(kaipuidLM == null){
					Util.log("该栏目开普资源库中不存在。","addLMlog",0);
					System.out.println("该栏目开普资源库中不存在。");
				}else{
					//存入开普数据库传参数
					JSONObject json= new JSONObject();
			        json.put("dirName", SITENAME);//目录名称
			        json.put("dirId", kaipuidLM);//目录id  开普的id
			        json.put("isOrig", "0");//dirPid为资源库的目录ID 
			        json.put("memo",SITEDESC);//栏目描述
			        json.put("modifierId",CRUSER);//修改人
			        
			        ResDirectory rd= new ResDirectory();
			        String result=null;
			        try {
			        	result=rd.updateResDirectory(json);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			        //处理返回结果，拿到开普栏目的id
					JSONObject jsonKp = JSON.parseObject(result);
					String resultkp=jsonKp.getString("code");
					if(resultkp.equals("0")){
						Util.log(SITENAME+"，站点修改成功！","addLMlog",0);
						System.out.println(SITENAME+"，站点修改成功！");
					}else{
						Util.log(SITENAME+"，站点修改失败！："+jsonKp.getString("msg"),"addLMlog",0);
						System.out.println(SITENAME+"，站点修改失败！："+jsonKp.getString("msg"));
					}
				}
		}	
	 Util.log("------------修改站点结束------------","addLMlog",0);		
	 System.out.println("------------修改站点结束------------");	
	 

	}
	
	
	/**
	 * 删除站点
	 * @param jsondata
	 */
	public  void  deSite(JSONObject jsondata){
		

		Util.log("------------删除站点开始------------","addLMlog",0);	
		System.out.println("------------删除站点开始------------");	
		
		if(jsondata!=null){
			
			//对解析出的数组进行遍历
			String SITENAME=jsondata.getString("SITENAME");//站点名称
			String SITEID=jsondata.getString("SITEID");//站点id
				
				//根据站点id 获取到开普对应的资源库id
				String kaipuidLM=null;
				try {
					//查询站点是否存在开普
					kaipuidLM=cR.channelReceiverCheck(SITEID);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(kaipuidLM == null){
					Util.log("该栏目开普资源库中不存在。","addLMlog",0);
					System.out.println("该栏目开普资源库中不存在。");
				}else{
					//删除开普数据库中该站点
					JSONObject json= new JSONObject();
			        json.put("dirIds", kaipuidLM);//目录id 开普
			        json.put("isOrig", "0");//isOrig=0时，dirIds为资源库的目录ID
			        
			        ResDirectory rd= new ResDirectory();
			        String result=null;
			        try {
			        	result=rd.delAllDirs(json);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			        //处理返回结果，拿到开普栏目的id
					JSONObject jsonKp = JSON.parseObject(result);
					String resultkp=jsonKp.getString("code");
					if(resultkp.equals("0")){
						Util.log(SITENAME+"，站点删除成功！","addLMlog",0);
						System.out.println(SITENAME+"，站点删除成功！");
						//删除中间表
						JDBC jdbc=new JDBC();
						Integer delete=0;
						try {
							delete = jdbc.JDBCDriver("DELETE  FROM "+TABLE+" where "+SOURCE+"='ZD' and "+FIELDHY+"='"+SITEID+"'");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(delete==1){
							Util.log("站点中间表删除成功!","addLMlog",0);
							System.out.println("站点中间表删除成功!");
						}else{
							Util.log("站点中间表删除失败!","addLMlog",0);
							System.out.println("站点中间表删除失败!");
						}
						
					}else{
						Util.log(SITENAME+"，站点删除失败！："+jsonKp.getString("msg"),"addLMlog",0);
						System.out.println(SITENAME+"，站点删除失败！："+jsonKp.getString("msg"));
					}
				}
			}
			Util.log("------------删除站点结束------------","addLMlog",0);
			System.out.println("------------删除站点结束------------");
	
	}

}
