package com.trs.mqadddoc;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.hy.Reslibrary;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.oauth.ConstantUtil;

public class MQaddDocument {
	
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static JDBCIDS  jdbcids=new JDBCIDS();
	
	private static String USER = ConstantUtil.USER;
	
	/**
	 * 添加文档MQ消息
	 * @param jsondata
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static  void  addDoc(JSONObject jsondata) throws Exception {
		
		System.out.println("------------MQ消息新建文档开始------------");
		//MQ消息队列数据获取
		
		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  站点id SITEID  父栏目id  PARENTID
		String CHANNELID=jsondata.getString("CHNLID");//id
		String DOCID=jsondata.getString("DOCID");//文档id
		String SITEID=jsondata.getString("SITEID");//站点id
	
		System.out.println("==========================================================================");
		System.out.println(CHANNELID+"-------------------文档的栏目id和文档id-----------------"+DOCID);
		System.out.println("==========================================================================");
		
		//根据栏目id查询栏目信息
		String sServiceId="gov_site";
		String sMethodName="whetherOpenData";
		Map<String, String> savemap = new HashMap();
		savemap.put("ChannelID",CHANNELID);
		savemap.put("CurrUserName", USER); // 当前操作的用户
		String hyLM = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		
		JSONObject jsonObjectHYSE = JSON.parseObject(hyLM);
		Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		
		Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
		System.out.println("id:"+DOCID+"--海云返回结果："+hyLM);
		
		String viewid = null;
		if("true".equals(dataArrHYSave)){
			viewid =jsonObjectHYGPSE.getString("VIEWID");//视图id
			
			if("".equals(viewid) || viewid==null){
				//其他默认咨询视图站点
				addDoc.addDocument(DOCID, CHANNELID,SITEID);
			}else{
				//根据视图id查询视图短名
				String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+viewid+"'";
				String viewName=jdbcids.JDBCVIEWIDSELECT(sql);
				System.out.println("视图短名："+viewName.toUpperCase());
				AddDocByView.addDocByView(DOCID, CHANNELID, viewName.toUpperCase(),SITEID);
			}
			
		}else{
			System.out.println("海云中该栏目不存在！！！");
		}
		
		System.out.println("------------MQ消息新建文档结束------------");
		
	
	}
}
