package com.trs.mqadddoc;

import java.sql.ResultSet;

import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpsdzc.AddDocumentControllerCZXX;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.ResResource;
import com.trs.hy.Reslibrary;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;

public class SelectSiteName {
	
	
	ChannelReceiver cR=new ChannelReceiver();
	Reslibrary  rl= new Reslibrary();
	ResResource  re= new ResResource();
	HttpFileUpload hfl=new HttpFileUpload();
	JDBCIIP iip=new JDBCIIP();
	AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	static AddDocumentControllerCZXX zwgk=new AddDocumentControllerCZXX();
	
	/**
	 * 根据站点id查询站点名称
	 * @param SITEID
	 * @return
	 * @throws Exception
	 */
	public static String selectSiteName(String SITEID) throws Exception{
		String sql="SELECT SITENAME FROM wcmwebsite WHERE SITEID ='"+SITEID+"'";
		String  sn=JDBCIDS.JDBCSelectSitename(sql);
		return sn;
	}

}
