package com.trs.kptohysdzc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class PutZCJD {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map putZCJDHY(Map oPostData,JSONObject jsondata){ 
		
		//处理非默认字段
		for(String str:jsondata.keySet()){
			
			if(str.indexOf("@@")>=0){
				String valueKp=jsondata.get(str).toString();
				String[] nameStrArray=str.split("@@");
				String vName=nameStrArray[0];//视图短名
				String zName=nameStrArray[1];//字段名称
				//开始处理字段
				if("webzcjd".equals(vName.toLowerCase())){
					System.out.println("oPostData@@处理:"+oPostData);
					System.out.println("开普推送过来的数据政策解读键值对:"+str + "==========:===========" +jsondata.get(str));
					
					if("gjc".equals(vName.toLowerCase())){
						oPostData.put("GJZ", valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}else if("fbrq".equals(vName.toLowerCase())){
						oPostData.put(zName, valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}
					
				}
			}
		}
		
//		//分类法写死
//		List<Object> list=new ArrayList<>();
//		Map<Object, Object>  sxflMap= new HashMap<Object, Object>();
//		sxflMap.put("type", "content");
//		sxflMap.put("value", "官方`官方~媒体`媒体~专家`专家~图解`图解~视频`视频");
//		list.add(sxflMap);
//		Map<Object, Object>  sxflMap1= new HashMap<Object, Object>();
//		sxflMap1.put("type", "value");
//		sxflMap1.put("value", "");
//		list.add(sxflMap1);
//		oPostData.put("sxfl", JSON.toJSONString(list));
//		
//		List<Object> listztfl=new ArrayList<>();
//		Map<Object, Object>  ztflMap= new HashMap<Object, Object>();
//		ztflMap.put("type", "content");
//		ztflMap.put("value", "经济投资`经济投资~农村农民`农村农民~人口社会`人口社会~能源环境`能源环境~交通出行`交通出行~社保救助`社保救助~医疗健康`医疗健康~土地住房`土地住房~教育就业`教育就业~其他`其他");
//		listztfl.add(ztflMap);
//		Map<Object, Object>  ztflMap1= new HashMap<Object, Object>();
//		ztflMap1.put("type", "value");
//		ztflMap1.put("value", "");
//		listztfl.add(ztflMap1);						
//		oPostData.put("ztfl", JSON.toJSONString(listztfl)); 
		
		return oPostData;
	}

	
	
	/**
	 * 处理字段问题
	 * 政策解读字段处理
	 * @param zName
	 * @param oPostData
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map ZIDuanZCJD(String zName,Map oPostData,String value){ 
		
		
		return oPostData;
	}
}
