package com.trs.kptohysdzc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.kptohyzfwz.AddDocZFWZ;
import com.trs.kptohyzfwz.FileUplodeZFWZ;
import com.trs.kptohyzfwz.PutZFGBZFWZ;
import com.trs.kptohyzfwz.PutZXZFWZ;
import com.trs.kptohyzfwz.ZWuplod;
import com.trs.oauth.ConstantUtil;

public class putDocument {
	
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String pageSize = ConstantUtil.pageSize;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	
	ResResource  re= new ResResource();
	ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	JDBCIIP iip=new JDBCIIP();
	JDBC  jdbc=new JDBC();
	JDBCIDS  jdbcids=new JDBCIDS();

	/*
	 * 自定义视图同步数据
	 */
	@SuppressWarnings("unchecked")
	public String  documentZDY(String data) throws Exception {
		
		System.out.println("------------------开普推送文档开始---------------");
		String jsonData=DESUtil.decrypt(data, ZWBJ_STR_DEFAULT_KEY);
		System.out.println("文档接收到的数据:"+jsonData);
		Map result = new HashMap();
		Map oPostData = new HashMap();
		JSONObject jsondata = JSON.parseObject(jsonData);
		JSONArray dataArray = jsondata.getJSONArray("attachments");
		System.out.println("附件值："+dataArray);
		if(data != null){
			
			//处理默认字段正文，摘要，标题，附件
			String channelId=jsondata.getString("channelId");//栏目id
			String resID=jsondata.getString("resID");//文档资源id
			String abstracts=jsondata.getString("abstracts");//摘要
			String name=jsondata.getString("name");//名称 政策文件的文件名称
			String resTitle=jsondata.getString("resTitle");//文件名称  政策解读的文件名称
			String author=jsondata.getString("author");//作者
			String content=jsondata.getString("content");//正文
			String source=jsondata.getString("source");//来源
//			String source=jsondata.getString("source");//来源
			String type=jsondata.getString("type");//1图片类型；2网页类型;3音频类型；4视频类型;5文档类型
			
			//根据栏目id查询到视图
			String sServiceIdView="gov_site";
			String sMethodNameView="whetherOpenData";
			Map<String, String> savemapView = new HashMap();
			savemapView.put("ChannelID",channelId);
			savemapView.put("CurrUserName", USER); // 当前操作的用户
			String hyLMView = HyUtil.dataMoveDocumentHyRbj(sServiceIdView,sMethodNameView,savemapView);
			JSONObject jsonObjectHYSEView = JSON.parseObject(hyLMView);
			Object dataArrView = jsonObjectHYSEView.get("DATA");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSEView = JSON.parseObject(dataArrView.toString());
			System.out.println("开普推送数据到海云查询视图短名返回结果=================："+jsonObjectHYSEView.get("MSG"));
			//视图id
			String viewid =jsonObjectHYGPSEView.getString("VIEWID");
			System.out.println("首都之窗viewid:"+viewid);
			System.out.println("首都之窗viewid:"+viewid.isEmpty());
			System.out.println("首都之窗viewid:"+viewid.equals(""));
			
			if(viewid==null||viewid.equals("")||viewid.isEmpty()){
				String zxresult=PutZXZFWZ.documentZX(jsonData);
				System.out.println("默认视图新增结果："+zxresult);
				System.out.println("------------------开普推送默认视图文档结束---------------");
				return zxresult;
				
			}else{
				
				//根据视图id查询视图短名
				String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+viewid+"'";
				String viewName=jdbcids.JDBCVIEWIDSELECT(sql);
				System.out.println("转换为大写的视图短名================："+viewName.toUpperCase());
				
				if("ZCJD".equals(viewName.toUpperCase())){
					//固定字段
					oPostData.put("jdmc", name);
					oPostData.put("wjmc", resTitle);
					oPostData.put("nrzy", abstracts);
					oPostData.put("ly", source);//来源
					//处理其他字段开始
					oPostData=PutZCJD.putZCJDHY(oPostData, jsondata);
					
					//处理默认附件
					oPostData=FileUplode.fujianMR(type,oPostData,jsondata);
					
					//未处理的正文
					if(content!=null){
						String pcontent=null;
						String fcontent=null;
						//处理图片
						pcontent=ZWuplod.contentResourceUpload(content,5,"(src=\").*?(\")");
						//处理附件
						fcontent=ZWuplod.contentResourceUpload(pcontent,6,"(href=\").*?(\")");
						oPostData.put("zw", fcontent);
						System.out.println("====================处理后的正文==================");
						System.out.println(fcontent);
						System.out.println("====================处理后的正文==================");
						Util.log("====================处理后的正文==================","kplog",0);
						Util.log(fcontent,"kplog",0);
						Util.log("====================处理后的正文==================","kplog",0);
					}else {
						oPostData.put("zw", "&nbsp&nbsp&nbsp&nbsp&nbsp");
					}
					
				}else if("ZCWJ".equals(viewName.toUpperCase())){
					System.out.println("政策文件name："+name);
					//固定字段
					oPostData.put("wjmc", name);
					if(resTitle!=null){
						oPostData.put("tjbt", resTitle);// 文章推荐标题	
					}
					if(abstracts!=null){
						oPostData.put("nrzy", abstracts);
					}
					System.out.println("政策文件oPostData："+oPostData);
					//处理其他字段开始
					oPostData=PutZCWJ.putZCWJHY(oPostData, jsondata);
					
					//处理默认附件
					oPostData=FileUplode.fujianMR(type,oPostData,jsondata);
					
					//未处理的正文
					if(content!=null){
						String pcontent=null;
						String fcontent=null;
						//处理图片
						pcontent=ZWuplod.contentResourceUpload(content,5,"(src=\").*?(\")");
						//处理附件
						fcontent=ZWuplod.contentResourceUpload(pcontent,6,"(href=\").*?(\")");
						oPostData.put("zw", fcontent);
						System.out.println("====================处理后的正文==================");
						System.out.println(fcontent);
						System.out.println("====================处理后的正文==================");
						Util.log("====================处理后的正文==================","kplog",0);
						Util.log(fcontent,"kplog",0);
						Util.log("====================处理后的正文==================","kplog",0);
					}else {
						oPostData.put("zw", "&nbsp&nbsp&nbsp&nbsp&nbsp");
					}
					
				}else if("LSGB".equals(viewName.toUpperCase())){
					
					//默认字段
					//历史公报
					oPostData.put("sybt", name);//首页标题
					oPostData.put("fbt", resTitle);//副标题
					oPostData.put("zy", abstracts);//摘要
					oPostData.put("ly", source);//来源
					//未处理的正文
					if(content!=null){
						String pcontent=null;
						String fcontent=null;
						//处理图片
						pcontent=ZWuplod.contentResourceUpload(content,5,"(src=\").*?(\")");
						//处理附件
						fcontent=ZWuplod.contentResourceUpload(pcontent,6,"(href=\").*?(\")");
						oPostData.put("zw", fcontent);
						System.out.println("====================处理后的正文==================");
						System.out.println(fcontent);
						System.out.println("====================处理后的正文==================");
						Util.log("====================处理后的正文==================","kplog",0);
						Util.log(fcontent,"kplog",0);
						Util.log("====================处理后的正文==================","kplog",0);
					}else {
						oPostData.put("zw", "&nbsp&nbsp&nbsp&nbsp&nbsp");
					}
					
					//处理其他字段开始
					oPostData=PutZFGB.putZFGBsdzc(oPostData, jsondata);
					
					//处理默认附件
					Util.log("---------------首都之窗------政府公报 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------首都之窗------政府公报 处理默认附件开始-------------");
					oPostData=FileUplode.fujianMR(type,oPostData,jsondata);
					
					
				}else if("CZXX".equals(viewName.toUpperCase())){
					//财政信息
					oPostData.put("xxmc", name);//信息名称
					oPostData.put("fbt", resTitle);//副标题
					oPostData.put("nrzy", abstracts);//摘要
					oPostData.put("ly", source);//来源
					oPostData.put("zz", author);//来源
					
					//处理其他字段
					//未处理的正文
					if(content!=null){
						String pcontent=null;
						String fcontent=null;
						//处理图片
						pcontent=ZWuplod.contentResourceUpload(content,5,"(src=\").*?(\")");
						//处理附件
						fcontent=ZWuplod.contentResourceUpload(pcontent,6,"(href=\").*?(\")");
						oPostData.put("zw", fcontent);
						System.out.println("====================处理后的正文==================");
						System.out.println(fcontent);
						System.out.println("====================处理后的正文==================");
						Util.log("====================处理后的正文==================","kplog",0);
						Util.log(fcontent,"kplog",0);
						Util.log("====================处理后的正文==================","kplog",0);
					}else {
						oPostData.put("zw", "&nbsp&nbsp&nbsp&nbsp&nbsp");
					}
					
					//处理其他字段开始
					oPostData=PutCZXX.putCZXX(oPostData, jsondata);
					
					//处理默认附件
					System.out.println("---------------首都之窗-----财政信息 处理默认附件开始-------------");
					oPostData=FileUplode.fujianMRCZ(type,oPostData,jsondata);
					
				}
			}

//			else if("WEB".contains(viewName.toUpperCase())){
//				//其他政府网站类型
//				
//			}
			
			//打印之前封装数据
			Util.log("=====================================oPostData==================================","kplog",0);
			Util.log(oPostData,"kplog",0);
			Util.log("=====================================oPostData==================================","kplog",0);
			System.out.println("=====================================oPostData==================================："+oPostData);
			
			//封装数据
			oPostData.put("ChannelId", channelId);
			oPostData.put("ObjectId", "0");   
			oPostData.put("CurrUserName", USER); // 当前操作的用户
			//开始添加到海云
			String addDocument=AddDocZFWZ.addHyDoc(oPostData,result,resID);	
			
			return addDocument;

		}else{
			result.put("code", "-1");
			result.put("data", "接收开普数据失败!");
			System.out.println("接收开普数据失败!");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------开普推送文档结束---------------");
			return jsonResult.toString();
		}
		
	}

	
}
