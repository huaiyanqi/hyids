package com.trs.kptohysdzc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.kptohyzfwz.PutZXZFWZ;
import com.trs.oauth.ConstantUtil;





/**
 * OAuth2.0Controller
 */
@Controller
@RequestMapping(value = "/jyh")
public class JYHController{
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String pageSize = ConstantUtil.pageSize;
	putDocument pd=new putDocument();
	ResResource  re= new ResResource();
	ChannelReceiver cR=new ChannelReceiver();
	HttpFileUpload hfl=new HttpFileUpload();
	JDBCIIP iip=new JDBCIIP();
	JDBC  jdbc=new JDBC();
	JDBCIDS  jdbcids=new JDBCIDS();
	/**
	 * 开普站点查询接口
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/selectSite"},produces = "application/json; charset=utf-8")
	public String  selectSite(@RequestParam(value ="userName") String userName) throws Exception {
		System.out.println("------------------开普查询站点开始------------------");
		Util.log("开普当前操作用户======"+userName,"PutFromKaipuyunlog",0);
		System.out.println("开普当前操作用户："+userName);
		Map<String, Object> map=new HashMap<>();
		List<Object> list=new ArrayList<>();
		// 查询站点
		String sServiceId = "gov_site";
		String sMethodName = "querySitesOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		//设置查询多少个用户
		mSE.put("pageSize", pageSize);
		mSE.put("pageindex", "1");
		mSE.put("MediaType", "1");
		mSE.put("CurrUserName", userName); // 当前操作的用户
		
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2!=null){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					String siteId=dataBean.getString("SITEID");
					String siteName=dataBean.getString("SITENAME");
					System.out.println(siteId+"-------------------"+siteName);
					Map<String, Object> mapSite=new HashMap<>();
					mapSite.put("siteId", siteId);
					mapSite.put("siteName", siteName);
					list.add(mapSite);
				}
			}
			map.put("data", list);
			map.put("msg", "操作成功");
			map.put("code", "0");
		}else{
			System.out.println("操作海云接口查询站点失败:"+jsonObject1.get("MSG"));
			map.put("code", "-1");
			map.put("data", jsonObject1.get("MSG"));
		}
		JSONObject json =new JSONObject(map);
		System.out.println("封装好的数据转换json:"+json);
		System.out.println("------------------开普查询站点结束------------------");
		return json.toJSONString();
	}
	
	
	/**
	 * 开普，栏目查询接口
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/selectChannel"},produces = "application/json; charset=utf-8")
	public String selectChannel(@RequestParam(value ="data") String data) throws Exception {
		
		System.out.println("------------------开普查询栏目开始--------------------");
		System.out.println("栏目查询接口："+data);
		String jsonData=DESUtil.decrypt(data, Z_APPID);
		System.out.println("接收到栏目数据的请求"+jsonData);
		JSONObject jsonObjectSE = JSON.parseObject(jsonData);
		
		String  siteId =jsonObjectSE.getString("siteId");
		String  pid =jsonObjectSE.getString("pid");
		String  userName=jsonObjectSE.getString("userName");
		System.out.println("pid:"+pid);
		Map<String, Object> map=new HashMap<>();
		List<Object> list=new ArrayList<>();
		
		if(pid == null){
			pid ="0";
		}
		System.out.println("pid判断后:"+pid);
		List<Object>  li=selectCh(siteId,pid,userName);
		if(li.size()>0){
			list.addAll(li);
			map.put("data", list);
			map.put("msg", "操作成功");
			map.put("code", "0");
		}else{
			map.put("code", "0");
			map.put("msg", "该站点下没有相关栏目");
			map.put("data", list);
		}
		
		JSONObject json =new JSONObject(map);
		System.out.println("封装好的数据转换json:"+json);
		System.out.println("------------------开普查询栏目结束------------------");
		return json.toString();
		
		
	}
	
	
	/**
	 * 查询栏目封装
	 * @throws Exception
	 */
	public static List<Object> selectCh(String sitId,String pId,String userName) throws Exception{
		
		System.out.println(sitId+"-----------栏目查询接口-----------"+pId);
		List<Object> list=new ArrayList<>();
		// 根据站点id查询下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", userName); // 当前操作的用户
		//设置一次查询数量
		mSE.put("pageSize", "1500");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", sitId);
		mSE.put("ParentChannelId", pId);
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		System.out.println("结果"+hy1);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2.size()>0){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					String SITEID=dataBean.getString("SITEID");
					String CHNLNAME=dataBean.getString("CHNLNAME");
					String CHANNELID=dataBean.getString("CHANNELID");
					String HASCHILDREN=dataBean.getString("HASCHILDREN");//是否存在下级栏目
					System.out.println(SITEID+"-------------------"+CHNLNAME+"----------------"+CHANNELID);
					Map<String, Object> mapChnl=new HashMap<>();
					mapChnl.put("siteId", SITEID);
					mapChnl.put("channelName", CHNLNAME);
					mapChnl.put("channelId", CHANNELID);
					if("0".equals(pId)){
						mapChnl.put("pid", "");//上一级栏目id,0表示一级栏目
					}else{
						mapChnl.put("pid", pId);//上一级栏目id,0表示一级栏目
					}
					if("true".equals(HASCHILDREN)){
						mapChnl.put("isLeaf", "0");
						list.add(mapChnl);
					}else{
						mapChnl.put("isLeaf", "1");
						list.add(mapChnl);
					}
					
				}
			}
		}
		return list;
	}

	/**
	 * 开普推送文档接口
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ResponseBody
	@RequestMapping(value = {"/addDocument"},method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String  pushDocument(@RequestParam(value ="data") String data) throws Exception {
		Util.log("------------------开普推送文档开始---------------"+Z_APPID,"kplogsdzc",0);
		System.out.println("------------------开普推送文档开始---------------");
		String jsonData=DESUtil.decrypt(data, Z_APPID);
		Util.log("文档接收到的数据:"+jsonData,"kplogsdzc",0);
		System.out.println("文档接收到的数据:"+jsonData);
		Map result = new HashMap();
		JSONObject jsondata = JSON.parseObject(jsonData);

		if(data != null){
			
			String view=jsondata.getString("flagMetadataList");//判断推送到自定义视图还是默认视图
			if("1".equals(view)){
				//默认视图
				String DESdata=DESUtil.decrypt(data, Z_APPID);
				String zxresult=PutZXZFWZ.documentZX(DESdata);
				System.out.println("默认视图新增结果："+zxresult);
				System.out.println("------------------开普推送默认视图文档结束---------------");
				return zxresult;
			}else{
				//自定义视图
				String zdyresult=pd.documentZDY(data);
				System.out.println("自定义视图返回结果："+zdyresult);
				System.out.println("------------------开普推送自定义视图文档结束---------------");
				return zdyresult;
//				return "自定义视图无法测试";
			}
			
		}else{
			result.put("code", "-1");
			result.put("data", "接收开普数据失败!");
			System.out.println("接收开普数据失败!");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------开普推送文档结束---------------");
			return jsonResult.toString();
		}
		
	}
	
	
}