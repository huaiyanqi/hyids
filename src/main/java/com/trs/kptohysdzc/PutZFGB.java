package com.trs.kptohysdzc;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.kafka.Util;
import com.trs.kptohyzfwz.FileUplodeYSJJ;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

public class PutZFGB {
	
	private static String resouse_path = ConstantUtil.resouse_path;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map putZFGBsdzc(Map oPostData,JSONObject jsondata) throws Exception{ 
		
		//处理非默认字段
		for(String str:jsondata.keySet()){
			
			if(str.indexOf("@@")>=0){
				String valueKp=jsondata.get(str).toString();
//				System.out.println("获取到的值："+valueKp);
				String[] nameStrArray=str.split("@@");
				String vName=nameStrArray[0];//视图短名
				String zName=nameStrArray[1];//字段名称
				//开始处理字段
				if("webzfgb".equals(vName.toLowerCase())){
					System.out.println("开普推送过来的政府网站政府公报数据政策文件键值对:"+str+ "==========:===========" +jsondata.get(str));
					if("fjgbfy".equals(zName.toLowerCase())||"fjgbqzb".equals(zName.toLowerCase())){
						System.out.println("元数据集附件：："+valueKp);
						//处理元数据集附件
						oPostData=FileUplodeYSJJ.fujianYSJJFJ(oPostData, valueKp,zName.toLowerCase());
					}else if("gbnf".equals(zName.toLowerCase())){
						//公报年份
						oPostData.put(zName, valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}else if("gbqs".equals(zName.toLowerCase())){
						//公报期数
						oPostData.put("QS", valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}else if("gbzqs".equals(zName.toLowerCase())){
						//公报总期数
						oPostData.put("ZQS", valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}else if("fbrq".equals(zName.toLowerCase())){
						//发布日期
						oPostData.put("RQ", valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}

				}
			}
		}
		
		return oPostData;
	}
	
	/**
	 * 处理元数据集附件
	 * @param oPostData
	 * @param dowurl
	 * @param fjkeyName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static Map fujianYSJJFJ(Map oPostData,String dowurl,String fjkeyName) throws Exception{
		if(dowurl.indexOf("group1")>=0){
			String[] url=dowurl.split(",");
			List flist=new ArrayList<>();
			for(int s=0;s<url.length;s++){
				  byte[] file=zfwzUtil.downloadFile(url[s]);
				  String fjName=url[s].substring(url[s].lastIndexOf("/")+1);
				  System.out.println("元数据集附件处理:"+fjName);
				  FileOutputStream downloadFile = new FileOutputStream(resouse_path+fjName);
			      downloadFile.write(file, 0, file.length);
			      downloadFile.flush();
			      downloadFile.close();
				  //上传海云
				  System.out.println("--------------下载之后上传到海云开始-----------------");
				  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+fjName);
				  String fileName = oDispatch.getUploadShowName();
				  System.out.println("上传附件之后的附件名称"+fileName);

				  Map pmap=new HashMap<>();
				  pmap.put("SrcFile", fjName);
				  pmap.put("AppFile", fileName);
				  pmap.put("AppDesc", fjName.substring(0,fjName.lastIndexOf(".")));
				  pmap.put("AppendixId", "0");
				  flist.add(pmap);
				  Util.log("处理附件："+pmap+"------------------"+flist,"kplog",0);
				  System.out.println("处理附件："+pmap+"------------------"+flist);
			}
			if("fjgbfy".equals(fjkeyName)){
				oPostData.put("gbfy",JSON.toJSONString(flist));
			}else{
				oPostData.put("gbqzb",JSON.toJSONString(flist));
			}
			
		}else{
			Util.log(fjkeyName+"==附件格式存在问题！！！==！"+dowurl,"kplog",0);
			System.out.println(fjkeyName+"==附件格式存在问题！！！==！"+dowurl);	
		}
		
		return oPostData;
	}
}
