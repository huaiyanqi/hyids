package com.trs.kptohysdzc;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.HttpFileUpload;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

public class FileUplode {
	
	
	private static String resouse_path = ConstantUtil.resouse_path;
	
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	
	/**
	 * 处理开普过来的附件
	 * @param htmlStr
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public static Map fujian(String type,Map oPostData,JSONObject jsondata){ 
		
		if("1".equals(type)){  
			oPostData.put("DOCTYPE",50);  //图片
		}else if("2".equals(type)){
			System.out.println("---------------------------开普推送文档类型进入--------------------");
			oPostData.put("DOCTYPE",20);  //文字
			//正文不做处理，直接处理附件
			System.out.println("附件：：：：获取之前");
			JSONArray attach=jsondata.getJSONArray("attachments");//相附件
			System.out.println("附件：：：："+attach);
			if(attach!=null){
				System.out.println("---------------------------开始处理附件--------------------");
				 List flist=new ArrayList<>();
				for(int i = 0; i < attach.size(); i++){
					  String  kuURL="http://192.141.1.10:7002/repo-web";
					  String  fjName=attach.getJSONObject(i).getString("name");
					  String  fjpath= attach.getJSONObject(i).getString("path");
//					  String  fjtype=attach.getJSONObject(i).getString("type");format
					  String  format=attach.getJSONObject(i).getString("format");
					  
					  if(fjpath != null){
						  System.out.println("下载附件开始");
						  //代表存在附件，处理上传
						  //先下载再上传
						  try {
							System.out.println("下载地址："+kuURL+fjpath);
							System.out.println("下载文件名："+fjName.substring(0,fjName.lastIndexOf("."))+"."+format);
							System.out.println("下载文件resouse_path："+resouse_path);
							hfl.downLoadFromUrl(kuURL+fjpath,fjName.substring(0,fjName.lastIndexOf("."))+"."+format,resouse_path);
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("------------------下载附件失败！！开普推送文档结束---------------");
						  }
						  //上传海云
						  System.out.println("--------------下载之后上传到海云开始-----------------");
						  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+fjName.substring(0,fjName.lastIndexOf("."))+"."+format);
						  String fileName = oDispatch.getUploadShowName();
						  System.out.println("上传附件之后的附件名称"+fileName);

						  Map pmap=new HashMap<>();
						  pmap.put("SrcFile", fjName);
						  pmap.put("AppFile", fileName);
						  pmap.put("AppDesc", fjName.substring(0,fjName.lastIndexOf(".")));
						  pmap.put("AppendixId", "0");
						  flist.add(pmap);
						  System.out.println("处理附件："+pmap+"------------------"+flist);
						}
				}
				
//				System.out.println("图片附件处理："+JSON.toJSONString(plist));
				System.out.println("附件处理："+JSON.toJSONString(flist));
//				if(plist.size()!=0) {
//					oPostData.put("DocRelPic",JSON.toJSONString(plist));
//				}
				if(flist.size()!=0) {
					oPostData.put("fj",JSON.toJSONString(flist));
				}
			}else{
				System.out.println("同步文档不存在附件！！！");
			}
			
		}else if("30".equals(type)){
			oPostData.put("DOCTYPE",30);  //链接型文档
		}else if("40".equals(type)){
			oPostData.put("DOCTYPE",40);  //外部文件文档
		}
		
		return oPostData;
	}
	
	
	/**
	 * 处理首都之窗--默认附件
	 * @param htmlStr
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public static Map fujianMR(String type,Map oPostData,JSONObject jsondata) throws Exception{ 
		
		if("1".equals(type)){  
			oPostData.put("DOCTYPE",50);  //图片
		}else if("2".equals(type)){
			
			oPostData.put("DOCTYPE",20);  //文字
			//正文不做处理，直接处理附件
			System.out.println("附件：：：：获取之前");
			JSONArray attach=jsondata.getJSONArray("attachments");//相附件
			System.out.println("附件：：：："+attach);
			if(attach!=null){
				Util.log("---------------------------开始处理首都之窗默认附件--------------------","kplog",0);
				System.out.println("---------------------------开始处理首都之窗默认附件--------------------");
				List flist=new ArrayList<>();
				for(int i = 0; i < attach.size(); i++){
					  String  kuURL="http://192.141.1.10:7002/repo-web";
					  String  fjName=attach.getJSONObject(i).getString("name");
					  String  fjpath= attach.getJSONObject(i).getString("path");
//					  String  fjtype=attach.getJSONObject(i).getString("type");format
					  String  format=attach.getJSONObject(i).getString("format");
					  
					  if(fjpath != null){
						  //截取后调用开普下载接口
						  String[] dowpath=fjpath.split("filePath=");
						  Util.log("下载附件地址：："+dowpath[1],"kplog",0);					  
						  byte[] file=zfwzUtil.downloadFile(dowpath[1]);
						  String writName=fjpath.substring(fjpath.lastIndexOf("/")+1);
						  FileOutputStream downloadFile = new FileOutputStream(resouse_path+writName);
					      downloadFile.write(file, 0, file.length);
					      downloadFile.flush();
					      downloadFile.close();
						  
						  System.out.println("--------------下载附件结束--------------");
						  //上传海云
						  System.out.println("--------------下载之后上传到海云开始-----------------");
						  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+writName);
						  String fileName = oDispatch.getUploadShowName();
						  Util.log("上传附件之后的附件名称"+fileName,"kplog",0);		
						  System.out.println("上传附件之后的附件名称"+fileName);

						  Map pmap=new HashMap<>();
						  pmap.put("SrcFile", fjName);
						  pmap.put("AppFile", fileName);
						  pmap.put("AppDesc", fjName);
						  pmap.put("AppendixId", "0");
						  flist.add(pmap);
						  Util.log("处理附件："+pmap+"------------------"+flist,"kplog",0);	
						  System.out.println("处理附件："+pmap+"------------------"+flist);
						}else{
						  System.out.println("附件参数fjpath===="+fjpath);
						}
				}
				
				Util.log("全部入默认附件："+JSON.toJSONString(flist),"kplog",0);	
				System.out.println("全部入默认附件："+JSON.toJSONString(flist));
//				if(plist.size()!=0) {
//					oPostData.put("DocRelPic",JSON.toJSONString(plist));
//				}
				if(flist.size()!=0) {
					oPostData.put("DOCATTACHFILEFIELD",JSON.toJSONString(flist));
				}
			}else{
				Util.log("首都之窗默认附件同步文档不存在附件！！！","kplog",0);
				System.out.println("首都之窗默认附件同步文档不存在附件！！！");
			}
			
		}else if("30".equals(type)){
			oPostData.put("DOCTYPE",30);  //链接型文档
		}else if("40".equals(type)){
			oPostData.put("DOCTYPE",40);  //外部文件文档
		}
		
		return oPostData;
	}


	/**
	 * 处理首都之窗--财政视图默认附件
	 * @param htmlStr
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public static Map fujianMRCZ(String type,Map oPostData,JSONObject jsondata) throws Exception{ 
		
		if("1".equals(type)){  
			oPostData.put("DOCTYPE",50);  //图片
		}else if("2".equals(type)){
			
			oPostData.put("DOCTYPE",20);  //文字
			//正文不做处理，直接处理附件
			System.out.println("附件：：：：获取之前");
			JSONArray attach=jsondata.getJSONArray("attachments");//相附件
			System.out.println("附件：：：："+attach);
			if(attach!=null){
				Util.log("---------------------------开始处理首都之窗财政默认附件--------------------","kplog",0);
				System.out.println("---------------------------开始处理首都之窗财政默认附件--------------------");
				List flist=new ArrayList<>();
				for(int i = 0; i < attach.size(); i++){
					  String  kuURL="http://192.141.1.10:7002/repo-web";
					  String  fjName=attach.getJSONObject(i).getString("name");
					  String  fjpath= attach.getJSONObject(i).getString("path");
//					  String  fjtype=attach.getJSONObject(i).getString("type");format
					  String  format=attach.getJSONObject(i).getString("format");
					  
					  if(fjpath != null){
						  //截取后调用开普下载接口
						  String[] dowpath=fjpath.split("filePath=");
						  Util.log("下载附件地址：："+dowpath[1],"kplog",0);					  
						  byte[] file=zfwzUtil.downloadFile(dowpath[1]);
						  String writName=fjpath.substring(fjpath.lastIndexOf("/")+1);
						  FileOutputStream downloadFile = new FileOutputStream(resouse_path+writName);
					      downloadFile.write(file, 0, file.length);
					      downloadFile.flush();
					      downloadFile.close();
						  
						  System.out.println("--------------下载附件结束--------------");
						  //上传海云
						  System.out.println("--------------下载之后上传到海云开始-----------------");
						  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+writName);
						  String fileName = oDispatch.getUploadShowName();
						  Util.log("上传附件之后的附件名称"+fileName,"kplog",0);		
						  System.out.println("上传附件之后的附件名称"+fileName);

						  Map pmap=new HashMap<>();
						  pmap.put("SrcFile", fjName);
						  pmap.put("AppFile", fileName);
						  pmap.put("AppDesc", fjName);
						  pmap.put("AppendixId", "0");
						  flist.add(pmap);
						  Util.log("处理附件："+pmap+"------------------"+flist,"kplog",0);	
						  System.out.println("处理附件："+pmap+"------------------"+flist);
						}else{
						  System.out.println("附件参数fjpath===="+fjpath);
						}
				}
				
				Util.log("全部入默认附件："+JSON.toJSONString(flist),"kplog",0);	
				System.out.println("全部入默认附件："+JSON.toJSONString(flist));
//				if(plist.size()!=0) {
//					oPostData.put("DocRelPic",JSON.toJSONString(plist));
//				}
				if(flist.size()!=0) {
					oPostData.put("fj",JSON.toJSONString(flist));
				}
			}else{
				Util.log("首都之窗财政默认附件同步文档不存在附件！！！","kplog",0);
				System.out.println("首都之窗财政默认附件同步文档不存在附件！！！");
			}
			
		}else if("30".equals(type)){
			oPostData.put("DOCTYPE",30);  //链接型文档
		}else if("40".equals(type)){
			oPostData.put("DOCTYPE",40);  //外部文件文档
		}
		
		return oPostData;
	}

}
