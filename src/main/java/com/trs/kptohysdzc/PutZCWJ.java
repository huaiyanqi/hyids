package com.trs.kptohysdzc;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.trs.kptohyzfwz.FileUplodeYSJJ;

public class PutZCWJ {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map putZCWJHY(Map oPostData,JSONObject jsondata) throws Exception{ 
		
		//处理非默认字段
		for(String str:jsondata.keySet()){
			
			if(str.indexOf("@@")>=0){
				String valueKp=jsondata.get(str).toString();
//				System.out.println("获取到的值："+valueKp);
				String[] nameStrArray=str.split("@@");
				String vName=nameStrArray[0];//视图短名
				String zName=nameStrArray[1];//字段名称
				//开始处理字段
				if("webzcwj".equals(vName.toLowerCase())){
					
					System.out.println("开普推送过来的数据政策文件键值对:"+str+ "==========:===========" +jsondata.get(str));
					
					if("wjyxx".equals(zName.toLowerCase())){
						//处理有效性
						if("是".equals(valueKp)||"有效".equals(valueKp)){
							oPostData.put("yxx", "0"); // 有效	
						}else if("否".equals(valueKp)||"无效".equals(valueKp)){
							oPostData.put("yxx", "1"); // 无效	
						}
					}else if("fj".equals(zName.toLowerCase())){
						if(valueKp!=null){
							System.out.println("元数据集附件：："+valueKp);
							//处理元数据集附件
							oPostData=FileUplodeYSJJ.fujianYSJJZCWJ(oPostData, valueKp);
						}
					}else if("idxid".equals(zName.toLowerCase())){
						if(valueKp!=null){
							oPostData.put("idxid",valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); //索引
							oPostData.put("organcat",valueKp.split("/")[0]); // 有效
						}

					}if("cwrq".equals(zName.toLowerCase())){
						oPostData.put(zName,valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}else if("gjz".equals(zName.toLowerCase())){
						oPostData.put("gjc",valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}else if("ssrq".equals(zName.toLowerCase())){
						oPostData.put(zName,valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}else if("fzrq".equals(zName.toLowerCase())){
						oPostData.put(zName,valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}else if("fbrq".equals(zName.toLowerCase())){
						oPostData.put(zName,valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}else if("fwxh".equals(zName.toLowerCase())){
						oPostData.put(zName,valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}else if("fwnf".equals(zName.toLowerCase())){
						oPostData.put(zName,valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}else if("xgdz".equals(zName.toLowerCase())){
						oPostData.put("jgdz",valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}
						
				}

			}
		}
		
		
		return oPostData;
	}
	
	
	/**
	 * 处理字段问题
	 * 政策文件字段处理
	 * @param zName
	 * @param oPostData
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map ZIDuanZCWJ(String zName,Map oPostData,String value){ 
		
//		System.out.println("处理其他字段开始：zName："+zName);
//		System.out.println("处理其他字段开始：oPostData："+oPostData);
//		System.out.println("处理其他字段开始：value："+value);
		String fieldName="gjc;cwrq;ssrq;fzrq;fbrq;jgdz;fwnf;fwxh;gbnf;gbxh;gbzxh;px;xgzcbt;xgjdbt;";
		String[] fileList=fieldName.split(";");
		for(int s=0;s<fileList.length;s++){
			if(zName.equals(fileList[s])){
				//处理包含字段
				oPostData.put(zName, value.replace("[\"", "").replace("\"]", "").replace("[]", ""));
			}else if(zName.indexOf("fwdw")>=0){
				oPostData.put("fwdw", "");
			}else if(zName.indexOf("wtfl")>=0){
				oPostData.put("wtfl", "");
			}else if(zName.indexOf("lhfwdw")>=0){
				oPostData.put("lhfwdw", "");
			}else if(zName.indexOf("ztfl")>=0){
				oPostData.put("ztfl", "");
			}
		}
		System.out.println("处理其他字段结束：oPostData："+oPostData);
		
		return oPostData;
	}
}
