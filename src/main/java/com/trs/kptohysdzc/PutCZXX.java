package com.trs.kptohysdzc;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.trs.kptohyzfwz.FileUplodeYSJJ;

public class PutCZXX {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map putCZXX(Map oPostData,JSONObject jsondata) throws Exception{ 
		
		//处理非默认字段
		for(String str:jsondata.keySet()){
			
			if(str.indexOf("@@")>=0){
				String valueKp=jsondata.get(str).toString();
//				System.out.println("获取到的值："+valueKp);
				String[] nameStrArray=str.split("@@");
				String vName=nameStrArray[0];//视图短名
				String zName=nameStrArray[1];//字段名称
				//开始处理字段
				if("webcz".equals(vName.toLowerCase())){
					
					System.out.println("开普推送过来的数据政策文件键值对:"+str+ "==========:===========" +jsondata.get(str));
					
					if("fbrq".equals(zName.toLowerCase())){
						
						oPostData.put("fbrq",valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}else if("gkrq".equals(zName.toLowerCase())){
						
						oPostData.put("gkrq",valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); 
					}
				}

			}
		}
		
		
		return oPostData;
	}
	
	
	/**
	 * 处理字段问题
	 * 政策文件字段处理
	 * @param zName
	 * @param oPostData
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map ZIDuanZCWJ(String zName,Map oPostData,String value){ 
		
//		System.out.println("处理其他字段开始：zName："+zName);
//		System.out.println("处理其他字段开始：oPostData："+oPostData);
//		System.out.println("处理其他字段开始：value："+value);
		String fieldName="gjc;cwrq;ssrq;fzrq;fbrq;jgdz;fwnf;fwxh;gbnf;gbxh;gbzxh;px;xgzcbt;xgjdbt;";
		String[] fileList=fieldName.split(";");
		for(int s=0;s<fileList.length;s++){
			if(zName.equals(fileList[s])){
				//处理包含字段
				oPostData.put(zName, value.replace("[\"", "").replace("\"]", "").replace("[]", ""));
			}else if(zName.indexOf("fwdw")>=0){
				oPostData.put("fwdw", "");
			}else if(zName.indexOf("wtfl")>=0){
				oPostData.put("wtfl", "");
			}else if(zName.indexOf("lhfwdw")>=0){
				oPostData.put("lhfwdw", "");
			}else if(zName.indexOf("ztfl")>=0){
				oPostData.put("ztfl", "");
			}
		}
		System.out.println("处理其他字段结束：oPostData："+oPostData);
		
		return oPostData;
	}
}
