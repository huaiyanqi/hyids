package com.trs.export.excel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class excelTest {
	
	
    public static void main(String[] args) {

		// TODO Auto-generated method stub
		String path = "D://demo.xlsx";
        String name = "test";
        List<String> titles =new ArrayList();
        titles.add("id");
        titles.add("姓名");
        titles.add("age");
        titles.add("birthday");
        titles.add("gender");
        titles.add("date");
        List<Map<String, Object>> values = new ArrayList();
        for (int i = 0; i < 10; i++) {
            Map<String, Object> map = new HashMap();
            map.put("id", i + 1D);
            map.put("姓名", "test_" + i);
            map.put("age", i * 1.5);
            map.put("gender", "man");
            map.put("birthday", new Date());
            map.put("date",  Calendar.getInstance());
            values.add(map);
        }
        System.out.println(ExcelUtil.writerExcel(path, name, titles, values));
    }
	

}
