package com.trs.zfwz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.mqadddoc.AddDocByView;
import com.trs.oauth.ConstantUtil;

public class zfwzUtil {
	
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String USER = ConstantUtil.USER;
//    public static String PResource(String  pPath,String pName) {
//    	
//    	System.out.println("开普上传附件接口pPath："+pPath);
//    	System.out.println("开普上传附件接口pName："+pName);
//    	String PKPpath=null;
//    	String urlKp=null;
//    	try {
//			PKPpath=uploadFile(pPath, pName);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		//处理返回结果
//		JSONObject jsonObject = JSON.parseObject(PKPpath);
//		Object dataArr = jsonObject.get("data");
//		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
//		String code=jsonObject.getString("code");
//		if("0".equals(code)){
//			urlKp = jsonObjectHYGPSE.getString("fileURL");
//		}else{
//			System.out.println("开普上传失败："+jsonObject.get("MSG"));	
//		}
//		
//    	return urlKp;
//    }
//    
    
//  1.3.9.	上传文件（只上传文件，不会转为资源对象） 
	/**
	 * 上传文件 视频 图片 
	 * @throws Exception
	 */
  public static String uploadFile(String path , String name,String bt,String CHANNELID,String sitename,String SITEID) throws Exception {
	  
	  System.out.println("地址："+path);
      JSONObject  json = new JSONObject();
      //  "F:\\cms最新接口升级文件.zip"
      File file = new File(path);
      FileInputStream fileInputStream = new FileInputStream(file);
      String encrypt = DESUtil.encrypt(json.toJSONString(), ZWBJ_APPID);
	  String url =Z_URL + "/resource/uploadFile?data=" + encrypt + "&appId="+ZWBJ_STR_DEFAULT_KEY;
	  
//	  System.out.println("上传接口地址："+Z_URL + "/resource/uploadFile");
      String s = HttpUtil.uploadFileByOkHttp(url,fileInputStream,name);
      System.out.println(s);
	  JSONObject jsonObject = JSON.parseObject(s);
	  try{
		  Object dataArr = jsonObject.get("data");
		  JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		  String code=jsonObject.getString("code");
		  String urlKp=null;
		  if("0".equals(code)){
			  urlKp = jsonObjectHYGPSE.getString("fileURL");
		  }else{
			Util.log("文章名称："+bt,"erro"+sitename+SITEID,0);
			Util.log("栏目id："+CHANNELID,"erro"+sitename+SITEID,0);
			Util.log("错误信息："+jsonObject.get("MSG"),"erro"+sitename+SITEID,0);
			Util.log("=============================================================================","erro"+sitename+SITEID,0);
			System.out.println("开普上传失败："+jsonObject.get("MSG"));	
		  }
		  System.out.println(urlKp);
		  return urlKp;
		}catch(Exception e){//e是异常类型，一般根父类为Exception类
			Util.log("文章名称："+bt,"erro"+sitename+SITEID,0);
			Util.log("栏目id："+CHANNELID,"erro"+sitename+SITEID,0);
			Util.log("错误信息："+e,"erro"+sitename+SITEID,0);
			Util.log("错误信息："+s,"erro"+sitename+SITEID,0);
			Util.log("=============================================================================","erro"+sitename+SITEID,0);
		}
	  //因为必须有返回值，需要修改接受返回值并且判断得地方太多  ，因此这两行代码用来报错停止代码继续运行
	  Object dataArr = jsonObject.get("data");
	  JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
	  return null;
  }
  
  
	/**
	 * 修改开普资源
	 * @throws Exception
	 */
	//resTranMode=0
	public static String  updateRes0(JSONArray jsonArray,JSONObject json) throws Exception {
		  	
		  	json.put("attachments", jsonArray.toJSONString());//附件
		    
		    String url =Z_URL+ "/resource/updateRes";
		    System.out.println(url);
		    Map<String, String> map = new HashMap<String, String>();
		    map.put("appId", ZWBJ_APPID);
		        map.put("data", DESUtil.encrypt(json.toJSONString(),ZWBJ_STR_DEFAULT_KEY));
		    String post = HttpUtil.doPost(url, map,json);
		    System.out.println(post); 
		    return post;
	}
	
	
	//1.3.10.	下载文件
	public static byte[]  downloadFile(String upLodaUrl) throws Exception {
	    JSONObject json = new JSONObject();
	    json.put("fileURL", upLodaUrl);
	    String encrypt = DESUtil.encrypt(json.toJSONString(), ZWBJ_STR_DEFAULT_KEY);
	    String url = "http://192.141.1.10:7002/zuul/repo-api/api/res/resource/downloadFile?data=" + encrypt + "&appId="+ZWBJ_APPID+"";
		byte[] data = HttpUtil.downloadFile(url);
		String str =new String(data);
		Util.log("开普下载文件接口地址：："+url,"kplog",0);
		Util.log("数组长度："+str.length(),"kplog",0);
	    System.out.println("数组长度："+str.length());
	    
	    return data;
	}

	
	
    public static byte[] downloadFile2(String dowurl) throws Exception {
        JSONObject json = new JSONObject();
        json.put("fileURL", dowurl);
        String encrypt = DESUtil.encrypt(json.toJSONString(), ZWBJ_STR_DEFAULT_KEY);
        String url = "http://192.141.1.10:7002/zuul/repo-api/api/res/resource/downloadFile?appId="+ZWBJ_APPID+"&data="+ encrypt;
//        String url1 = "http://192.141.1.10:7002/zuul/repo-api/api/res/resource/downloadFile?appId=dahanapp&data=f1e60ac52cb3fe7bb6e93dcf11c0b581%26017eaa522a75a307277252fe2606497fc9fbb51e38a17711dc259abb8e7176f3f0360665dc6f71d01258d157539a445fb585dc856155db6dae3a297795bf3dd7e8dae286e751029ac3bdf8c92e76c23b0c7b0ea4adfe8e14ab4a721b3fbb3bd0";
//        String url = "http://192.140.199.187:7302/api/res/resource/downloadFile?appId=default&data="+ encrypt;
        byte[] bytes = HttpUtil.downloadFile(url);
	    System.out.println("数组长度："+bytes.length);
//        FileOutputStream downloadFile = new FileOutputStream("D:\\3333.jpg");
//        downloadFile.write(bytes, 0, bytes.length);
//        downloadFile.flush();
//        downloadFile.close();
        return bytes;
    }
    
    
    /** 
     * 过滤掉特殊字符 
     * 
     * @param fileName 
     * @return 
     */  
    public static String filterSpecialChar(String fileName) {  
		//删除所有的空格  
		Pattern pattern = Pattern.compile("[\\s\\\\/:\\*\\?\\\"<>\\|]");
		Matcher matcher = pattern.matcher(fileName);
		fileName= matcher.replaceAll("_"); // 将匹配到的非法字符以空替换
        return fileName;  
    } 
    
    /** 
     * 根据栏目id查询视图id
     * 
     * @param fileName 
     * @return 
     */  
    public static String selectViewid(String chnalid) {  
		//根据栏目id查询栏目信息
		String sServiceId="gov_site";
		String sMethodName="whetherOpenData";
		Map<String, String> savemap = new HashMap();
		savemap.put("ChannelID",chnalid);
		savemap.put("CurrUserName", USER); // 当前操作的用户
		String hyLM=null;
		try {
			hyLM = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSONObject jsonObjectHYSE = JSON.parseObject(hyLM);
		Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		
		Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
		
		String viewid = null;
		if("true".equals(dataArrHYSave)){
			viewid =jsonObjectHYGPSE.getString("VIEWID");//视图id
			
		}else{
			System.out.println("海云中该栏目不存在！！！");
		}
        return viewid;  
    } 
    
    
}
