package com.trs.mqupdatedoc;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.addhytokpsdzc.AddDocumentControllerBMWD;
import com.trs.addhytokpsdzc.AddDocumentControllerCZXX;
import com.trs.addhytokpsdzc.AddDocumentControllerQLQD;
import com.trs.addhytokpsdzc.AddDocumentControllerQXZMCX;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpsdzc.AddDocumentControllerZWMC;
import com.trs.addhytokpsdzc.AddDocumentControllerZWMCDBP;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.hy.Reslibrary;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.AddDocByView;
import com.trs.oauth.ConstantUtil;
import com.trs.updatetokpsdzc.UpdateBMWD;
import com.trs.updatetokpsdzc.UpdateCZXX;
import com.trs.updatetokpsdzc.UpdateLSGB;
import com.trs.updatetokpsdzc.UpdateQLQD;
import com.trs.updatetokpsdzc.UpdateQXZMCX;
import com.trs.updatetokpsdzc.UpdateZCJD;
import com.trs.updatetokpsdzc.UpdateZCWJ;
import com.trs.updatetokpsdzc.UpdateZWMC;
import com.trs.updatetokpsdzc.UpdateZWMCDBP;
import com.trs.updatetokpzfwz.UpdateZFWZCZ;
import com.trs.updatetokpzfwz.UpdateZFWZGH;
import com.trs.updatetokpzfwz.UpdateZFWZHDJLXJYTS;
import com.trs.updatetokpzfwz.UpdateZFWZHDJLYJZJ;
import com.trs.updatetokpzfwz.UpdateZFWZHDJLZXDC;
import com.trs.updatetokpzfwz.UpdateZFWZHDJLZXFT;
import com.trs.updatetokpzfwz.UpdateZFWZHY;
import com.trs.updatetokpzfwz.UpdateZFWZJGGS;
import com.trs.updatetokpzfwz.UpdateZFWZLDJL;
import com.trs.updatetokpzfwz.UpdateZFWZRQGS;
import com.trs.updatetokpzfwz.UpdateZFWZRSRM;
import com.trs.updatetokpzfwz.UpdateZFWZZCJD;
import com.trs.updatetokpzfwz.UpdateZFWZZCWJ;
import com.trs.updatetokpzfwz.UpdateZFWZZFGB;
import com.trs.updatetokpzfwz.UpdateZX;

public class UpdateController {
	private static String  host=ConstantUtil.MQ_ADDRESSES;
	private static String  port=ConstantUtil.MQ_PORT;
//	private static Address[] addresses= ConstantUtil.MQ_ADDRESSES;
	private static String password = ConstantUtil.MQ_PASSWORD;
	private static String virtualHost = ConstantUtil.MQ_VIRTUALHOST;
	private static String username = ConstantUtil.MQ_USERNAME;
	
	private static String exchangeName= ConstantUtil.MQ_EXCHANGENAME_L;
	private static String exchangeType = ConstantUtil.MQ_EXCHANGETYPE;
	private static Boolean exchangeDurable =true;
	private static String routingKey= ConstantUtil.MQ_ROUTINGKEY_L;
	private static String queueName = ConstantUtil.MQ_QUEUENAME;
	private static String consumerTag = ConstantUtil.MQ_CONSUMERTAG;
	
	private static String pageSize = ConstantUtil.pageSize;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String Z_ID =null;
	
	
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	
	
	ChannelReceiver cR=new ChannelReceiver();
	Reslibrary  rl= new Reslibrary();
	ResResource  re= new ResResource();
	HttpFileUpload hfl=new HttpFileUpload();
	JDBCIIP iip=new JDBCIIP();
	AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	static JDBCIDS  jdbcids=new JDBCIDS();
	AddDocByView ac=new AddDocByView();
	
	/**
	 * 修改文档MQ消息
	 * @param jsondata
	 * @throws Exception 
	 */
	public static  void  upDocment(JSONObject jsondata) throws Exception {
		
		System.out.println("------------MQ消息修改文档开始------------");
		//MQ消息队列数据获取
		
		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  站点id SITEID  父栏目id  PARENTID
		String CHANNELID=jsondata.getString("CHNLID");//id
		String DOCID=jsondata.getString("DOCID");//文档id
		String SITEID=jsondata.getString("SITEID");//站点id
	
		System.out.println("==========================================================================");
		System.out.println(CHANNELID+"-------------------文档的栏目id和文档id-----------------"+DOCID);
		System.out.println("==========================================================================");
		
		//根据栏目id查询栏目信息
		String sServiceId="gov_site";
		String sMethodName="whetherOpenData";
		Map<String, String> savemap = new HashMap<String, String>();
		savemap.put("ChannelID",CHANNELID);
		savemap.put("CurrUserName", USER); // 当前操作的用户
		String hyLM = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		
		JSONObject jsonObjectHYSE = JSON.parseObject(hyLM);
		Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		
		Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
		System.out.println("id:"+DOCID+"--海云返回结果："+jsonObjectHYSE.get("MSG"));
		
		String viewid = null;
		if("true".equals(dataArrHYSave)){
			viewid =jsonObjectHYGPSE.getString("VIEWID");//视图id
			
			if("".equals(viewid) || viewid==null){
				//其他默认咨询视图站点
				UpdateZX.upDOc(jsondata,SITEID);
			}else {
				//根据视图id查询视图短名
				String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+viewid+"'";
				String viewName=jdbcids.JDBCVIEWIDSELECT(sql);
				System.out.println("视图短名："+viewName.toUpperCase());
				//自定义视图开始修改文章
				if("WEBLDJL".equals(viewName.toUpperCase())){
					//政府网站-------------领导简历  
					UpdateZFWZLDJL.upDOcZFWZLDJL(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBZCWJ".equals(viewName.toUpperCase())){
					//政府网站--------------政策文件
					UpdateZFWZZCWJ.upDOcZCWJZFWZ(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBZCJD".equals(viewName.toUpperCase())){
					//政府网站--------------政策解读  
					UpdateZFWZZCJD.upDOcZFWZZCJD(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBRSRM".equals(viewName.toUpperCase())){
					//政府网站--------------人事任免
					UpdateZFWZRSRM.upDOcZFWZRSRM(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBCZ".equals(viewName.toUpperCase())){
					//政府网站--------------财政
					UpdateZFWZCZ.upDOcZFWZCZ(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBCZ".equals(viewName.toUpperCase())){
					//政府网站--------------任前公示
					UpdateZFWZRQGS.upDOcZFWZRQGS(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBGH".equals(viewName.toUpperCase())){
					//政府网站--------------规划
					UpdateZFWZGH.upDOcZFWZGH(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBHY".equals(viewName.toUpperCase())){
					//政府网站--------------会议
					UpdateZFWZHY.upDOcZFWZHY(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBZFGB".equals(viewName.toUpperCase())){
					//政府网站--------------政府公报
					UpdateZFWZZFGB.upDOcZFWZZFGB(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBHDJLZXJYTS".equals(viewName.toUpperCase())){
					//政府网站--------------互动交流咨询建议投诉
					UpdateZFWZHDJLXJYTS.upDOcZFWZHDJLXJYTS(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBHDJLZXDC".equals(viewName.toUpperCase())){
					//政府网站--------------互动交流在线调查
					UpdateZFWZHDJLZXDC.upDOcZFWZHDJLZXDC(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBHDJLZXFT".equals(viewName.toUpperCase())){
					//政府网站--------------互动交流在线访谈
					UpdateZFWZHDJLZXFT.upDOcZFWZZXFT(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBHDJLYJZJ".equals(viewName.toUpperCase())){
					//政府网站--------------互动交流意见征集
					UpdateZFWZHDJLYJZJ.upDOcZFWZHDJLYJZJ(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("WEBJGGS".equals(viewName.toUpperCase())){
					//政府网站--------------结果公示
					UpdateZFWZJGGS.upDOcZFWZJGGS(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("ZCWJ".equals(viewName.toUpperCase())){
					//首都之窗--------------政策文件
					UpdateZCWJ.upDOcZCWJ(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("ZCJD".equals(viewName.toUpperCase())){
					//首都之窗-------------- 政策解读
					UpdateZCJD.upDocZCJD(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("LSGB".equals(viewName.toUpperCase())){
					//首都之窗历史公报--------------  
					UpdateLSGB.upDOcLSGB(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("CZXX".equals(viewName.toUpperCase())){
					//首都之窗 ------- 财政信息
					UpdateCZXX.upDOcCZXX(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("ZWMC".equals(viewName.toUpperCase())){
					System.out.println("----------------------------------进入政务名词视图修改--------------------------------");
					//首都之窗 ------- 政务名词   以下视图  不做回推代码
					UpdateZWMC.upDOcZWNC(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);	
				}else if("ZWMCDBP".equals(viewName.toUpperCase())){
					//首都之窗 ------- 政务名词大比拼
					UpdateZWMCDBP.upDOcZWNCDBP(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);	
				}else if("XQZMCX".equals(viewName.toUpperCase())){
					//首都之窗 ------- 取消证明查询
					UpdateQXZMCX.upDOcQXZMCX(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);	
				}else if("BMWD".equals(viewName.toUpperCase())){
					//首都之窗 ------- 便民问答
					UpdateBMWD.upDOcBMWD(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}else if("QLQD".equals(viewName.toUpperCase())){
					//首都之窗 -------权力清单
					UpdateQLQD.upDOcQLQD(CHANNELID,DOCID,viewName.toUpperCase(),SITEID);
				}
				
			}
			
		}else{
			System.out.println("海云中该栏目不存在！！！");
		}

		System.out.println("------------MQ消息修改文档结束------------");
		
	
	}

}
