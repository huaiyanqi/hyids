package com.trs.updatetokpzfwz;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.SelectSiteName;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;


/**
 * 修改    政府网站-互动交流咨询建议投诉
 * @author epro1
 *
 */
public class UpdateZFWZHDJLXJYTS {
	
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String HYCHNLID = ConstantUtil.HYCHNLID;
	private static String HYSITEID = ConstantUtil.HYSITEID;
	
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	
	 /**
	 * 修改文档-互动交流咨询建议投诉
	 * @param jsondata
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unused", "static-access" })
	public static void upDOcZFWZHDJLXJYTS(String channelid,String docid,String viewname,String SITEID) throws Exception{
		
		System.out.println("------------修改      政府网站-互动交流咨询建议投诉            文档开始------------");
		//开普接口json
		JSONObject jsonOb = new JSONObject();
		JSONArray json = new JSONArray();// 默认附件
		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  开普同步到海云id SITEID  父栏目id  PARENTID
		String CHANNELID=channelid;//id
		String DOCID=docid;//文档id
		String zw=null;//正文
		String pContent=null;
		String otherContent=null;
		String bt=null;//解读名称
		String fbt=null;//副标题
		String zy=null;//文档摘要
		String ly=null;//来源
		String zz=null;//作者
		String kaipuid=null;
		try {
				kaipuid=cR.doCheck(DOCID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(kaipuid==null){
			System.out.println("开普中不存在该文档！");
		}else{
			//查询站点名称 打印日志需要使用
			String sitename=SelectSiteName.selectSiteName(SITEID);
			System.out.println("----------------------------处理文档开始-----------------------------");
			//调用海云接口查询文档详细数据
			String sServiceId="gov_webdocument";
			String sMethodName="findOpenDataDocumentById";
			Map<String, String> savemap = new HashMap<String, String>();
			savemap.put("DocId",DOCID);
			savemap.put("ChannelId",CHANNELID);
			savemap.put("CurrUserName", USER); // 当前操作的用户
			String hy11= HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
			JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
			Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
			
			Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
			System.out.println("id:"+DOCID+"--海云返回结果："+jsonObjectHYSE.get("MSG"));
			
			if("true".equals(dataArrHYSave)){
				
				//处理获取结果
				//默认字段处理
				bt =jsonObjectHYGPSE.getString("BT");//文件名称
				fbt =jsonObjectHYGPSE.getString("FBT");//副标题
				zy=jsonObjectHYGPSE.getString("ZY");//内容摘要
				ly =jsonObjectHYGPSE.getString("LY");
				zz =jsonObjectHYGPSE.getString("ZZ");//作者
				
				//循环处理其他字段
				for(String str:jsonObjectHYGPSE.keySet()){
					
					if(str.indexOf("DOCATTACHFILEFIELD")>=0||"DOCATTACHPICFIELD".equals(str)){
						//政府网站默认附件处理
					    JSONArray DOCRELFILE=jsonObjectHYGPSE.getJSONArray(str);//相关附件
					    String docPath=null;
					    String dName=null;
					    if(DOCRELFILE!=null){
					    	for(int i = 0; i < DOCRELFILE.size(); i++){
						    	String url="http://192.141.252.5/gov/file/read_file.jsp?DownName=DOCUMENT&FileName=";
						    	docPath=DOCRELFILE.getJSONObject(i).getString("APPFILE");
						    	dName=DOCRELFILE.getJSONObject(i).getString("APPDESC");
						    	//处理附件名称带特殊字符
						    	dName=zfwzUtil.filterSpecialChar(dName);
						    	String Pv=docPath.substring(docPath.lastIndexOf("."),docPath.length());
						    	//上传之前先下载
								try {
									HttpFileUpload.downLoadFromUrl(url+docPath,dName+Pv,resouse_path);
								} catch (IOException e) {
									Util.log("文章名称："+bt,"erroUpdateUpdate"+sitename+SITEID,0);
									Util.log("栏目id："+CHANNELID,"erroUpdateUpdate"+sitename+SITEID,0);
									Util.log("错误信息：附件下载错误"+e,"erroUpdateUpdate"+sitename+SITEID,0);
									Util.log("=============================================================================","erroUpdateUpdate"+sitename+SITEID,0);
									e.printStackTrace();
								}
						    	String pKPpath=zfwzUtil.uploadFile(resouse_path+docPath,dName+Pv,bt,CHANNELID,sitename,SITEID);
						    	//返回的开普上传路径，需要拼接成开普需要的格式
						    	//循环添加到JSONArray中
						 	    JSONObject fpCon= new JSONObject();
						 	    fpCon.put("name", dName);//附件名称
						 	    fpCon.put("path", pKPpath);// 附件路径
						 	    json.add(fpCon);
						    }
					    }
				    
					}else if(str.indexOf("ZW")>=0){
						zw=jsonObjectHYGPSE.get(str).toString();
						// 处理正文 内容  
						//图片 Content_regular_p    截取开始 src='  5个字符串
						pContent = re.contentResource(zw, 5, Content_regular_p,bt,CHANNELID,sitename,SITEID);
						//处理 zip  pdf  rar   截取开始  href=' 6个字符串
						otherContent = re.contentResource(pContent, 6, Content_regular,bt,CHANNELID,sitename,SITEID);
							
					}else{
						String value=jsonObjectHYGPSE.get(str).toString();
						if(value!=null){
							jsonOb.put(viewname+"@@"+str, value.replace("[\"", "").replace("\"]", "").replace("\'", "").replace("[", "").replace("]", ""));
						}
					}
					
				}
			    
				//修改文档固定参数   
				jsonOb.put("name", bt); 
				jsonOb.put("resTranMode", "0");// 0  文本内容    1：文件资源-文件传输  
				jsonOb.put("isOrig", "1");
				 //默认字段
			    System.out.println("正文数据是否为空："+zw);
			    if(zw==null||"".equals(zw)){
			    	jsonOb.put("content", "&nbsp&nbsp&nbsp&nbsp&nbsp");//正文
			    }else{
			    	jsonOb.put("content", otherContent);//正文
			    }
			    if(zy!=null){
			    	jsonOb.put("abstracts", zy);	
			    }
				if(fbt!=null){
					jsonOb.put("resTitle", fbt);
				}
			    if(zz!=null){
			    	jsonOb.put("author", zz);
			    }
			    if(ly!=null){
			    	jsonOb.put("source", ly); //来源
			    }
				String pubUrl=JDBCIIP.JDBCDriverSelectPubUrl(DOCID);
			    if(pubUrl!=null){
			    	jsonOb.put("online", pubUrl);
			    }
				String zxsj=jsonObjectHYGPSE.getString("ZXSJ");//撰写时间
			    if(zxsj!=null){
			    	jsonOb.put("pubDate", zxsj);
			    }
				jsonOb.put("resId",ZWBJ_APPID+ DOCID); //  资源id
				System.out.println("存入开普数据jsonOb==:"+jsonOb);
				System.out.println("存入开普数据json==:"+json);
				String result=null;
				try {
				   result=zfwzUtil.updateRes0(json, jsonOb);
				} catch (Exception e) {
					// TODO Auto-generated catch block
						e.printStackTrace();
				}
				    //处理开普新增返回结果
				JSONObject jsonresult = JSON.parseObject(result);
				Integer  dataresult = (Integer) jsonresult.get("code"); 
				if(dataresult==0){
					System.out.println("修改政府网站财政资源成功！");
				}else{
					Util.log("文章名称："+bt,"erroUpdate"+sitename+SITEID,0);
					Util.log("栏目id："+CHANNELID,"erroUpdate"+sitename+SITEID,0);
					Util.log("错误信息：修改政府网站-互动交流咨询建议投诉资源失败-----"+jsonresult.get("msg"),"erroUpdate"+sitename+SITEID,0);
					Util.log("=============================================================================","erroUpdate"+sitename+SITEID,0);
					System.out.println("修改政府网站财政资源失败："+jsonresult.get("msg"));
				}
				
			}else{
				Util.log("文章名称："+bt,"erroUpdate"+sitename+SITEID,0);
				Util.log("栏目id："+CHANNELID,"erroUpdate"+sitename+SITEID,0);
				Util.log("错误信息：调用海云接口查询文章失败-----"+jsonObjectHYSE.get("MSG"),"erroUpdate"+sitename+SITEID,0);
				Util.log("=============================================================================","erroUpdate"+sitename+SITEID,0);
				System.out.println("海云查询文档详细信息接口返回信息:"+jsonObjectHYSE.get("MSG"));
			}
			
		
		}
		System.out.println("------------修改     政府网站-互动交流咨询建议投诉        文档结束------------");	
	
	}

}
