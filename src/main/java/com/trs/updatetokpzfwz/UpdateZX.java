package com.trs.updatetokpzfwz;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.hy.Reslibrary;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.mqadddoc.AddDocByView;
import com.trs.mqadddoc.SelectSiteName;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

public class UpdateZX {
	 /**
     * 请求编码
     */
    public static String requestEncoding = "UTF-8";
    /**
     * 连接超时
     */
    private static int connectTimeOut = 5000;
    /**
     * 读取数据超时
     */
    private static int readTimeOut = 10000;
	
	private static String  host=ConstantUtil.MQ_ADDRESSES;
	private static String  port=ConstantUtil.MQ_PORT;
//	private static Address[] addresses= ConstantUtil.MQ_ADDRESSES;
	private static String password = ConstantUtil.MQ_PASSWORD;
	private static String virtualHost = ConstantUtil.MQ_VIRTUALHOST;
	private static String username = ConstantUtil.MQ_USERNAME;
	
	private static String exchangeName= ConstantUtil.MQ_EXCHANGENAME_L;
	private static String exchangeType = ConstantUtil.MQ_EXCHANGETYPE;
	private static Boolean exchangeDurable =true;
	private static String routingKey= ConstantUtil.MQ_ROUTINGKEY_L;
	private static String queueName = ConstantUtil.MQ_QUEUENAME;
	private static String consumerTag = ConstantUtil.MQ_CONSUMERTAG;
	
	private static String pageSize = ConstantUtil.pageSize;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String Z_ID =null;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	private static String Z_URL = ConstantUtil.Z_URL;
	
	
	static ChannelReceiver cR=new ChannelReceiver();
	static Reslibrary  rl= new Reslibrary();
	static ResResource  re= new ResResource();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static AddDocByView ac=new AddDocByView();
	
	
	/**
	 * 修改文档
	 * @param jsondata
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unused", "unchecked", "static-access" })
	public static void upDOc(JSONObject jsondata,String SITEID) throws Exception{
		
		System.out.println("------------修改文档开始------------");
		//MQ消息队列数据获取
		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  站点id SITEID  父栏目id  PARENTID
		String DOCID=jsondata.getString("DOCID");//文档id
		String CHANNELID=jsondata.getString("CHNLID");//id
		String DOCSTATUS=null;//文档状态  2-待编，18-待审，16-待签，10-已发
		String DOCTITLE=null;//文档标题
		String SUBDOCTITLE=null;//副标题
		String DOCKEYWORDS=null;//关键词
		String DOCABSTRACT=null;//文档摘要
		String DOCSOURCENAME=null;//来源
		String DOCRELTIME=null;//撰写时间
		String DOCPUBTIME=null;//发布时间
		String DOCAUTHOR=null;//作者
		
		String kaipuid=null;
		String kaipupush=null;
		String DOCLINK=null;//外部链接
		try {
				kaipuid=cR.doCheck(DOCID);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(kaipuid==null){
			System.out.println("开普中不存在该文档！");
		}else{
			//查询站点名称 打印日志需要使用
			String sitename=SelectSiteName.selectSiteName(SITEID);
			//调用海云接口查询文档详细数据
			String sServiceId="gov_webdocument";
			String sMethodName="findDocumentById";
			Map savemap = new HashMap();
			savemap.put("DocId",DOCID);
			savemap.put("ChannelId",CHANNELID);
			savemap.put("CurrUserName", USER); // 当前操作的用户
			String hy11=null;
			try {
				hy11 = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
			Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
			
			Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
			System.out.println("id:"+DOCID+"--海云返回结果："+jsonObjectHYSE.get("MSG"));
			
			if("true".equals(dataArrHYSave)){
				//处理获取结果
				DOCTITLE =jsonObjectHYGPSE.getString("DOCTITLE");//标题
				SUBDOCTITLE=jsonObjectHYGPSE.getString("SUBDOCTITLE");// 副标题
				DOCKEYWORDS=jsonObjectHYGPSE.getString("DOCKEYWORDS");// 关键词
				DOCABSTRACT =jsonObjectHYGPSE.getString("DOCABSTRACT");//摘要
				DOCSOURCENAME=jsonObjectHYGPSE.getString("DOCSOURCENAME");//来源
				DOCAUTHOR=jsonObjectHYGPSE.getString("DOCAUTHOR");//作者
				DOCRELTIME=jsonObjectHYGPSE.getString("DOCRELTIME");//撰写时间
				DOCPUBTIME=jsonObjectHYGPSE.getString("DOCPUBTIME");//发布时间
				DOCLINK =jsonObjectHYGPSE.getString("DOCLINK");// 外部连接SUBDOCTITLE
				String DOCHTMLCON=jsonObjectHYGPSE.getString("DOCHTMLCON");//正文  带标签的 正文
				
				DOCSTATUS=jsonObjectHYGPSE.getString("DOCSTATUS");//文档状态  2-待编，18-待审，16-待签，10-已发
				SITEID=jsonObjectHYGPSE.getString("SITEID");//开普同步到海云id
				String DOCCONTENT=jsonObjectHYGPSE.getString("DOCCONTENT");//正文  不带标签的正文
				String docType=jsonObjectHYGPSE.getString("DOCTYPE");//文章类型
				System.out.println("文章类型："+docType);
				
				if(DOCHTMLCON!=null){
					// 处理正文 内容  
					
					String pContent=null;
					String otherContent=null;
//					String videoContent=null;
					try {
						//图片 Content_regular_p    截取开始 src='  5个字符串
						pContent = re.contentResource(DOCHTMLCON, 5, Content_regular_p,DOCTITLE,CHANNELID,sitename,SITEID);
						//处理 zip  pdf  rar   截取开始  href=' 6个字符串
						otherContent = re.contentResource(pContent, 6, Content_regular,DOCTITLE,CHANNELID,sitename,SITEID);
						//处理正文中的视频标签
//						String videoNmae=CHANNELID+"_"+DOCTITLE+"_"+DOCID;
//						videoContent = re.contentResourceVideo(otherContent, 1, Content_regular_v,videoNmae);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					
					//处理附件
					//处理 图片  
					
					JSONArray DOCRELPIC=jsonObjectHYGPSE.getJSONArray("DOCRELPIC");//相关图
				    String Ppath=null;
				    String APPDESC=null;
				    JSONArray json = new JSONArray();// 默认附件
				    for(int i = 0; i < DOCRELPIC.size(); i++){
				    	Ppath=DOCRELPIC.getJSONObject(i).getString("APPFILEURL");
				    	APPDESC=DOCRELPIC.getJSONObject(i).getString("APPDESC");
				    	//处理附件名称带特殊字符
				    	APPDESC=zfwzUtil.filterSpecialChar(APPDESC);
				    	String Pv=Ppath.substring(Ppath.lastIndexOf("."),Ppath.length());
				    	String upname=Ppath.substring(Ppath.lastIndexOf("/")+1);
				    	//上传之前先下载
						try {
							HttpFileUpload.downLoadFromUrl(Ppath,APPDESC+Pv,resouse_path);
						} catch (IOException e) {
							Util.log("文章名称："+DOCTITLE,"erroUpdateUpdate"+sitename+SITEID,0);
							Util.log("栏目id："+CHANNELID,"erroUpdateUpdate"+sitename+SITEID,0);
							Util.log("错误信息：附件下载错误"+e,"erroUpdateUpdate"+sitename+SITEID,0);
							Util.log("=============================================================================","erroUpdateUpdate"+sitename+SITEID,0);
							e.printStackTrace();
						}
				    	
				    	String pKPpath=zfwzUtil.uploadFile(resouse_path+upname,APPDESC+Pv,DOCTITLE,CHANNELID,sitename,SITEID);
				    	//返回的开普上传路径，需要拼接成开普需要的格式
				    	//循环添加到JSONArray中
				 	    JSONObject fpCon= new JSONObject();
				 	    fpCon.put("name", APPDESC);//图片名称
				 	    fpCon.put("path", pKPpath);// 图片路径
				 	    json.add(fpCon);
				    }
				    
					//处理附件
				    JSONArray DOCRELFILE=jsonObjectHYGPSE.getJSONArray("DOCRELFILE");//相关附件
				    String docPath=null;
				    String dName=null;
				    for(int i = 0; i < DOCRELFILE.size(); i++){
				    	String url="http://192.141.252.5/gov/file/read_file.jsp?DownName=DOCUMENT&FileName=";
				    	docPath=DOCRELFILE.getJSONObject(i).getString("APPFILE");
				    	dName=DOCRELFILE.getJSONObject(i).getString("APPDESC");
				    	//处理附件名称带特殊字符
				    	dName=zfwzUtil.filterSpecialChar(dName);
				    	String Pv=docPath.substring(docPath.lastIndexOf("."),docPath.length());
				    	//上传之前先下载
						try {
							HttpFileUpload.downLoadFromUrl(url+docPath,dName+Pv,resouse_path);
						} catch (IOException e) {
							Util.log("文章名称："+DOCTITLE,"erroUpdateUpdate"+sitename+SITEID,0);
							Util.log("栏目id："+CHANNELID,"erroUpdateUpdate"+sitename+SITEID,0);
							Util.log("错误信息：附件下载错误"+e,"erroUpdateUpdate"+sitename+SITEID,0);
							Util.log("=============================================================================","erroUpdateUpdate"+sitename+SITEID,0);
							e.printStackTrace();
						}
						
				    	String pKPpath=zfwzUtil.uploadFile(resouse_path+docPath,dName+Pv,DOCTITLE,CHANNELID,sitename,SITEID);
				    	//返回的开普上传路径，需要拼接成开普需要的格式
				    	//循环添加到JSONArray中
				 	    JSONObject fpCon= new JSONObject();
				 	    fpCon.put("name", dName);//附件名称
				 	    fpCon.put("path", pKPpath);// 附件路径
				 	    json.add(fpCon);
				    }
					
				    //处理视频
				    JSONArray DOCRELVIDEO=jsonObjectHYGPSE.getJSONArray("DOCRELVIDEO");////相关视频
				    String vpath=null;
				    String vName=null;
				    for(int i = 0; i < DOCRELVIDEO.size(); i++){
				    	String APPFROMID=DOCRELVIDEO.getJSONObject(i).getString("APPFROMID");//视频id
				    	
				    	String sServiceIdurl="gov_mas";
						String sMethodNameurl="getDownLoadVideoURL";
						Map urlmap = new HashMap();
						urlmap.put("videoIds",APPFROMID);
						urlmap.put("CurrUserName", "dev"); // 当前操作的用户
						String hyurl=null;
						try {
							hyurl = HyUtil.dataMoveDocumentHyRbj(sServiceIdurl,sMethodNameurl,urlmap);
						} catch (Exception e) {
							Util.log("文章名称："+DOCTITLE,"erroUpdateUpdate"+sitename+SITEID,0);
							Util.log("栏目id："+CHANNELID,"erroUpdateUpdate"+sitename+SITEID,0);
							Util.log("错误信息：附件下载错误"+e,"erroUpdateUpdate"+sitename+SITEID,0);
							Util.log("=============================================================================","erroUpdateUpdate"+sitename+SITEID,0);
							e.printStackTrace();
						}
						
						JSONObject jsonObjecturl = JSON.parseObject(hyurl);
						String  url = jsonObjecturl.getString("DATA");
						//海云下载url
						System.out.println(url);
				    	//获取视频名称
				    	try {
							vName=hfl.urlName(url);
					    	//处理附件名称带特殊字符
							vName=zfwzUtil.filterSpecialChar(vName);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				    	//上传之前先下载
						try {
							HttpFileUpload.downLoadFromUrl(url,vName,resouse_path);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
				    	String pKPpath=zfwzUtil.uploadFile(resouse_path+vName,vName,DOCTITLE,CHANNELID,sitename,SITEID);
				    	//返回的开普上传路径，需要拼接成开普需要的格式
				    	//循环添加到JSONArray中
				 	    JSONObject fpCon= new JSONObject();
				 	    fpCon.put("name", vName);//视频名称
				 	    fpCon.put("path", pKPpath);// 视频路径
				 	    json.add(fpCon);
				    }
				    //查询对应栏目的开普id  
				    //根据站点id 获取到开普对应的资源库id  SITEID
					
					
				    // 处理正常字段同步
				    JSONObject jsonOb = new JSONObject();
				    
				    jsonOb.put("resTranMode", "0");
					if("20".equals(docType)||"50".equals(docType)){
						if(DOCHTMLCON == null|| DOCHTMLCON.isEmpty()){
							jsonOb.put("content", "&nbsp&nbsp&nbsp&nbsp&nbsp");//正文
						}else{
//							String newUrl="<video controls='controls' loop='loop' autoplay='autoplay'";
//							jsonOb.put("content", otherContent.replace("<iframe", newUrl).replace("</iframe", "</video"));//正文
							jsonOb.put("content", otherContent);//正文
						}
					}else if("30".equals(docType)){
						jsonOb.put("content", DOCLINK);//正文  外部连接做为正文传递  连接文档
					}else if("40".equals(docType)){
						jsonOb.put("content", "&nbsp&nbsp&nbsp&nbsp&nbsp");//正文  //外部文件文档
					}

				    jsonOb.put("name", DOCTITLE); 
				    jsonOb.put("GOVDOCNEWS@@"+"SUBDOCTITLE", SUBDOCTITLE); //副标题
				    jsonOb.put("GOVDOCNEWS@@"+"DOCKEYWORDS", DOCKEYWORDS); //关键字
				    jsonOb.put("source", DOCSOURCENAME); //来源 
				    jsonOb.put("abstracts", DOCABSTRACT); //摘要
				    jsonOb.put("author", DOCAUTHOR);//作者
				    jsonOb.put("GOVDOCNEWS@@"+"DOCRELTIME", DOCRELTIME);//撰写时间
				    jsonOb.put("GOVDOCNEWS@@"+"DOCPUBTIME", DOCPUBTIME);//发布时间
					
				    jsonOb.put("isOrig", "1");
				    
					String pubUrl=JDBCIIP.JDBCDriverSelectPubUrl(DOCID);
				    if(pubUrl!=null && ! pubUrl.isEmpty() ){
				    	jsonOb.put("online", pubUrl);
				    }
					if(DOCRELTIME!=null){
				    	jsonOb.put("pubDate", DOCRELTIME);
				    }
				    jsonOb.put("resId", ZWBJ_APPID+DOCID); //  资源id
				    System.out.println("存入开普数据jsonOb==:"+jsonOb);
				    System.out.println("存入开普数据json==:"+json);
				    String result=null;
				    try {
				    	result=updateRes0(json, jsonOb);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    //处理开普新增返回结果
				    JSONObject jsonresult = JSON.parseObject(result);
				    Integer  dataresult = (Integer) jsonresult.get("code"); 
					if(dataresult==0){
						System.out.println("修改资源成功！");
					}else{
						Util.log("文章名称："+DOCTITLE,"erroUpdate"+sitename+SITEID,0);
						Util.log("栏目id："+CHANNELID,"erroUpdate"+sitename+SITEID,0);
						Util.log("错误信息：修改咨询资源失败-----"+jsonresult.get("msg"),"erroUpdate"+sitename+SITEID,0);
						Util.log("=============================================================================","erroUpdate"+sitename+SITEID,0);
						System.out.println("修改咨询资源失败："+jsonresult.get("msg"));
					}
					
				}
				
			}else{
				Util.log("文章名称："+DOCTITLE,"erroUpdate"+sitename+SITEID,0);
				Util.log("栏目id："+CHANNELID,"erroUpdate"+sitename+SITEID,0);
				Util.log("错误信息：调用海云接口查询文章失败-----"+jsonObjectHYSE.get("MSG"),"erroUpdate"+sitename+SITEID,0);
				Util.log("=============================================================================","erroUpdate"+sitename+SITEID,0);
				System.out.println("海云查询文档详细信息接口返回信息:"+jsonObjectHYSE.get("MSG"));
			}
			
		
		}
		System.out.println("------------修改文档结束------------");	
	
	}
	
	
	/**
	 * 修改开普资源
	 * @throws Exception
	 */
	//  resTranMode=0
	public static String  updateRes0(JSONArray jsonArray,JSONObject json) throws Exception {
		  	
		  	json.put("attachments", jsonArray.toJSONString());//附件
		    
		    String url =Z_URL+ "/resource/updateRes";
		    System.out.println(url);
		    Map<String, String> map = new HashMap<String, String>();
		    map.put("appId",ZWBJ_APPID);
		        map.put("data", DESUtil.encrypt(json.toJSONString(),ZWBJ_STR_DEFAULT_KEY));
		    String post = HttpUtil.doPost(url, map,json);
		    System.out.println(post); 
		    return post;
	 }
	
	
	 public static String doPost(String reqUrl, Map parameters) {
	    	System.out.println("------reqUrl开始------"+reqUrl);
	    	System.out.println("------parameters开始------"+parameters);
	    	System.out.println("------dopost开始------");
	        HttpURLConnection url_con = null;
	        String responseContent = null;
	        try {
//	        	System.out.println("进入try：");
	            String params = getMapParamsToStr(parameters, requestEncoding);
//	            System.out.println("dopost里的params"+params);
	            URL url = new URL(reqUrl);
//	            System.out.println("dopost里的url"+url);
	            url_con = (HttpURLConnection) url.openConnection();
	            url_con.setRequestMethod("POST");
	            System.setProperty("sun.net.client.defaultConnectTimeout", String.valueOf(connectTimeOut));// （单位：毫秒）jdk1.4换成这个,连接超时
	            System.setProperty("sun.net.client.defaultReadTimeout", String.valueOf(readTimeOut)); // （单位：毫秒）jdk1.4换成这个,读操作超时
	            url_con.setRequestProperty("User-agent","Mozilla/4.0");
	            url_con.setDoOutput(true);
	            byte[] b = params.toString().getBytes();
	            url_con.getOutputStream().write(b, 0, b.length);
	            url_con.getOutputStream().flush();
	            url_con.getOutputStream().close();
	            int responseCode = url_con.getResponseCode();  
//	            System.out.println("responseCode:"+responseCode);
	            InputStream in=null;
		    	if (responseCode == 200) {  
		    		in = new BufferedInputStream(url_con.getInputStream());  
		    	} else {  
		    		in = new BufferedInputStream(url_con.getErrorStream());  
		    	} 
	            BufferedReader rd = new BufferedReader(new InputStreamReader(in, requestEncoding));
	            String tempLine = rd.readLine();
	            StringBuffer tempStr = new StringBuffer();
	            String crlf = System.getProperty("line.separator");
	            while (tempLine != null) {
	                tempStr.append(tempLine);
	                tempStr.append(crlf);
	                tempLine = rd.readLine();
	            }
	            responseContent = tempStr.toString();
	            rd.close();
	            in.close();
	        } catch (IOException e) {
	            System.out.println("网络故障");
	            e.printStackTrace();
	        } finally {
	        	System.out.println("进入finally");
	            if (url_con != null) {
	                url_con.disconnect();
	            }
	        }
	        return responseContent;
	    }

	    public static void test(){
	    	System.out.println("调用测试方法调用");
	    }

	    private static String getMapParamsToStr(Map paramMap, String requestEncoding) throws IOException {
	        StringBuffer params = new StringBuffer();
	        // 设置边界
	        for (Iterator iter = paramMap.entrySet().iterator(); iter.hasNext(); ) {
	            Map.Entry element = (Map.Entry) iter.next();
	            params.append(element.getKey().toString());
	            params.append("=");
	            params.append(URLEncoder.encode(element.getValue().toString(), requestEncoding));
	            params.append("&");
	        }

	        if (params.length() > 0) {
	            params = params.deleteCharAt(params.length() - 1);
	        }

	        return params.toString();
	    }
}
