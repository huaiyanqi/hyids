package com.trs.deletedoc;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HyUtil;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.mqadddoc.AddDocByView;
import com.trs.oauth.ConstantUtil;

public class DeleteDocByView {
	

	private static String USER = ConstantUtil.USER;
	
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	
	static JDBCIIP iip=new JDBCIIP();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static ChannelReceiver cR=new ChannelReceiver();
	
	/**
	 * 区分 政府网站还是首都之窗删除数据
	 * @param jsonObjectHYName
	 * @throws SQLException
	 * @throws Exception
	 */
	public static void deleteByView(JSONObject jsonObjectHYName) throws SQLException, Exception{
		//删除的文档id
		String dele=jsonObjectHYName.getString("DATA");
		System.out.println("删除recid:"+dele);
//		String CHNLID=iip.JDBCDriverSelectChID("SELECT * FROM wcmchnldoc WHERE RECID='"+dele+"'");
//		String DOCID=iip.JDBCDriverSelect("SELECT * FROM wcmchnldoc WHERE RECID='"+dele+"'");
		JSONObject jsonOb = new JSONObject();
		jsonOb.put("isOrig", "1"); 
		jsonOb.put("resIds", ZWBJ_APPID+dele);
		String kaipuid=cR.doCheck(dele);
		if(kaipuid==null){
			System.out.println("没有需要删除的文档！！！！！！！！！");
		}else{
			DeletekpDoc.deDocView(jsonOb,dele);
		}

		
		//根据栏目id查询到视图
//		String sServiceIdView="gov_site";
//		String sMethodNameView="whetherOpenData";
//		Map<String, String> savemapView = new HashMap();
//		savemapView.put("ChannelID",CHNLID);
//		savemapView.put("CurrUserName", USER); // 当前操作的用户
//		String hyLMView = HyUtil.dataMoveDocumentHyRbj(sServiceIdView,sMethodNameView,savemapView);
//		JSONObject jsonObjectHYSEView = JSON.parseObject(hyLMView);
//		Object dataArrView = jsonObjectHYSEView.get("DATA");//根据json对象中数组的名字解析出其所对应的值
//		JSONObject jsonObjectHYGPSEView = JSON.parseObject(dataArrView.toString());
//		System.out.println("删除数据到海云查询视图短名返回结果=================："+jsonObjectHYSEView.get("MSG"));
//		//视图id
//		String viewid =jsonObjectHYGPSEView.getString("VIEWID");
//
//		if(kaipuid==null){
//			System.out.println("中间表中没有需要删除的文档!!");
//		}else{
//			
//			if(viewid==null||viewid.equals("")||viewid.isEmpty()){
//				//默认视图删除文档
//				DeletekpDoc.deDoc(jsonOb,DOCID);
//			}else{
//				//根据视图id查询视图短名
//				String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+viewid+"'";
//				String viewName=jdbcids.JDBCVIEWIDSELECT(sql);
//				System.out.println("政府网站：转换为大写的视图短名================："+viewName.toUpperCase());
//				//其他视图
//				DeletekpDoc.deDocView(jsonOb,DOCID);
//			}
//
//		}
		
	}
	
	
	/**
	 * 撤销发布文档MQ消息
	 * @param jsondata
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static  void  delDoc(JSONObject jsondata) throws Exception {
		
		System.out.println("------------MQ消息撤销发布文档删除开普资源开始------------");
		//MQ消息队列数据获取
		
		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  站点id SITEID  父栏目id  PARENTID
		String CHANNELID=jsondata.getString("CHNLID");//id
		String dele=jsondata.getString("DOCID");//需要删除的文档id
		String SITEID=jsondata.getString("SITEID");//站点id
	
		System.out.println("==========================================================================");
		System.out.println(CHANNELID+"-------------------文档的栏目id和需要删除的文档id-----------------"+dele);
		System.out.println("==========================================================================");
		System.out.println("撤销发布recid:"+dele);
//		String DOCID=iip.JDBCDriverSelect("SELECT * FROM wcmchnldoc WHERE RECID='"+dele+"'");
		JSONObject jsonOb = new JSONObject();
		jsonOb.put("isOrig", "1"); 
		jsonOb.put("resIds", ZWBJ_APPID+dele);
		String kaipuid=cR.doCheck(dele);
		if(kaipuid==null){
			System.out.println("没有需要删除的文档！！！！！！！！！");
		}else{
			DeletekpDoc.deDocView(jsonOb,dele);
		}
		
		System.out.println("------------MQ消息消息撤销发布文档删除开普资源结束------------");
		
	
	}

}
