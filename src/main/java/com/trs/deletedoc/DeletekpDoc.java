package com.trs.deletedoc;

import java.sql.SQLException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.ResResource;
import com.trs.hy.Reslibrary;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.mqadddoc.AddDocByView;
import com.trs.oauth.ConstantUtil;

public class DeletekpDoc {
	
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Z_STR_DEFAULT_KEY = ConstantUtil.Z_STR_DEFAULT_KEY;
	private static String TABLE = "document";
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String Z_URL = ConstantUtil.Z_URL;
	
	static ChannelReceiver cR=new ChannelReceiver();
	Reslibrary  rl= new Reslibrary();
	static ResResource  re= new ResResource();
	HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	JDBCIDS  jdbcids=new JDBCIDS();
	AddDocByView ac=new AddDocByView();
	
	
	
	
	/**
	 * 删除文档   首都之窗
	 * @param jsonObjectHYName
	 */
	public static  void  deDoc(JSONObject jsonObject,String DOCID){

		System.out.println("------------删除文档开始------------");	
		 try {
			String deleResult=deleRes(jsonObject,Z_APPID,Z_STR_DEFAULT_KEY);
			//处理开普新增返回结果
		    JSONObject jsonresult = JSON.parseObject(deleResult);
		    Integer  dataresult = (Integer) jsonresult.get("code"); 
			if(0 == dataresult){
				System.out.println("删除资源成功！");
				JDBC jdbc=new JDBC();
				Integer delete=0;
				try {
					delete = jdbc.JDBCDriver("DELETE  FROM "+TABLE+" where "+SOURCE+"='WD' and "+FIELDHY+"='"+DOCID+"'");
					//删除开普同步过来得文章   根据文档id唯一性可以确定是同步到开普得 还是开普同步到海云得   只删除中间表中得数据
					Integer	deleteKP = jdbc.JDBCDriver("DELETE  FROM "+TABLE+" where "+SOURCE+"='KP' and "+FIELDHY+"='"+DOCID+"'");
					if(deleteKP==1){
						if(delete==1){
							Util.log("开普同步过来的文档中间表删除成功!","log",0);
							System.out.println("开普同步过来的文档中间表删除成功!");
						}else{
							Util.log("开普同步过来的文档中间表删除失败!","log",0);
							System.out.println("开普同步过来的文档中间表删除失败!");
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(delete==1){
					Util.log("文档中间表删除成功!","log",0);
					System.out.println("文档中间表删除成功!");
				}else{
					Util.log("文档中间表删除失败!","log",0);
					System.out.println("文档中间表删除失败!");
				}
			}else{
				System.out.println("删除资源失败："+jsonresult.get("msg"));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("------------删除文档结束------------");	
	
		
	}
	
	
	/**
	 * 删除文档   政府网站
	 * @param jsonObjectHYName
	 */
	public static  void  deDocView(JSONObject jsonObject,String DOCID){

		System.out.println("----------------------------------政府网站删除文档开始----------------------------");	
		 try {
			JDBC jdbc=new JDBC();
			Integer delete=0;
			delete = jdbc.JDBCDriver("DELETE  FROM "+TABLE+" where "+SOURCE+"='WD' and "+FIELDHY+"='"+DOCID+"'");

			if(delete==1){
				String deleResult=deleRes(jsonObject,ZWBJ_APPID,ZWBJ_STR_DEFAULT_KEY);
				//处理开普新增返回结果
			    JSONObject jsonresult = JSON.parseObject(deleResult);
			    Integer  dataresult = (Integer) jsonresult.get("code"); 
				if(0 == dataresult){
					System.out.println("删除政府网站资源成功！");
				}else{
					System.out.println("政府网站删除资源失败："+jsonresult.get("msg"));
				}
				Util.log("政府网站文档中间表删除成功!","log",0);
				System.out.println("政府网站文档中间表删除成功!");
			}else{
				try {
					//删除开普同步过来得文章   根据文档id唯一性可以确定是同步到开普得 还是开普同步到海云得   只删除中间表中得数据
					Integer	deleteKP = jdbc.JDBCDriver("DELETE  FROM "+TABLE+" where "+SOURCE+"='KP' and "+FIELDHY+"='"+DOCID+"'");
					if(deleteKP==1){
						if(delete==1){
							Util.log("开普同步过来的政府网站文档中间表删除成功!","log",0);
							System.out.println("开普同步过来的政府网站文档中间表删除成功!");
						}else{
							Util.log("开普同步过来的政府网站文档中间表删除失败!","log",0);
							System.out.println("开普同步过来的政府网站文档中间表删除失败!");
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Util.log("政府网站文档中间表删除失败!","log",0);
				System.out.println("政府网站文档中间表删除失败!");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("-----------------------------------政府网站删除文档结束-------------------------------------");	
	
		
	}

	/**
	 * 删除开普资源
	 * @throws Exception
	 */
	public static String  deleRes(JSONObject json,String appid,String key) throws Exception {
		    
			System.out.println("删除json==="+json);
		    String encrypt = DESUtil.encrypt(json.toJSONString(), key);
		    String url =Z_URL+ "/resource/delRes?data=" + encrypt + "&appId="+appid;
		    String post = HttpUtil.doGet(url, "UTF-8");
		    System.out.println(post); 
		    return post;
	 }
}
