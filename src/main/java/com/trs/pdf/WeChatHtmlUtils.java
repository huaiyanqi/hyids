package com.trs.pdf;


/**
 * Created by on 2020/10/30.
 */
public class WeChatHtmlUtils {


    /**
     * 注意事项：

     1.输入的HTML页面必须是标准的XHTML页面。页面的顶上必须是这样的格式：

     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
     <html xmlns="http://www.w3.org/1999/xhtml">
     */
    public static String start_html="<!DOCTYPE html>\n" +
            "<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"  xmlns:th=\"http://www.thymeleaf.org\" xmlns:sec=\"http://www.thymeleaf.org/extras/spring-security\">";
//	  public static String start_html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">";

	  public static String head="\t<head>\n" +
            "\t\t<meta charset=\"utf-8\"></meta>" +
            "\t\t<title></title>\n" +
            "\t\t<style>\n" +
//            "\t\t\telement.style {\n" +
//            "\t\t\tmargin: 0 auto;\n" +
//            "\t\t\t\twidth:50%;\n" +
//            "\t\t\t}\n" +
            
//            "\t\t\t* {\n" +
//            "\t\t\t\tmargin: 0;\n" +
//            "\t\t\t\tpadding: 0;\n" +
//            "\t\t\t}\n" +
//            "\t\t.cla{\n" +
//            "\t\t\tcolor: coral;\n" +
//            "\t\t\t}\n" +
//            "\t\t\thtml,body{\n" +
//            "\t\t\t\tfont-size: 14px;\n" +
//            "\t\t\t\tfont-family:Microsoft Yahei, \"微软雅黑\", Arial;\n" +
//            "\t\t\t\tcolor: #333;\n" +
//            "\t\t\t\tbackground: #FFFFFF;\n" +
//            "\t\t\t}\n" +
//            "\t\t\t.container{\n" +
//            "\t\t\t\twidth: 1200px;\n" +
//            "\t\t\t\tmargin: 0 auto;\n" +
//            "\t\t\t\theight: 100%;\n" +
//            "\t\t\t}\n" +
//            "\t\t\t.title{\n" +
//            "\t\t\t\twidth: 100%;\n" +
//            "\t\t\t\theight: 40px;\n" +
//            "\t\t\t\tline-height: 40px;\n" +
//            "\t\t\t\tfont-size: 20px;\n" +
//            "\t\t\t\tfont-weight: 900;\n" +
//            "\t\t\t\tborder-bottom: 2px solid #ccc;\n" +
//            "\t\t\t\tmargin-bottom: 24px;\n" +
//            "\t\t\t}\n" +
//            "\t\t\t.title span{\n" +
//            "\t\t\t\tdisplay: inline-block;\n" +
//            "\t\t\t\tpadding-right: 20px;\n" +
//            "\t\t\t\tborder-bottom: 2px solid #000;\n" +
//            "\t\t\t}\n" +
//            "\t\t\t.descripe span{\n" +
//            "\t\t\t\ttext-indent: 2em;\n" +
//            "\t\t\t\tdisplay: block;\n" +
//            "\t\t\t\tmargin-bottom: 20px;\n" +
//            "\t\t\t\tline-height: 24px;\n" +
//            "\t\t\t}\n" +
//            "\t\t\t.chartsTitle{\n" +
//            "\t\t\t\tbackground: #8692A8;\n" +
//            "\t\t\t\ttext-align: center;\n" +
//            "\t\t\t\twidth: 100%;\n" +
//            "\t\t\t\theight: 40px;\n" +
//            "\t\t\t\tline-height: 40px;\n" +
//            "\t\t\t\tcolor: #000;\n" +
//            "\t\t\t\tfont-weight: 900;\n" +
//            "\t\t\t\tfont-size: 16px;\n" +
//            "\t\t\t\tmargin-bottom: 20px;\n" +
//            "\t\t\t}\n" +
//            "\t\t\t.chartDes{\n" +
//            "\t\t\t\ttext-align: center;\n" +
//            "\t\t\t\tcolor: #2F9E2C;\n" +
//            "\t\t\t\tfont-weight: 900;\n" +
//            "\t\t\t\tmargin-top: 20px;\n" +
//            "\t\t\t}\n" +
//            "\t\t\t.chartImgBox {\n" +
//            "\t\t\t\twidth: 100%;\n" +
//            "\t\t\t\tdisplay: flex;\n" +
//            "\t\t\t}\n" +
//            "\t\t\t.chartImg:first-child{\n" +
//            "\t\t\t\tmargin-right: 20px;\n" +
//            "\t\t\t}\n" +
//            "\t\t\t.chartImg img{\n" +
//            "\t\t\t\twidth: 100%;\n" +
//            "\t\t\t}\n" +
            "\t\t</style>\n" +
            "\t</head>";

    public static String start_body="\t<body  style = \"font-family: SimSun;\">\n" +
            "\t\t<div align=\"center\"  style =\"margin:0 auto; width:40%;\">";



    public static String end_body="\t\t</div>\n" +
            "\t</body>";

    public static String end_html="</html>";

    /**
     * 获取标题
     * @return
     */
    public static String getTitle(String title){
        StringBuilder stringHtml = new StringBuilder();
        stringHtml.append("<div class=\"title\">");
        stringHtml.append("<span>"+title+"</span>");
        stringHtml.append("</div>");

        return stringHtml.toString();
    }

    /**
     * 获取内容
     * @param content
     * @return
     */
    public static String getContent(String content,String imgPath,String imgDescribe){
        StringBuilder stringHtml = new StringBuilder();
        stringHtml.append("<div class=\"content\">");
        stringHtml.append("<p class=\"descripe\">"+content+"</p>");

        //------------图片----------
        stringHtml.append("<div class=\"charts\">");
        stringHtml.append("<p class=\"chartsTitle\">ALL</p>");
        stringHtml.append("<div class=\"chartImgBox\">" +
                "<div class=\"chartImg\">");

        stringHtml.append("<img src='"+imgPath+"'/>");


        stringHtml.append("</div>" +
                "</div>");

//        stringHtml.append("<p class=\"chartDes\">"+imgDescribe+"</p>");//图片描述
        stringHtml.append("</div>");
        //------------图片end----------


        stringHtml.append("</div>");

        return stringHtml.toString();
    }



}

