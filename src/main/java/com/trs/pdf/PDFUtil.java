package com.trs.pdf;


import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.layout.font.FontProvider;
import com.trs.oauth.ConstantUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created by on 2020/10/31.
 */
public class PDFUtil {
	
	private static String resouse_path = ConstantUtil.resouse_path;
	private static final String FONT_RESOURCE_DIR = "/font";
    public static void html2Pdf(String pdfPath,String htmlPath,String savepdfPath) throws IOException{
        ConverterProperties converterProperties =  new ConverterProperties();
        //指定目录，支持将html相对路径的图片转换到pdf，注意pom文件需要配置将img文件加入到编译后的项目中
        converterProperties.setBaseUri(pdfPath);
        FontProvider fontProvider = new FontProvider();
        String resources = PDFUtil.class.getResource(FONT_RESOURCE_DIR).getPath();
        fontProvider.addDirectory(resources);
        
        converterProperties.setFontProvider(fontProvider);

        //读取的html文件地址
        File htmlFile = new File(htmlPath);
        //生成的pdf地址
        File pdfFile = new File(savepdfPath);
        try {
            //HtmlFile TO PdfFile
            HtmlConverter.convertToPdf(htmlFile,pdfFile,converterProperties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
    	String pdfPath=resouse_path;
    	String htmlPath=resouse_path+"lgn.html";
    	String savepdfPath=resouse_path+"lgn.pdf";
    	
        html2Pdf(pdfPath,htmlPath,savepdfPath);
    	
        //删除一个文件夹下的所有文件(包括子目录内的文件)
//        File file = new File("D:\\lgn.pdf");//输入要删除文件目录的绝对路径
//
//        // 删除文件
//        boolean value = file.delete();
//        if(value) {
//          System.out.println("JavaFile.java已成功删除.");
//        }
//        else {
//          System.out.println("文件不存在");
//        }

    }


}
