package com.trs.pdf;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFilesImpl;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.HTML;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

public class Html2pdf {

	public static void htmlTopdf(String html, File file) throws Exception {
		try {
			// step 1
			Document document = new Document();
			BaseFont bfChinese;
			bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", false);
			MyFontProvider myFontProvider = new MyFontProvider(BaseColor.BLACK, "", "", false, false, 16, 1, bfChinese);
			// step 2
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
			// step 3
			document.open();

			final TagProcessorFactory tagProcessorFactory = Tags.getHtmlTagProcessorFactory();
			tagProcessorFactory.removeProcessor(HTML.Tag.IMG);

			final CssFilesImpl cssFiles = new CssFilesImpl();
			cssFiles.add(XMLWorkerHelper.getInstance().getDefaultCSS());
			final StyleAttrCSSResolver cssResolver = new StyleAttrCSSResolver(cssFiles);
			final HtmlPipelineContext hpc = new HtmlPipelineContext(new CssAppliersImpl(myFontProvider));
			hpc.setAcceptUnknown(true).autoBookmark(true).setTagFactory(tagProcessorFactory);
			final HtmlPipeline htmlPipeline = new HtmlPipeline(hpc, new PdfWriterPipeline(document, writer));
			final Pipeline<?> pipeline = new CssResolverPipeline(cssResolver, htmlPipeline);

			final XMLWorker worker = new XMLWorker(pipeline, true);

			final Charset charset = Charset.forName("UTF-8");
			final XMLParser xmlParser = new XMLParser(true, worker, charset);

			ByteArrayInputStream bais = new ByteArrayInputStream(html.getBytes("UTF-8"));
			xmlParser.parse(bais, charset);

			// step 5
			document.close();
			bais.close();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	public static void main(String[] args) {
		String content = "<html><head><meta content=\"text/html;charset=UTF-8\"></meta></head><body>"
				+ "<p>　　原标题：沿滩区召开2022年党外年轻干部座谈会</p><a href='http:www.baidu.com'>打开百度</a><p>　　	为进一步做好党外年轻干部培养工作，增强党外干部队伍的凝聚力和向心力，4月26日，沿滩区召开2022年党外年轻干部座谈会。沿滩区委常委、统战部部长、区总工会主席王揖辉出席会议并讲话。区委统战部常务副部长毛小华主持会议。</p><p><div align='center'><IMAGE SRC='http://sc.people.com.cn/mediafile/pic/20220428/55/11381169685977438803.jpg' style='max-width:600px;' /></div></p><p>　　	会上，30名来自全区各部门的党外年轻干部代表立足自身岗位实际，结合成长经历畅谈心得体会，围绕加强党外干部队伍建设积极建言献策，充分展示沿滩党外年轻干部朝气蓬勃、奋发进取的精神风貌。</p><p>　　	针对大家提出的意见建议及工作中、生活中遇到的困难，区委组织部常务副部长周秀容一一回应、答疑解惑，她勉励大家要坚定政治立场，注重提升工作本领，知重负重，担当作为，端正心态，严守规矩，以优异成绩接受组织挑选。</p><p>　　	王揖辉对党外年轻干部所提意见建议表示充分的肯定，并对全区党外年轻干部提出了殷切希望：一是要做政治上信得过、靠得住、能放心的好干部。要坚持用党的创新理论武装头脑，加强政治历练，积累政治经验。二是要在实践中经风雨、长见识、增才干。要把理论融入实践，促进学用结合、思悟贯通，在解决工作难题的具体实践中升华理论、增进认识，提升自身的能力与本领。三是要说老实话、办老实事、做老实人。要踏踏实实做事，敢于坚持真理，善于独立思考，坚持求真务实，以真心实意、真情实感为群众谋福利。四要做到心有所畏、言有所戒、行有所止。要筑牢思想防线，守住廉洁底线，自觉摆正心态，始终坚持将纪律规矩挺在前面，干干净净做事、清清白白做人。（王艺睿 川观新闻记者 秦勇）</p><p>　　(责编：罗昱、章华维)</p>"
				+ "</body></html>";
		File file = new File("d:/pdf2.pdf");
		try {
			htmlTopdf(content, file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	

}


