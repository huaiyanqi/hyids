package com.trs.pdf;

public class HtmlUtils
{
  public static String start_html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">";
  
  public static String head = "\t<head>\n\t\t<meta charset=\"utf-8\"></meta>\t\t<title></title>\n\t</head>";
  
  public static String start_body = "\t<body  style = \"font-family: SimSun;\">\n\t\t<div class=\"container\">";
  
  public static String end_body = "\t\t</div>\n\t</body>";
  
  public static String end_html = "</html>";
  
  public static String getTitle(String title)
  {
    StringBuilder stringHtml = new StringBuilder();
    stringHtml.append("<div class=\"title\">");
    stringHtml.append("<span>" + title + "</span>");
    stringHtml.append("</div>");
    
    return stringHtml.toString();
  }
  
  public static String getContent(String content, String imgPath, String imgDescribe)
  {
    StringBuilder stringHtml = new StringBuilder();
    stringHtml.append("<div class=\"content\">");
    stringHtml.append("<p class=\"descripe\">" + content + "</p>");
    
    stringHtml.append("<div class=\"charts\">");
    stringHtml.append("<p class=\"chartsTitle\">ALL</p>");
    stringHtml.append("<div class=\"chartImgBox\"><div class=\"chartImg\">");
    
    stringHtml.append("<img src='" + imgPath + "'/>");
    
    stringHtml.append("</div></div>");
    
    stringHtml.append("</div>");
    
    stringHtml.append("</div>");
    
    return stringHtml.toString();
  }
}
