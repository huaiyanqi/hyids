package com.trs.pdf;

import java.io.File;

/**
 * 官网下载  windows版本    https://wkhtmltopdf.org/
 * 本地安装，指定目录即可
 * @author 18304
 *
 */
public class wkhtmltopdfUtil {

	//wkhtmltopdf在系统中的路径
    private static final String toPdfTool = "D:\\D\\tool\\wkhtmltox\\wkhtmltopdf\\bin\\wkhtmltopdf.exe";

    private static final String toImgTool = "D:\\D\\tool\\wkhtmltox\\wkhtmltopdf\\bin\\wkhtmltoimage.exe";

    /**
     * html转PDF
     *
     * @param htmlPath html路径
     * @param pdfPath  pdf路径
     */

    public static void htmlToPdf(String htmlPath, String pdfPath) {
        File file = new File(pdfPath);
        File parent = file.getParentFile();
        //如果pdf保存路径不存在，则创建路径
        if (!parent.exists()) {
            parent.mkdirs();
        }
        StringBuilder cmd = new StringBuilder();
        if (System.getProperty("os.name").indexOf("Windows") == -1) {
            //非windows 系统
            //toPdfTool = FileUtil.convertSystemFilePath("/home/ubuntu/wkhtmltox/bin/wkhtmltopdf");
        }
        cmd.append(toPdfTool);
        cmd.append(" ");
        cmd.append("-B 0 -L 0 -R 0 -T 0 ");
        //开启本地文件访问
        cmd.append("--enable-local-file-access ");
        //cmd.append(" --header-line");//页眉下面的线
        //cmd.append(" --header-center 这里是页眉这里是页眉这里是页眉这里是页眉 ");//页眉中间内容
        //cmd.append(" --margin-top 3cm ");//设置页面上边距 (default 10mm)
        //cmd.append(" --header-html file:///" + WebUtil.getServletContext().getRealPath("") + FileUtil.convertSystemFilePath("\\style\\pdf\\head.html"));// (添加一个HTML页眉,后面是网址)
        //cmd.append(" --header-spacing 5 ");// (设置页眉和内容的距离,默认0)
        //cmd.append(" --footer-center (设置在中心位置的页脚内容)");//设置在中心位置的页脚内容
        //cmd.append(" --footer-html file:///" + WebUtil.getServletContext().getRealPath("") + FileUtil.convertSystemFilePath("\\style\\pdf\\foter.html"));// (添加一个HTML页脚,后面是网址)
        //cmd.append(" --footer-line");//* 显示一条线在页脚内容上)
        //cmd.append(" --footer-spacing 5 ");// (设置页脚和内容的距离)
        cmd.append(htmlPath);
        cmd.append(" ");
        cmd.append(pdfPath);

        boolean result = true;
        try {
            Process proc = Runtime.getRuntime().exec(cmd.toString());

            proc.waitFor();
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }
    }
    
	//wkhtmltoimage  www.bing.com bing2.png
    //@Override
    public static boolean htmlToImg(String srcPath, String destPath) {
        try {
            File file = new File(destPath);
            File parent = file.getParentFile();
            //如果pdf保存路径不存在，则创建路径
            if (!parent.exists()) {
                parent.mkdirs();
            }
            StringBuilder cmd = new StringBuilder();
            if (System.getProperty("os.name").indexOf("Windows") == -1) {
                //非windows 系统
                //toPdfTool = "/home/ubuntu/wkhtmltox/bin/wkhtmltopdf";
            }
            cmd.append(toImgTool);
            cmd.append(" ");
            //cmd.append("--crop-w 400 --width 1680 --quality 50 ");
            cmd.append(srcPath);
            cmd.append(" ");
            cmd.append(destPath);

            Process proc = Runtime.getRuntime().exec(cmd.toString());
            proc.waitFor();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    
    
    public static void main(String[] args) {
		
    	htmlToPdf("D:\\data\\ZGRT_Resources\\lgn.html", "D:\\data\\ZGRT_Resources\\lgn.pdf");
    	htmlToImg("D:\\data\\ZGRT_Resources\\lgn.html", "D:\\data\\ZGRT_Resources\\lgn.png");
	}

}
