package com.trs.pdf;

import java.io.FileOutputStream;
import java.io.OutputStream;
 
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;
 

import java.io.FileInputStream;
import java.io.FileOutputStream;


/**
 * Java根据html模板创建 html文件
 * Created by on 2020/10/29.
 */
public class CreateHtmlUtils {
    /**
     * @Title: MakeHtml
     * @Description: 创建html
     * @param    filePath 设定模板文件
     * @param    imagePath 需要显示图片的路径
     * @param    disrPath  生成html的存放路径
     * @param    fileName  生成html名字
     * @return void    返回类型
     * @throws
     */
    public static void MakeHtml(String filePath,String imagePath,String disrPath,String fileName ){
        try {
            String title = "<image src="+'"'+imagePath+'"'+"/>";
            System.out.print(filePath);
            String templateContent = "";
            FileInputStream fileinputstream = new FileInputStream(filePath);// 读取模板文件
            int lenght = fileinputstream.available();
            byte bytes[] = new byte[lenght];
            fileinputstream.read(bytes);
            fileinputstream.close();
            templateContent = new String(bytes);
            System.out.println(templateContent);
            templateContent = templateContent.replaceAll("###title###", title);
            System.out.println("---------------开始替换--------------");
            System.out.print(templateContent);

            String fileame = fileName + ".html";
            fileame = disrPath+"/" + fileame;// 生成的html文件保存路径。
            FileOutputStream fileoutputstream = new FileOutputStream(fileame);// 建立文件输出流
            System.out.print("文件输出路径:");
            System.out.print(fileame);
            byte tag_bytes[] = templateContent.getBytes();
            fileoutputstream.write(tag_bytes);
            fileoutputstream.close();
        } catch (Exception e) {
            System.out.print(e.toString());
        }
    }

    /**
     * 拼接动态 生成html
     */
    public static void appendHtml(String htmlFile, String html){

//        String imagePath ="D:\\123.jpg";
    	
        //用于存储html字符串
        StringBuilder stringHtml = new StringBuilder();

        //输入HTML文件内容
        stringHtml.append(HtmlUtils.start_html);
        stringHtml.append(HtmlUtils.head);
        stringHtml.append(HtmlUtils.start_body);
        stringHtml.append(html);
        stringHtml.append(HtmlUtils.end_body);
        stringHtml.append(HtmlUtils.end_html);

        String templateContent=stringHtml.toString();

        System.out.println("---------------templateContent--------------"+templateContent);



        try{
            //将HTML文件内容写入文件中
            FileOutputStream fileoutputstream = new FileOutputStream(htmlFile);// 建立文件输出流

            byte tag_bytes[] = templateContent.getBytes();
            fileoutputstream.write(tag_bytes);
            fileoutputstream.close();


        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void appendHtmlWechat(String htmlFile, String html){

//      String imagePath ="D:\\123.jpg";
  	
      //用于存储html字符串
      StringBuilder stringHtml = new StringBuilder();

      //输入HTML文件内容
      stringHtml.append(WeChatHtmlUtils.start_html);
      stringHtml.append(WeChatHtmlUtils.head);
      stringHtml.append(WeChatHtmlUtils.start_body);
      stringHtml.append(html);
      stringHtml.append(WeChatHtmlUtils.end_body);
      stringHtml.append(WeChatHtmlUtils.end_html);

      String templateContent=stringHtml.toString();

      System.out.println("---------------templateContent--------------"+templateContent);



      try{
          //将HTML文件内容写入文件中
          FileOutputStream fileoutputstream = new FileOutputStream(htmlFile);// 建立文件输出流

          byte tag_bytes[] = templateContent.getBytes();
          fileoutputstream.write(tag_bytes);
          fileoutputstream.close();


      }catch(Exception e){
          e.printStackTrace();
      }
  }


    public static void main(String[] args) {

        String htmlFile = "D:\\lgn.html";
        String imagePath ="D:\\123.jpg";
        String pdfFile = "D:\\lgn.pdf";
        //MakeHtml(filePath,imagePath,disrPath,fileName);

        appendHtml(htmlFile,imagePath);

    }

}

