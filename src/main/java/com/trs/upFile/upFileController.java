package com.trs.upFile;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.ResResource;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIIP;





/**
 * OAuth2.0Controller
 */
@Controller
@RequestMapping(value = "/wbUF")
public class upFileController{
//	private static String Z_APPID = ConstantUtil.Z_APPID;
//	private static String USER = ConstantUtil.USER;
//	private static String TABLE = ConstantUtil.TABLE;
//	private static String FIELDKP = ConstantUtil.FIELDKP;
//	private static String FIELDHY = ConstantUtil.FIELDHY;
//	private static String SOURCE = ConstantUtil.SOURCE;
//	private static String PARENTID = ConstantUtil.PARENTID;
//	private static String Content_regular = ConstantUtil.Content_regular;
//	private static String Content_regular_p = ConstantUtil.Content_regular_p;
//	private static String Content_regular_v = ConstantUtil.Content_regular_v;
//	private static String resouse_path = ConstantUtil.resouse_path;
//	private static String pageSize = ConstantUtil.pageSize;
	
	ResResource  re= new ResResource();
	ChannelReceiver cR=new ChannelReceiver();
	HttpFileUpload hfl=new HttpFileUpload();
	JDBCIIP iip=new JDBCIIP();
	JDBC  jdbc=new JDBC();
	
	
	@ResponseBody
	@RequestMapping(value = {"/wbUpFile"},method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String  wbUpFile(@RequestParam(value ="data") String data) throws Exception {
		
		data = new String (data.getBytes("ISO8859-1"),"utf-8");
		System.out.println("------------------外部上传附件开始---------------");
		System.out.println("附件上傳接收到的參數："+data);
		JSONObject jsondata = JSON.parseObject(data);
		String FJURL=jsondata.getString("FJURL");//判断推送视图
		String FJNAME=jsondata.getString("FJNAME");//判断推送视图
		FJURL = new String (FJURL.getBytes("ISO8859-1"),"utf-8");
		FJNAME = new String (FJNAME.getBytes("ISO8859-1"),"utf-8");
		
		Dispatch oDispatch = WCMServiceCaller.UploadFile(FJURL+FJNAME);
		String fileName = oDispatch.getUploadShowName();
		System.out.println("上传附件之后的附件名称"+fileName);
		System.out.println("------------------外部上传附件开始---------------");
    	return fileName;
		
		
	}
	
	@ResponseBody
	@RequestMapping(value = {"/wbUpFileGet"},method = RequestMethod.GET,produces = "application/json; charset=utf-8")
	public String  wbUpFileGet(@RequestParam(value ="data") String data) throws Exception {
		
		data = new String (data.getBytes("ISO8859-1"),"utf-8");
		System.out.println("------------------外部上传附件开始---------------");
		JSONObject jsondata = JSON.parseObject(data);
		String FJURL=jsondata.getString("FJURL");//判断推送视图
		String FJNAME=jsondata.getString("FJNAME");//判断推送视图
		FJURL = new String (FJURL.getBytes("ISO8859-1"),"utf-8");
		FJNAME = new String (FJNAME.getBytes("ISO8859-1"),"utf-8");
    	String PKPpath=null;
    	String urlReturn=null;
    	try {
			PKPpath=re.uploadFile(FJURL, FJNAME);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		//处理返回结果
		JSONObject jsonObject = JSON.parseObject(PKPpath);
		Object dataArr = jsonObject.get("data");
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		String code=jsonObject.getString("code");
		if("0".equals(code)){
			urlReturn = jsonObjectHYGPSE.getString("fileURL");
		}else{
			System.out.println("附件上传失败："+jsonObject.get("MSG"));	
		}
		System.out.println("------------------外部上传附件开始---------------");
    	return urlReturn;
		
		
	}
}