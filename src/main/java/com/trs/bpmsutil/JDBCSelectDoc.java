package com.trs.bpmsutil;

import java.io.IOException;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mysql.jdbc.Connection;
import com.trs.hy.HttpFileUpload;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;
import com.trs.pdf.CreateHtmlUtils;
import com.trs.pdf.PDFUtil;

import gui.ava.html.parser.HtmlParser;
import gui.ava.html.parser.HtmlParserImpl;
import gui.ava.html.renderer.ImageRenderer;
import gui.ava.html.renderer.ImageRendererImpl;


public class JDBCSelectDoc {
	
    public static final String  jdbc_driver=  ConstantUtil.jdbc_driver;
    

    public static final String  jdbc_url= ConstantUtil.jdbc_urlwcm;
    public static final String  jdbc_username=  ConstantUtil.jdbc_usernamewcm;
    public static final String  jdbc_password= ConstantUtil.jdbc_passwordwcm;

    
    public static final String  bpms_file_url= ConstantUtil.bpms_file_url;
	private static String HR_URL = ConstantUtil.HR_URL;
	private static String BPMS_URL = ConstantUtil.BPMS_URL;
	private static String BPMS_VIDEO = ConstantUtil.BPMS_VIDEO;
	private static String bpms_fromCode_gr = ConstantUtil.bpms_fromCode_gr;
	private static String bpms_fromCode_wz = ConstantUtil.bpms_fromCode_wz;
	private static String bpms_fromCode_wx = ConstantUtil.bpms_fromCode_wx;
	private static String bpms_fromCode_zm = ConstantUtil.bpms_fromCode_zm;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	
//	Integer  uppsd=jdbc.JDBCDriver("update kpuser set password='"+psssword+"' where username='"+USERNAME+"'");	
	
//	String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+viewid+"'";
//	String viewName=jdbcids.JDBCVIEWIDSELECT(sql);   webhdjlzwxx
	
	public static void main(String[] args) throws SQLException, Exception {
		String zw1="<body><p style=\"text-align: center;\"><img class=\"trsresize\" type=\"personal\" meiziid=\"862\" relationid=\"865\" src=\"W020220816567284524215.jpg\" videoid=\"71\" resourcetype=\"video\" style=\"max-width: 550px; cursor: pointer;\" OLDSRC=\"W020220816567284524215.jpg\" /> </p><p><br></p></body>";
		String zw=contentResourceVideo(zw1,0,Content_regular_v,"123");
		
	}
	

	/**
	 * 处理微信正文
	 * @param docid
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, String>  selectWeChatDoc(String docid) throws Exception{
		
		String selectsql="  SELECT wx.*,doc.* from wcmmetatablewechat wx ,wcmdocument doc ,wcmchnldoc chnl where chnl.RECID='"+docid+"' and wx.MetaDataId =doc.docid  AND doc.docid=chnl.docid ;";
//		String selectsql=" SELECT * from wcmmetatablewechat,wcmdocument where metadataid ='"+docid+"'  AND docid='"+docid+"' ";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        HashMap<String , String> res=new HashMap<>();
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  title = rs.getString("title");//标题
        	String  keywords = rs.getString("keywords");//关键词
        	String  newabstract = rs.getString("abstract");//摘要
        	String  newssources = rs.getString("docsourcename");//来源
        	String  signatureauthor = rs.getString("author");//作者
        	String  remarks = rs.getString("remarks");//备注
        	String  originaladdress = rs.getString("originaladdress");//原文链接
        	String  crdept = rs.getString("crdept");//组织机构
        	String  flowversiontime = rs.getString("flowversiontime");//最后编辑时间
        	String  content = rs.getString("content");//b不带html标签正文
//        	String  editortruename = rs.getString("editortruename");//责任编辑
        	
        	String  htmlcontent = rs.getString("htmlcontent");//html正文
        	String  doctype = rs.getString("doctype");//html正文

        	//格式化时间
			//首先设置时间格式没得说吧
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//然后把获取的Timestamp用.getTime()方法获取long类型转化为java的Date
			//这个time是我读取数据库的Timestamp的时间变量
			Date date = sdf.parse(flowversiontime);
			//然后将date用.format()方法变成字符串就ok了
			String java_date=sdf.format(date);

        	
        	res.put("来源", newssources);
        	res.put("最后版本时间",java_date);
        	res.put("作者", signatureauthor);
        	res.put("正文", htmlcontent);
        	res.put("关键词", keywords);
        	res.put("状态", "BPMS审核");
        	res.put("原文链接", originaladdress);
        	res.put("备注", remarks);
        	res.put("摘要", newabstract);
        	res.put("作者信息-单位", crdept.substring(0,crdept.length()-1));
        	res.put("作者信息-部门",crdept.substring(0,crdept.length()-1));
        	res.put("作者信息-姓名", signatureauthor);
        	res.put("文章标题", title);	
        	res.put("稿件类型", doctype);

        	System.out.println(res);
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			
        }
    	System.out.println("-----------------------------一正文--------------------------------"+res);

		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
		return res;
	
	}
	
	
	/**
	 * 获取正文处理微信正文
	 * @param docid
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, String>  selectWeChatDocHtml(String docid) throws Exception{
		
		String selectsql="  SELECT wx.*,doc.* from wcmmetatablewechat wx ,wcmdocument doc ,wcmchnldoc chnl where chnl.RECID='"+docid+"' and wx.MetaDataId =doc.docid  AND doc.docid=chnl.docid ;";
//		String selectsql=" SELECT * from wcmmetatablewechat,wcmdocument where metadataid ='"+docid+"'  AND docid='"+docid+"' ";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        HashMap<String , String> res=new HashMap<>();
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	
        	String  htmlcontent = rs.getString("htmlcontent");//html正文

        	res.put("正文", htmlcontent);

        	System.out.println(res);
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			
        }
    	System.out.println("-----------------------------一正文--------------------------------"+res);

		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
		return res;
	
	}
	
	/**
	 * 处理纸媒正文
	 * @param docid
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, String>  selectPaperDoc(String docid) throws Exception{
		
//		String selectsql=" SELECT * from wcmmetatablereleasesource where metadataid ='21' ";
		String selectsql="select a.title,a.docsource,a.flowversiontime,a.author,a.remarks,a.htmlcontent,b.doctype,a.crdept,c.docgenre,d.chnldesc "
				+ " from wcmchnldoc e  "
				+ " LEFT JOIN  wcmdocument b  ON b.docid=e.DOCID "
				+ " LEFT JOIN  xwcmfgd c  ON c.metadataid=e.DOCID "
				+ " LEFT JOIN  wcmchannel d  ON d.ChannelId=e.CHNLID "
				+ " LEFT JOIN  wcmmetatablepaper a ON a.MetaDataId = e.DOCID "
				+ " where  e.RECID ='"+docid+"'";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        HashMap<String , String> res=new HashMap<>();
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  title = rs.getString("title");//标题
        	String  newssources = rs.getString("docsource");//来源
        	String  flowversiontime = rs.getString("flowversiontime");//最后编辑时间
        	String  signatureauthor = rs.getString("author");//作者
        	String  remarks = rs.getString("remarks");//备注
        	String  crdept = rs.getString("crdept");//组织机构
  	        String  htmlcontent = rs.getString("htmlcontent");//html正文
        	String  doctype = rs.getString("doctype");//html正文
        	String  docgenre = rs.getString("docgenre");//体裁
        	String  chnldesc = rs.getString("chnldesc");//版面

        	//格式化时间
			//首先设置时间格式没得说吧
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//然后把获取的Timestamp用.getTime()方法获取long类型转化为java的Date
			//这个time是我读取数据库的Timestamp的时间变量
			Date date = sdf.parse(flowversiontime);
			//然后将date用.format()方法变成字符串就ok了
			String java_date=sdf.format(date);
			//处理载体
			JSONObject jsonObjectHYSE = JSON.parseObject(docgenre);
			String docgenrename = jsonObjectHYSE.get("name").toString();//获取到token
			
        	res.put("来源", newssources);
        	res.put("最后版本时间",java_date);
        	res.put("作者", signatureauthor);
        	res.put("正文", htmlcontent);
        	res.put("状态", "今日稿");
        	res.put("备注", remarks);
        	res.put("版面", chnldesc);
        	res.put("体裁", docgenrename);
        	res.put("作者信息-单位", crdept.substring(0,crdept.length()-1));
//        	res.put("作者信息-单位","中国融通医疗健康集团有限公司（筹）");
        	res.put("作者信息-部门",crdept.substring(0,crdept.length()-1));
        	res.put("作者信息-姓名", signatureauthor);
        	res.put("文章标题", title);	
        	res.put("稿件类型", doctype);

        	System.out.println(res);
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			
        }
        
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
		return res;
	
	}
	
	
	
	/**
	 * 处理个人稿库正文  当作子公司审核
	 * @param docid
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, String>  selectGrDoc(String docid) throws Exception{
		
		System.out.println("--------------------------------------子公司同步bpms处理基础字段开始--------------------------------------------------");
//		String selectsql=" SELECT * from wcmmetatablereleasesource where metadataid ='21' ";
		String selectsql="  SELECT gr.*,doc.* from wcmmetatablereleasesource gr ,wcmdocument doc ,wcmchnldoc chnl where chnl.RECID='"+docid+"' and gr.MetaDataId =doc.docid  AND doc.docid=chnl.docid ";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        HashMap<String , String> res=new HashMap<>();
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  title = rs.getString("title");//标题
        	String  keywords = rs.getString("keywords");//关键词
        	String  newabstract = rs.getString("abstract");//摘要
        	String  newssources = rs.getString("newssources");//来源
        	String  signatureauthor = rs.getString("signatureauthor");//作者
        	String  remarks = rs.getString("remarks");//备注
        	String  crdept = rs.getString("crdept");//组织机构
        	String  flowversiontime = rs.getString("flowversiontime");//最后编辑时间
    
        	
        	String  htmlcontent = rs.getString("htmlcontent");//html正文
        	String  doctype = rs.getString("doctype");//html正文

        	//格式化时间
			//首先设置时间格式没得说吧
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//然后把获取的Timestamp用.getTime()方法获取long类型转化为java的Date
			//这个time是我读取数据库的Timestamp的时间变量
			Date date = sdf.parse(flowversiontime);
			//然后将date用.format()方法变成字符串就ok了
			String java_date=sdf.format(date);

        	
        	res.put("来源", newssources);
        	res.put("关键词", keywords);
        	res.put("作者", signatureauthor);
        	res.put("状态", "子公司提交审核");
        	res.put("备注", remarks);
        	res.put("摘要", newabstract);
        	res.put("最后版本时间",java_date);
        	res.put("作者信息-单位", crdept.substring(0,crdept.length()-1));
        	res.put("作者信息-部门",crdept.substring(0,crdept.length()-1));
        	res.put("作者信息-姓名", signatureauthor);
        	res.put("文章标题", title);
        	res.put("正文", htmlcontent);
        	res.put("稿件类型", doctype);

        	System.out.println(res);
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			
        }
		System.out.println("子公司基础字段："+res);

		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
		System.out.println("--------------------------------------子公司同步bpms处理基础字段结束--------------------------------------------------");
		return res;
	
	}
	
	/**
	 * 处理接团正文  网站中稿件
	 * @param docid
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, String>  selectWebSiteDoc(String docid) throws Exception{
		
		System.out.println("--------------------------------------处理基础字段开始--------------------------------------------------");
//		String selectsql=" SELECT * from wcmmetatablereleasesource where metadataid ='21' ";
		String selectsql="  SELECT wz.*,doc.* from wcmmetatablewebsite wz ,wcmdocument doc ,wcmchnldoc chnl where chnl.RECID='"+docid+"' and wz.MetaDataId =doc.docid  AND doc.docid=chnl.docid  ";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        HashMap<String , String> res=new HashMap<>();
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  title = rs.getString("title");//标题
        	String  keywords = rs.getString("keywords");//关键词
        	String  newabstract = rs.getString("abstract");//摘要
        	String  newssources = rs.getString("docsourcename");//来源
        	String  signatureauthor = rs.getString("author");//作者
        	String  remarks = rs.getString("remarks");//备注
        	String  crdept = rs.getString("crdept");//组织机构
        	String  flowversiontime = rs.getString("flowversiontime");//最后编辑时间
        	String  editortruename = rs.getString("editortruename");//责任编辑
        	
        	String  htmlcontent = rs.getString("htmlcontent");//html正文
        	String  doctype = rs.getString("doctype");//html正文

        	//格式化时间
			//首先设置时间格式没得说吧
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//然后把获取的Timestamp用.getTime()方法获取long类型转化为java的Date
			//这个time是我读取数据库的Timestamp的时间变量
			Date date = sdf.parse(flowversiontime);
			//然后将date用.format()方法变成字符串就ok了
			String java_date=sdf.format(date);

        	
        	res.put("来源", newssources);
        	res.put("关键词", keywords);
        	res.put("作者", signatureauthor);
        	res.put("状态", "BPMS审核");
        	res.put("责任编辑", editortruename);
        	res.put("备注", remarks);
        	res.put("摘要", newabstract);
        	res.put("最后版本时间",java_date);
        	res.put("作者信息-单位", crdept.substring(0,crdept.length()-1));
        	res.put("作者信息-部门",crdept.substring(0,crdept.length()-1));
        	res.put("作者信息-姓名", signatureauthor);
        	res.put("文章标题", title);
        	res.put("正文", htmlcontent);
        	res.put("稿件类型", doctype);

        	System.out.println(res);
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			
        }
        
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
		System.out.println("--------------------------------------处理基础字段结束--------------------------------------------------");
		return res;
	
	}
	
	public static String  getUUID(){
		
//		String str = UUID.randomUUID().toString().replaceAll("-", "");

        int first = new Random(10).nextInt(8) + 1;
//        System.out.println(first);
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if (hashCodeV < 0) {//有可能是负数
            hashCodeV = -hashCodeV;
        }
        // 0 代表前面补充0
        // 4 代表长度为4
        // d 代表参数为正数型
        return first + String.format("%015d", hashCodeV);

	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap<String, String>  updateFile(HashMap<String,String> bpmsmap,String docid,String token,String qudao) throws Exception{
		
		System.out.println("--------------------------------------开始处理附件--------------------------------------------------");
		//先处理视频
        String zw=bpmsmap.get("正文");
    
    	zw=contentResourceVideo(zw,0,Content_regular_v,token);
		
		String doctype=bpmsmap.get("稿件类型");
		
		if("7".equals(doctype)){//处理视频
			bpmsmap=getVideo(docid,bpmsmap);
		}
		
		String selectsql=" SELECT ax.* from wcmappendix ax ,wcmdocument doc ,wcmchnldoc chnl where chnl.RECID='"+docid+"' and ax.appdocid =doc.docid  AND doc.docid=chnl.docid  ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
       
        int pic=0;
        int fil=0;
        StringBuffer picfileurl=new StringBuffer();
        StringBuffer filfileurl=new StringBuffer();
        String wxf1=null;
        String wxf2=null;

        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  file = rs.getString("appfile");//附件
        	String  fileext = rs.getString("fileext");//附件后缀
        	String  appflag = rs.getString("appflag");//0  正文中图片  130 是附件 稿件背景得附件      20  微信封面    10 是附件字段得附件 
        	String  attribute = rs.getString("attribute");//TopPic=1  首页封面图
        	SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        	Date date = new Date(System.currentTimeMillis());
        	System.out.println(formatter.format(date));
			

        	
        	//再处理附件和正文中图片
			if("gif".equals(fileext)||"jpg".equals(fileext)||"jpeg".equals(fileext)||"png".equals(fileext)){//代表图片 gif,jpg,jpeg,png
				
				if("0".equals(appflag)){//正文中的图片		
					//下载正文中的图片  
					if(file.contains("W0")){
						
						String fid=FileUploadUtil.DownLoadUpFile(file,token);
						//替换到指定地址
						zw=zw.replace(file,BPMS_URL+fid+"&createDate="+formatter.format(date)+"");
					}else{
						
						String fid=FileUploadUtil.DownLoadUpFile(file,token);
						//替换到指定地址
						zw=zw.replace(file,BPMS_URL+fid+"&createDate="+formatter.format(date)+"");
					}
					
				}else if("20".equals(appflag)){ //处理微信封面图
					
					if("TopPic=1".equals(attribute)){
						//上传图片附件
						wxf1=FileUploadUtil.DownLoadUpFile(file,token);

					}else{
						//上传图片附件
						wxf2=FileUploadUtil.DownLoadUpFile(file,token);
					}
					
				}else{
					//上传图片附件
					String picurl=FileUploadUtil.DownLoadUpFile(file,token);
					if(pic==0){
						picfileurl.append(picurl);
					}else{
						picfileurl.append(","+picurl);
					}
					pic++;
				}

			}else{
				//上传其他附件
				String filurl=FileUploadUtil.DownLoadUpFile(file,token);
				if(fil==0){
					filfileurl.append(filurl);
				}else{
					filfileurl.append(","+filurl);
				}
				fil++;
			}
			
        }
        if(pic>0){//存在图片
        	bpmsmap.put("imgKey", "field0008");
        	bpmsmap.put("imgValue", picfileurl.toString());
        }else{
        	bpmsmap.put("imgKey", "field0008");
        	bpmsmap.put("imgValue", "");
        }
        if(fil>0){//存在附件
        	bpmsmap.put("fileKey", "field0018");
        	bpmsmap.put("fileValue", filfileurl.toString());
        }else{
        	bpmsmap.put("fileKey", "field0018");
        	bpmsmap.put("fileValue", "");
        }
        
        bpmsmap.put("fieldKey","field0026");
        bpmsmap.put("fieldValue",docid);
//        bpmsmap.put("contentKey","field0028");//测试环境
        bpmsmap.put("contentKey","field0027");//正式环境
        bpmsmap.put("contentValue",zw);
        
        bpmsmap.put("fmtKey", "field0023");
    	bpmsmap.put("fmtValue", wxf1);//封面1
    	bpmsmap.put("fmt1Key", "field0024");
    	bpmsmap.put("fmt1Value", wxf2);//封面2
        bpmsmap.put("formName", bpms_fromCode_wx);
        
        
        bpmsmap.remove("正文");
		System.out.println("结果：："+bpmsmap);

		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
		System.out.println("--------------------------------------处理附件结束--------------------------------------------------");

		return bpmsmap;
	
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap<String, String>  selectFile(HashMap<String,String> bpmsmap,String docid,String token,String qudao) throws Exception{
		
		System.out.println("--------------------------------------开始处理附件--------------------------------------------------");
		//先处理视频
        String zw=bpmsmap.get("正文");
    
    	zw=contentResourceVideo(zw,0,Content_regular_v,token);
		
		String doctype=bpmsmap.get("稿件类型");
		
		if("7".equals(doctype)){//处理视频
			bpmsmap=getVideo(docid,bpmsmap);
		}
		
//		String selectsql=" SELECT * from wcmappendix where appdocid ='21' ";
		String selectsql=" SELECT ax.* from wcmappendix ax ,wcmdocument doc ,wcmchnldoc chnl where chnl.RECID='"+docid+"' and ax.appdocid =doc.docid  AND doc.docid=chnl.docid  ";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        HashMap res=new HashMap<>();
        List reslist=new ArrayList<>();
        
        //生成随机数
		Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
        String sig=String.valueOf(month)+String.valueOf(day);
        
        String picUUID=sig+getUUID();//图片统一uuid
        String filUUID=sig+getUUID();//附件统一uuid
        String zwUUID=sig+getUUID();//正文统一uuid
        String wxf1=sig+getUUID();//微信封面1
        String wxf2=sig+getUUID();//微信封面2
        int pic=0;
        int fil=0;
        String fileurl=null;

        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  file = rs.getString("appfile");//附件
        	String  fileext = rs.getString("fileext");//附件后缀
        	String  appflag = rs.getString("appflag");//0  正文中图片  130 是附件 稿件背景得附件      20  微信封面    10 是附件字段得附件 
        	String  attribute = rs.getString("attribute");//TopPic=1  首页封面图
        	
        	SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        	Date date = new Date(System.currentTimeMillis());
        	System.out.println(formatter.format(date));
			

        	
        	//再处理附件和正文中图片
			if("gif".equals(fileext)||"jpg".equals(fileext)||"jpeg".equals(fileext)||"png".equals(fileext)){//代表图片 gif,jpg,jpeg,png
				
				if("0".equals(appflag)){//正文中的图片		
					//下载正文中的图片  
					if(file.contains("W0")){
						
						String url=HR_URL+"/webpic/"+file.substring(0,8)+"/"+file.substring(0,10)+"/"+file;
//						HttpFileUpload.downLoadFromUrl(url,file,resouse_path);
						
						String fid=FileUploadUtil.DownLoadUpFile(file,token);

						//替换到指定地址
//						zw=zw.replace(url,BPMS_URL+fid);
						zw=zw.replace(file,BPMS_URL+fid+"&createDate="+formatter.format(date)+"");
						
					}else{
						
						String url=HR_URL+"/protect/"+file.substring(0,8)+"/"+file.substring(0,10)+"/"+file;
						String fid=FileUploadUtil.DownLoadUpFile(file,token);
						//替换到指定地址
//						zw=zw.replace(url,BPMS_URL+fid);
						zw=zw.replace(file,BPMS_URL+fid+"&createDate="+formatter.format(date)+"");
//						
					}
					
				}else if("20".equals(appflag)){ //处理微信封面图
					
					if("TopPic=1".equals(attribute)){
						//上传图片附件
						fileurl=FileUploadUtil.DownLoadUpFile(file,token);
						HashMap<String , String> respic=new HashMap<>();
						respic.put("subReference", wxf1);
						respic.put("fileUrl", fileurl);
						respic.put("sort", "0");
						reslist.add(respic);
					}else{
						//上传图片附件
						fileurl=FileUploadUtil.DownLoadUpFile(file,token);
						HashMap<String , String> respic=new HashMap<>();
						respic.put("subReference", wxf2);
						respic.put("fileUrl", fileurl);
						respic.put("sort", "0");
						reslist.add(respic);
					}
					
				}else {
					//上传图片附件
					fileurl=FileUploadUtil.DownLoadUpFile(file,token);
					HashMap<String , String> respic=new HashMap<>();
					respic.put("subReference", picUUID);
					respic.put("fileUrl", fileurl);
					respic.put("sort", "0");
					reslist.add(respic);

					pic++;
				}

			}else{
				//上传其他附件
				fileurl=FileUploadUtil.DownLoadUpFile(file,token);
				HashMap<String , String> resfujian=new HashMap<>();
				resfujian.put("subReference", filUUID);
				resfujian.put("fileUrl", fileurl);
				resfujian.put("sort", "0");
				reslist.add(resfujian);
				fil++;
			}
			
        }
        if(pic>0){//存在图片
        	bpmsmap.put("上传图片", picUUID);
        }
        if(fil>0){//存在图片
        	bpmsmap.put("附件", filUUID);
        }
        if("wx".equals(qudao)){
        	bpmsmap.put("封面图片信息1", wxf1);
        	bpmsmap.put("封面图片信息2", wxf2);
        } 
        
//        if(!"".equals(zw)&&zw!=null){//存在正文 则进行处理
//            //正文当作pdf进行传送
//            //生成html
//            String htmlFile = resouse_path+"lgn.html";
//            if("wx".equals(qudao)){
//            	CreateHtmlUtils.appendHtmlWechat(htmlFile,zw);
//            }else{
//            	CreateHtmlUtils.appendHtml(htmlFile,zw);
//            }
//            
//            
//            
//            //生成pdf
//        	String pdfPath=resouse_path;
//        	String htmlPath=resouse_path+"lgn.html";
//        	String savepdfPath=resouse_path+"lgn.pdf";
//        	
//        	PDFUtil.html2Pdf(pdfPath,htmlPath,savepdfPath);
//            
//            
//            //上传给bpms
//			//下载后进行上传
//			MultipartFile createMfileByPath=FileUploadUtil.createMfileByPath(savepdfPath);
//			Map<String, String> paramMap =new HashMap<String, String>();
//			String result=FileUploadUtil.doPostFormData(token,bpms_file_url,"file",createMfileByPath,paramMap);
//			System.out.println("正文作为pdf上传给bpms返回结果："+result);
//			JSONObject jsonObject = JSON.parseObject(result.toString());
//			String fileurlzw=null;
//			JSONArray Arr = jsonObject.getJSONArray("atts");//根据json对象中数组的名字解析出其所对应的值
//			for (int i = 0; i < Arr.size(); i++) {
//				JSONObject dataBean = (JSONObject) Arr.get(i);//得到数组中对应下标对应的json对象
//				fileurlzw = dataBean.getString("fileUrl");//根据json对象中的数据名解析出相应数据
//				System.out.println("正文作为附件id："+fileurlzw);//打印输出
//			}
//			
//			HashMap<String , String> reszw=new HashMap<>();
//			reszw.put("subReference", zwUUID);
//			reszw.put("fileUrl", fileurlzw);
//			reszw.put("sort", "0");
//			reslist.add(reszw);
//            //传正文
//			bpmsmap.put("正文", zwUUID);
//        }
//        String zw1="[CDATA[";
//        String zw2="]]";
        
        //处理正文中的视频
       

        bpmsmap.put("富文本1",zw);
        bpmsmap.put("正文", zw);
        
        res.put("thirdAttachments", reslist);
        
        if("gr".equals(qudao)){
        	res.put(bpms_fromCode_gr, bpmsmap);
        } if("wz".equals(qudao)){
        	res.put(bpms_fromCode_wz, bpmsmap);
        } if("wx".equals(qudao)){
        	res.put(bpms_fromCode_wx, bpmsmap);
        } if("zm".equals(qudao)){
        	res.put(bpms_fromCode_zm, bpmsmap);
        }
        
        
        
		System.out.println("结果：："+res);

		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
		System.out.println("--------------------------------------处理附件结束--------------------------------------------------");

		return res;
	
	}
	
	/**
	 * 处理正文中的视频  regVideo = "(<iframe).*?(/iframe)"
	 * @param content
	 * @param sub
	 * @param regular
	 * @param videoName   临时视频名称
	 * @return
	 * @throws IOException 
	 */
    @SuppressWarnings({ "static-access", "unused" })
	public static String contentResourceVideo(String content,int sub,String regular,String token) throws IOException {
    	
    	
    	String regVideoSrc = "(videoid=\").*?(res)"; //获取视频id
    	String regVideo = regular;//正文中视频
		List<String> list=getContentResource(regVideo, sub, content);
		for(int i = 0 ; i < list.size() ; i++) {
			System.out.println(list.get(i));
			
			//拼接海云资源路径
			String iframContent=list.get(i);
			List<String> listurl=getContentResource(regVideoSrc,9, iframContent);
			if(listurl.size()!= 0){
				String videoid="";
				for(int k = 0 ; k < listurl.size() ; k++) {
					videoid=listurl.get(k).replace("\" re", "");
				}
				//查询视频
				HashMap<String, String> videoMap = null;
				try {
					videoMap = JDBCmas.selectMasVideo(videoid);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String filename=videoMap.get("FILENAME");
				String videname=videoMap.get("NAME");
				String subpath=videoMap.get("SUBPATH");
				//拼接海融视频地址
				String urlPtah=HR_URL+"masvod/public/"+subpath+filename;
				
				//上传之前先下载
				HttpFileUpload.downLoadFromUrl(urlPtah,videname,resouse_path);
				System.out.println("视频名称："+videname);
				//调用上传接口上传
				String fid=null;//返回的附件id
				try {
					fid=FileUploadUtil.uploadBpms(filename,token);
					
					if(!fid.equals("")||fid != null){
						String urlbpms=BPMS_VIDEO+fid;
						//替换正文中的海云路径
						String bpmsvideourl="<video src=\""+urlbpms+"\" controls=\"controls\" style=\"width:550px\" autoplay=\"autoplay\"></video";
						content=content.replace(list.get(i), bpmsvideourl);
	
						System.out.println("旧路径："+list.get(i)+"------新路径："+bpmsvideourl);
					}else{
						System.out.println("正文视频上传bpms失败!!!");	
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				System.out.println("正文中不存在视频："+iframContent);
			}
			
		}
    	return content;
    }
	
	/**
	 * 获取正文内资源
	 * List<String> list = new ArrayList<String>();
	 */
	public static List<String>  getContentResource(String regExImg,int subStart,String content){
		List<String> list = new ArrayList<String>();
		Pattern patternImg = Pattern.compile(regExImg);
		Matcher matcherImg = patternImg.matcher(content);
		while (matcherImg.find()) {
			String picPath = matcherImg.group();
			if(picPath.contains("videoid=")){
				picPath = picPath.substring(subStart, picPath.length()-1);
//				String fullPath = getFullPath(picPath);
				System.out.println("匹配到的路径："+picPath);
				list.add(picPath);
			}

		}
		List<String> remove=removeDuplicate(list);
		return remove;
	}
	
	/**
	 * 去除重复数据
	 */
	public static List<String> removeDuplicate(List<String> list) {   
	    HashSet<String> h = new HashSet<String>(list);   
	    list.clear();   
	    list.addAll(h);   
	    return list;   
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap<String, String>  getVideo(String docid,HashMap<String,String> bpmsmap) throws Exception{
		
	
//		String selectsql=" SELECT * from wcmappendix where appdocid ='21' ";
		String selectsql=" SELECT * from xwcmappendixvideo where metadataid ='"+docid+"' ";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  videoUrl = rs.getString("URL");//附件

        	bpmsmap.put("视频地址", videoUrl);
		
			
        }
     
      
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
		return bpmsmap;
	
	}
	
	/**
	 * 查询站点
	 * ZhuBanSet 报纸设置内容
	 * ispdf 是否是pdf
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String  getWebsite(String websiteid,String ispdf) throws Exception{
		
		
		String selectsql=" SELECT ZhuBanSet from WCMWEBSITE where siteid ='"+websiteid+"' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        String path=null;
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  ZhuBanSet = rs.getString("ZhuBanSet");//附件
        	JSONObject oPaperZhunBanSet= JSONObject.parseObject(ZhuBanSet);
        	if("false".equals(ispdf)){//图片
        		path=oPaperZhunBanSet.getString("DAYANGMONITORPATH");
        	}else{//pdf
        		path=oPaperZhunBanSet.getString("FITFILEPATH");
        	}
		
			
        }
        String [] pt=path.split("paper");
        String repath=pt[1];
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
		return "paper"+repath;
	
	}

	
	/**
	 * 查询站点
	 * ZhuBanSet 报纸设置内容
	 * ispdf 是否是pdf
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String  getComposeInfo(String composeId) throws Exception{
		
		
		String selectsql=" SELECT DyFilePath from xwcmcomposepageinfo where COMPOSEPAGEINFOID ='"+composeId+"' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        String DyFilePath=null;
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	DyFilePath = rs.getString("DyFilePath");
		
			
        }

		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
		return DyFilePath;
	
	}

	
	/**
	 * 执行sql
	 * String upsqldoc="update wcmmetatablewebldjl  set  fbt = '"+jieshu+"'  WHERE  MetaDataId= '"+id+"'";
	 * System.out.println(upsqldoc);
	 * JDBCDriver(upsqldoc);
	 * @param sql
	 * @return 
	 * @throws SQLException
	 * @throws Exception
	 */
    public static  int   updataOpinion(String sql) throws SQLException,Exception{
    	
//    	String sql="update xwcmmetadatalog  set  OPINION = '"+opinion+"'  WHERE  MetaDataId= '"+docid+"'";
    	System.out.println("插入bpms返回结果日志："+sql);
        java.sql.Connection conn = null;
        Statement stat = null;
        int res=0;
        try {
        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);

        // 执行sql
        stat = conn.createStatement();  
        res = stat.executeUpdate(sql);
        //执行完毕  关闭连接
        stat.close();
        conn.close();
        } catch (SQLException e) {
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
    
        return res;
    }
   
    
	/**
	 * 执行sql
	 * String upsqldoc="update wcmmetatablewebldjl  set  fbt = '"+jieshu+"'  WHERE  MetaDataId= '"+id+"'";
	 * System.out.println(upsqldoc);
	 * JDBCDriver(upsqldoc);
	 * @param sql
	 * @return 
	 * @throws SQLException
	 * @throws Exception
	 */
    public static  int   updataSql(String sql) throws SQLException,Exception{
    	
    	
    	
        java.sql.Connection conn = null;
        Statement stat = null;
        int res=0;
        try {
        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);

        // 执行sql
        stat = conn.createStatement();  
        res = stat.executeUpdate(sql);
        //执行完毕  关闭连接
        stat.close();
        conn.close();
        } catch (SQLException e) {
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
    
        return res;
    }
    
    /**
     * 查询日志表最大得日志id
     * @param docid
     * @param bpmsmap
     * @return
     * @throws Exception
     */
	public static String  getMetaDataLogId() throws Exception{
		
		
		String selectsql=" SELECT max(METADATALOGID) METADATALOGID from xwcmmetadatalog";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        String  METADATALOGID ="";
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	METADATALOGID = rs.getString("METADATALOGID");//附件

			
        }
     
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        //转换成int  进行加1 后转string返回
		int mid=Integer.parseInt(METADATALOGID)+1;
		
		
		return String.valueOf(mid);
	
	}
    
    /**
     * 根据id查询稿件得最新日志
     * @param docid
     * @return
     * @throws Exception
     */
	public static String  getMetaDataLogSql(String selectsql,String opinion) throws Exception{
		
		
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        String sql="";
        while(rs.next()){  // 遍历结果集ResultSet
        	Date date = new Date();
        	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	String last_time = dtf.format(date);
        	System.out.println("bpms返回结果返回时间：："+last_time);
        	String mid=getMetaDataLogId();
        	String xg="\\";
        	sql="INSERT INTO xwcmmetadatalog (METADATALOGID, METADATAID, OPERDESC, MEDIAID, MEDIATYPE,SRCMETADATAID, TYPE,SRCCHNLID,DESCHNLID, OPERTIME,"
        			+ " OPERUSER, OPINION, TARGETNAME, SOURCENAME, OPERKEY, DESUSER, CrUser, CrTime, ATTRIBUTE, GroupId, crdept, desgroupid, desdept, TENANTID, XCBOPINION, ISXCB) VALUES"
        			+ " ('"+mid+"', '"+rs.getString("METADATAID")+"', 'BPMS审核结果', '"+rs.getString("MEDIAID")+"', '"+rs.getString("MEDIATYPE")+"', '"+rs.getString("SRCMETADATAID")+"', '2', '"+rs.getString("SRCCHNLID")+"', NULL, "
        					+ "'"+last_time+"', '"+rs.getString("OPERUSER")+"', '"+opinion+"', '', '', '"+rs.getString("OPERKEY")+"', NULL, '"+rs.getString("DESUSER")+"', "
        							+ "'"+last_time+"', '"+rs.getString("ATTRIBUTE")+"', '"+rs.getString("GroupId")+"', '"+rs.getString("crdept")+xg+"', '"+rs.getString("desgroupid")+"', NULL, '"+rs.getString("TENANTID")+"', NULL, NULL);";
     
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");

			
        }
     
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
		return sql;
	
	}
	
	
	
	/**
	 * 执行sql
	 * 插入数据到日志记录表
	 * @param sql
	 * @return 
	 * @throws SQLException
	 * @throws Exception
	 */
    public static  int  insertBpmsLog(String sql) throws SQLException,Exception{
    	
//    	String sql="update xwcmmetadatalog  set  OPINION = '"+opinion+"'  WHERE  MetaDataId= '"+docid+"'";
    	System.out.println("bpmslog日志执行sql："+sql);
        java.sql.Connection conn = null;
        Statement stat = null;
        int res=0;
        try {
        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);

        // 执行sql
        stat = conn.createStatement();  
        res = stat.executeUpdate(sql);
        //执行完毕  关闭连接
        stat.close();
        conn.close();
        } catch (SQLException e) {
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
    
        return res;
    }
	
	
	/**
	 * 查询是否已经推送bpms，流程是否结束
	 * @param docid
	 * @return
	 * @throws Exception
	 */
	public static int  selectBpmslog(String docid,String qd) throws Exception{
		
		System.out.println("--------------------------------------查询推送oa标识开始--------------------------------------------------");
		String selectsql=" SELECT pushsig FROM xwcmmetadatabpmslog WHERE recid = '"+docid+"' and channel = '"+qd+"' ";
        System.out.println("查询bpmslog的sql："+selectsql);
        Util.log("查询bpmslog的sql："+selectsql,"bpms",0);
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        int pushsig=0;
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	pushsig = rs.getInt("pushsig");
        	
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			
        }
        System.out.println("查询bpmslog的sql结果："+pushsig);
        Util.log("查询bpmslog的sql结果："+pushsig,"bpms",0);
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
		System.out.println("--------------------------------------查询推送oa标识结束--------------------------------------------------");
		return pushsig;
	
	}
	
	
	/**
	 * 查询微信预览的账号
	 * @param docid
	 * @return
	 * @throws Exception
	 */
	public static String  selectWXUser() throws Exception{
		
		System.out.println("--------------------------------------查询微信预览账号开始--------------------------------------------------");
		String selectsql=" SELECT cvalue FROM wcmhyidsconfig WHERE ctype = '1' and ckey = 'wxnumber'  and characteristic = 'wx'  ";
        System.out.println("查询bpmslog的sql："+selectsql);
        Util.log("查询bpmslog的sql："+selectsql,"bpms",0);
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        String cvalue=null;
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	//目前默认的 watnry7
        	cvalue = rs.getString("cvalue");
        	
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			
        }
        System.out.println("查询wcmhyidsconfig的sql结果："+cvalue);
        Util.log("查询wcmhyidsconfig的sql结果："+cvalue,"bpms",0);
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
		System.out.println("--------------------------------------查询微信预览账号结束--------------------------------------------------");
		return cvalue;
	
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static boolean  selectFengMian(String docid) throws Exception{
		
		System.out.println("--------------------------------------开始查询封面图--------------------------------------------------");
		String selectsql=" SELECT ax.* from wcmappendix ax ,wcmdocument doc ,wcmchnldoc chnl where chnl.RECID='"+docid+"' and ax.appdocid =doc.docid  AND doc.docid=chnl.docid  ";
        System.out.println("封面图查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        boolean fmt= false;

        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  fileext = rs.getString("fileext");//附件后缀
        	String  appflag = rs.getString("appflag");//0  正文中图片  130 是附件 稿件背景得附件      20  微信封面    10 是附件字段得附件 
        	String  attribute = rs.getString("attribute");//TopPic=1  首页封面图
        	
	
        	//再处理附件和正文中图片
			if("gif".equals(fileext)||"jpg".equals(fileext)||"jpeg".equals(fileext)||"png".equals(fileext)){//代表图片 gif,jpg,jpeg,png
				
				if("20".equals(appflag)){ //处理微信封面图
					
					fmt=true;
					
				}

			}
			
        }
        System.out.println("是否设置了封面图：：："+fmt);
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
		System.out.println("--------------------------------------开始查询封面图结束--------------------------------------------------");

		return fmt;
	
	}
	
	
	/**
	 * 执行sql
	 * updatesql
	 * @param sql
	 * @return 
	 * @throws SQLException
	 * @throws Exception
	 */
    public static  int  updateSql(String sql) throws SQLException,Exception{
    	
//    	String sql="update xwcmmetadatalog  set  OPINION = '"+opinion+"'  WHERE  MetaDataId= '"+docid+"'";
    	System.out.println("执行updatesql："+sql);
        java.sql.Connection conn = null;
        Statement stat = null;
        int res=0;
        try {
        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);

        // 执行sql
        stat = conn.createStatement();  
        res = stat.executeUpdate(sql);
        //执行完毕  关闭连接
        stat.close();
        conn.close();
        } catch (SQLException e) {
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
    
        return res;
    }
    
    
    
    
	/**
	 * 处理微信稿件查询操作用户的id  并且判断是否是微信稿件
	 * @param docid
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String,String>  stWeChatChannel(String recid) throws Exception{
		
		String selectsql="  SELECT wu.USERID,wu.TRUENAME,chnl.DOCID, chnl.CHNLID,doc.DOCTITLE  from wcmuser wu ,wcmdocument doc, wcmchnldoc chnl where chnl.RECID='"+recid+"' and wu.USERNAME = chnl.OPERUSER  AND doc.docid = chnl.docid  ";

        System.out.println("处理微信稿件查询操作用户的id,并且判断是否是微信稿件sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_url,jdbc_username,jdbc_password);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        HashMap<String,String> map=new HashMap<>();
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  userid = rs.getString("USERID");
        	String  chnlid = rs.getString("CHNLID");
        	String  docid = rs.getString("DOCID");
        	String  title = rs.getString("DOCTITLE");
        	String  truename = rs.getString("TRUENAME");
 
        	System.out.println("操作人的id=="+userid);
        	System.out.println("稿件所属栏目chnlid=="+chnlid);
        	
    		map.put("userid", userid);
    		map.put("chnlid", chnlid);
    		map.put("docid", docid);
    		map.put("title", title);
    		map.put("truename", truename);
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			
        }

		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
		return map;
	
	}
    
	

}
