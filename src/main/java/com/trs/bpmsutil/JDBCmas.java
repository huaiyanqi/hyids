package com.trs.bpmsutil;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import com.mysql.jdbc.Connection;
import com.trs.oauth.ConstantUtil;

public class JDBCmas {
	
	
    public static final String  jdbc_driver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_urlmas= ConstantUtil.jdbc_urlmas;
    public static final String  jdbc_usernamemas=  ConstantUtil.jdbc_usernamemas;
    public static final String  jdbc_passwordmas= ConstantUtil.jdbc_passwordmas;
	
	/**
	 * 获取mas视频
	 * @param masid
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, String>  selectMasVideo(String masid) throws Exception{
		
		String selectsql=" SELECT FILENAME,NAME,SUBPATH FROM mas_masvideo  WHERE ID = '"+masid+"' ";

        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_urlmas,jdbc_usernamemas,jdbc_passwordmas);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        HashMap<String , String> res=new HashMap<>();
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	String  FILENAME = rs.getString("FILENAME");//附件播放名字
        	String  NAME = rs.getString("NAME");//附件名称
        	String  SUBPATH = rs.getString("SUBPATH");//播放地址

        	
        	res.put("FILENAME", FILENAME);
        	res.put("NAME",NAME);
        	res.put("SUBPATH", SUBPATH);

        	System.out.println(res);
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			
        }
    	System.out.println("-----------------------------一mas--------------------------------"+res);

		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
		return res;
	
	}

}
