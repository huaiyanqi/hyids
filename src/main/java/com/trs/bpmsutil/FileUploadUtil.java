package com.trs.bpmsutil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.HttpFileUpload;
import com.trs.oauth.ConstantUtil;
 
public class FileUploadUtil {
 
	
    public static final String  bpms_file_url= ConstantUtil.bpms_file_url;
	private static String HR_URL = ConstantUtil.HR_URL;
	private static String resouse_path = ConstantUtil.resouse_path;
	
	public static void main(String[] args) throws ClientProtocolException, IOException {

		MultipartFile createMfileByPath=createMfileByPath("D:\\D\\word\\中国融通\\测试文本.txt");
		Map<String, String> paramMap =new HashMap<String, String>();
		String result=doPostFormData("629dd39f-184e-4260-b3ba-0540b0f70d1f","http://123.121.155.161:801/seeyon/rest/attachment","file",createMfileByPath,paramMap);
		System.out.println("bpms返回结果："+result);
		JSONObject jsonObject = JSON.parseObject(result.toString());

		String fileurl=null;

		JSONArray Arr = jsonObject.getJSONArray("atts");//根据json对象中数组的名字解析出其所对应的值
		for (int i = 0; i < Arr.size(); i++) {
			JSONObject dataBean = (JSONObject) Arr.get(i);//得到数组中对应下标对应的json对象
			fileurl = dataBean.getString("fileUrl");//根据json对象中的数据名解析出相应数据
			System.out.println("附件id："+fileurl);//打印输出
		}

		
		System.out.println(fileurl);
	}
	
	/**
	 * 下载附件并上传
	 * @param file
	 * @param token
	 * @return
	 * @throws IOException
	 */
	public static String DownLoadUpFile(String file,String token) throws IOException{
		
		//附件下载地址
		String url="";
		if(file.contains("W0")){			
			url=HR_URL+"/webpic/"+file.substring(0,8)+"/"+file.substring(0,10)+"/"+file;	
		}else{
			url=HR_URL+"/protect/"+file.substring(0,8)+"/"+file.substring(0,10)+"/"+file;
		}
    	//拼接下载地址
		//获取名称
		HttpFileUpload.downLoadFromUrl(url,file,resouse_path);
		//下载后进行上传
		MultipartFile createMfileByPath=createMfileByPath(resouse_path+file);
		Map<String, String> paramMap =new HashMap<String, String>();
		String result=FileUploadUtil.doPostFormData(token,bpms_file_url,"file",createMfileByPath,paramMap);
		System.out.println("bpms返回结果："+result);
		JSONObject jsonObject = JSON.parseObject(result.toString());
		String fileurl=null;
		JSONArray Arr = jsonObject.getJSONArray("atts");//根据json对象中数组的名字解析出其所对应的值
		for (int i = 0; i < Arr.size(); i++) {
			JSONObject dataBean = (JSONObject) Arr.get(i);//得到数组中对应下标对应的json对象
			fileurl = dataBean.getString("fileUrl");//根据json对象中的数据名解析出相应数据
			System.out.println("附件id："+fileurl);//打印输出
		}
		
		return fileurl;
		
	}
	//本地附件上传给 bpms
	public static String uploadBpms(String file,String token){
		
		MultipartFile createMfileByPath=createMfileByPath(resouse_path+file);
		Map<String, String> paramMap =new HashMap<String, String>();
		String result=FileUploadUtil.doPostFormData(token,bpms_file_url,"file",createMfileByPath,paramMap);
		System.out.println("bpms返回结果："+result);
		JSONObject jsonObject = JSON.parseObject(result.toString());
		String fileurl=null;
		JSONArray Arr = jsonObject.getJSONArray("atts");//根据json对象中数组的名字解析出其所对应的值
		for (int i = 0; i < Arr.size(); i++) {
			JSONObject dataBean = (JSONObject) Arr.get(i);//得到数组中对应下标对应的json对象
			fileurl = dataBean.getString("fileUrl");//根据json对象中的数据名解析出相应数据
			System.out.println("附件id："+fileurl);//打印输出
		}
		return fileurl;
	}
	
	   /**
     * @description:  根据文件路径，获取MultipartFile对象
     * @author: nisan
     * @date: 2022/1/18 13:08
     * @param path
     * @return org.springframework.web.multipart.MultipartFile
     */
    public static MultipartFile createMfileByPath(String path) {
        MultipartFile mFile = null;
        try {
            File file = new File(path);
            FileInputStream fileInputStream = new FileInputStream(file);

            String fileName = file.getName();
            fileName = fileName.substring((fileName.lastIndexOf("/") + 1));
            mFile =  new MockMultipartFile(fileName, fileName, ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);
        } catch (Exception e) {
            System.out.println("封装文件出现错误：{}"+e);
            //e.printStackTrace();
        }
        return mFile;
    }
	
	
	/**
     * 以post方式调用第三方接口,以form-data 形式  发送 MultipartFile 文件数据
     *
     * @param url  post请求url
     * @param fileParamName 文件参数名称
     * @param multipartFile  文件
     * @param paramMap 表单里其他参数
     * @return
     */
    public static String doPostFormData(String token,String url, String fileParamName, MultipartFile multipartFile, Map<String, String> paramMap) {
    	System.out.println("开始上传附件fileParamName：："+fileParamName);
    	
    	System.out.println("开始上传附件token：："+token);
    	System.out.println("开始上传附件url：："+url);
        // 创建Http实例
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // 创建HttpPost实例
        HttpPost httpPost = new HttpPost(url);
       
        // 请求参数配置
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectTimeout(60000)
                .setConnectionRequestTimeout(10000).build();
        httpPost.setConfig(requestConfig);
        httpPost.setHeader("token",token);
       
        try {
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setCharset(java.nio.charset.Charset.forName("UTF-8"));
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
           
            String fileName = multipartFile.getOriginalFilename();
            // 文件流
            builder.addBinaryBody(fileParamName, multipartFile.getInputStream(), ContentType.MULTIPART_FORM_DATA, fileName);
            //表单中其他参数
           for(Map.Entry<String, String> entry: paramMap.entrySet()) {
                builder.addPart(entry.getKey(),new StringBody(entry.getValue(), ContentType.create("text/plain", Consts.UTF_8)));
            }
           
            HttpEntity entity = builder.build();
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost);// 执行提交
                      
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                // 返回
                String res = EntityUtils.toString(response.getEntity(), java.nio.charset.Charset.forName("UTF-8"));
                return res;
            }
           
        } catch (Exception e) {
        	System.out.println("附件上传接口错误信息："+e);
            e.printStackTrace();
           
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    System.out.println("失败！！");
                }
            }
        }       
        return null;              
    }

	
 
}