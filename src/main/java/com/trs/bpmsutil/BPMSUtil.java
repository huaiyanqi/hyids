package com.trs.bpmsutil;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.http.entity.ContentType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;

public class BPMSUtil {
	private static String bpms_url= ConstantUtil.bpms_url;
	public static final String  bpms_userName= ConstantUtil.bpms_userName;
	private static String bpms_password = ConstantUtil.bpms_password;
	private static String bpms_templateCode_gr = ConstantUtil.bpms_templateCode_gr;
	private static String bpms_templateCode_wz = ConstantUtil.bpms_templateCode_wz;
	private static String bpms_appName = ConstantUtil.bpms_appName;
	private static String bpms_from_url = ConstantUtil.bpms_from_url;
	private static String bpms_from_update_url = ConstantUtil.bpms_from_update_url;
	private static String bpms_templateCode_wx = ConstantUtil.bpms_templateCode_wx;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String bpms_file_url = ConstantUtil.bpms_file_url;

	
	
	public static boolean checkParameterDY(String websiteid,String composeId,String IsPdf,String PubDate,String jbdc,String jbbm,String qd,String loginname){
		
		if(websiteid==null||"".equals(websiteid)){
			return false;
		}
		if(composeId==null||"".equals(composeId)){
			return false;
		}
		if(IsPdf==null||"".equals(IsPdf)){
			return false;
		}
		if(PubDate==null||"".equals(PubDate)){
			return false;
		}
		if(jbdc==null||"".equals(jbdc)){
			return false;
		}
		if(jbbm==null||"".equals(jbbm)){
			return false;
		}
		if(qd==null||"".equals(qd)){
			return false;
		}
		if(loginname==null||"".equals(loginname)){
			return false;
		}
		
		return true;
	}
	
	
	public static String encodePathVariable(String pathVariable) {
		   String ret = "default";
		    try {
		        ret = URLEncoder.encode(pathVariable, "utf-8");
		        System.out.println(pathVariable + " : " + ret);
		    }catch(Exception e) {
		        System.out.println(e);
		    }
		    return ret;
	}

	
	public static boolean checkParameter(String parameter){
		
		if(parameter==null||"".equals(parameter)){
			return false;
		}
				
		return true;
	}
	
	
	public static String upBpmsFile(String file,String token){
		
		//下载后进行上传
		MultipartFile createMfileByPath=createMfileByPath(resouse_path+file);
		Map<String, String> paramMap =new HashMap<String, String>();
		String result=FileUploadUtil.doPostFormData(token,bpms_file_url,"file",createMfileByPath,paramMap);
		System.out.println("bpms返回结果："+result);
		JSONObject jsonObject = JSON.parseObject(result.toString());
		String fileurl=null;
		JSONArray Arr = jsonObject.getJSONArray("atts");//根据json对象中数组的名字解析出其所对应的值
		for (int i = 0; i < Arr.size(); i++) {
			JSONObject dataBean = (JSONObject) Arr.get(i);//得到数组中对应下标对应的json对象
			fileurl = dataBean.getString("fileUrl");//根据json对象中的数据名解析出相应数据
			System.out.println("附件id："+fileurl);//打印输出
		}
		
		return fileurl;
	}
	
	   /**
	  * @description:  根据文件路径，获取MultipartFile对象
	  * @author: nisan
	  * @date: 2022/1/18 13:08
	  * @param path
	  * @return org.springframework.web.multipart.MultipartFile
	  */
	 public static MultipartFile createMfileByPath(String path) {
	     MultipartFile mFile = null;
	     try {
	         File file = new File(path);
	         FileInputStream fileInputStream = new FileInputStream(file);
	
	         String fileName = file.getName();
	         fileName = fileName.substring((fileName.lastIndexOf("/") + 1));
	         mFile =  new MockMultipartFile(fileName, fileName, ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);
	     } catch (Exception e) {
	         System.out.println("封装文件出现错误：{}"+e);
	         //e.printStackTrace();
	     }
	     return mFile;
	 }
	
    public static char getPathSeperator(String _sPath) {
        if (_sPath.indexOf("\\") >= 0) {
            return '\\';
        }
        return '/';
    }

    /**
     * 生成随机数
     * @param _sPath
     * @return
     */
    public static String getSuiJiShu() {
        //生成随机数
    	Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
        String sig=String.valueOf(month)+String.valueOf(day);
        String filUUID=sig+getUUID();//附件统一uuid
        return filUUID;
    }
    
    /**
     * 获取uuid
     * @return
     */
	public static String  getUUID(){
		
//		String str = UUID.randomUUID().toString().replaceAll("-", "");

        int first = new Random(10).nextInt(8) + 1;
//        System.out.println(first);
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if (hashCodeV < 0) {//有可能是负数
            hashCodeV = -hashCodeV;
        }
        // 0 代表前面补充0
        // 4 代表长度为4
        // d 代表参数为正数型
        return first + String.format("%015d", hashCodeV);

	}
    
	/**
	 * 调用bpms修改接口  
	 * 微信修改接口
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public static String bpmsUpdateWX(String docid,String qd,String lgname) throws Exception{
		
		Util.log("--------------------------------------微信同步更新bpms开始--------------------------------------------------","bpms",0);
		System.out.println("--------------------------------------微信同步更新bpms开始--------------------------------------------------");
		//获取token
		String result;
		try {
			result = HttpController.testGetToken(bpms_userName,bpms_password,lgname,bpms_url);
		} catch (Exception e) {
			e.printStackTrace();
			Map hrMap =new HashMap<String, String>();
			hrMap.put("code", "-1");	
			hrMap.put("message","获取OAtoken失败！！！！");
			JSONObject hrJson =new JSONObject(hrMap);
			System.out.println("更新oa微信正文时，获取token失败！！！");
    		Util.log("--------------------------------------微信同步更新bpms结束--------------------------------------------------","bpms",0);
    		System.out.println("--------------------------------------微信同步更新bpms结束--------------------------------------------------");
			return hrJson.toJSONString();
		}
		//解析返回结果
		JSONObject jsonObjectHYSE = JSON.parseObject(result);
		String token = jsonObjectHYSE.get("id").toString();//获取到token
        
		//查询到需要传给bpms接口的书文档数据
		HashMap<String,String> bpmsmap=JDBCSelectDoc.selectWeChatDocHtml(docid);//基础数据

		//查询附件 并进行上传
		HashMap<String,String> bpmsfile=JDBCSelectDoc.updateFile(bpmsmap,docid,token,qd);//基础数据

		//转换成json数据
		String  json= JSON.toJSONString(bpmsfile);
		
		//调用上传表单接口
		//设置表单头
	    Map<String, String> maphead = new HashMap<String, String>();
	    maphead.put("token", token);
	    Util.log("更新微信bpms调用json："+json,"bpms",0);
	    System.out.println("更新微信bpms调用json："+json);
	    //调用接口
//	    String post = doPost1("fuserest","609011ab-8662-4678-9280-a8eff259a288","seeyonas","http://123.121.155.161:801/seeyon/rest/token");
	    String postfrom = sendPost(bpms_from_update_url,json,maphead);
	    Util.log("更新微信bpms发起表单接口返回结果："+postfrom,"bpms",0);
	    System.out.println("更新微信bpms发起表单接口返回结果：："+postfrom);
	    //解析结果
		JSONObject jsonform = JSON.parseObject(postfrom);
		String formcode = jsonform.get("code").toString();//获取到token
		
		//修改稿件状态
		String updateUnderreView=" UPDATE WCMChnlDoc  SET UnderReview = '1' WHERE  RECID = '"+docid+"' ";
		int uuv=JDBCSelectDoc.updataSql(updateUnderreView);
		if(uuv>0){
			Util.log("更新微信修改稿件状态成功："+updateUnderreView,"bpms",0);
			System.out.println("更新微信修改稿件状态成功："+updateUnderreView);
		}else{
			Util.log("更新微信修改稿件状态失败："+updateUnderreView,"bpms",0);
			System.out.println("更新微信修改稿件状态失败："+updateUnderreView);
		}
		
		Map hrMap =new HashMap<String, String>();
		//返回海融数据
		if("0".equals(formcode)){
			hrMap.put("code", "0");	
			hrMap.put("message","更新上传成功！！！");	
		}else{
			hrMap.put("code", "-1");	
			hrMap.put("message", "更新上传失败！！稿件id为["+docid+"]");	
		}
		JSONObject hrJson =new JSONObject(hrMap);
		return hrJson.toString();
		
	}
	
	
	 /**
	  * @param url 访问地址
	  *  @param param 需要传输参数参数；对象可以通过json转换成String
	  * @param header header 参数；可以通过下面工具类将string类型转换成map
	  * @return 返回网页返回的数据
	  */
	 public static String sendPost(String url, String param, Map<String, String> header) throws UnsupportedEncodingException, IOException {
	         OutputStreamWriter out;
	         URL realUrl = new URL(url);
	         // 打开和URL之间的连接
	         HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
	         //设置超时时间
	         conn.setConnectTimeout(5000);
	         conn.setReadTimeout(15000);
	         // 设置通用的请求属性
	         if (header!=null) {
	             for (Entry<String, String> entry : header.entrySet()) {
	                 conn.setRequestProperty(entry.getKey(), entry.getValue());
	             }
	         }
	         conn.setRequestMethod("POST");
	         conn.addRequestProperty("Content-Type", "application/json");
	         conn.setRequestProperty("accept", "*/*");
	         conn.setRequestProperty("connection", "Keep-Alive");
	         conn.setRequestProperty("user-agent",
	                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	         // 发送POST请求必须设置如下两行
	         conn.setDoOutput(true);
	         conn.setDoInput(true);
	         // 获取URLConnection对象对应的输出流
	         out = new OutputStreamWriter( conn.getOutputStream(),"UTF-8");// utf-8编码
	         // 发送请求参数
	         out.write(param);

	         // flush输出流的缓冲
	         out.flush();
	         int responseCode = conn.getResponseCode();  
	         InputStream in1=null;
	         // 定义BufferedReader输入流来读取URL的响应
			 if (responseCode == 200) {  
				 in1 = new BufferedInputStream(conn.getInputStream());  
			 } else {  
				 in1 = new BufferedInputStream(conn.getErrorStream());  
			 } 
             BufferedReader rd = new BufferedReader(new InputStreamReader(in1,"utf8"));
             String tempLine = rd.readLine();
             StringBuffer tempStr = new StringBuffer();
             String crlf = System.getProperty("line.separator");
             while (tempLine != null) {
                tempStr.append(tempLine);
                tempStr.append(crlf);
                tempLine = rd.readLine();
             }
             String responseContent = tempStr.toString();
             
//			 
//	         in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf8"));
//	         String line;
//	         while ((line = in.readLine()) != null) {
//	             result += line;
//	         }
//	         
	         if(out!=null){
	             out.close();
	         }
	         return responseContent;
	     }

}
