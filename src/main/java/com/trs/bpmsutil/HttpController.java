package com.trs.bpmsutil;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.client.methods.HttpPost;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.hycloud.HttpClientExcuteException;
import com.trs.hycloud.HttpClientRenderException;
import com.trs.hycloud.ResponseBuddy;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.mqadddoc.SelectSiteName;
import com.trs.oauth.ConstantUtil;
import com.trs.oauth.log.HttpUtils;
import com.trs.zfwz.zfwzUtil;





/**
 * 政府网站-财政
 */
@Controller
public class HttpController{
	 /**
     * 请求编码
     */
    public static String requestEncoding = "UTF-8";
    /**
     * 连接超时
     */
    private static int connectTimeOut = 5000;
    /**
     * 读取数据超时
     */
    private static int readTimeOut = 10000;
	private static String Z_URL = ConstantUtil.Z_URL;

	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	

	
	
	 public static  String  testGetToken(String userName,String password,String  loginName,String url) throws Exception {

		    Map<String, String> map = new HashMap<String, String>();
		    map.put("userName", userName);
		    map.put("password", password);
		    map.put("loginName", loginName);
		    
		    JSONObject json  = new JSONObject();
		    json.put("userName", userName);
		    json.put("password", password);
		    json.put("loginName", loginName);
		    
//		    String post = doPost1("fuserest","609011ab-8662-4678-9280-a8eff259a288","seeyonas","http://123.121.155.161:801/seeyon/rest/token");
		    String post = sendPost(url,json.toJSONString(),map);
//		    System.out.println(post); 
		    return post;
	} 
	
	
	
	 public static  String  createRes2(JSONArray jsonArray,JSONObject json ) throws Exception {
		 	System.out.println("createRes2");
		    json.put("attachments", jsonArray.toJSONString());//附件
		    
		    String url =Z_URL+ "/resource/createRes";
		    Map<String, String> map = new HashMap<String, String>();
		    System.out.println("新建文档内容json："+json);
		    System.out.println("新建文档内容url："+url);
		    map.put("appId", ZWBJ_APPID);
		    map.put("data", DESUtil.encrypt(json.toJSONString(),ZWBJ_STR_DEFAULT_KEY));
//		    System.out.println("map"+map);
		    String post = doPost(url, map);
//		    System.out.println(post); 
		    return post;
	} 
	 
	 
	 /**
	     * 测试上传图片
	     * 
	     */
	    public static void testUploadImage(){
	        String url = "http://xxxtest/Api/testUploadModelBaking";
	        String fileName = "e:/username/textures/antimap_0017.png";
	        Map<String, String> textMap = new HashMap<String, String>();
	        //可以设置多个input的name，value
	        textMap.put("name", "testname");
	        textMap.put("type", "2");
	        //设置file的name，路径
	        Map<String, String> fileMap = new HashMap<String, String>();
	        fileMap.put("upfile", fileName);
	        String contentType = "";//image/png
	        String ret = formUpload(url, textMap, fileMap,contentType);
	        System.out.println(ret);
	        //{"status":"0","message":"add succeed","baking_url":"group1\/M00\/00\/A8\/CgACJ1Zo-LuAN207AAQA3nlGY5k151.png"}
	    }
	 
	    /**
	     * 上传图片
	     * @param urlStr
	     * @param textMap
	     * @param fileMap
	     * @param contentType 没有传入文件类型默认采用application/octet-stream
	     * contentType非空采用filename匹配默认的图片类型
	     * @return 返回response数据
	     */
	    @SuppressWarnings("rawtypes")
	    public static String formUpload(String urlStr, Map<String, String> textMap,
	            Map<String, String> fileMap,String contentType) {
	        String res = "";
	        HttpURLConnection conn = null;
	        // boundary就是request头和上传文件内容的分隔符
	        String BOUNDARY = "---------------------------123821742118716"; 
	        try {
	            URL url = new URL(urlStr);
	            conn = (HttpURLConnection) url.openConnection();
	            conn.setConnectTimeout(5000);
	            conn.setReadTimeout(30000);
	            conn.setDoOutput(true);
	            conn.setDoInput(true);
	            conn.setUseCaches(false);
	            conn.setRequestMethod("POST");
	            conn.setRequestProperty("Connection", "Keep-Alive");
	            // conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.6)");
	            conn.setRequestProperty("Content-Type","multipart/form-data; boundary=" + BOUNDARY);
	            OutputStream out = new DataOutputStream(conn.getOutputStream());
	            // text
	            if (textMap != null) {
	                StringBuffer strBuf = new StringBuffer();
	                Iterator iter = textMap.entrySet().iterator();
	                while (iter.hasNext()) {
	                    Map.Entry entry = (Map.Entry) iter.next();
	                    String inputName = (String) entry.getKey();
	                    String inputValue = (String) entry.getValue();
	                    if (inputValue == null) {
	                        continue;
	                    }
	                    strBuf.append("\r\n").append("--").append(BOUNDARY).append("\r\n");
	                    strBuf.append("Content-Disposition: form-data; name=\"" + inputName + "\"\r\n\r\n");
	                    strBuf.append(inputValue);
	                }
	                out.write(strBuf.toString().getBytes());
	            }
	            // file
	            if (fileMap != null) {
	                Iterator iter = fileMap.entrySet().iterator();
	                while (iter.hasNext()) {
	                    Map.Entry entry = (Map.Entry) iter.next();
	                    String inputName = (String) entry.getKey();
	                    String inputValue = (String) entry.getValue();
	                    if (inputValue == null) {
	                        continue;
	                    }
	                    File file = new File(inputValue);
	                    String filename = file.getName();
	                    
	                    //没有传入文件类型，同时根据文件获取不到类型，默认采用application/octet-stream
	                    contentType = new MimetypesFileTypeMap().getContentType(file);
	                    //contentType非空采用filename匹配默认的图片类型
	                    if(!"".equals(contentType)){
	                        if (filename.endsWith(".png")) {
	                            contentType = "image/png"; 
	                        }else if (filename.endsWith(".jpg") || filename.endsWith(".jpeg") || filename.endsWith(".jpe")) {
	                            contentType = "image/jpeg";
	                        }else if (filename.endsWith(".gif")) {
	                            contentType = "image/gif";
	                        }else if (filename.endsWith(".ico")) {
	                            contentType = "image/image/x-icon";
	                        }
	                    }
	                    if (contentType == null || "".equals(contentType)) {
	                        contentType = "application/octet-stream";
	                    }
	                    StringBuffer strBuf = new StringBuffer();
	                    strBuf.append("\r\n").append("--").append(BOUNDARY).append("\r\n");
	                    strBuf.append("Content-Disposition: form-data; name=\"" + inputName + "\"; filename=\"" + filename + "\"\r\n");
	                    strBuf.append("Content-Type:" + contentType + "\r\n\r\n");
	                    out.write(strBuf.toString().getBytes());
	                    DataInputStream in = new DataInputStream(new FileInputStream(file));
	                    int bytes = 0;
	                    byte[] bufferOut = new byte[1024];
	                    while ((bytes = in.read(bufferOut)) != -1) {
	                        out.write(bufferOut, 0, bytes);
	                    }
	                    in.close();
	                }
	            }
	            byte[] endData = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();
	            out.write(endData);
	            out.flush();
	            out.close();
	            // 读取返回数据
	            StringBuffer strBuf = new StringBuffer();
	            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                strBuf.append(line).append("\n");
	            }
	            res = strBuf.toString();
	            reader.close();
	            reader = null;
	        } catch (Exception e) {
	            System.out.println("发送POST请求出错。" + urlStr);
	            e.printStackTrace();
	        } finally {
	            if (conn != null) {
	                conn.disconnect();
	                conn = null;
	            }
	        }
	        return res;
	    }


	 
	 
	 
	 
	 /**
	  * @param url 访问地址
	  *  @param param 需要传输参数参数；对象可以通过json转换成String
	  * @param header header 参数；可以通过下面工具类将string类型转换成map
	  * @return 返回网页返回的数据
	  */
	 public static String sendPost(String url, String param, Map<String, String> header) throws UnsupportedEncodingException, IOException {
	         OutputStreamWriter out;
	         BufferedReader in = null;
	         String result = "";
	         URL realUrl = new URL(url);
	         // 打开和URL之间的连接
	         HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
	         //设置超时时间
	         conn.setConnectTimeout(5000);
	         conn.setReadTimeout(15000);
	         // 设置通用的请求属性
	         if (header!=null) {
	             for (Entry<String, String> entry : header.entrySet()) {
	                 conn.setRequestProperty(entry.getKey(), entry.getValue());
	             }
	         }
	         conn.setRequestMethod("POST");
	         conn.addRequestProperty("Content-Type", "application/json");
	         conn.setRequestProperty("accept", "*/*");
	         conn.setRequestProperty("connection", "Keep-Alive");
	         conn.setRequestProperty("user-agent",
	                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	         // 发送POST请求必须设置如下两行
	         conn.setDoOutput(true);
	         conn.setDoInput(true);
	         // 获取URLConnection对象对应的输出流
	         out = new OutputStreamWriter( conn.getOutputStream(),"UTF-8");// utf-8编码
	         // 发送请求参数
	         out.write(param);

	         // flush输出流的缓冲
	         out.flush();
	         
	         int responseCode = conn.getResponseCode();  
	         InputStream in1=null;
	         // 定义BufferedReader输入流来读取URL的响应
			 if (responseCode == 200) {  
				 in1 = new BufferedInputStream(conn.getInputStream());  
			 } else {  
				 in1 = new BufferedInputStream(conn.getErrorStream());  
			 } 
             BufferedReader rd = new BufferedReader(new InputStreamReader(in1,"utf8"));
             String tempLine = rd.readLine();
             StringBuffer tempStr = new StringBuffer();
             String crlf = System.getProperty("line.separator");
             while (tempLine != null) {
                tempStr.append(tempLine);
                tempStr.append(crlf);
                tempLine = rd.readLine();
             }
             String responseContent = tempStr.toString();
	         
	         // 定义BufferedReader输入流来读取URL的响应
	         in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf8"));
	         String line;
	         while ((line = in.readLine()) != null) {
	             result += line;
	         }
	         if(out!=null){
	             out.close();
	         }
	         if(in!=null){
	             in.close();
	         }
	         return responseContent;
	     }

	 
	 public static String doPost1(String userName,String password,String  loginName,String url) {
		 try {

		    PostMethod postMethod = null;
		    postMethod = new PostMethod(url) ;
		    postMethod.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8") ;
		    //参数设置，需要注意的就是里边不能传NULL，要传空字符串
		    NameValuePair[] data = {
		            new NameValuePair("userName",userName),
		            new NameValuePair("password",password),           
		            new NameValuePair("loginName",loginName)            
		    };
		    postMethod.setRequestBody(data);
		    org.apache.commons.httpclient.HttpClient httpClient = new org.apache.commons.httpclient.HttpClient();
		    int response = httpClient.executeMethod(postMethod); // 执行POST方法
		    String result = postMethod.getResponseBodyAsString() ;
		    return result;
		} catch (Exception e) {
		    
		    throw new RuntimeException(e.getMessage());
		}
	 }
	 public static String doPost(String reqUrl, Map parameters) {

	        HttpURLConnection url_con = null;
	        String responseContent = null;
	        try {
	            String params = getMapParamsToStr(parameters, requestEncoding);

	            URL url = new URL(reqUrl);
	            url_con = (HttpURLConnection) url.openConnection();
	            url_con.setRequestMethod("POST");
	            System.setProperty("sun.net.client.defaultConnectTimeout", String.valueOf(connectTimeOut));// （单位：毫秒）jdk1.4换成这个,连接超时
	            System.setProperty("sun.net.client.defaultReadTimeout", String.valueOf(readTimeOut)); // （单位：毫秒）jdk1.4换成这个,读操作超时
	            url_con.setRequestProperty("User-agent","Mozilla/4.0");
	            url_con.setDoOutput(true);
	            byte[] b = params.toString().getBytes();
	            url_con.getOutputStream().write(b, 0, b.length);
	            url_con.getOutputStream().flush();
	            url_con.getOutputStream().close();
	            int responseCode = url_con.getResponseCode();  
	            InputStream in=null;
		    	if (responseCode == 200) {  
		    		in = new BufferedInputStream(url_con.getInputStream());  
		    	} else {  
		    		in = new BufferedInputStream(url_con.getErrorStream());  
		    	} 
	            BufferedReader rd = new BufferedReader(new InputStreamReader(in, requestEncoding));
	            String tempLine = rd.readLine();
	            StringBuffer tempStr = new StringBuffer();
	            String crlf = System.getProperty("line.separator");
	            while (tempLine != null) {
	                tempStr.append(tempLine);
	                tempStr.append(crlf);
	                tempLine = rd.readLine();
	            }
	            responseContent = tempStr.toString();
	            rd.close();
	            in.close();
	        } catch (IOException e) {
	        	Util.log(parameters,"网络故障WD",0); 
	            System.out.println("网络故障");
	            e.printStackTrace();
	            return "网络故障,连接超时";
	        } finally {
	            if (url_con != null) {
	                url_con.disconnect();
	            }
	        }
	        return responseContent;
	    }

	    public static void test(){
	    	System.out.println("调用测试方法调用");
	    }

	    private static String getMapParamsToStr(Map paramMap, String requestEncoding) throws IOException {
	        StringBuffer params = new StringBuffer();
	        // 设置边界
	        for (Iterator iter = paramMap.entrySet().iterator(); iter.hasNext(); ) {
	            Map.Entry element = (Map.Entry) iter.next();
	            params.append(element.getKey().toString());
	            params.append("=");
	            params.append(URLEncoder.encode(element.getValue().toString(), requestEncoding));
	            params.append("&");
	        }

	        if (params.length() > 0) {
	            params = params.deleteCharAt(params.length() - 1);
	        }

	        return params.toString();
	    }
	    


	    
	    @SuppressWarnings("unchecked")
		public static void main(String[] args) throws UnsupportedEncodingException, IOException {
	    	
	    	
	    	// 发布通知公告
//			String urlget ="http://hr.grayidea.cn/wcm/token/getToken.action?username=zhaolichao@crtamg.com.cn_1&tenantId=1";
//			String get=HttpUtils.sendGet(urlget);
//			
//			JSONObject jsonObjectHYSE = JSON.parseObject(get);
//			Object dataArr = jsonObjectHYSE.get("properties");//根据json对象中数组的名字解析出其所对应的值
//			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
//			String token=jsonObjectHYGPSE.getString("TOKEN");//办理事项  标题
//			System.out.println(token);
//			Map mappub = new HashMap<String, String>();
//			mappub.put("noticeids", "18");
//			mappub.put("serviceid","notice");
//			mappub.put("method","publishNotice");
//			mappub.put("token",token);
//			String url ="http://hr.grayidea.cn/wcm/message/sendmessage";
//			String post = doPost(url, mappub);
//	    	System.out.println(post);


	    	//创建通知公告
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("typeid", "3");
//			map.put("title", "微信xxxx稿件审核通过！！");
//			map.put("dispatchunit","BPMS");
//			map.put("titlecolor","");
//			map.put("content","<p>微信xxxx稿件审核通过！！</p>");
//			map.put("appendixinfo","[]");
//			map.put("alluser","false");
//			map.put("tenantuser", "false");
//			map.put("serviceid","notice");
//			map.put("method","saveNotice");
//			map.put("userids","397");
//			String urlmak ="http://hr.grayidea.cn/wcm/message/sendmessage";
//			String postmak = doPost(urlmak, map);
//			System.out.println(postmak);
//	    	
//	    	
//			JSONObject jsonObjectHYSE = JSON.parseObject(postmak);
//			Object dataArr = jsonObjectHYSE.get("data");//根据json对象中数组的名字解析出其所对应的值
//			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
//			String noticeid=jsonObjectHYGPSE.getString("noticeid");//办理事项  标题
//			String issuccess=jsonObjectHYSE.getString("issuccess");//办理事项  标题
//			System.out.println(issuccess);
//			System.out.println(noticeid);
	    	
	    	//自动签发网站数据
			String urlget ="http://hr.grayidea.cn/wcm/websiteqianfa.do?ChannelId=256&ChnlDocIds=177&CurrChnlId=256&MetaDataId=161&MetaDataIds=161&methodname=webDaiShenPublish&serviceid=mlf_websiteoper";
			String get=HttpUtils.sendGet(urlget);
	
			JSONObject jsonObjectHYSE = JSON.parseObject(get);
			String ISSUCCESS=jsonObjectHYSE.getString("ISSUCCESS");
			
			System.out.println("自动签发网站数据返回结果：："+get);
	    	//判断是否自动签发成功 	
			JSONObject json = JSON.parseObject(get);
			
			if("true".equals(ISSUCCESS)){

				System.out.println("网站设置自动签发成功！！！");
			}else{
				JSONArray jsonarray=json.getJSONArray("REPORTS");
				for(int i = 0; i < jsonarray.size(); i++){
				   String  DETAIL=jsonarray.getJSONObject(i).getString("DETAIL");

				   System.out.println("网站设置自动签发失败！！！失败原因：：："+DETAIL);
				  						   
				}
			}
			
		}
	    
	   
		 /**
		  * @param url 访问地址
		  *  @param param 需要传输参数参数；对象可以通过json转换成String
		  * @param header header 参数；可以通过下面工具类将string类型转换成map
		  * @return 返回网页返回的数据
		  */
		 public static String sendDoPost(String url, String param, Map<String, String> header) throws UnsupportedEncodingException, IOException {
		         OutputStreamWriter out;
		         URL realUrl = new URL(url);
		         // 打开和URL之间的连接
		         HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
		         //设置超时时间
		         conn.setConnectTimeout(5000);
		         conn.setReadTimeout(15000);
		         // 设置通用的请求属性
		         if (header!=null) {
		             for (Entry<String, String> entry : header.entrySet()) {
		                 conn.setRequestProperty(entry.getKey(), entry.getValue());
		             }
		         }
		         conn.setRequestMethod("POST");
		         conn.addRequestProperty("Content-Type", "application/json");
		         conn.setRequestProperty("accept", "*/*");
		         conn.setRequestProperty("connection", "Keep-Alive");
		         conn.setRequestProperty("user-agent",
		                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
		         // 发送POST请求必须设置如下两行
		         conn.setDoOutput(true);
		         conn.setDoInput(true);
		         // 获取URLConnection对象对应的输出流
		         out = new OutputStreamWriter( conn.getOutputStream(),"UTF-8");// utf-8编码
		         // 发送请求参数
		         out.write(param);

		         // flush输出流的缓冲
		         out.flush();
		         int responseCode = conn.getResponseCode();  
		         InputStream in1=null;
		         // 定义BufferedReader输入流来读取URL的响应
				 if (responseCode == 200) {  
					 in1 = new BufferedInputStream(conn.getInputStream());  
				 } else {  
					 in1 = new BufferedInputStream(conn.getErrorStream());  
				 } 
	             BufferedReader rd = new BufferedReader(new InputStreamReader(in1,"utf8"));
	             String tempLine = rd.readLine();
	             StringBuffer tempStr = new StringBuffer();
	             String crlf = System.getProperty("line.separator");
	             while (tempLine != null) {
	                tempStr.append(tempLine);
	                tempStr.append(crlf);
	                tempLine = rd.readLine();
	             }
	             String responseContent = tempStr.toString();
		         if(out!=null){
		             out.close();
		         }

		         return responseContent;
		     }



}