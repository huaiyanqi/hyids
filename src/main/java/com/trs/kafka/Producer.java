package com.trs.kafka;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.log4j.Logger;

import com.trs.oauth.ConstantUtil;


/**
 * @Date 2018.06.05
 *
 *
 * @Note Kafka Producer
 */
public class Producer {
	private static Logger logger = Logger.getLogger(Producer.class);
    private static KafkaProducer<String, String> producer;
    /**
     * topic主题被拆分成分区。 对于每个主题，Kafka保存一个分区的数据。
     */
    private String topic;
    private Properties props = new Properties();
    
    private static int key = 0;
    
    private static final AtomicInteger CONSUMER_CLIENT_ID_SEQUENCE = new AtomicInteger(1);
    
	private static String USERNAMEKAFKA = ConstantUtil.USERNAMEKAFKA;
	private static String PASSWORDKAFKA = ConstantUtil.PASSWORDKAFKA;
	private static String P_CLIENTID = ConstantUtil.P_CLIENTID;
    
    /**
     * 
     * @param topic  主题名称
     * @param regConfig  kafka服务端主机配置信息
     */
    public Producer(String topic,String bootstrapServers) {
    	 //Kafka服务端的主机名和端口号
    	props.put("bootstrap.servers", bootstrapServers);
    	props.put("client.id", P_CLIENTID+CONSUMER_CLIENT_ID_SEQUENCE.getAndIncrement());
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        
        /**
         * Producer可以用来缓存数据的内存大小。该值实际为RecordAccumulator类中的BufferPool，即Producer所管理的最大内存。
如果数据产生速度大于向broker发送的速度，producer会阻塞max.block.ms，超时则抛出异常
         */
        props.put("buffer.memory", 33554432);
        /**
         * 请求的最大字节数。这也是对最大消息大小的有效限制。注意：server具有自己对消息大小的限制，这些大小和这个设置不同。
         * 此项设置将会限制producer每次批量发送请求的数目，以防发出巨量的请求。
         */
        props.put("max.request.size", 5120000);
        //消息的key和value都是字节数组，为了将Java对象转化为字节数组，可以配置
        //key.serializer和value.serializer两个序列化器，完成转化
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        
        //以下配置为开启认证后需要的参数
        props.put("security.protocol", "SASL_PLAINTEXT");
        props.put("sasl.mechanism", "PLAIN");
        props.put("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username='"+USERNAMEKAFKA+"'  password='"+PASSWORDKAFKA+"';");
        producer = new KafkaProducer<String, String>(props);
        this.topic = topic;
    }

	/**
     * 发送消息到kafka 服务监测
     * @param msg
     */
    public void simpleAddQueueRD(String msg) {
    	long startTime = System.currentTimeMillis();
        producer.send(new ProducerRecord<String, String>("uas_RD01_queue", Integer.toString(key), msg), new DemoCallBack<String, String>(startTime, Integer.toString(key), msg));
        logger.info("发送消息"+msg+"到主题"+"uas_RD01_queue");
        System.err.println("发送消息"+msg+"到主题"+"uas_RD01_queue");
        producer.close();
    }
    
	/**
     * 发送消息到kafka 
     * @param msg
     */
    public void simpleAddQueue(String msg) {
    	long startTime = System.currentTimeMillis();
        producer.send(new ProducerRecord<String, String>(topic, Integer.toString(key), msg), new DemoCallBack<String, String>(startTime, Integer.toString(key), msg));
        logger.info("发送消息"+msg+"到主题"+topic);
        System.err.println("发送消息"+msg+"到主题"+topic);
        producer.close();
    }
    
    
    
    class DemoCallBack<K, V> implements Callback {
    	
        private final long startTime;
        private final K key;
        private final V value;

        public DemoCallBack(long startTime, K key, V value) {
            this.startTime = startTime;
            this.key = key;
            this.value = value;
        }

        /**
         * 生产者成功发送消息，收到Kafka服务端发来的ACK确认消息后，会调用此回调函数
         *
         * @param recordMetadata 生产者发送的消息的元数据，如果发送过程中出现异常，此参数为null
         * @param exception      发送过程中出现的异常，如果发送成功，则此参数为null
         */
	    public void onCompletion(RecordMetadata recordMetadata, Exception exception) {
            long elapsedTime = System.currentTimeMillis() - startTime;
            if(recordMetadata != null){
            	logger.info("-------------消息发送成功,耗时"+elapsedTime+"------------");
            	System.out.println("-------------消息发送成功,耗时"+elapsedTime+"------------");
            }else{
            	exception.printStackTrace();
            	logger.info("-------------消息发送失败,耗时"+elapsedTime+"------------");
            	System.out.println("-------------消息发送失败,耗时"+elapsedTime+"------------");
            }
            
	    }
	    
    }
    
}
