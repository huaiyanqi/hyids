package com.trs.kafka;


import java.util.Arrays;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


import com.trs.oauth.ConstantUtil;




public class ConsumerListener implements ServletContextListener {
	
//	protected Logger logger = LoggerFactory.getLogger(getClass());
//	private static Logger logger = Logger.getLogger(ConsumerListener.class);
	
	private static String UAS_TOPIC = ConstantUtil.UAS_TOPIC;
	private static String BOOTSTRAP_SERVERS = ConstantUtil.BOOTSTRAP_SERVERS;
	private static String UAS_TOPIC_LOGIN_OUT = ConstantUtil.UAS_TOPIC_LOGIN_OUT;
	private static String SERVICE_UAS_TOPIC = ConstantUtil.SERVICE_UAS_TOPIC;
	
	

	public void contextInitialized(ServletContextEvent sce) {
		//参数  监听消息   topic   kafka服务器ip
//		Consumer con = new Consumer(Arrays.asList(UAS_TOPIC),BOOTSTRAP_SERVERS);
//		con.start();
		//服务监测消息主题
//		Consumer con1 = new Consumer(Arrays.asList(SERVICE_UAS_TOPIC),BOOTSTRAP_SERVERS);
//		con1.start();
		//待办任务消息主题 
//		Consumer con2 = new Consumer(Arrays.asList("uas_T01_topic"),"192.168.1.22:9092");
//		con2.start();
		//通知类消息主题
//		Consumer con3 = new Consumer(Arrays.asList("uas_N01_topic"),"192.168.1.22:9092");
//		con3.start();
		//订阅登录、登出消息主题
//		Consumer con4 = new Consumer(Arrays.asList(UAS_TOPIC_LOGIN_OUT),BOOTSTRAP_SERVERS);
//		con4.start();
//		Util.log("------启动消费者----","log",0);
//		System.out.println("------启动消费者----");
	}

	public void contextDestroyed(ServletContextEvent sce) {
		Util.log("tomcat关闭了..."+sce,"log",0);
		System.out.println("tomcat关闭了...");
	}
}
