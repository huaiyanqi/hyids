package com.trs.kafka;

public enum EnumMessageCode {
	//通知类(系统公告)
	BULLETIN("Bulletin","N01"),//系统公告
	//任务类
	PENDINGTASK("pendingTask","T01"),//待办任务
	//协作类(用户、部门同步)
	ADDDEPT("addDept","C01"),//添加部门
	DELDEPT("delDept","C02"),//删除部门
	EDITDEPT("editDept","C03"),//更新部门
	ADDUSER("addUser","C04"),//添加用户
	DELUSER("delUser","C05"),//删除用户
	EDITUSER("editUser","C06"),//更新用户
	ENUSER("enUser","C07"),//启用用户
	DISUSER("disUser","C08"),//禁用用户
	USERLOGIN("userLogin","C11"),//用户登录
	USERLOGOUT("userLogout","C12"),//用户登出
	//数据类
	SERVICESTATE("serviceState","D01"),//服务状态监测
	//响应结果类
	RESPENDINGTASK("resPendingTask","RT01"),//待办任务响应
	RESADDDEPT("resAddDept","RC01"),//添加部门响应
	RESDELDEPT("resDelDept","RC02"),//删除部门响应
	RESEDITDEPT("resEditDept","RC03"),//更新部门响应
	RESADDUSER("resAddUser","RC04"),//添加用户响应
	RESDELUSER("resDelUser","RC05"),//删除用户响应
	RESEDITUSER("resEditUser","RC06"),//更新用户响应
	RESENUSER("resEnUser","RC07"),//启用用户响应
	RESDISUSER("resDisUser","RC08"),//禁用用户响应
	RESSERVICESTATE("resServiceState","RD01"),//服务状态监测响应
	RESUSERLOGOUT("resUserLogout","RC12")//用户登出
	
	;
	
	private String name;
	
	private String value;
	
	/**
     * 私有构造,防止被外部调用
     * @param desc
     */
	private EnumMessageCode(String name,String value){
        this.name = name;
        this.value = value;
    }
	
	/**
     * 定义方法,返回描述,跟常规类的定义没区别
     * @return
     */
    
    
    
    public String getValue() {
		return value;
	}


	public String getName() {
		return name;
	}

}
