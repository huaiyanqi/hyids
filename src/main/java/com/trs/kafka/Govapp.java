package com.trs.kafka;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.HyUtil;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIIP;
import com.trs.oauth.ConstantUtil;
import com.trs.oauth.OAuthController;

public class Govapp {
	
//	private static Logger logger = Logger.getLogger(Govapp.class);
	private static String R_QUEUE = ConstantUtil.R_QUEUE;
	private static String BOOTSTRAP_SERVERS = ConstantUtil.BOOTSTRAP_SERVERS;
	private static String pageSize = ConstantUtil.pageSize;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	/**
	 * 海云退出接口
	 * @return
	 */
	public void loginOUT(String name){
		//调用接口退出登录
		OAuthController oau=new OAuthController();
		oau.logout(name);
		
	}
	
	/**
	 * 启用用户接口
	 * @param loginName
	 * @param parms
	 * @param fromJson
	 * @throws Exception
	 */
	public void  enableUsers(String loginName,Map<String, Object> parms,Map<String, Object> fromJson) throws Exception{
		
		Map<String, String> map=new HashMap<String, String>();
		//查询是否存在该用户
		String sServiceIdSE = "gov_user";
		String sMethodNameSE = "checkUserExist";
		map.put("CurrUserName", USER); // 当前操作的用户
		map.put("SEARCHVALUE", loginName); // 用户名称
		String hySE=HyUtil.dataMoveDocumentHyRbj(sServiceIdSE,sMethodNameSE,map);
		Util.log("用户是否存在接口返回信息："+hySE,"logkafka",0);
		JSONObject jsonObjectSE = JSON.parseObject(hySE);
		Object dataArrHYSE = jsonObjectSE.get("DATA");
		if("true".equals(dataArrHYSE)){
			// 用户存在
			String sServiceId = "gov_user";
			String sMethodName = "queryUsersNoAuth";
			Map<String, String> mSE=new HashMap<String, String>();
			mSE.put("CurrUserName", USER); // 当前操作的用户
//			mSE.put("SEARCHFIELDS", userName); // 用户名称
			//设置查询多少个用户
			mSE.put("pageSize", pageSize);
			mSE.put("pageindex", "1");
			String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
			Util.log("查询所有用户返回信息："+hy1,"logkafka",0);
			JSONObject jsonObject1 = JSON.parseObject(hy1);
			Object dataArr1 = jsonObject1.get("DATA");
			if("true".equals(jsonObject1.get("ISSUCCESS"))){
				JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
				JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
				String userid=null;
				if(hy2!=null){									
					for(int a=0;a<hy2.size();a++){
						JSONObject dataBean = (JSONObject) hy2.get(a);
						String name=dataBean.getString("USERNAME");
						if(loginName.equals(name)){
							userid=dataBean.getString("USERID");
						}
					}
					if(userid!= null){
						//调用接口启用用户
						String sS = "gov_user";
						String sM = "enableUsers";
						Map<String, String> m=new HashMap<String, String>();
						m.put("CurrUserName", USER); // 当前操作的用户
//						mSE.put("SEARCHFIELDS", userName); // 用户名称
						//设置查询多少个用户
						m.put("USERIDS", userid);
						String hye=HyUtil.dataMoveDocumentHyRbj(sS,sM,m);
						Util.log("启用用户接口返回信息："+hye,"logkafka",0);
						JSONObject json = JSON.parseObject(hye);
						JSONArray hyjs=json.getJSONArray("DATA");
						for(int a=0;a<hyjs.size();a++){
							JSONObject dataBean = (JSONObject) hyjs.get(a);//得到数组中对应下标对应的json对象
							String ISSUCCESS=(String) dataBean.get("ISSUCCESS");
							if("true".equals(ISSUCCESS)){
								Util.log("启用成功","logkafka",0);
								System.out.println("启用成功");
								Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
								parms = KafkaUtil.buildSync(parms, fromJson);
								pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
							}else{
								Util.log("启用失败","logkafka",0);
								System.out.println("启用失败");
								Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
								parms = KafkaUtil.buildSyncFalse(parms, fromJson,(String) dataBean.get("TITLE"));
								pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
							}
						}
					}
					
				}else{
					Util.log("调用发布平台接口失败："+jsonObjectHYName.get("MSG"),"logkafka",0);
					System.out.println("调用发布平台接口失败："+jsonObjectHYName.get("MSG"));
					Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
					parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHYName.get("MSG").toString());
					pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
				}
				
			}else{
				Util.log("调用发布平台接口失败："+jsonObject1.get("MSG"),"logkafka",0);
				System.out.println("调用发布平台接口失败："+jsonObject1.get("MSG"));
				Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
				parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObject1.get("MSG").toString());
				pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
				
			}
		}else{
			Util.log("调用发布平台接口失败："+jsonObjectSE.get("MSG"),"logkafka",0);
			System.out.println("调用发布平台接口失败："+jsonObjectSE.get("MSG"));
			Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
			parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectSE.get("MSG").toString());
			pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
		}
		
		
	}

	/**
	 * 用户停用接口
	 * @param loginName
	 * @param parms
	 * @param fromJson
	 * @throws Exception
	 */
	public void  disableUsers(String loginName,Map<String, Object> parms,Map<String, Object> fromJson) throws Exception{
		
		Map<String, String> map=new HashMap<String, String>();
		//查询是否存在该用户
		String sServiceIdSE = "gov_user";
		String sMethodNameSE = "checkUserExist";
		map.put("CurrUserName", USER); // 当前操作的用户
		map.put("SEARCHVALUE", loginName); // 用户名称
		String hySE=HyUtil.dataMoveDocumentHyRbj(sServiceIdSE,sMethodNameSE,map);
		Util.log("停用用户查询用户是否存在接口返回信息："+hySE,"logkafka",0);
		JSONObject jsonObjectSE = JSON.parseObject(hySE);
		Object dataArrHYSE = jsonObjectSE.get("DATA");
		if("true".equals(dataArrHYSE)){
			// 用户存在
			String sServiceId = "gov_user";
			String sMethodName = "queryUsersNoAuth";
			Map<String, String> mSE=new HashMap<String, String>();
			mSE.put("CurrUserName", USER); // 当前操作的用户
//			mSE.put("SEARCHFIELDS", userName); // 用户名称
			//设置查询多少个用户
			mSE.put("pageSize", pageSize);
			mSE.put("pageindex", "1");
			String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
			Util.log("停用用户查询全部用户接口返回信息："+hy1,"logkafka",0);
			JSONObject jsonObject1 = JSON.parseObject(hy1);
			Object dataArr1 = jsonObject1.get("DATA");
			if("true".equals(jsonObject1.get("ISSUCCESS"))){
				JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
				JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
				//查询发布平台库中要删除的用户id
				String userid=null;
				if(hy2!=null){									
					for(int a=0;a<hy2.size();a++){
						JSONObject dataBean =  (JSONObject) hy2.get(a);
						String name=dataBean.getString("USERNAME");
						if(loginName.equals(name)){
							userid=dataBean.getString("USERID");
						}
					}
					if(userid!= null){
						//调用接口停用用户
						String sS = "gov_user";
						String sM = "disableUsers";
						Map<String, String> m=new HashMap<String, String>();
						m.put("CurrUserName", USER); // 当前操作的用户
						m.put("USERIDS", userid);
						String hye=HyUtil.dataMoveDocumentHyRbj(sS,sM,m);
						Util.log("停用用户接口返回信息："+hye,"logkafka",0);
						JSONObject json = JSON.parseObject(hye);
						JSONArray hyjs=json.getJSONArray("DATA");
						for(int a=0;a<hyjs.size();a++){
							JSONObject dataBean = (JSONObject) hyjs.get(a);//得到数组中对应下标对应的json对象
							String ISSUCCESS=(String) dataBean.get("ISSUCCESS");
							if("true".equals(ISSUCCESS)){
								Util.log("停用成功","logkafka",0);
								System.out.println("停用成功");
								Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
								parms = KafkaUtil.buildSync(parms, fromJson);
								pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
							}else{
								Util.log("停用失败","logkafka",0);
								System.out.println("停用失败");
								Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
								parms = KafkaUtil.buildSyncFalse(parms, fromJson,(String) dataBean.get("TITLE"));
								pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
							}
						}
					}
					
				}else{
					Util.log("调用发布平台接口失败："+jsonObjectHYName.get("MSG"),"logkafka",0);
					System.out.println("调用发布平台接口失败："+jsonObjectHYName.get("MSG"));
					Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
					parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHYName.get("MSG").toString());
					pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
					
				}
				
			}else{
				Util.log("调用发布平台接口失败："+jsonObject1.get("MSG"),"logkafka",0);
				System.out.println("调用发布平台接口失败："+jsonObject1.get("MSG"));
				Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
				parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObject1.get("MSG").toString());
				pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
				
			}
		}else{
			Util.log("调用发布平台接口失败："+jsonObjectSE.get("MSG"),"logkafka",0);
			System.out.println("调用发布平台接口失败："+jsonObjectSE.get("MSG"));
			Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
			parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectSE.get("MSG").toString());
			pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));	
		}
	}

	/**
	 * 添加用户接口
	 * @param USERNAME
	 * @param parms
	 * @param fromJson
	 * @param map
	 * @throws Exception
	 */
	public void  addUser(String USERNAME,String password,Map<String, Object> parms,Map<String, Object> fromJson,Map<String, String> map,String userid) throws Exception{
		
		//添加用户前查询是否存在该用户
		String sServiceIdSE = "gov_user";
		String sMethodNameSE = "checkUserExist";
		map.put("CurrUserName", USER); // 当前操作的用户
		map.put("SEARCHVALUE", USERNAME); // 用户名称
		String hySE=HyUtil.dataMoveDocumentHyRbj(sServiceIdSE,sMethodNameSE,map);
		Util.log("添加用户接口查询是否存在该用户接口返回信息："+hySE,"logkafka",0);
		JSONObject jsonObjectSE = JSON.parseObject(hySE);
		Object dataArrHYSE = jsonObjectSE.get("DATA");
		if("true".equals(dataArrHYSE)){
			Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
			parms = KafkaUtil.buildSyncFalse(parms, fromJson,"该用户已经存在");
			pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
		}else{
			//调用发布平台接口添加用户
			String sServiceId = "gov_user";
			String sMethodName = "saveUser";
			map.put("CurrUserName", USER); // 当前操作的用户
			map.put("USERID", "0");
			String hy=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,map);
			Util.log("添加用户接口返回信息："+hy,"logkafka",0);
			JSONObject jsonObjectHY = JSON.parseObject(hy);
			Object dataArrHY = jsonObjectHY.get("ISSUCCESS");//根据json对象中数组的名字解析出其所对应的值
			Util.log("同步添加用户返回结果："+dataArrHY,"logkafka",0);
			System.out.println("同步添加用户返回结果："+dataArrHY);
			
			if("true".equals(dataArrHY)){
				Util.log("用户存入海云成功，开始存入中间表","logkafka",0);
				//成功后添加中间表
				JDBC  jdbc=new JDBC();
				
				System.out.println("开普同步到海云存入sql："+"INSERT INTO kpuser ( username,password,userid) VALUES ('"+USERNAME+"','"+password+"','"+userid+"')");
				Util.log("开普同步到海云存入sql："+"INSERT INTO kpuser ( username,password,userid) VALUES ('"+USERNAME+"','"+password+"','"+userid+"')","logkafka",0);
				try {
					Integer jd=jdbc.JDBCDriver("INSERT INTO kpuser ( username,password,userid) VALUES ('"+USERNAME+"','"+password+"','"+userid+"')");
					if(jd!=-1){
						Util.log("开普同步用户到海云中间表存入成功!","logkafka",0);
						System.out.println("开普同步用户到海云中间表存入成功!");
						Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
						parms = KafkaUtil.buildSync(parms, fromJson);
						pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
					}else{
						Util.log("开普同步到海云中间表存入失败!","logkafka",0);
						System.out.println("开普同步到海云中间表存入失败!");
					}
				} catch (SQLException e) {
					Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
					parms = KafkaUtil.buildSyncFalse(parms, fromJson,"存入中间表用户密码失败！！！！");
					pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
					e.printStackTrace();
				} catch (Exception e) {
					Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
					parms = KafkaUtil.buildSyncFalse(parms, fromJson,"存入中间表用户密码失败！！！！");
					pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
					e.printStackTrace();
				}
				
			}else{
				//失败 返回失败的信息
				Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
				parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHY.get("MSG").toString());
				pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
			}
		}
		Util.log("----同步添加用户结束----","logkafka",0);
		System.out.println("----同步添加用户结束----");
		
	}

	/**
	 * 删除用户接口
	 * @param userName
	 * @param parms
	 * @param fromJson
	 * @throws Exception
	 */
	public void  deleteUser(String userName,Map<String, Object> parms,Map<String, Object> fromJson) throws Exception{
		
		if("admin".equals(userName)){
			
			Util.log("--------------------------------------开始删除用户------------------------------","logkafkadeleteadmin",0);
			Util.log("--------------------------------------admin为必要用户不做删除------------------------------","logkafkadeleteadmin",0);
			Util.log("--------------------------------------删除用户结束------------------------------","logkafkadeleteadmin",0);
			
		}else{

			Util.log("--------------------------------------开始删除用户------------------------------","logkafka",0);
			//根据用户名查询用户是否存在
			String uID=JDBCIIP.JDBCDriverSelectUser("SELECT * FROM wcmuser WHERE USERNAME = '"+userName+"'");
			if(uID!=null){
				//开始删除用户
				String sServiceIdDE = "gov_user";
				String sMethodNameDE = "deleteUsers";
				Map<String, String> mDE=new HashMap<String, String>();
				mDE.put("CurrUserName", USER); // 当前操作的用户
				//设置查询多少个用户
				mDE.put("USERIDS", uID);
				String hyDE=HyUtil.dataMoveDocumentHyRbj(sServiceIdDE,sMethodNameDE,mDE);
				Util.log("删除用户接口返回信息："+hyDE,"logkafka",0);
				JSONObject jsonObjectHYDE = JSON.parseObject(hyDE);
				Object dataArrHYDE = jsonObjectHYDE.get("ISSUCCESS");
				if("true".equals(dataArrHYDE)){
					//开始删除中间表
					JDBC  jdbc=new JDBC();
					System.out.println("删除用户sql："+"DELETE  FROM  kpuser  where username='"+userName+"'");
					try {
						Integer jd=jdbc.JDBCDriver("DELETE  FROM  kpuser  where username='"+userName+"'");
						if(jd!=-1){
							Util.log("删除"+userName+"成功！","logkafka",0);
							System.out.println("删除"+userName+"成功！");
							Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
							parms = KafkaUtil.buildSync(parms, fromJson);
							pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
						}else{
							Util.log("删除用户中间表存入失败!","logkafka",0);
							System.out.println("删除用户中间表存入失败!");
						}
					} catch (SQLException e) {
						Util.log("删除"+userName+"失败！","logkafka",0);
						System.out.println("删除"+userName+"失败！");
						//失败 返回失败的信息
						Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
						parms = KafkaUtil.buildSyncFalse(parms, fromJson,"删除中间表失败！！！！");
						pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
						e.printStackTrace();
					} catch (Exception e) {
						Util.log("删除"+userName+"失败！","logkafka",0);
						System.out.println("删除"+userName+"失败！");
						//失败 返回失败的信息
						Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
						parms = KafkaUtil.buildSyncFalse(parms, fromJson,"删除中间表失败！！！！");
						pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
						e.printStackTrace();
					}
					
				}else{
					Util.log("删除"+userName+"失败！","logkafka",0);
					System.out.println("删除"+userName+"失败！");
					//失败 返回失败的信息
					Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
					parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHYDE.get("MSG").toString());
					pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
				}
			}else{
				Util.log("无此用户，无法删除，请核实是否存在！！！"+userName,"logkafka",0);
				System.out.println("调用发布平台接口返回无用户");
				Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
				parms = KafkaUtil.buildSyncFalse(parms, fromJson,"无此用户，无法删除，请核实是否存在！！！");
				pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
			}

			Util.log("---------------------------------------同步删除用户结束------------------------------------","logkafka",0);
			System.out.println("----同步删除用户结束----");
		
		}
		
		
	}

	
	/**
	 * 更新用户接口
	 * @param USERNAME
	 * @param parms
	 * @param fromJson
	 * @param map
	 * @throws Exception
	 */
	public void  updatUser(String USERNAME,Map<String, Object> parms,Map<String, Object> fromJson,Map<String, String> map,String psssword,String kpuserid) throws Exception{
		
		//查询所有用户
		String sServiceId = "gov_user";
		String sMethodName = "queryUsersNoAuth";
		Map<String, String> oPostData = new HashMap<String, String>();
		oPostData.put("CurrUserName", USER); // 当前操作的用户
		oPostData.put("pageSize", pageSize);//
		oPostData.put("pageindex", "1");
		String hySE = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		Util.log("发布平台接口queryUsersNoAuth用户返回信息："+hySE,"logkafka",0);
//		System.out.println("发布平台接口用户返回信息："+hySE);
		JSONObject jsonObjectSE = JSON.parseObject(hySE);
		Object dataArrHYSE = jsonObjectSE.get("DATA");
		if("true".equals(jsonObjectSE.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArrHYSE.toString());
			JSONArray hy1=jsonObjectHYName.getJSONArray("DATA");
			//查询发布平台库中要更新的用户id
			if(hy1!=null){
				String userid=null;
				for(int a=0;a<hy1.size();a++){
					JSONObject dataBean = (JSONObject) hy1.get(a);
					String name=dataBean.getString("USERNAME");
					if(name.equals(USERNAME)){
						userid=dataBean.getString("USERID");
					}
				}
				if(userid!= null){
					
					//根据用户id查询到用户之前的角色
					StringBuffer roelsids=new StringBuffer();
					StringBuffer groupsids=new StringBuffer();
					Map<String, String> userRole=new HashMap<>();
					String sServiceJS = "gov_user";
					String sMethodJS = "findUserById";
					userRole.put("CurrUserName", USER); // 当前操作的用户
					userRole.put("USERID", userid);
					System.out.println("查找用户角色前map："+userRole);
					String hyrole=HyUtil.dataMoveDocumentHyRbj(sServiceJS,sMethodJS,userRole);
					System.out.println("查找用户角色返回结果：："+hyrole);
					Util.log("查找用户角色返回结果：："+hyrole,"logkafka",0);
					JSONObject jsonObjectrole = JSON.parseObject(hyrole);
					Object dataArrrole = jsonObjectrole.get("DATA");//根据json对象中数组的名字解析出其所对应的值
					
					if(dataArrrole!=null){
						JSONObject jsonObjectHYrole = JSON.parseObject(dataArrrole.toString());
						
						//获取应用权限
						String app=jsonObjectHYrole.getString("COAPPS");
						System.out.println("应用名称COAPPS==="+app);
						map.put("COAPPS", app.replace("\"", "").replace("[", "").replace("]", ""));
						
						//获取到角色
						JSONArray DOCRELPIC=jsonObjectHYrole.getJSONArray("ROLES");//相角色
						for(int i = 0; i < DOCRELPIC.size(); i++){
					    	String role=DOCRELPIC.getJSONObject(i).getString("ROLEID");
					    	if(i != 0){
					    		roelsids.append(",");	
					    	}
					    	roelsids.append(role);
						}
						
						//获取组织
						JSONArray grop=jsonObjectHYrole.getJSONArray("GROUPS");//组织
						for(int a = 0; a < grop.size(); a++){
					    	String gpId=grop.getJSONObject(a).getString("GROUPID");
					    	if(a != 0){
					    		groupsids.append(",");	
					    	}
					    	groupsids.append(gpId);
						}
					}
					System.out.println("========================================================================");
					Util.log("用户组织id："+groupsids,"logkafka",0);
					Util.log("用户角色id："+roelsids,"logkafka",0);
					System.out.println("用户组织id："+groupsids);
					System.out.println("用户角色id："+roelsids);
					System.out.println("========================================================================");
					//开始更新用户
					String sServiceIdDE = "gov_user";
					String sMethodNameDE = "saveUser";
					map.put("CurrUserName", USER); // 当前操作的用户
					map.put("USERID", userid);
					map.put("ROLEIDS", roelsids.toString());
					map.put("GROUPIDS", groupsids.toString());
					Util.log("更新用户前map："+map,"logkafka",0);
					System.out.println("更新用户前map："+map);
					String hyUP=HyUtil.dataMoveDocumentHyRbj(sServiceIdDE,sMethodNameDE,map);
					Util.log("更新用户接口saveUser返回信息："+hyUP,"logkafka",0);
					System.out.println("更新用户返回结果：："+hyUP);
					JSONObject jsonObjectHYUP = JSON.parseObject(hyUP);
					Object dataArrHYUP = jsonObjectHYUP.get("ISSUCCESS");
					if("true".equals(dataArrHYUP)){
						//查询密码是否需要重置
						JDBC  jdbc=new JDBC();
						String  psd=jdbc.JDBCDriverSelectUserPassword("select * from kpuser where username='"+USERNAME+"'");
						Util.log("旧密码：："+psd,"logkafka",0);
						Util.log("新密码：："+psssword,"logkafka",0);
						System.out.println("旧密码：："+psd);

						//密码需要重置
						//编辑成功后 重置密码
						String jiami = Base64.encode(psssword.getBytes("utf-8"));
				        System.out.println("加密后的内容：" + jiami);
						//开始重置密码
				        Map<String, String> mappass=new HashMap<>();
						String sServicePass = "gov_user";
						String sMethodPass = "resetPassword";
						mappass.put("CurrUserName", USER); // 当前操作的用户
						mappass.put("USERIDS", userid);
						mappass.put("oldPassword", "__encoded__");
						mappass.put("useRandomPassword", "0");
						mappass.put("newPassword", "__encoded__"+jiami);
						mappass.put("againPassword", "__encoded__"+jiami);
						Util.log("重置密码mappass："+mappass,"logkafka",0);
						System.out.println("重置密码mappass："+mappass);
						String hyPass=HyUtil.dataMoveDocumentHyRbj(sServicePass,sMethodPass,mappass);
						System.out.println("重置密码返回结果：："+hyPass);
						JSONObject jsonObjectPass = JSON.parseObject(hyPass);
						Object dataArrPass = jsonObjectPass.get("ISSUCCESS");
						if("true".equals(dataArrPass)){
							//重置密码成功，更新中间表
							Integer  uppsd=jdbc.JDBCDriver("update kpuser set password='"+psssword+"' , userid='"+kpuserid+"' where username='"+USERNAME+"'");	
							System.out.println(uppsd);
							if(uppsd == 1){
								Util.log("中间表密码重置成功","logkafka",0);
								Util.log("编辑"+USERNAME+"成功！","logkafka",0);
								System.out.println("编辑"+USERNAME+"成功！");
								Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
								parms = KafkaUtil.buildSync(parms, fromJson);
								pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
							}else{
								Util.log("编辑"+USERNAME+"失败！:::"+"更新中间表密码失败！！！","logkafka",0);
								System.out.println("编辑"+USERNAME+"失败！:::"+"更新中间表密码失败！！！");
								//失败 返回失败的信息
								Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
								parms = KafkaUtil.buildSyncFalse(parms, fromJson,"更新中间表密码失败！！！");
								pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
							}

						}else{
							Util.log("编辑"+USERNAME+"失败！:::"+jsonObjectPass.get("MSG").toString(),"logkafka",0);
							System.out.println("编辑"+USERNAME+"失败！:::"+jsonObjectPass.get("MSG").toString());
							//失败 返回失败的信息
							Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
							parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectPass.get("MSG").toString());
							pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
						}
					
					}else{
						Util.log("编辑"+USERNAME+"失败！:::"+jsonObjectHYUP.get("MSG").toString(),"logkafka",0);
						System.out.println("编辑"+USERNAME+"失败！:::"+jsonObjectHYUP.get("MSG").toString());
						//失败 返回失败的信息
						Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
						parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHYUP.get("MSG").toString());
						pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
					}
				}else{
					Util.log("调用发布平台接口返回无用户","logkafka",0);
					System.out.println("调用发布平台接口返回无用户");
					Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
					parms = KafkaUtil.buildSyncFalse(parms, fromJson,"调用发布平台接口返回无用户");
					pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
				}
				
				
			}
			
		}
	}

	
	/**
	 * 增加组织
	 * @param kpid
	 * @param parentNames
	 * @param name
	 * @param shortName
	 * @param parms
	 * @param fromJson
	 * @throws Exception
	 */
	public void  addGroup(String kpid,String parentNames,String name,String shortName,Map<String, Object> parms,Map<String, Object> fromJson)throws Exception{
		
		//1级机构 查询一级机构
		String sServiceId = "gov_group";
		String sMethodName = "queryGroups";
		HashMap<String, String> oPostData = new HashMap<String, String>();
		oPostData.put("CurrUserName",USER); // 当前操作的用户
		oPostData.put("PAGESIZE",pageSize);
		oPostData.put("PARENTID", "0");
//		oPostData.put("PAGEINDEX", "1");
//		oPostData.put("containsChildren", "true");
		String hyGp=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		Util.log("查询机构接口返回信息："+hyGp,"logkafka",0);
		JSONObject jsonObjectHYSE = JSON.parseObject(hyGp);
		Object dataArrHYSE = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArrHYSE.toString());
		JSONArray hySEGP=jsonObjectHYGPSE.getJSONArray("DATA");
		if("true".equals(jsonObjectHYSE.get("ISSUCCESS"))){
			//拆分添加机构的机构名称路径
			String GNAME=null;
			String GROUPID="0";
			if(parentNames==null||"".equals(parentNames)||parentNames.length()==0){
					System.out.println("parentNames.length:"+parentNames.length());
					System.out.println("parentNames:"+parentNames);
					int sig=0;
					for(int d=0;d<hySEGP.size();d++){
						JSONObject dataBeanHYSEGP = (JSONObject) hySEGP.get(d);
						//一级名称
						GNAME = dataBeanHYSEGP.getString("GNAME");
						if(GNAME.equals(name)){
							//有相同的代表存在该组织
							sig=1;
							//一级的id
							GROUPID = dataBeanHYSEGP.getString("GROUPID");	
						}
					}
				
					if(sig == 1){
						Util.log("该"+GNAME+"机构已经存在","logkafka",0);
						//失败 返回失败的信息
						Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
						parms = KafkaUtil.buildSyncFalse(parms, fromJson,"该机构已经存在");
						pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
					}else {
						//存入发布平台数据库
						String sServiceIdSave = "gov_group";
						String sMethodNameSave = "saveGroup";
						HashMap<String, String> oPostDataSave = new HashMap<String, String>();
						oPostDataSave.put("CurrUserName",USER); // 当前操作的用户
						oPostDataSave.put("GROUPID","0");
						oPostDataSave.put("GNAME", name);
						oPostDataSave.put("PARENTID", "0");
						oPostDataSave.put("GDESC", shortName);
						String hyGpSave=HyUtil.dataMoveDocumentHyRbj(sServiceIdSave,sMethodNameSave,oPostDataSave);
						Util.log("新建机构saveGroup接口返回信息："+hyGpSave,"logkafka",0);
						JSONObject jsonObjectHYSave = JSON.parseObject(hyGpSave);
						Object dataArrHYSAVE = jsonObjectHYSave.get("ISSUCCESS");//根据json对象中数组的名字解析出其所对应的值
						Object MSG = jsonObjectHYSave.get("MSG");
						JSONObject dataBeanHYID = JSON.parseObject(jsonObjectHYSave.get("DATA").toString());
//						System.out.println("MSG:"+MSG);
						if("true".equals(dataArrHYSAVE)){
							//组织id
							Integer SITEID=Integer.parseInt(dataBeanHYID.getString("GROUPID"));
							JDBC  jdbc=new JDBC();
							Util.log("添加机构成功，开始插入中间表数据："+"存入sql："+"INSERT INTO "+TABLE+"( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+kpid+"','"+SITEID+"','BM','0')","logkafka",0);
							System.out.println("INSERT INTO "+TABLE+"( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+kpid+"','"+SITEID+"','BM','0')");
							Integer jd=jdbc.JDBCDriver("INSERT INTO "+TABLE+"( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+kpid+"','"+SITEID+"','BM','0')");
							if(jd!=-1){
								Util.log("中间表存入成功!","logkafka",0);
								System.out.println("中间表存入成功!");
							}
							Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
							parms = KafkaUtil.buildSync(parms, fromJson);
							pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
						}else{
							Util.log("添加机构失败："+jsonObjectHYSave.get("MSG"),"logkafka",0);
							System.out.println("添加机构失败："+jsonObjectHYSave.get("MSG"));
							//失败 返回失败的信息
							Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
							parms = KafkaUtil.buildSyncFalse(parms, fromJson,MSG.toString());
							System.out.println("parms:"+parms);
							pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
						}
					}
			}else{

//						parentNames=parentNames.substring(0,parentNames.length()-1);
						String [] SPN=parentNames.split(","); 
						int size=SPN.length - 1;
						for (int i = 0; i < SPN.length; i++) { 
							//1级机构 查询一级机构
							String sServiceId1 = "gov_group";
							String sMethodName1 = "queryGroups";
							HashMap<String, String> oPostData1 = new HashMap<String, String>();
							oPostData1.put("CurrUserName",USER); // 当前操作的用户
							oPostData1.put("PAGESIZE",pageSize);
							oPostData1.put("PARENTID", GROUPID);
			//				oPostData.put("PAGEINDEX", "1");
			//				oPostData.put("containsChildren", "true");
							String hyGp1=HyUtil.dataMoveDocumentHyRbj(sServiceId1,sMethodName1,oPostData1);
							Util.log("查询机构接口返回信息："+hyGp1,"logkafka",0);
							JSONObject jsonObjectHYSE1 = JSON.parseObject(hyGp1);
							Object dataArrHYSE1 = jsonObjectHYSE1.get("DATA");//根据json对象中数组的名字解析出其所对应的值
							JSONObject jsonObjectHYGPSE1 = JSON.parseObject(dataArrHYSE1.toString());
							JSONArray hySEGP1=jsonObjectHYGPSE1.getJSONArray("DATA");
							Util.log("多级机构添加："+parentNames,"logkafka",0);
							   System.out.println("多级机构名称--->"+parentNames);
							   String spname=SPN[i];
							   for(int d=0;d<hySEGP1.size();d++){
									JSONObject dataBeanHYSEGP = (JSONObject) hySEGP1.get(d);
									//一级名称
									GNAME = dataBeanHYSEGP.getString("GNAME");
									if(GNAME.equals(spname)){
										GROUPID = dataBeanHYSEGP.getString("GROUPID");	
									}
								}
							   if(GROUPID.equals("0")){
								   Util.log("机构不存在："+spname,"logkafka",0);
								   System.out.println("机构不存在---"+spname);
									//失败 返回失败的信息
									Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
									parms = KafkaUtil.buildSyncFalse(parms, fromJson,"添加机构失败：添加机构"+spname+"不存在");
									pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
							   }else if(i == size){
									String sServiceId2 = "gov_group";
									String sMethodName2 = "queryGroups";
									HashMap<String, String> oPostData2 = new HashMap<String, String>();
									oPostData2.put("CurrUserName",USER); // 当前操作的用户
									oPostData2.put("PAGESIZE",pageSize);
									oPostData2.put("PAGEINDEX", "1");
									oPostData2.put("PARENTID", GROUPID);
									oPostData2.put("SEARCHFIELDS","GNAME -"+ name);
//									oPostData2.put("containsChildren", "true");
									String hyGp2=HyUtil.dataMoveDocumentHyRbj(sServiceId2,sMethodName2,oPostData2);
									Util.log("查询机构接口queryGroups返回信息："+hyGp2,"logkafka",0);
									JSONObject jsonObjectHYSE2 = JSON.parseObject(hyGp2);
									Object dataArrHYSE2 = jsonObjectHYSE2.get("DATA");//根据json对象中数组的名字解析出其所对应的值
									JSONObject jsonObjectHYGPSE2 = JSON.parseObject(dataArrHYSE2.toString());
									JSONArray hySEGP2=jsonObjectHYGPSE2.getJSONArray("DATA");
									if("true".equals(jsonObjectHYSE2.get("ISSUCCESS"))){
										if(i == SPN.length-1){
											int lk=0;
											for (int l=0;l<hySEGP2.size();l++){
												JSONObject dataBean2 = (JSONObject) hySEGP2.get(l);
												//一级名称
												GNAME = dataBean2.getString("GNAME");
												if(GNAME.equals(name)){
													//代表存在
													lk=1;
												}
											}
											if(lk==0){
												//没有 那么代表该级机构下没有相同机构 添加
												String sServiceIdSave = "gov_group";
												String sMethodNameSave = "saveGroup";
												HashMap<String, String> oPostDataSave = new HashMap<String, String>();
												oPostDataSave.put("CurrUserName",USER); // 当前操作的用户
												oPostDataSave.put("GROUPID","0");
												oPostDataSave.put("GNAME", name);
												oPostDataSave.put("PARENTID", GROUPID);
												oPostDataSave.put("GDESC", shortName);
												String hyGpSave=HyUtil.dataMoveDocumentHyRbj(sServiceIdSave,sMethodNameSave,oPostDataSave);
												Util.log("添加机构接口saveGroup返回信息："+hyGpSave,"logkafka",0);
												JSONObject jsonObjectHYSave = JSON.parseObject(hyGpSave.toString());
//												JSONArray dataArrHYSAVE=jsonObjectHYSave.getJSONArray("ISSUCCESS");
												Object dataArrHYSAVE = jsonObjectHYSave.get("ISSUCCESS");//根据json对象中数组的名字解析出其所对应的值
												if("true".equals(dataArrHYSAVE)){
													Util.log("添加机构成功!","logkafka",0);
													System.out.println("添加机构成功!");	
													JSONObject  dataArrID = (JSONObject) jsonObjectHYSave.get("DATA");
													Integer SITEID=Integer.parseInt(dataArrID.getString("GROUPID"));
													JDBC  jdbc=new JDBC();
													Util.log("存入中间表："+"存入sql："+"INSERT INTO "+TABLE+"( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+kpid+"','"+SITEID+"','BM',"+GROUPID+")","logkafka",0);
													System.out.println("存入sql："+"INSERT INTO "+TABLE+"( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+kpid+"','"+SITEID+"','BM',"+GROUPID+")");
													Integer jd=jdbc.JDBCDriver("INSERT INTO "+TABLE+"( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+kpid+"','"+SITEID+"','BM',"+GROUPID+")");
													if(jd!=-1){
														Util.log("中间表存入成功!","logkafka",0);
														System.out.println("中间表存入成功!");
													}
													Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
													parms = KafkaUtil.buildSync(parms, fromJson);
													pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
												}else{
													Util.log("添加机构失败："+jsonObjectHYSave.get("MSG"),"logkafka",0);
													System.out.println("添加机构失败："+jsonObjectHYSave.get("MSG"));
													//失败 返回失败的信息
													Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
													parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHYSave.get("MSG").toString());
													pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
												}
											}
											else{
												Util.log("已经存在机构---"+name,"logkafka",0);
												System.out.println("已经存在机构---"+name);
												//失败 返回失败的信息
												Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
												parms = KafkaUtil.buildSyncFalse(parms, fromJson,"添加机构失败：已经存在机构");
												pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
											}
										}else{
											for(int d=0;d<hySEGP2.size();d++){
												JSONObject dataBeanHYSEGP = (JSONObject) hySEGP2.get(d);
												//有相同的代表存在该组织  给下一级父组织id赋值
												GROUPID=dataBeanHYSEGP.getString("GROUPID");
											}
										}
										
									}else {
										Util.log("添加机构失败："+jsonObjectHYGPSE2.get("MSG"),"logkafka",0);
										System.out.println("添加机构失败："+jsonObjectHYGPSE2.get("MSG"));
										//失败 返回失败的信息
										Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
										parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHYGPSE2.get("MSG").toString());
										pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
									}
							   }
							
					}
			}
		
		}else{
			Util.log("添加机构失败："+jsonObjectHYGPSE.getJSONArray("MSG"),"logkafka",0);
			System.out.println("添加机构失败："+jsonObjectHYGPSE.getJSONArray("MSG"));
			//失败 返回失败的信息
			Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
			parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHYGPSE.getJSONArray("MSG").toString());
			pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
		}
	}

	/**
	 * 更新组织接口
	 * @param kpid
	 * @param name
	 * @param shortName
	 * @param parms
	 * @param fromJson
	 * @throws Exception
	 */
	public void  updataGroup(String kpid,String name,String shortName,Map<String, Object> parms,Map<String, Object> fromJson)throws Exception{
		
		JDBC  jdbc=new JDBC();
		String  kSitId=jdbc.JDBCDriverSelect("SELECT * FROM "+TABLE+" where "+SOURCE+"='BM' and "+FIELDKP+"='"+kpid+"'");
		String  PARENTID=jdbc.JDBCDriverSelectPARENTID("SELECT * FROM "+TABLE+" where "+SOURCE+"='BM' and "+FIELDKP+"='"+kpid+"'");
		Util.log("更新机构调用中间表查询机构id："+"SELECT * FROM "+TABLE+" where "+SOURCE+"='BM' and "+FIELDKP+"='"+kpid+"'","logkafka",0);
		if(kSitId==null||kSitId==""){
			//没有该修改部门
			Util.log("修改机构失败：没有该修改部门","logkafka",0);
			System.out.println("修改机构失败：没有该修改部门");
			//失败 返回失败的信息
			Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
			parms = KafkaUtil.buildSyncFalse(parms, fromJson,"没有该修改部门");
			pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
		}else{
			
			StringBuffer roelsgroup=new StringBuffer();
			//根据组织id 查找组织的权限
			String sServiceIdSaveG = "gov_group";
			String sMethodNameSaveG = "findGroupById";
			HashMap<String, String> oPostDataF = new HashMap<String, String>();
			oPostDataF.put("CurrUserName",USER); // 当前操作的用户
			oPostDataF.put("GROUPID",kSitId);
			String Gprole=HyUtil.dataMoveDocumentHyRbj(sServiceIdSaveG,sMethodNameSaveG,oPostDataF);
			System.out.println("更新组织的权限：：：：："+Gprole);
			JSONObject jsonObjectrole = JSON.parseObject(Gprole);
			Object dataArrrole = jsonObjectrole.get("DATA");//根据json对象中数组的名字解析出其所对应的值
			if(dataArrrole!=null){
				JSONObject jsonObjectHYrole = JSON.parseObject(dataArrrole.toString());
				//获取到角色
				JSONArray DOCRELPIC=jsonObjectHYrole.getJSONArray("ROLES");//相角色
				for(int i = 0; i < DOCRELPIC.size(); i++){
			    	String role=DOCRELPIC.getJSONObject(i).getString("ROLEID");
			    	if(i != 0){
			    		roelsgroup.append(",");	
			    	}
			    	roelsgroup.append(role);
				}
			}
			//更新到发布平台数据库
			String sServiceIdSave = "gov_group";
			String sMethodNameSave = "saveGroup";
			HashMap<String, String> oPostDataSave = new HashMap<String, String>();
			oPostDataSave.put("CurrUserName",USER); // 当前操作的用户
			oPostDataSave.put("GROUPID",kSitId);
			oPostDataSave.put("GNAME", name);
			oPostDataSave.put("PARENTID", PARENTID);
			oPostDataSave.put("GDESC", shortName);
			oPostDataSave.put("ROLEIDS", roelsgroup.toString());
			System.out.println("更新组织前调用参数：：：：："+oPostDataSave);
			String hyGpSave=HyUtil.dataMoveDocumentHyRbj(sServiceIdSave,sMethodNameSave,oPostDataSave);
			Util.log("更新机构接口saveGroup返回信息："+hyGpSave,"logkafka",0);
			JSONObject jsonObjectHYSave = JSON.parseObject(hyGpSave);
			Object dataArrHYSAVE = jsonObjectHYSave.get("ISSUCCESS");//根据json对象中数组的名字解析出其所对应的值
			if("true".equals(dataArrHYSAVE)){
				Util.log("修改机构成功!","logkafka",0);
				System.out.println("修改机构成功!");	
				Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
				parms = KafkaUtil.buildSync(parms, fromJson);
				pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
			}else{
				Util.log("修改机构失败："+jsonObjectHYSave.get("MSG"),"logkafka",0);
				System.out.println("修改机构失败："+jsonObjectHYSave.get("MSG"));
				//失败 返回失败的信息
				Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
				parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHYSave.get("MSG").toString());
				pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
			}
		}
		
		Util.log("----同步更新机构结束----","logkafka",0);
		System.out.println("----同步更新机构结束----");
	}
	
	/**
	 * 删除组织接口
	 * @param kpid
	 * @param parms
	 * @param fromJson
	 * @throws Exception
	 */
	public void  deleteGroup(String kpid,Map<String, Object> parms,Map<String, Object> fromJson)throws Exception{
		
		//从中间表获取到海云中组织得id
		JDBC  jdbc=new JDBC();
		String kSitId=jdbc.JDBCDriverSelect("SELECT * FROM "+TABLE+" where "+SOURCE+"='BM' and "+FIELDKP+"='"+kpid+"'");
		Util.log("删除机构：查询中间表中机构id："+"SELECT * FROM "+TABLE+" where "+SOURCE+"='BM' and "+FIELDKP+"='"+kpid+"'","logkafka",0);
		if(kSitId==null||kSitId==""){
			//没有该修改部门
			Util.log("删除机构失败："+"没有该删除机构","logkafka",0);
			System.out.println("删除机构失败："+"没有该删除机构");
			//失败 返回失败的信息
			Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
			parms = KafkaUtil.buildSyncFalse(parms, fromJson,"没有该删除机构");
			pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
		}else{
			//发布平台数据库  删除
			String sServiceIdSave = "gov_group";
			String sMethodNameSave = "removeGroups";
			HashMap<String, String> oPostDataSave = new HashMap<String, String>();
			oPostDataSave.put("CurrUserName",USER); // 当前操作的用户
			oPostDataSave.put("GROUPIDS",kSitId);
			String hyGpSave=HyUtil.dataMoveDocumentHyRbj(sServiceIdSave,sMethodNameSave,oPostDataSave);
			Util.log("删除接口removeGroups返回信息："+hyGpSave,"logkafka",0);
			JSONObject jsonObjectHYSave = JSON.parseObject(hyGpSave);
			Object dataArrHYSAVE = jsonObjectHYSave.get("ISSUCCESS");//根据json对象中数组的名字解析出其所对应的值
			if("true".equals(dataArrHYSAVE)){
				Integer delete=jdbc.JDBCDriver("DELETE  FROM "+TABLE+" where "+SOURCE+"='BM' and "+FIELDHY+"='"+kSitId+"'");
				if(delete==1){
					Util.log("中间表删除成功!","logkafka",0);
					System.out.println("中间表删除成功!");
				}else{
					Util.log("中间表删除失败!","logkafka",0);
					System.out.println("中间表删除失败!");
				}
				Util.log("删除中间表："+"DELETE  FROM "+TABLE+" where "+SOURCE+"='BM' and "+FIELDHY+"='"+kSitId+"'","logkafka",0);
				Util.log("删除机构成功!","logkafka",0);
				System.out.println("删除机构成功!");	
				Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
				parms = KafkaUtil.buildSync(parms, fromJson);
				pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
			}else{
				Util.log("删除机构失败："+jsonObjectHYSave.get("MSG"),"logkafka",0);
				System.out.println("删除机构失败："+jsonObjectHYSave.get("MSG"));
				//失败 返回失败的信息
				Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
				parms = KafkaUtil.buildSyncFalse(parms, fromJson,jsonObjectHYSave.get("MSG").toString());
				pro.simpleAddQueue(JsonMapper.getInstance().toJson(parms));
			}
		}
		Util.log("----同步删除机构结束----","logkafka",0);
		System.out.println("----同步删除机构结束----");
	}

}
