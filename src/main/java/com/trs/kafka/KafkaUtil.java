package com.trs.kafka;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class KafkaUtil {
	
	/**
	 * 消息方式同步数据封装
	 * @param parms
	 * @param syncData
	 * @param appApps
	 * @return
	 */
	public static Map<String,Object> buildSync(Map<String,Object> parms,Map<String, Object> fromJson){
		long currentTimeMillis = System.currentTimeMillis();
		parms.put("version", "1.0");
		parms.put("replyMessageId", UUID.randomUUID().toString().replaceAll("-", ""));
		parms.put("type", getType(fromJson.get("type").toString()));
		parms.put("timestamp", currentTimeMillis);
		parms.put("expiryTime", getInvalidTime(currentTimeMillis));
		parms.put("messageId", fromJson.get("messageId"));
		parms.put("sourceApp","WCM");
		parms.put("targetApp", fromJson.get("targetApp"));

		return syncDataHandle(parms, fromJson);
			
	}
	public static Map<String,Object> buildSyncFalse(Map<String,Object> parms,Map<String, Object> fromJson,String ids){
		long currentTimeMillis = System.currentTimeMillis();
		parms.put("version", "1.0");
		parms.put("replyMessageId", UUID.randomUUID().toString().replaceAll("-", ""));
		parms.put("type",getType(fromJson.get("type").toString()));
		parms.put("timestamp", currentTimeMillis);
		parms.put("expiryTime", getInvalidTime(currentTimeMillis));
		parms.put("messageId", fromJson.get("messageId"));
		parms.put("sourceApp", "WCM");
		parms.put("targetApp", fromJson.get("targetApp"));
		return syncDataHandleFalse(parms, fromJson,ids);
	}
	/**
	 * 处理同步数据  失败返回消息
	 * @param parms
	 * @param fromJson
	 * @return
	 * @throws MyException 
	 */
	public static Map<String,Object> syncDataHandleFalse(Map<String,Object> parms,Map<String, Object> fromJson,String ids){
		String type = fromJson.get("type").toString();
		Map<String, Object> data = KafkaUtil.json2Map(fromJson.get("data").toString());
		Map<String, Object> params = new HashMap<String, Object>();
//		System.out.println("海云type信息："+type);
		if("C01".equals(type) || "C03".equals(type)){
			//添加、更新部门
			
			params.put("result", "false");
			params.put("desc", ids);
			params.put("deptId", data.get("deptId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("name", data.get("name"));
			params.put("parentNames", data.get("parentNames"));
			params.put("parentId", data.get("parentId"));
			params.put("parentNo", data.get("parentNo"));
			parms.put("data", params);
		}else if("C02".equals(type)){
			//删除部门
			
			params.put("result", "false");
			params.put("desc", ids);
			params.put("deptId", data.get("deptId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("name", data.get("name"));
			params.put("parentNames", data.get("parentNames"));
			params.put("parentId", data.get("parentId"));
			params.put("parentNo", data.get("parentNo"));
			parms.put("data", params);
		}else if("C04".equals(type) || "C06".equals(type) || "C07".equals(type) || "C08".equals(type)){
			//添加   C04   、更新用户   C06   、   启用用户  C07    禁用用户  C08   
			
			//返回数据封装
			params.put("result", "false");
			params.put("desc", ids);
			params.put("userId", data.get("userId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("userName", data.get("userName"));
			params.put("loginName", data.get("loginName"));
			
			parms.put("data", params);
		}else if("C05".equals(type)){
			//删除用户
			
			
			params.put("result", "false");
			params.put("desc", ids);
			params.put("userId", data.get("userId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("userName", data.get("userName"));
			params.put("loginName", data.get("loginName"));
			parms.put("data", params);
		}else if("C12".equals(type)){
			//退出
			
			params.put("result", "false");
			params.put("desc", ids);
			params.put("userId", data.get("userId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("userName", data.get("userName"));
			params.put("loginName", data.get("loginName"));
			parms.put("data", params);
		}else if("D01".equals(type)){
			
			long currentTimeMillis = System.currentTimeMillis();
			params.put("result", "false");
			params.put("desc", ids);
			params.put("appState", "stop");
			params.put("appName", "WCM");
			params.put("statTime", currentTimeMillis);
			
			parms.put("data", params);
		}
		System.out.println("parms："+parms);
		return parms;
	}
	
	
	/**
	 * 处理同步数据
	 * @param parms
	 * @param fromJson
	 * @return
	 * @throws MyException 
	 */
	public static Map<String,Object> syncDataHandle(Map<String,Object> parms,Map<String, Object> fromJson){
		String type = fromJson.get("type").toString();
		Map<String, Object> data = KafkaUtil.json2Map(fromJson.get("data").toString());
		Map<String, Object> params = new HashMap<String, Object>();
		if("C01".equals(type) || "C03".equals(type)){
			//添加、更新部门
			
			params.put("result", "true");
			params.put("desc", "Successfully");
			params.put("deptId", data.get("deptId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("name", data.get("name"));
			params.put("parentNames", data.get("parentNames"));
			params.put("parentId", data.get("parentId"));
			params.put("parentNo", data.get("parentNo"));
			parms.put("data", params);
		}else if("C02".equals(type)){
			//删除部门
			
			params.put("result", "true");
			params.put("desc", "Successfully");
			params.put("deptId", data.get("deptId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("name", data.get("name"));
			params.put("parentNames", data.get("parentNames"));
			params.put("parentId", data.get("parentId"));
			params.put("parentNo", data.get("parentNo"));
			parms.put("data", params);
		}else if("C04".equals(type) || "C06".equals(type) || "C07".equals(type) || "C08".equals(type)){
			//添加   C04   、更新用户   C06   、   启用用户  C07    禁用用户  C08   
			
			//返回数据封装
			params.put("result", "true");
			params.put("desc", "Successfully");
			params.put("userId", data.get("userId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("userName", data.get("userName"));
			params.put("loginName", data.get("loginName"));
			
			parms.put("data", params);
		}else if("C05".equals(type)){
			//删除用户
			
			
			params.put("result", "true");
			params.put("desc", "Successfully");
			params.put("userId", data.get("userId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("userName", data.get("userName"));
			params.put("loginName", data.get("loginName"));
			parms.put("data", params);
		}else if("C12".equals(type)){
			//退出
			
			params.put("result", "false");
			params.put("desc", "Successfully");
			params.put("userId", data.get("userId"));
			params.put("codeNum", data.get("codeNum"));
			params.put("userName", data.get("userName"));
			params.put("loginName", data.get("loginName"));
			parms.put("data", params);
		}else if("D01".equals(type)){
			
			long currentTimeMillis = System.currentTimeMillis();
			params.put("result", "true");
			params.put("desc", "Successfully");
			params.put("appState", "running");
			params.put("appName", "WCM");
			params.put("statTime", currentTimeMillis);
			
			parms.put("data", params);
		}
		return parms;
	}
	
	/**
	 * 计算消息过期时间
	 * @param currentTimeMillis 消息生成时间
	 * @param key 系统配置中的过期时间配置的key值
	 * @return
	 */
	private static Long getInvalidTime(long currentTimeMillis){
		//这里默认了20分钟，根据实际情况修改
		currentTimeMillis = currentTimeMillis + 20*60000;
		return currentTimeMillis;
	}
	/**
	 * 用户、部门同步返回结果时获取同步类型
	 * @param syncData
	 * @return
	 */
	private static String getType(String syncType){
			switch (syncType){
				case "C01":
					return EnumMessageCode.valueOf("RESADDDEPT").getValue();
				case "C02":
					return EnumMessageCode.valueOf("RESDELDEPT").getValue();
				case "C03":
					return EnumMessageCode.valueOf("RESEDITDEPT").getValue();
				case "C04":
					return EnumMessageCode.valueOf("RESADDUSER").getValue();
				case "C05":
					return EnumMessageCode.valueOf("RESDELUSER").getValue();
				case "C06":
					return EnumMessageCode.valueOf("RESEDITUSER").getValue();
				case "C07":
					return EnumMessageCode.valueOf("RESENUSER").getValue();
				case "C08":
					return EnumMessageCode.valueOf("RESDISUSER").getValue();
				case "D01":
					return EnumMessageCode.valueOf("RESSERVICESTATE").getValue();
				case "T01":
					return EnumMessageCode.valueOf("RESPENDINGTASK").getValue();
				case "C12":
					return EnumMessageCode.valueOf("RESUSERLOGOUT").getValue();
			}
		return "error";
	}
	
	/**
     * 将json字符串转为Map结构
     * 如果json复杂，结果可能是map嵌套map
     * @param jsonStr 入参，json格式字符串
     * @return 返回一个map
     */
    public static Map<String, Object> json2Map(String jsonStr) {
        Map<String, Object> map = new HashMap<String, Object>();
        if(jsonStr != null && !"".equals(jsonStr)){
            //最外层解析
            JSONObject json = JSONObject.fromObject(jsonStr);
            for (Object k : json.keySet()) {
                Object v = json.get(k);
                //如果内层还是数组的话，继续解析
                if (v instanceof JSONArray) {
                    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
                    @SuppressWarnings("unchecked")
					Iterator<JSONObject> it = ((JSONArray) v).iterator();
                    while (it.hasNext()) {
                        JSONObject json2 = it.next();
                        list.add(json2Map(json2.toString()));
                    }
                    map.put(k.toString(), list);
                } else {
                    map.put(k.toString(), v);
                }
            }
            return map;
        }else{
            return null;
        }
    }
	

}
