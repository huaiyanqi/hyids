package com.trs.kafka;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import com.trs.kafka.KafkaUtil;
import com.trs.oauth.ConstantUtil;
import net.sf.json.JSONObject;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

/**
 * @Date 2018.06.05
 *
 * @Note Kafka Consumer
 */
public class Consumer extends Thread {
    private KafkaConsumer<String, String> consumer;
    
	private static String USERNAMEKAFKA = ConstantUtil.USERNAMEKAFKA;
	private static String PASSWORDKAFKA = ConstantUtil.PASSWORDKAFKA;
	private static String GROUOID = ConstantUtil.GROUOID;
	private static String CLIENTID = ConstantUtil.CLIENTID;	
	private static String IDS_USER_DEFPWD = ConstantUtil.IDS_USER_DEFPWD;
	Govapp gov=new Govapp();
	
    private static final AtomicInteger CONSUMER_CLIENT_ID_SEQUENCE = new AtomicInteger(1);
    
    //bootstrapServers  kafka服务器ip
    public Consumer(List<String> topic,String bootstrapServers) {
        consumer = this.consumerConfig(bootstrapServers,topic);
    }

    private KafkaConsumer<String, String> consumerConfig(String bootstrapServers,List<String> topic) {
        Properties props = new Properties();
        props.put("bootstrap.servers", bootstrapServers);
        //组名 不同组名可以重复消费。例如你先使用了组名A消费了kafka的1000条数据，但是你还想再次进行消费这1000条数据，并且不想重新去产生，那么这里你只需要更改组名就可以重复消费了
        props.put("group.id",GROUOID);
        //共性应用消费者者客户端id
        props.put("client.id", CLIENTID+CONSUMER_CLIENT_ID_SEQUENCE.getAndIncrement());
        //是否自动提交，默认为true
        props.put("enable.auto.commit", "true");
        //从poll(拉)的回话处理时长。
        props.put("auto.commit.interval.ms", "1000");
        props.put("key.deserializer", StringDeserializer.class);
        props.put("value.deserializer", StringDeserializer.class);
        props.put("security.protocol", "SASL_PLAINTEXT");
        props.put("sasl.mechanism", "PLAIN");
        props.put("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username='"+USERNAMEKAFKA+"'  password='"+PASSWORDKAFKA+"';");
        KafkaConsumer<String, String> con = new KafkaConsumer<String, String>(props);
        con.subscribe(topic);
        return con;
    }

    @Override
    public void run() {
    	while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100L);
            for (ConsumerRecord<String, String> record : records) {
                try {
					
					Map<String, Object> fromJson = KafkaUtil.json2Map(record.value());
					Map<String,Object> parms = new HashMap<String,Object>();
					Util.log("kafka应用接收数据 ---type="+fromJson.get("type").toString(),"logkafka",0);
					Util.log("kafka数据 ="+record.value(),"logkafka",0);
					System.out.println("应用接收数据 ---type="+fromJson.get("type").toString());
					System.out.println("数据 ="+record.value());
					//截取 type 值的首字母 判断  属于哪类消息
					String type = fromJson.get("type").toString().substring(0,1);
					//N 通知类  D 数据类  T 任务类  C  协作类  R  响应结果类
					if("C".equals(type)){
						Util.log("----------------进入Ctype操作开始-----------------","logkafka",0);
						System.out.println("----------------进入Ctype操作开始-----------------");
						if("C12".equals(fromJson.get("type").toString())){
							Util.log("----用户登出----","logkafka",0);
							System.out.println("----用户登出----");
							Map data = JSONObject.fromObject(fromJson.get("data"));
							String loginName= data.get("loginName").toString();
							
							//调用平台接口退出登录
							gov.loginOUT(loginName);
							
						}else if("C07".equals(fromJson.get("type").toString())){
							Util.log("----启用用户----","logkafka",0);
							System.out.println("----启用用户----");
							//获取需要的值
							Map data = JSONObject.fromObject(fromJson.get("data"));
							String loginName= data.get("loginName").toString();
							
							//调用平台接口启用用户
							gov.enableUsers(loginName, parms, fromJson);
							
						}else if("C08".equals(fromJson.get("type").toString())){
							Util.log("----停用用户----","logkafka",0);
							System.out.println("----停用用户----");
							//获取需要的值
							Map data = JSONObject.fromObject(fromJson.get("data"));
							String loginName= data.get("loginName").toString();
							
							//调用平台接口停用用户
							gov.disableUsers(loginName, parms, fromJson);
						
						}else
						if("C04".equals(fromJson.get("type").toString())){
							Util.log("----同步添加用户----","logkafka",0);
							System.out.println("----同步添加用户----");
							//用户同步，获取数据，存入数据
							Map<String, String> map=new HashMap<String, String>();
							
							//获取需要的值
							Map data = JSONObject.fromObject(fromJson.get("data"));
							String USERNAME= data.get("loginName").toString();
							String password=data.get("enablePassword").toString();
							String kpuserid=data.get("userId").toString();
							map.put("USERNAME",USERNAME);
								//密码
							map.put("PASSWORD",data.get("enablePassword").toString());
								//邮箱
							map.put("EMAIL", data.get("email").toString());
								//真实姓名
							map.put("TRUENAME",data.get("userName").toString());
								//电话
							map.put("MOBILE",data.get("mobile").toString());
							Util.log("添加用户为："+USERNAME,"logkafka",0);
							System.out.println("添加用户为："+USERNAME);
							Util.log("添加用户为："+USERNAME,"logkafka",0);
							Util.log("添加用户kpid："+kpuserid,"logkafka",0);
							Util.log("更添加用户前调用参数："+map,"logkafka",0);
							//调用平台接口进行新增
							gov.addUser(USERNAME, password,parms, fromJson, map,kpuserid);
							
						}
						//删除用户根据需求开放是否同步
						else if("C05".equals(fromJson.get("type").toString())){
							Util.log("----同步删除用户----","logkafka",0);
							System.out.println("----同步删除用户----");
							//获取需要的值
							Map data = JSONObject.fromObject(fromJson.get("data"));
							String userName= data.get("loginName").toString();
							Util.log("删除用户名称：：：："+userName,"logkafka",0);
							
							//调用平台接口删除用户
							gov.deleteUser(userName, parms, fromJson);

						}
						else if("C06".equals(fromJson.get("type").toString())){
							Util.log("----同步更新用户----","logkafka",0);
							System.out.println("----同步更新用户----");
							Map<String, String> map = new HashMap<String, String>();
							//获取需要的值
							Map data = JSONObject.fromObject(fromJson.get("data"));
							
							String USERNAME= data.get("loginName").toString();
							String psssword=data.get("enablePassword").toString();
							String kpuserid=data.get("userId").toString();
							map.put("USERNAME",USERNAME);
								//邮箱
							map.put("EMAIL", data.get("email").toString());
								//真实姓名
							map.put("TRUENAME",data.get("userName").toString());
								//电话
							map.put("MOBILE",data.get("mobile").toString());
							Util.log("更新用户为："+USERNAME,"logkafka",0);
							Util.log("更新用户kpid："+kpuserid,"logkafka",0);
							Util.log("用户更新的密码：：：："+psssword,"logkafka",0);
							Util.log("更新前调用参数："+map,"logkafka",0);
							System.out.println("更新用户为："+USERNAME);
							System.out.println("更新用户kpid："+kpuserid);
							System.out.println("更新前调用参数："+map);
							//调用平台接口更新用户
							gov.updatUser(USERNAME, parms, fromJson, map,psssword,kpuserid);
							
						}else if("C01".equals(fromJson.get("type").toString())){
							Util.log("----同步机构----","logkafka",0);
							System.out.println("----同步机构----");
							//获取需要的值
							Map data = JSONObject.fromObject(fromJson.get("data"));
							String kpid= data.get("deptId").toString();
							String parentNames=data.get("parentNames").toString();
							String name=data.get("name").toString();
							String shortName=data.get("shortName").toString();
							
							//调用平台接口增加组织
							gov.addGroup(kpid, parentNames, name, shortName, parms, fromJson);
							
							
						}else 
						if("C03".equals(fromJson.get("type").toString())){
							Util.log("----同步更新机构----","logkafka",0);
							System.out.println("----同步更新机构----");
							//获取需要的值
							System.out.println("record.value()："+record.value());
							Map data = JSONObject.fromObject(fromJson.get("data"));
							String kpid= data.get("deptId").toString();
							String name=data.get("name").toString();
							String shortName=data.get("shortName").toString();
							
							//调用平台接口更新组织
							gov.updataGroup(kpid, name, shortName, parms, fromJson);
							
						}else if("C02".equals(fromJson.get("type").toString())){
							Util.log("----同步删除机构开始----","logkafka",0);
							System.out.println("----同步删除机构开始----");
							//获取需要的值
							System.out.println("record.value()："+record.value());
							Map data = JSONObject.fromObject(fromJson.get("data"));
							String kpid= data.get("deptId").toString();
							
							//调用平台接口删除组织
							gov.deleteGroup(kpid, parms, fromJson);
						}
						
					}else if("D".equals(type)){
						// D01  为服务监测
						Util.log("----------------进入Ctype---service操作开始-----------------","servicekafka",0);
						System.out.println("----------------进入Ctype---service操作开始-----------------");
						if("D01".equals(fromJson.get("type").toString())){
							//直接调用接口 查询一个海云得接口 看看海云服务是否还在运行
							Util.log("----开始调用接口判断是否在运行----","servicekafka",0);
							ServiceDetective.ServiceDetective(parms, fromJson);
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
                	
            }
        }
    }
    
}
