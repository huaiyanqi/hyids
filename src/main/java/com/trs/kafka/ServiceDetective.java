package com.trs.kafka;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.HyUtil;
import com.trs.oauth.ConstantUtil;

public class ServiceDetective {
	
	private static String R_QUEUE = ConstantUtil.R_QUEUE;
	private static String BOOTSTRAP_SERVERS = ConstantUtil.BOOTSTRAP_SERVERS;


    /**
     * 调用海云接口  判断海云是否还在运行
     * @param parms
     * @param fromJson
     */
    public static void ServiceDetective(Map<String, Object> parms,Map<String, Object> fromJson){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
//        System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
    	Util.log("--------------------进入判断方法-------------","servicekafka",0);
    	
		// 根据站点id查询下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", "dev"); // 当前操作的用户
		//设置一次查询数量
		mSE.put("pageSize", "1500");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", "80");
		mSE.put("ParentChannelId", "0");
		String hy1=null;
		try {
			Util.log("--------------------调用海云接口-------------","servicekafka",0);
			hy1 = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
			JSONObject jsonObject1 = JSON.parseObject(hy1);
			Util.log("--------------------海云接口返回结果-------------","servicekafka",0);
			Util.log(hy1,"servicekafka",0);
			Util.log("--------------------海云接口返回结果-------------","servicekafka",0);
			if("true".equals(jsonObject1.get("ISSUCCESS"))){
				Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
				parms = KafkaUtil.buildSync(parms, fromJson);
				Util.log("--------------------调用生产者返回消息-------------","servicekafka",0);
				Util.log(JsonMapper.getInstance().toJson(parms),"servicekafka",0);
				Util.log("调用方法时间：：："+df.format(new Date()),"servicekafka",0);
				pro.simpleAddQueueRD(JsonMapper.getInstance().toJson(parms));
			}
		} catch (Exception e) {
			
			Producer pro = new Producer(R_QUEUE,BOOTSTRAP_SERVERS);
			parms = KafkaUtil.buildSyncFalse(parms, fromJson,"服务可能被停止");
			
			Util.log("--------------------调用生产者返回消息-------------","servicekafka",0);
			Util.log(JsonMapper.getInstance().toJson(parms),"servicekafka",0);
			Util.log("调用方法时间：：："+df.format(new Date()),"servicekafka",0);
			pro.simpleAddQueueRD(JsonMapper.getInstance().toJson(parms));
			e.printStackTrace();
		}

    	
    }
    
    

	
	/**
	 * 计算消息过期时间
	 * @param currentTimeMillis 消息生成时间
	 * @param key 系统配置中的过期时间配置的key值
	 * @return
	 */
	private static Long getInvalidTime(long currentTimeMillis){
		//这里默认了20分钟，根据实际情况修改
		currentTimeMillis = currentTimeMillis + 20*60000;
		return currentTimeMillis;
	}
}
