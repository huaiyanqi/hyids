package com.trs.ids;

import java.security.MessageDigest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import com.trs.oauth.ConstantUtil;
import com.trs.oauth.DesEncryptUtil;
import com.trs.oauth.StringHelper;


public class SyncIdsObjectUtil {
	
	private static String IDS_URL= ConstantUtil.IDS_URL;
	private static String IDS_APPNAME = ConstantUtil.IDS_APPNAME;
	private static String IDS_ENCRYPTPWD = ConstantUtil.IDS_ENCRYPTPWD;
	
	public static String getUserGroups(String deptCode){		
		String allGroupFullPaths = "";
		StringBuffer sb = new StringBuffer(30);
		
		JSONArray arr = JSONArray.fromObject(deptCode);	
		for(int i=0; i < arr.size(); i++){
			JSONObject obj = (JSONObject) arr.get(i);
			
			String code = obj.getString("code");			
			String groupId = getsearchObject("Group", "groupCode", code, "groupId");
			//String groupPath = getGroupInfoById(groupId, "groupDN");
			sb.append(groupId + "~~");
		}
		
		allGroupFullPaths = sb.toString();
		if(allGroupFullPaths != null && allGroupFullPaths.length() > 0){
			allGroupFullPaths = allGroupFullPaths.substring(0, allGroupFullPaths.length()-2);
		}
		System.out.println(allGroupFullPaths);
		return allGroupFullPaths ;
	}
	
	/**
	 * 获取指定字段
	 * @param searchObject
	 * @param searchField
	 * @param searchValue
	 * @param resultField
	 */
	public static String getsearchObject(String searchObject, String searchField, String searchValue, String resultField){
		String result = "0";
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=searchObject";		
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("type", "json");
			
			String data = "objectName=" + searchObject 
					+ "&attributeName=" + searchField 
					+ "&attributeValue=" + searchValue;

			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes("UTF-8")); 
			byte[] digestByte = md.digest();
			String digest = StringHelper.toString(digestByte);
			String base64Encoded = new String(Base64.encode(data.getBytes("UTF-8")));
			String dataAfterDESEncode =  DesEncryptUtil.encryptToHex(base64Encoded.getBytes("UTF-8"), IDS_ENCRYPTPWD);
			String finalData = digest + "&" + dataAfterDESEncode;
			methodPost.addParameter("data", finalData);
			
			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			// 拆分摘要和结果信息
			String[] digestAndResult = StringHelper.split(response, "&");
			String digestResult = digestAndResult[1];
			
			// 解密响应结果
			String afterDESResult = DesEncryptUtil.decrypt(digestResult, IDS_ENCRYPTPWD);
			String afterBase64Decode = new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
			System.out.println("==afterBase64Decode:"+afterBase64Decode);

	        JSONObject obj = JSONObject.fromObject(afterBase64Decode);  
	        JSONArray listObj = (JSONArray) obj.get("list");  
	        JSONObject contentObj = (JSONObject) listObj.get(0);
	        
	        result = contentObj.getString(resultField);

	        System.out.println(result);
			methodPost.releaseConnection();		
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	

	/**
	 * 根据组织id查询组织详细信息
	 * @param grpId
	 * @param searchField
	 * @return
	 */
	public static String getGroupInfoById(String grpId, String searchField){
		String grpValue = "";
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=groupQueryByGroupId";		
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("type", "json");
			
			String data = "groupId=" + grpId;

			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes("UTF-8")); 
			byte[] digestByte = md.digest();
			String digest = StringHelper.toString(digestByte);
			String base64Encoded = new String(Base64.encode(data.getBytes("UTF-8")));
			String dataAfterDESEncode =  DesEncryptUtil.encryptToHex(base64Encoded.getBytes("UTF-8"), IDS_ENCRYPTPWD);
			String finalData = digest + "&" + dataAfterDESEncode;
			methodPost.addParameter("data", finalData);
			
			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			// 拆分摘要和结果信息
			String[] digestAndResult = StringHelper.split(response, "&");
			String digestResult = digestAndResult[1];
			
			// 解密响应结果
			String afterDESResult = DesEncryptUtil.decrypt(digestResult, IDS_ENCRYPTPWD);
			String afterBase64Decode = new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
			System.out.println("==afterBase64Decode:"+afterBase64Decode);

	        JSONObject obj = JSONObject.fromObject(afterBase64Decode);  
	        JSONArray listObj = (JSONArray) obj.get("groups");  
	        JSONObject contentObj = (JSONObject) listObj.get(0);
	        
	        grpValue = contentObj.getString(searchField);

	        System.out.println(grpValue);
			methodPost.releaseConnection();		
			
			return grpValue;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return grpValue;
	}
	
	
	public static String getUserInfoByName(String userName, String searchField){
		String userValue = "";
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=userQueryForManage";		
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("type", "json");
			
			String data = "userName=" + userName;

			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes("UTF-8")); 
			byte[] digestByte = md.digest();
			String digest = StringHelper.toString(digestByte);
			String base64Encoded = new String(Base64.encode(data.getBytes("UTF-8")));
			String dataAfterDESEncode =  DesEncryptUtil.encryptToHex(base64Encoded.getBytes("UTF-8"), IDS_ENCRYPTPWD);
			String finalData = digest + "&" + dataAfterDESEncode;
			methodPost.addParameter("data", finalData);
			
			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			// 拆分摘要和结果信息
			String[] digestAndResult = StringHelper.split(response, "&");
			String digestResult = digestAndResult[1];
			
			// 解密响应结果
			String afterDESResult = DesEncryptUtil.decrypt(digestResult, IDS_ENCRYPTPWD);
			String afterBase64Decode = new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
			System.out.println("==afterBase64Decode:"+afterBase64Decode);

	        //JSONObject obj = JSONObject.fromObject(afterBase64Decode);  
	        //JSONArray listObj = (JSONArray) obj.get("entry");  
	        //JSONObject contentObj = (JSONObject) listObj.get(0);
	        
	        //grpValue = contentObj.getString(searchField);

	        System.out.println(userValue);
			methodPost.releaseConnection();		
			
			return userValue;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userValue;
	}
	
	/**
	 * 修改单一用户的所属组织机构
	 * @param userName	用户名
	 * @param beforeGroupNames 用户修改前所属组织名构成的串，格式要求如：groupName1~~groupName2~~groupName3,以上组织名必须在 IDS
	 * @param currentGroupNames 修改用户所示组织机构后的组织机构名构成的串，格式同上
	 * @return
	 */
	public static boolean changeGroupsForUser(String userName, String beforeGroupNames, String currentGroupNames){
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=changeGroupsForUser";
		
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("appName", IDS_APPNAME);
			
			//拼接用户数据
			 String data = "userName="+userName+"&beforeGroupNames="+beforeGroupNames+"&currentGroupNames="+currentGroupNames;
			 MessageDigest md = MessageDigest.getInstance("MD5");
			 md.update(data.getBytes("UTF-8")); 
			 byte[] digestByte = md.digest();
			 String digest = StringHelper.toString(digestByte);
			 String base64Encoded = new String(Base64.encode(data.getBytes("UTF-8")));
			 String dataAfterDESEncode =  DesEncryptUtil.encryptToHex(base64Encoded.getBytes("UTF-8"), IDS_ENCRYPTPWD);
			 String finalData = digest + "&" + dataAfterDESEncode;
			 methodPost.addParameter("data", finalData);
			
			 HttpClient client = new HttpClient();
			 client.executeMethod(methodPost);
			
			 String response = new String(methodPost.getResponseBody(), "utf-8");
			 System.out.println(response);
			 // 拆分摘要和结果信息
			 String[] digestAndResult = StringHelper.split(response, "&");
			 String result = digestAndResult[1];
				
			 // 解密响应结果
			 String afterDESResult = DesEncryptUtil.decrypt(result, IDS_ENCRYPTPWD);
			 String afterBase64Decode =new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
			 System.out.println("==afterBase64Decode:"+afterBase64Decode);
			 
			 methodPost.releaseConnection();
			 
		} catch (Exception e) {
			e.printStackTrace();
			return  false;
		}
		 return true;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//getsearchObject("Group", "groupCode", "1234560101", "groupId");
		//getGroupInfoById("31", "groupName");
		JSONArray arr = new JSONArray();
		JSONObject obj = new JSONObject();
		obj.put("code", "1234560101");
		obj.put("mainFlag", "01");
		obj.put("orgCode", "000");
		JSONObject obj2 = new JSONObject();
		obj2.put("code", "123456010101");
		obj2.put("mainFlag", "02");
		obj2.put("orgCode", "000");
		arr.add(obj);
		arr.add(obj2);		
		System.out.println(arr.toString());
		
		//getUserGroups(arr.toString());
		
		//getUserInfoByName("zhangsan", "userId");
		changeGroupsForUser("wanger5", "EveryOne", "33");
	}

}
