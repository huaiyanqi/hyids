package com.trs.ids;

import net.sf.json.*;


public class JsonResult {
	/** 预留，暂时无意义 */
	private String code;
	/**
	 * 业务响应码 1000---成功返回数据;2000---Token为空、失效或已过期
	 * ;3000---参数错误;4000---服务器内部错误;5000---权限不足
	 */
	private String resultCode;
	/** 结果返回消息 */
	private String resultMessage;
	/** 业务调用成功与否 */
	private boolean success;
	/** 业务返回数据 */
	private JSON result;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public JSON getResult() {
		return result;
	}

	public void setResult(JSON result) {
		this.result = result;
	}
}
