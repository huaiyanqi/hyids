package com.trs.ids;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import com.trs.oauth.ConstantUtil;
import com.trs.oauth.DesEncryptUtil;
import com.trs.oauth.StringHelper;



public class SyncIdsUser {
	
	private static String IDS_URL= ConstantUtil.IDS_URL;
	private static String IDS_APPNAME = ConstantUtil.IDS_APPNAME;
	private static String IDS_ENCRYPTPWD = ConstantUtil.IDS_ENCRYPTPWD;
	private static String IDS_USER_DEFPWD = ConstantUtil.IDS_USER_DEFPWD;
	private static String beforeGroupNames = "EveryOne";
	
	public static JsonResult addUser(Map<String, String> map) {
		JsonResult result = new JsonResult();	
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=userManageService";
		String manageServiceTag = "managerAddUser";
		String finalData = getEncryptData(map, manageServiceTag);
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("manageServiceTag", manageServiceTag);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("data", finalData);

			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			result = getResult(response);
			//if(result.getCode() == "0"){
//				String userName = map.get("userName");
//				String deptCode = map.get("deptCode");
//				String newGrpNames = SyncIdsObjectUtil.getUserGroups(deptCode);
//				System.out.println("newGrpNames::"+newGrpNames);
//				SyncIdsObjectUtil.changeGroupsForUser(userName, beforeGroupNames, newGrpNames);
			//}
			
			methodPost.releaseConnection();	
			System.out.println("返回结果======"+result.getResultMessage().toString());
			if(result.getCode().equals("0")){
				//用户授权
//				JsonResult resultapps=appsForUsers(map);
//				if(resultapps.getCode().equals("0")){
//					return result;
//				}else {
//					return resultapps;
//				}
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	
	public static JsonResult appsForUsers(Map<String, String> map) {
		JsonResult result = new JsonResult();	
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=assignAppsForUsers";
		String manageServiceTag = "";
		String finalData = getEncryptData(map, manageServiceTag);
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("manageServiceTag", manageServiceTag);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("coAppNames", IDS_APPNAME);
			methodPost.addParameter("data", finalData);

			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			result = getResult(response);
			//if(result.getCode() == "0"){
//				String userName = map.get("userName");
//				String deptCode = map.get("deptCode");
//				String newGrpNames = SyncIdsObjectUtil.getUserGroups(deptCode);
//				System.out.println("newGrpNames::"+newGrpNames);
//				SyncIdsObjectUtil.changeGroupsForUser(userName, beforeGroupNames, newGrpNames);
			//}
			
			methodPost.releaseConnection();	
			System.out.println("返回结果======"+result.getResultMessage().toString());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static JsonResult updateUser(Map<String, String> map) {
		JsonResult result = new JsonResult();	
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=userManageService";
		String manageServiceTag = "managerUpdateUser";
		String finalData = getEncryptData(map, manageServiceTag);
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("manageServiceTag", manageServiceTag);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("data", finalData);

			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			result = getResult(response);
			
			methodPost.releaseConnection();			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static JsonResult deleteUser(Map<String, String> map) {
		JsonResult result = new JsonResult();	
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=userManageService";
		String manageServiceTag = "managerRemoveUser";
		String finalData = getEncryptData(map, manageServiceTag);
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("manageServiceTag", manageServiceTag);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("data", finalData);

			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			result = getResult(response);
			
			methodPost.releaseConnection();			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 获取应用参数并进行加密处理
	 * @param map
	 * @return
	 */
	private static String getEncryptData(Map<String, String> map, String serviceTag) {
		String encryptData = "";
		try {
			//map集合获取参数			
			String firstName = map.get("firstName");
			String lastName = map.get("lastName");
			
			String trueName = firstName+lastName;
			String userName = map.get("userName");
			String phone = map.get("mobile");
			String isActived=map.get("isActived");
			String creditID=map.get("creditID");
			String password=map.get("password");
			String sex = map.get("sex");
			if(sex!=""&& sex !=null){
				if(sex.equals("01")){
					sex = "1";
				}else if(sex.equals("02")){
					sex = "2";
				}else {
					sex = "3";
				}
			}
			
			String email = map.get("email");
//			String orders = map.get("orders");
//			String deptCode = map.get("deptCode");
			
			//String allGroupFullPaths = SyncIdsObjectUtil.getUserGroups(deptCode);
			
			// 1、将所有参数以key-value的形式拼成整串，内隔符分别为=外分隔符&。
			String data = "";
			if(serviceTag.equalsIgnoreCase("managerAddUser")){
				data +="userName=" + userName;
				data += "&password=" + IDS_USER_DEFPWD;
//				data += "&password=" + password;
				data += "&firstName=" + firstName 
						+ "&lastName=" + lastName 
						+ "&trueName=" + trueName
						+ "&mobile=" + phone
						+ "&isActived=" + isActived
						+ "&creditID=" + creditID
						+ "&gender=" + sex
						+ "&email=" + email;
//						+ "&orders=" + orders;
				
			}else if (serviceTag.equalsIgnoreCase("managerUpdateUser")) {
				data +="userName=" + userName;
				data += "&firstName=" + firstName 
						+ "&lastName=" + lastName 
						+ "&trueName=" + trueName
						+ "&mobile=" + phone
						+ "&isActived=" + isActived
						+ "&creditID=" + creditID
						+ "&gender=" + sex
						+ "&email=" + email;
			}else  if (serviceTag.equalsIgnoreCase("managerRemoveUser")){
				data +="userName=" + userName;
			}else{
				data +="userNames=" + userName;
				data += "&appName=" + IDS_APPNAME 
						+ "&coAppNames=" + IDS_APPNAME;
			}
			System.out.println("data::"+data);
			// 2、根据整串生成摘要签名
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes("UTF-8")); 
			byte[] digestByte = md.digest();
			// 2-1）将生成的摘要转成16进制数
			String digest = StringHelper.toString(digestByte);
			// 3、对整串进行加密处理
			// 3-1）对整串做base64编码（去编码化）
			String base64Encoded = new String(Base64.encode(data.getBytes("UTF-8")));
			// 3-3）对data进行加密处理 并将结果转换成16进制
			String dataAfterDESEncode =  DesEncryptUtil.encryptToHex(base64Encoded.getBytes("UTF-8"), IDS_ENCRYPTPWD);
			// 4、将摘要签名和加密后的参数拼接成最终提交IDS的
			encryptData = digest + "&" + dataAfterDESEncode;
			
			return encryptData;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return encryptData;
	}
	
	/**
	 * 对IDS返回结果进行分析处理
	 * @param response
	 * @return
	 */
	private static JsonResult getResult(String response) {
		JsonResult result = new JsonResult();
		try {
			// 拆分摘要和结果信息
			String[] digestAndResult = StringHelper.split(response, "&");
			String digestOfServer = digestAndResult[0];
			//System.out.println("digestOfServer::"+digestOfServer);
			String digestResult = digestAndResult[1];
			//System.out.println("result::"+result);

			// 解密响应结果
			String afterDESResult = DesEncryptUtil.decrypt(digestResult, IDS_ENCRYPTPWD);
			String afterBase64Decode = new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
			//System.out.println("==afterBase64Decode:"+afterBase64Decode);
			
			SAXReader reader = new SAXReader();   
			Document doc = reader.read(new ByteArrayInputStream(afterBase64Decode.getBytes("UTF-8")));
			Element root = doc.getRootElement();   
			for (Iterator i = root.elementIterator("response"); i.hasNext();) {   
				Element foo = (Element) i.next();   
				result.setCode(foo.elementText("code"));
				result.setResultMessage(foo.elementText("desc"));
				System.out.println("code:" + foo.elementText("code")+"desc:" + foo.elementText("desc"));   
			}   
			
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return result;		
	}
	
	public static void main(String[] args) {	
		

		Map<String, String> map=new HashMap<String, String>();

		

		map.put("userName", "admin123");
		map.put("password", "123qwe");
		map.put("email", "xiasnaaaaa@qq.com");
		map.put("firstName","小");
		map.put("lastName", "三");
		map.put("mobile", "13412344321");
		map.put("isActived","true");
		map.put("creditID", "232324199006066734");
		map.put("sex", "02");
		//添加用户ids
		addUser(map);
		//删除用户
//		deleteUser(map);
//		appsForUsers(map);
		
		//更新用户
//		updateUser(map);


	}

}
