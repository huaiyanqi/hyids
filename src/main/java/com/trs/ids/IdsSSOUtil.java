package com.trs.ids;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import com.trs.jdbc.JDBC;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;
import com.trs.oauth.DesEncryptUtil;
import com.trs.oauth.OAuthController;
import com.trs.oauth.StringHelper;



public class IdsSSOUtil {
	private static Logger logger = Logger.getLogger(IdsSSOUtil.class);
	private static String IDS_URL= ConstantUtil.IDS_URL;
	private static String IDS_ENCRYPTPWD = ConstantUtil.IDS_ENCRYPTPWD;
	private static String IDS_USER_PWD = ConstantUtil.IDS_USER_DEFPWD;
	private static String IDS_HYAPPNAME = ConstantUtil.IDS_HYAPPNAME;
	
	public boolean findUserBySSOID(HttpServletRequest req, String ssoSessionId){	
		System.out.println("findUserBySSOID()...........");
		boolean userLogon = false;
		String serviceUrl = IDS_URL + "/service?idsServiceType=httpssoservice&serviceName=findUserBySSOID";
		
		String coSessionId = req.getSession().getId();
		System.out.println("findUserBySSOID==coSessionId:  "+coSessionId);
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("coAppName", IDS_HYAPPNAME);
			
			String data = "ssoSessionId=" + ssoSessionId 
					 +"&coSessionId=" + coSessionId;
			 methodPost.addParameter("data", dealEncryptData(data));
			
			 HttpClient client = new HttpClient();
			 client.executeMethod(methodPost);
			
			 String response = new String(methodPost.getResponseBody(), "utf-8");
			 userLogon =  getfindUserResult(response);
			 
			 methodPost.releaseConnection();			 
			 return userLogon;
		} catch (Exception e) {
			e.printStackTrace();
			return  false;
		}
	}
	
	public boolean loginByUP(HttpServletRequest req, String userName, String ssoSessionId) throws SQLException, Exception{
		logger.info("loginByUP()...........");
		boolean userLogon = false;
		JDBC  jdbc=new JDBC();
		String  psd=jdbc.JDBCDriverSelectUserPassword("select * from kpuser where username='"+userName+"'");	
		System.out.println("登录查询到的密码：："+psd);
		String serviceUrl = IDS_URL + "/service?idsServiceType=httpssoservice&serviceName=loginByUP";
		String coSessionId = req.getSession().getId();
		logger.info("loginByUP登录coSessionId："+coSessionId);
		System.out.println("登录coSessionId："+coSessionId);
		
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
        Util.log("==================================================","loginUser",0);
        Util.log("当前进入登录方法，记录登录用户名称","loginUser",0);
		Util.log("当前登录用户：：：：："+userName,"loginUser",0);
		Util.log("当前登录时间：：：：："+df.format(new Date()),"loginUser",0);
		Util.log("当前登录查询用户密码","loginUser",0);
		Util.log("select * from kpuser where username='"+userName+"'","loginUser",0);
		Util.log("==================================================","loginUser",0);
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("coAppName", IDS_HYAPPNAME);
			
			String data = "userName=" + userName 
					+ "&password=" + psd 
					+ "&coSessionId=" + coSessionId
					+ "&ssoSessionId=" + ssoSessionId;
			methodPost.addParameter("data", dealEncryptData(data));		
			 
			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);
			
			String response = new String(methodPost.getResponseBody(), "utf-8");
			userLogon = getLoginResult(response);
			 
			//req.getSession().setAttribute("loginUser", userName);
			methodPost.releaseConnection();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace());
			e.printStackTrace();
			return  userLogon;
		}
		
		 return userLogon;
	}
	
	private static String dealEncryptData(String data){
		String finalData = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes("UTF-8"));
			byte[] digestByte = md.digest();
			String digest = StringHelper.toString(digestByte);
			String base64Encoded = new String(Base64.encode(data.getBytes("UTF-8")));
			String dataAfterDESEncode =  DesEncryptUtil.encryptToHex(base64Encoded.getBytes("UTF-8"), IDS_ENCRYPTPWD);
			finalData = digest + "&" + dataAfterDESEncode;
			return finalData;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return finalData;		 
	}
	
	@SuppressWarnings("restriction")
	private static boolean getfindUserResult(String response){
		boolean hasUser = false;	
		try {
			String[] digestAndResult = StringHelper.split(response, "&");
			String digestResult = digestAndResult[1];
			System.out.println("digestResult::"+digestResult);

			String afterDESResult = DesEncryptUtil.decrypt(digestResult, IDS_ENCRYPTPWD);
			String afterBase64Decode = new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
			System.out.println("==afterBase64Decode:"+afterBase64Decode);
			
			SAXReader reader = new SAXReader();   
			Document doc = reader.read(new ByteArrayInputStream(afterBase64Decode.getBytes("UTF-8")));
			Element root = doc.getRootElement();   
			for (Iterator i = root.elementIterator("responseHead"); i.hasNext();) {   
				Element foo = (Element) i.next();   
				String sCode = foo.elementText("responseCode");
				System.out.println("responseCode:" + foo.elementText("responseCode"));
				System.out.println("responseContent:" + foo.elementText("responseContent")); 
				if(sCode.equals("200")){
					hasUser = true;
				}				  
			}   
			return hasUser;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return hasUser;
	}
	
	
	private boolean getLoginResult(String response) {
		boolean isSuccess = false;
		try {
			String[] digestAndResult = StringHelper.split(response, "&");
			String digestResult = digestAndResult[1];

			String afterDESResult = DesEncryptUtil.decrypt(digestResult, IDS_ENCRYPTPWD);
			String afterBase64Decode = new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
			logger.info("登录ids返回结果解密==afterBase64Decode:"+afterBase64Decode);
			System.out.println("==afterBase64Decode:"+afterBase64Decode);
			
			SAXReader reader = new SAXReader();   
			Document doc = reader.read(new ByteArrayInputStream(afterBase64Decode.getBytes("UTF-8")));
			Element root = doc.getRootElement();   
			for (Iterator i = root.elementIterator("responseHead"); i.hasNext();) {   
				Element foo = (Element) i.next();   
				String sCode = foo.elementText("responseCode");
				logger.info("ids登录返回结果responseCode:" + foo.elementText("responseCode"));
				System.out.println("responseCode:" + foo.elementText("responseCode"));
				System.out.println("responseContent:" + foo.elementText("responseContent"));
				if(sCode.equals("200")){
					isSuccess = true;
//					for(Iterator a = root.elementIterator("responseBody"); a.hasNext();){
//						Element fooo = (Element) a.next();
//						for(Iterator aa = fooo.elementIterator("responseContent"); aa.hasNext();){
//							Element foooo = (Element) aa.next();
//							String sdToken = foooo.elementText("sdToken");
//							String [] sdid=sdToken.split("_");
//							ssoSessionId=sdid[0];
//						}
//					}
				}
			} 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return isSuccess;
		}
		return isSuccess;
	} 
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
