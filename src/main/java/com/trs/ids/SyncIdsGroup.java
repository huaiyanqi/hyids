package com.trs.ids;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import com.trs.oauth.ConstantUtil;
import com.trs.oauth.DesEncryptUtil;
import com.trs.oauth.StringHelper;

public class SyncIdsGroup{
	
	private static String IDS_URL= ConstantUtil.IDS_URL;
	private static String IDS_APPNAME = ConstantUtil.IDS_APPNAME;
	private static String IDS_ENCRYPTPWD = ConstantUtil.IDS_ENCRYPTPWD;
	
	public static JsonResult addGroup(Map<String, String> map) {
		JsonResult result = new JsonResult();	
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=groupManageService";
		String manageServiceTag = "managerAddGroup";
		String finalData = getEncryptData(map, manageServiceTag);
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("manageServiceTag", manageServiceTag);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("data", finalData);

			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			result = getResult(response);
			
			methodPost.releaseConnection();			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static JsonResult updateGroup(Map<String, String> map) {
		JsonResult result = new JsonResult();	
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=groupManageService";
		String manageServiceTag = "managerUpdateGroup";
		String finalData = getEncryptData(map, manageServiceTag);
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("manageServiceTag", manageServiceTag);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("data", finalData);

			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			result = getResult(response);
			
			methodPost.releaseConnection();			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
			
	}
	
	public static JsonResult deleteGroup(Map<String, String> map) {		
		JsonResult result = new JsonResult();	
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=groupManageService";
		String manageServiceTag = "managerRemoveGroup";
		String finalData = getEncryptData(map, manageServiceTag);
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("manageServiceTag", manageServiceTag);
			methodPost.addParameter("appName", IDS_APPNAME);
			methodPost.addParameter("data", finalData);

			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			result = getResult(response);
			
			methodPost.releaseConnection();			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	/**
	 * 获取应用参数并进行加密处理
	 * @param map
	 * @return
	 */
	private static String getEncryptData(Map<String, String> map, String serviceTag) {
		String encryptData = "";
		try {
			//组织名称
			String groupId = map.get("name");
			//部门名称
//			String groupDisplayName = map.get("name");
			//上级部门id
			String parentGroupId  = map.get("parentNames");
			String groupEmail  = map.get("email");
			String groupTel  = map.get("tel");
			String groupDesc = map.get("remark");
			//父组织名
//			String parentGroupName = map.get("parentNames");
			//组织全路径
//			String groupFullPath = map.get("parentNames");
			//管理员姓名
//			String groupAdminName = map.get("parentCode");
			//排序
//			String displayOrder = map.get("status");

			
//			if(parentCode != null && parentCode.length() > 0){// 根据groupCode查GrpId ????????????????
//				parentId = SyncIdsObjectUtil.getsearchObject("Group", "groupCode", parentCode, "groupId");  				
//			}
			// 1、将所有参数以key-value的形式拼成整串，内隔符分别为=外分隔符&。
			String data = "";
			if(serviceTag.equalsIgnoreCase("managerAddGroup")){
//				data = "groupDisplayName=" + groupDisplayName;
				data = "parentGroupId=" + parentGroupId
						+ "&groupEmail=" + groupEmail
						+ "&groupTel=" + groupTel
						+ "&groupId=" + groupId
//						+ "&parentGroupName=" + parentGroupName
						+ "&groupDesc=" + groupDesc;
			}else if (serviceTag.equalsIgnoreCase("managerUpdateGroup")) {
//				data = "groupDisplayName=" + groupDisplayName;
				data = "parentGroupId=" + parentGroupId
						+ "&groupEmail=" + groupEmail
						+ "&groupTel=" + groupTel
						+ "&groupId=" + groupId
//						+ "&parentGroupName=" + parentGroupName
						+ "&groupDesc=" + groupDesc;
			}else if(serviceTag.equalsIgnoreCase("managerRemoveGroup")){
//				if(deptCode != null && deptCode.length() > 0){
//					groupId = SyncIdsObjectUtil.getsearchObject("Group", "groupCode", deptCode, "groupId");  				
//				}
				data = "groupName=" + groupId;
			}
			System.out.println("data::"+data);
			// 2、根据整串生成摘要签名
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes("UTF-8")); 
			byte[] digestByte = md.digest();
			// 2-1）将生成的摘要转成16进制数
			String digest = StringHelper.toString(digestByte);
			// 3、对整串进行加密处理
			// 3-1）对整串做base64编码（去编码化）
			String base64Encoded = new String(Base64.encode(data.getBytes("UTF-8")));
			// 3-3）对data进行加密处理 并将结果转换成16进制
			String dataAfterDESEncode =  DesEncryptUtil.encryptToHex(base64Encoded.getBytes("UTF-8"), IDS_ENCRYPTPWD);
			// 4、将摘要签名和加密后的参数拼接成最终提交IDS的
			encryptData = digest + "&" + dataAfterDESEncode;
			
			return encryptData;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return encryptData;
	}
	
	/**
	 * 对IDS返回结果进行分析处理
	 * @param response
	 * @return
	 */
	private static JsonResult getResult(String response) {
		JsonResult result = new JsonResult();
		try {
			// 拆分摘要和结果信息
			String[] digestAndResult = StringHelper.split(response, "&");
			String digestResult = digestAndResult[1];
			//System.out.println("result::"+result);

			// 解密响应结果
			String afterDESResult = DesEncryptUtil.decrypt(digestResult, IDS_ENCRYPTPWD);
			String afterBase64Decode = new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
			//System.out.println("==afterBase64Decode:"+afterBase64Decode);
			
			SAXReader reader = new SAXReader();   
			Document doc = reader.read(new ByteArrayInputStream(afterBase64Decode.getBytes("UTF-8")));
			Element root = doc.getRootElement();   
			for (Iterator i = root.elementIterator("response"); i.hasNext();) {   
				Element foo = (Element) i.next();   
				result.setCode(foo.elementText("code"));
				result.setResultMessage(foo.elementText("desc"));
				System.out.println("code:" + foo.elementText("code")+"desc:" + foo.elementText("desc"));   
			}   
			
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return result;		
	}
	
	
	
	public static void main(String[] args) {
		  	/*"deptUnicode":"*****",唯一标识
			"deptName":"办公室",名称
			"deptType":"02",
			"existType":"01",
			"deptDscr":"***办公室",描述
			"deptCode":"*******",部门编码
			"orgCode":"*******",所属机构编码
			"parentCode":"*******",上级编码
			"fullName":"***办公室",全称
			"status":"01",机构状态01在用；02停用
			"orders":"0"排序*/
		
		Map<String, String> map=new HashMap<String, String>();
		//唯一标识。GRPNAME&&groupDisplayName
//		map.put("deptId", "test-2");
		map.put("name", "总经办-1");
		map.put("parentNames", "总经办");
		map.put("email", "23123@123.com");
		map.put("tel", "13412344321");
		map.put("remark", "-2描述");
//		map.put("parentNames", "测试机构的上级部门全路径");
		
		
//		//组织id
//		String groupId = "deptId";
//		//部门名称
//		String groupDisplayName = map.get("name");
//		//上级部门id
//		String parentGroupId  = map.get("parentId");
//		String groupEmail  = map.get("email");
//		String groupTel  = map.get("tel");
//		String groupDesc = map.get("remark");
//		//父组织名
//		String parentGroupName = map.get("parentNames");
		//调用测试
		addGroup(map);
//		updateGroup(map);
//		deleteGroup(map);
		
	}

}
