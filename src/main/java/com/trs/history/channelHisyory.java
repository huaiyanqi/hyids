package com.trs.history;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.hy.ResDirectory;
import com.trs.hy.ResResource;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


@Controller
@RequestMapping(value = "/his")
public class channelHisyory {

	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	//以下三个参数不做同步
//	private static String bmwd = ChannelidUtil.bmwd;
//	private static String zwmc = ChannelidUtil.zwmc;
//	private static String zwzsdbp = ChannelidUtil.zwzsdbp;
	
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	
	/**
	 * 历史栏目同步
	 * 历史数据同步
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		//站点id
		String siteid="9"; 
		//站点名称
		String sitename="北京市医保局";
		
		//栏目父id
		String pid="0";   
		//操作用户
		String userName="dev";
			
		//同步历史数据
		addLMtoKP(siteid,pid,userName,sitename);
		
		

	}
	
	
	
	
	/**
	 * 获取一级栏目进行历史栏目存入开普
	 * @throws Exception
	 */
	@ResponseBody
//	@RequestMapping(value = {"/hisLm"},method = RequestMethod.GET,produces = "application/json; charset=utf-8")
	@RequestMapping(value = {"/hisLm"},produces = "application/json; charset=utf-8")
	public static void addLMtoKP(String sitId,String pid,String userName ,String sitename) throws Exception{
		
		// 根据站点id查询下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", userName); // 当前操作的用户
		//设置一次查询数量
		mSE.put("pageSize", "1500");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", sitId);
		mSE.put("ParentChannelId", pid);
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		System.out.println("结果"+hy1);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2.size()>0){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					String SITEID=dataBean.getString("SITEID");
					String CHNLNAME=dataBean.getString("CHNLNAME");
					String CHNLDESC=dataBean.getString("CHNLDESC");//栏目名称
					String CHANNELID=dataBean.getString("CHANNELID");//栏目id
					String OPERUSER=dataBean.getString("OPERUSER");//创建人
					String HASCHILDREN=dataBean.getString("HASCHILDREN");//是否存在下级栏目
					//获取栏目首字母
					ChineseCharToEn cte = new ChineseCharToEn();
					String CHNLDESCPINYIN = cte.getAllFirstLetter(CHNLNAME);//栏目名称缩写
					System.out.println(SITEID+"-------------------"+CHNLDESC+"----------------"+CHANNELID);
					if("true".equals(HASCHILDREN)){
						//代表存在下级栏目   同步到开普该栏目  并且查询下级栏目
						addLm(sitename,SITEID,CHANNELID,CHNLDESC,CHNLDESC,OPERUSER,pid,CHNLDESCPINYIN);
						//继续查询下级栏目
						addLMtoKP(SITEID,CHANNELID,userName,sitename);
					}else{
						//无下级栏目 ，仅仅同步该栏目到开普
						addLm(sitename,SITEID,CHANNELID,CHNLDESC,CHNLDESC,OPERUSER,pid,CHNLDESCPINYIN);
					}
					
				}
			}else{
				System.out.println("没有查询到站点下得栏目");
			}
		}
	}

	/**
	 * 获取一级栏目进行历史栏目存入开普
	 * @throws Exception
	 */
	@ResponseBody
//	@RequestMapping(value = {"/hisLm"},method = RequestMethod.GET,produces = "application/json; charset=utf-8")
	@RequestMapping(value = {"/gethisLm"},produces = "application/json; charset=utf-8")
	public static void addLMtoKPGet(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String sitId="124";
		String pid="0";
		String userName="dev";
		String sitename="拓尔思政府网站测试";
//		String pid=request.getParameter("code");
//		String userName=request.getParameter("code");
//		String sitename=request.getParameter("code");
		
		// 根据站点id查询下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", userName); // 当前操作的用户
		//设置一次查询数量
		mSE.put("pageSize", "1500");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", sitId);
		mSE.put("ParentChannelId", pid);
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		System.out.println("结果"+hy1);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2.size()>0){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					String SITEID=dataBean.getString("SITEID");
					String CHNLNAME=dataBean.getString("CHNLNAME");
					String CHNLDESC=dataBean.getString("CHNLDESC");//栏目名称
					String CHANNELID=dataBean.getString("CHANNELID");//栏目id
					String OPERUSER=dataBean.getString("OPERUSER");//创建人
					String HASCHILDREN=dataBean.getString("HASCHILDREN");//是否存在下级栏目
					//获取栏目首字母
					ChineseCharToEn cte = new ChineseCharToEn();
					String CHNLDESCPINYIN = CHNLNAME;//栏目名称缩写
					System.out.println(SITEID+"-------------------"+CHNLDESC+"----------------"+CHANNELID);
					if("true".equals(HASCHILDREN)){
						//代表存在下级栏目   同步到开普该栏目  并且查询下级栏目
						addLm(sitename,SITEID,CHANNELID,CHNLNAME,CHNLDESC,OPERUSER,pid,CHNLDESCPINYIN);
						//继续查询下级栏目
						addLMtoKP(SITEID,CHANNELID,userName,sitename);
					}else{
						//无下级栏目 ，仅仅同步该栏目到开普
						addLm(sitename,SITEID,CHANNELID,CHNLNAME,CHNLDESC,OPERUSER,pid,CHNLDESCPINYIN);
					}
					
				}
			}else{
				System.out.println("没有查询到站点下得栏目");
			}
		}
	}

	
	
	public static void addLm(String SITENAME,String SITEID,String CHANNELID,String CHNLNAME,String CHNLDESC,String OPERUSER ,String PARENTID,String CHNLDESCPINYIN) throws UnsupportedEncodingException{
		
		//根据站点id 获取到开普对应的资源库id
		String kaipuid="无数据";
		String kaipuPARENTID="无数据";
		String kaipuidLM=null;
		String ysjjid="";
		String zykid="没有相关匹配资源库id";
		ResourceBundle bundle = ResourceBundle.getBundle("services");
		Z_ID = new String(bundle.getString("Z_ID").getBytes("ISO-8859-1"), "UTF8");
		String [] zid=Z_ID.split(";");
		for(int z=0;z<zid.length;z++){
			String zid1=zid[z];
			Util.log("第"+z+"组资源库id名称："+zid1,"addLMlog",0);
			System.out.println("第"+z+"组资源库id名称："+zid1);
			
			String [] zsting=zid1.split(",");
			
			Util.log("第"+z+"次资源库得名称："+zsting[1],"addLMlog",0);
			System.out.println("第"+z+"次资源库得名称："+zsting[1]);
			System.out.println(zsting[1]+"========================"+SITENAME);
			if(SITENAME.equals(zsting[1])){
				zykid=zsting[0];
				try {
					if(!PARENTID.equals("0")){
						kaipuPARENTID=cR.channelIDCheck(PARENTID);
					}
					// 站点对应的开普的id
					kaipuid=cR.channelReceiverCheck(SITEID);
					//查询栏目是否存在开普
					kaipuidLM=cR.LMReceiverCheck(CHANNELID);
					//查询新增栏目的视图id
					String viewid=zfwzUtil.selectViewid(CHANNELID);
					//查询开普元数据集id
					if("".equals(viewid)){
						System.out.println("咨询视图空字符串：："+viewid);
						viewid="5";
					}
					String sql="SELECT * FROM viewid WHERE viewid = '"+viewid+"'";
					ysjjid=JDBC.selectYsjjid(sql);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Util.log("查询栏目上级栏目是否存在开普：：："+kaipuPARENTID+"============================"+"开普是否存在该栏目得站点：：："+kaipuid,"addLMlog",0);
				System.out.println("查询栏目上级栏目是否存在开普：：："+kaipuPARENTID+"============================"+"开普是否存在该栏目得站点：：："+kaipuid);
				if(kaipuPARENTID != null && kaipuid != null){
					
					if(kaipuidLM!=null){
						Util.log("该栏目已经存在与开普资源库。","addLMlog",0);
						System.out.println("该栏目已经存在与开普资源库。");
					}else{
						//存入开普数据库传参数
						JSONObject json= new JSONObject();
				        json.put("dirName", CHNLDESC);//目录名称  CHNLDESC
				        json.put("dirCode", CHNLDESCPINYIN);//目录代号
				        json.put("seqNum", "1");//排序号
//				        json.put("dirType", "");//目录类型
				        if(!PARENTID.equals("0")){
					        json.put("dirPid", kaipuPARENTID);//父目录id，空表示为根目录
				        }else{
				        	
//				        	kaipuid=PARENTID;
				        	json.put("dirPid", kaipuid);//父目录id，空表示为根目录
				        }
				        json.put("metadataIds",ysjjid);//直接挂元数据集id
				        json.put("libId",zykid);//目录所属资源库ID  
				        json.put("isOrig", "0");//dirPid为资源库的目录ID 
				        json.put("memo",CHNLNAME);//栏目描述
				        json.put("creatorId",OPERUSER);//创建人
				        
				        ResDirectory rd= new ResDirectory();
				        String result=null;
				        System.out.println("调用开普新增接口：：");
				        System.out.println(json);
				        try {
				        	result=rd.addResDirectory(json);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Util.log("新增栏目：：："+json,"addLMlog",0);
				        //处理返回结果，拿到开普栏目的id
						JSONObject jsonKp = JSON.parseObject(result);
						JSONObject kpLanmu = jsonKp.getJSONObject("data");
						String resultkp=jsonKp.getString("code");
						Util.log("新增栏目返回结果：：："+result,"addLMlog",0);
						if(resultkp.equals("0")){
							String dirId=kpLanmu.getString("dirId");//站点id
							// 两方id存入到中间表
							JDBC  jdbc=new JDBC();
							Util.log("栏目存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTIDLM+",siteid) VALUES ('"+dirId+"','"+CHANNELID+"','LM','"+PARENTID+"',"+SITEID+")","addLMlog",0);
							System.out.println("栏目存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTIDLM+",siteid) VALUES ('"+dirId+"','"+CHANNELID+"','LM','"+PARENTID+"',"+SITEID+")");
							try {
								Integer jd=jdbc.JDBCDriver("INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTIDLM+",siteid) VALUES ('"+dirId+"','"+CHANNELID+"','LM','"+PARENTID+"',"+SITEID+")");
								if(jd!=-1){
									Util.log("该栏目中间表存入成功!","addLMlog",0);
									System.out.println("栏目中间表存入成功!");
								}else{
									Util.log("该栏目中间表存入失败!","addLMlog",0);
									System.out.println("栏目中间表存入失败!");
								}
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}else{
							Util.log("栏目同步开普失败"+jsonKp.getString("msg"),"addLMlog",0);
							System.out.println("栏目同步开普失败"+jsonKp.getString("msg"));
						}
						
						
					}	
				}else{
					Util.log("第"+z+"次资源库得名称："+zsting[1],"addLMlog",0);
					System.out.println("中间表中没有该栏目父栏目或站点数据，不做新建");
				}
				Util.log("资源库id结果"+zykid,"addLMlog",0);
				Util.log("站点名称"+SITENAME,"addLMlog",0);
				System.out.println("资源库id结果"+zykid);
				System.out.println("站点名称"+SITENAME);
			}else{
				Util.log("资源库没有相关站点id："+SITENAME,"addLMlog",0);
				System.out.println("资源库没有相关站点id："+SITENAME);
			}
		}	
	}
	
	

}
