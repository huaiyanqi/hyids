package com.trs.history;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mysql.jdbc.Connection;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.AddDocByView;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;


public class documentHisyory {

	
    public static final String  jdbc_idsdriver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_idsurl= ConstantUtil.jdbc_urlids;
    
    public static final String  jdbc_idsusername=  ConstantUtil.jdbc_usernameids;
    public static final String  jdbc_idspassword= ConstantUtil.jdbc_passwordids;
	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	//以下三个参数不做同步
//	private static String bmwd = ChannelidUtil.bmwd;
//	private static String zwmc = ChannelidUtil.zwmc;
//	private static String zwzsdbp = ChannelidUtil.zwzsdbp;
	
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	
	/**
	 * 整个站点数据
	 * 历史数据同步
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		//站点id 
		String siteid="126"; 
		//站点名称
		String sitename="北京市教育委员会";  
		 
		//栏目父id
		String pid="0";   
		//操作用户
		String userName="dev";
			
		//同步历史数据 
		addDotoKP(siteid,pid,userName,sitename);
		
		

	}
	
	
	
	
	/**
	 * 获取一级栏目进行历史栏目存入开普
	 * @throws Exception
	 */
	public static void addDotoKP(String sitId,String pid,String userName ,String sitename) throws Exception{
		
		// 根据站点id查询下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", userName); // 当前操作的用户
		//设置一次查询数量
		mSE.put("pageSize", "15000");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", sitId);
		mSE.put("ParentChannelId", pid);
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		System.out.println("结果"+hy1);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2.size()>0){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					
					String SITEID=dataBean.getString("SITEID");
					String CHNLDESC=dataBean.getString("CHNLDESC");//栏目名称
					String CHANNELID=dataBean.getString("CHANNELID");//栏目id
					//查询新增栏目的视图id
					String viewid=zfwzUtil.selectViewid(CHANNELID);
					
					String HASCHILDREN=dataBean.getString("HASCHILDREN");//是否存在下级栏目
					
					System.out.println(SITEID+"-------------------"+CHNLDESC+"----------------"+CHANNELID);
					if("true".equals(HASCHILDREN)){
						//代表存在下级栏目   同步到开普该栏目  并且查询下级栏目
						addDoc(sitename,SITEID,CHANNELID,CHNLDESC,viewid);
						//继续查询下级栏目
						addDotoKP(SITEID,CHANNELID,userName,sitename);
					}else{
						//无下级栏目 ，仅仅同步该栏目下得数据到开普
						addDoc(sitename,SITEID,CHANNELID,CHNLDESC,viewid);
					}
					
				}
			}else{
				System.out.println("没有查询到站点下得栏目");
			}
		}
	}

	
	
	public static void addDoc(String SITENAME,String SITEID,String CHANNELID,String CHNLNAME,String viewid) throws SQLException, Exception{
		int count=0;
		//根据栏目id查询到栏目下的已发实体数据数量
		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027)";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		int con=0;
		while(resultSet.next()) {
			con=resultSet.getInt("con");
			Util.log("查询到的条数："+con,SITENAME+CHANNELID+"文章总数", 0);
			System.out.println("查询到的文章数量："+con);
		}
		
		if(con!=0){
//			resultSet.close();
			con=con/1000+1;
			for(int i=0;i<con;i++) {
				Util.log("循环次数："+con, SITENAME+CHANNELID, 0);
				addByView(SITENAME,SITEID,CHANNELID,CHNLNAME,viewid,count);
				count++;
			}	
		}else{
			Util.log("该栏目下无数据！"+con, SITENAME+CHANNELID, 0);
		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
		
		
	}
	
	
	public static void addByView(String SITENAME,String SITEID,String CHANNELID,String CHNLNAME,String viewid,int count) throws Exception{
		
		//根据栏目id查询到栏目下的已发实体数据id
		int start=count*1000;
		String sqlcount="SELECT DOCID FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027) ORDER BY DOCID LIMIT "+start+",1000";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		
		while(resultSet.next()) {
			int DOCID=resultSet.getInt("DOCID");
			System.out.println("查询到的文章id："+DOCID);
			if("".equals(viewid) || viewid==null){
				//其他默认咨询视图站点
				addDoc.addDocument(String.valueOf(DOCID), CHANNELID,SITEID);
			}else{
				//根据视图id查询视图短名
				String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+viewid+"'";
				String viewName=jdbcids.JDBCVIEWIDSELECT(sql);
				System.out.println("视图短名："+viewName.toUpperCase());
				AddDocByView.addDocByView(String.valueOf(DOCID), CHANNELID, viewName.toUpperCase(),SITEID);
			}
		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
	}
	

}
