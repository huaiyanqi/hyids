package com.trs.history;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kptohysdzc.DESUtil;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;

public class test {
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Z_STR_DEFAULT_KEY = ConstantUtil.Z_STR_DEFAULT_KEY;
	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	//以下三个参数不做同步
//	private static String bmwd = ChannelidUtil.bmwd;
//	private static String zwmc = ChannelidUtil.zwmc;
//	private static String zwzsdbp = ChannelidUtil.zwzsdbp;
	
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	
    private final static String USER_AGENT = "Mozilla/5.0";

    /**
     * 请求编码
     */
     static String requestEncoding = "UTF-8";
    /**
     * 连接超时
     */
     static int connectTimeOut = 5000;
    /**
     * 读取数据超时
     */
     static int readTimeOut = 10000;
	
 	/**
 	 * 获取正文内资源
 	 * List<String> list = new ArrayList<String>();
 	 */
 	public static List<String>  getContentResource(String regExImg,int subStart,String content){
 		List<String> list = new ArrayList<String>();
 		Pattern patternImg = Pattern.compile(regExImg);
 		Matcher matcherImg = patternImg.matcher(content);
 		while (matcherImg.find()) {
 			String picPath = matcherImg.group();
 			System.out.println("匹配到的路径："+picPath);
 			list.add(picPath);

 		}
 		return list;
 	}
     
     
     public static void main(String[] args ) throws SQLException, Exception {
    	 
//    	 String aa="<a otheroperation=\"false\" "
//    	 		+ "href=\"/protect/P0202012/P020201207/P020201207533514697855.png\" /a> <a "
//    	 		+ "href=\"http://www.baidu.com\" a/> <a id=\"noProp\" target=\"_blank\" style=\"font-size:12px; color:#0066cc;\" otheroperation=\"false\" "
//    	 		+ "href=\"http://trswbj.dev3.trs.org.cn/gzdtzx/202011/t20201127_2152150.html\" a/>";
//    	 getContentResource("(href=\"/).*?(\")", 6, aa);
    	 
    	 
 		String mgc="文物建筑合理利用、低收农户帮扶、退休人员、行政执法、政务公开、自行车管理、污染防治、农村土地确权、健康北京、农田生态补偿、林业、开墙打洞、区域性火灾隐患";
 		String [] mg=mgc.split("、");
 		for(String str:mg){
 			System.out.println(str);
 		}
 		
    	 
//         String format = "yyyy-MM-dd HH:mm:ss";
//         Date nowTime = (Date) new SimpleDateFormat(format).parse("2020-06-27 15:24:21");
//         Date startTime = (Date) new SimpleDateFormat(format).parse("2020-01-01 00:00:00");
//         Date endTime = (Date) new SimpleDateFormat(format).parse("2020-12-31 00:00:00");
//         System.out.println(isEffectiveDate(nowTime, startTime, endTime));
    	 
    	 
    	 
//         List<String> lists = new ArrayList<String>();
//
//         for (int i = 0; i < 10; i++) {
//             // 添加随机数
//             lists.add(String.valueOf(new Random().nextInt(8)));
//
//         }
//         // 创建Map集合
//         Map<String, Integer> map = new HashMap<String, Integer>();
//
//         for (String list : lists) {
//             // 创建计数器 重复加一
//             Integer i = 1;
//             // 如果map集合取到添加的值 list取到的值赋值给mapkey mapvalue计数器+1;
//             if (map.get(list) != null) {
//                 // 计数器加一
//                 i = map.get(list) + 1;
//             }
//             // map更新重复value 加1 如果不重复赋值给map
//             map.put(list, i);
//         }
//         // value 值为list 集合出现的次数
//         System.out.println("map 中数据为" + map.toString());
//
//         // 遍历map 集合
//         for (String s : map.keySet()) {
//             // 查询value 出现的次数
//             if (map.get(s) > 1) {
//                 // 打印计数器出现1次以上的数据
//                 System.out.print(s + " " + "出现次数" + map.get(s) + "次      ");
//             }
//         }

    	 
    	 
    	 
    	 
//		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
//		    	  
//		Map<String, Object> mSE=new HashMap<String, Object>();
//		mSE.put("DOCSOURCENAME", "啊实打实的");
//		mSE.put("con", "12");
//		list.add(mSE);
//		Map<String, Object> mSE1=new HashMap<String, Object>();
//		mSE1.put("DOCSOURCENAME", "在在在在在在");
//		mSE1.put("con", "13");
//		list.add(mSE1);
//		
//		for(int i=0;i<list.size();i++){
//			Map<String, Object> aaa=list.get(i);
//			
//			String DOCSOURCENAME=(String) aaa.get("DOCSOURCENAME");
//			System.out.println(DOCSOURCENAME);
//		}
    	 
    	 
    	 
//		if("北京急救中心".equals("北京急救中心")){
//			System.out.println(trims("北京肿瘤医院 　 "));
//		}
    	 
    	 
    	 
//    	 String content="<img src=\"http://192.141.1.16/group1/M00/07/57/oYYBAF3KiCSAP9xVAAADmmhpwOc042.gif\"/><a target=\"_blank\" href=\"http://192.141.1.16/group1/M00/36/B3/oYYBAF363JCAF3DaAAGgABOjNSk316.doc\" appendix=\"true\">附件：相关文书附件1-8</a></p>";
//			//处理图片
//			String pcontent=ZWuplod.contentResourceUpload(content,5,"(src=\").*?(\")");
//			//处理附件
//			String fcontent=ZWuplod.contentResourceUpload(pcontent,6,"(href=\").*?(\")");
//			
//			System.out.println(CMyString.showNull(fcontent));
    	 
//    	 	String aaaa="qweqwsdasda ";
//    	 	System.out.println("aaaaa==="+CMyString.showNull(aaaa, ""));
    	 
    	 
// 		// 根据站点id查询下级栏目
// 		String sServiceId = "gov_site";
// 		String sMethodName = "queryChildrenChannelsOnEditorCenter";
// 		Map<String, String> mSE=new HashMap<String, String>();
// 		mSE.put("CurrUserName", "dev"); // 当前操作的用户
// 		//设置一次查询数量
// 		mSE.put("pageSize", "1500");
// 		mSE.put("pageindex", "1");
// 		mSE.put("SITEID", "167");
// 		mSE.put("ParentChannelId", "44774");
// 		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
// 		System.out.println(hy1);
    	 
//			String sServiceIdView="gov_site";
//			String sMethodNameView="whetherOpenData";
//			Map<String, String> savemapView = new HashMap();
//			savemapView.put("ChannelID","47841");
//			savemapView.put("CurrUserName", USER); // 当前操作的用户
//			String hyLMView = HyUtil.dataMoveDocumentHyRbj(sServiceIdView,sMethodNameView,savemapView);
//			JSONObject jsonObjectHYSEView = JSON.parseObject(hyLMView);
//			Object dataArrView = jsonObjectHYSEView.get("DATA");//根据json对象中数组的名字解析出其所对应的值
//			JSONObject jsonObjectHYGPSEView = JSON.parseObject(dataArrView.toString());
////			Util.log("政府网站：开普推送数据到海云查询视图短名返回结果=================："+hyLMView,"kplog",0);
//			System.out.println("政府网站：开普推送数据到海云查询视图短名返回结果=================："+hyLMView);
//			//视图id
//			String viewid =jsonObjectHYGPSEView.getString("VIEWID");
//			System.out.println("viewid:"+viewid);
//			System.out.println("viewid:"+viewid.isEmpty());
//			System.out.println("viewid:"+viewid.equals(""));
//			if(viewid==null||viewid.equals("")||viewid.isEmpty()){
//				System.out.println("政府网站工作动态name：");
//			}
    	 
    	 
//			String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID=''";
//			String viewName=jdbcids.JDBCVIEWIDSELECT(sql);
//			System.out.println(viewName);
    	 
    	 
    	 
    	 
    	 
    	 
// 	 	String  fjpath="/manager/getManagerFile?filePath=group1/M00/38/B8/oYYBAF38mMiADwriAABgAP3CuMA788.doc";
// 	 	String writName=fjpath.substring(fjpath.lastIndexOf("/")+1);
//    	 	ZWuplod.upload("group1/M00/38/B8/oYYBAF38mMiADwriAABgAP3CuMA788.doc",writName);
//
//    	 	
//    	 	System.out.println(writName);
//    	 	
//    	 	 Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+writName);
//    	 	String fileName = oDispatch.getUploadShowName();
//    	 	System.out.println(fileName);
    	 	
//			String sServiceId="gov_webdocument";
//			String sMethodName="findOpenDataDocumentById";
//			Map<String, String> savemap = new HashMap<String, String>();
//			savemap.put("DocId","1034142");
//			savemap.put("ChannelId","16587");
//			savemap.put("CurrUserName", USER); // 当前操作的用户
//			String hy11= HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
//			System.out.println(hy11);
    	 
    	 
			//调用海云接口查询文档详细数据
//			String sServiceId="gov_webdocument";
//			String sMethodName="findOpenDataDocumentById";
//			Map<String, String> savemap = new HashMap<String, String>();
//			savemap.put("DocId","1719080");
//			savemap.put("ChannelId","10934");
//			savemap.put("CurrUserName", USER); // 当前操作的用户
//			String hy11= HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
//			System.out.println(hy11);
//    	 
//=====================================================================================================================\
    	 
    	//删除资源
    	 
// 		JSONObject jsonOb = new JSONObject();
// 		jsonOb.put("isOrig", "1"); 
// 		jsonOb.put("resIds", "trswbj1940236");
// 		String deleResult=deleRes(jsonOb,"trswbj","trswbj2019"); 
//    	System.out.println(deleResult);
    	
    	
 //====================================================================================================================
    	 
    	 
    	 //修改资源
    	 
    	 
//  		JSONArray json = new JSONArray();// 默认附件
//  		String pKPpath=PResource("D:\\TestDataBridg\\P020190827558008950389.doc","P020190827558008950389.doc");
//   	    JSONObject fpCon= new JSONObject();
//   	    fpCon.put("name", "修改后的附件.doc");//附件名称
//   	    fpCon.put("path", pKPpath);// 附件路径
//   	    json.add(fpCon);
//     	 
//  		JSONObject jsonOb = new JSONObject();
//  		jsonOb.put("name", "测试标题20200610"); 
//  	    jsonOb.put("resTitle", "测试标题20200610"); 
//  	    jsonOb.put("source", "测试标题20200610"); //来源 
//  	    jsonOb.put("abstracts", "测试标题20200610"); //摘要
//  	    jsonOb.put("author", "测试标题20200610");//作者
//  	    jsonOb.put("content", "&nbsp&nbsp&nbsp&nbsp&nbsp正文1231231231232");//正文
//  	    jsonOb.put("fj", json.toJSONString());//附件
//  	    
//		jsonOb.put("resTranMode", "0");// 0  文本内容    1：文件资源-文件传输  
//		jsonOb.put("isOrig", "1");
//		jsonOb.put("resId","202006151540"); //  资源id
//  	    
//  	    System.out.println("存入开普数据jsonOb==:"+jsonOb);
//  	    System.out.println("存入开普数据json==:"+json);
//  	   
//  	    String result=updateRes0(json, jsonOb);
//  		System.out.println(result);
//    	 
    	 
    	 
   
 //=======================================================================================================
    	 
//   	新增资源 上传附件    	
//    	 String DOCID="1304740";
//    	 String CHANNELID="16590";
//    	 String viewName="WEBLDJL";
//    	 String SITEID="120";
////    	 AddDocByView.addDocByView(DOCID, CHANNELID,viewName ,SITEID);
//    	 addDoc.addDocument(DOCID, CHANNELID,SITEID);
    	 
// 		JSONArray json = new JSONArray();// 默认附件
// 		String pKPpath=PResource("D:\\TestDataBridg\\P020190902531933303621.JPG","P020190902531933303621.JPG");
//  	    JSONObject fpCon= new JSONObject();
//  	    fpCon.put("name", "领导照片");//附件名称
//  	    fpCon.put("path", pKPpath);// 附件路径
//  	    json.add(fpCon);
// 	    System.out.println("存入开普数据附件json==:"+json);
//  	    
 		
// 		String aaa="[{\"path\":\"group1/M00/BC/E9/oYYBAF8VRNCAU82dAAA0geOQz_A499.png\",\"name\":\"file0036\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRNKAaX-TAAAwmT51Pd0305.png\",\"name\":\"file0010\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRNSAado4AAAyex2cchg465.png\",\"name\":\"file0053\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRNeABab7AAA4Ht2l5wc714.png\",\"name\":\"file0022\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRNmAZk9QAABJwgpn5kY261.png\",\"name\":\"file0034\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRNyAD_ONAABNVuhOk80462.png\",\"name\":\"file0015\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRN6AFpHJAAArGDZFJwA495.png\",\"name\":\"file0011\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VROCAda1dAABDMPQkmPo223.png\",\"name\":\"file0020\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VROKABvFzAABGheAFePo460.png\",\"name\":\"file0035\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VROWAV6a8AABDqw0MVy4840.png\",\"name\":\"file0039\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VROeAUpsyAAA0YxNVnpo555.png\",\"name\":\"file0008\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VROqAGY-jAABIC6xRiwo763.png\",\"name\":\"file0025\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRO6AJ7DIAABVVANQL14162.png\",\"name\":\"file0047\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRPGAMhmIAAFsdKtj0LI135.png\",\"name\":\"file0049\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRPOAObL0AABGrG9z8QI805.png\",\"name\":\"file0033\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRPaAc8OvAABEXdpJgq0793.png\",\"name\":\"file0045\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRPiAErFBAABF2Pp6fFg691.png\",\"name\":\"file0016\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRPuANaxOAABIPypJyio673.png\",\"name\":\"file0029\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRP6AKUN-AAA0G8KUlgQ845.png\",\"name\":\"file0031\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRQGAJYCfAABGubtdk30571.png\",\"name\":\"file0060\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRQSAVERhAAAGmEIoa3g447.png\",\"name\":\"file0043\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRQeAQ4byAAEPQYJg1r8910.png\",\"name\":\"file0040\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRQmARbjnAAAwB8k7L-c991.png\",\"name\":\"file0054\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRQyAWsSeAAA3eDvHCGI761.png\",\"name\":\"file0030\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRQ-AFabwAABFf78TvGA106.png\",\"name\":\"file0026\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRRGAZYIsAABJRryFhHc642.png\",\"name\":\"file0004\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRROABPwsAABE3BVky3s091.png\",\"name\":\"file0024\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRRaAd83gAAAvBm0DQyg269.png\",\"name\":\"file0059\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRRmAVhTZAAA5yb8y-3M912.png\",\"name\":\"file0009\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRRuAfvDQAABKz_xtvhc988.png\",\"name\":\"file0006\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRR6AYV-oAABJFIP59zc993.png\",\"name\":\"file0046\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRSCAJeZHAABdq6yGdpg788.png\",\"name\":\"file0041\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRSOAZ0dBAADNd7YPEUo168.png\",\"name\":\"file0048\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRSaAOPkhAAAzSN2ixcg172.png\",\"name\":\"file0050\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRSmAdVT_AABGGluWEbI091.png\",\"name\":\"file0019\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRSuAeEWDAABGhJvq-Ek085.png\",\"name\":\"file0021\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRS2AWuoiAAA0EE_AyUM165.png\",\"name\":\"file0037\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRTGABpLzABLFS9BfGo8894.png\",\"name\":\"file0001\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRTOAEeNBAAA0wWuitT8041.png\",\"name\":\"file0042\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRTWAD5FMAAJK7-RUOZ8074.png\",\"name\":\"file0062\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRTiASticAAMLmmOYsmg495.png\",\"name\":\"file0061\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRTqADmyxAABF2VCOQJE503.png\",\"name\":\"file0028\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRTyAMvG5AABHlaulH68833.png\",\"name\":\"file0014\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRT6ARE7wAABQdbyO_Yc411.png\",\"name\":\"file0013\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRUCAejc3AABIsi_N3Ls431.png\",\"name\":\"file0038\"},{\"path\":\"group1/M00/BC/E9/oYYBAF8VRUKAGyvoAABJCRUVKNE670.png\",\"name\":\"file0017\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRUSACs3WAABM3gkFDek928.png\",\"name\":\"file0044\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRUaADI1iAAA-mevvjQc616.png\",\"name\":\"file0051\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRUmAJer1AAAyd0XbFZU735.png\",\"name\":\"file0057\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRUyARC0BAAA2-jvs4jk184.png\",\"name\":\"file0055\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRU-AWDftAABJLV_b86Q528.png\",\"name\":\"file0058\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRVKAbH05AABItQgwtDA065.png\",\"name\":\"file0003\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRVWARpmcAABGktpEEIw717.png\",\"name\":\"file0007\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRViAHluqAABDKw0aBIU978.png\",\"name\":\"file0032\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRVuAdNKiAAA11JixXMg011.png\",\"name\":\"file0056\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRV2AdcimAAA5szHm6BA027.png\",\"name\":\"file0023\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRWCAVYUOAABLBZrVV4Y102.png\",\"name\":\"file0005\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRWKAMCCnAAA7MQSjMnk624.png\",\"name\":\"file0027\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRWWAQPVIAABIuT7TtB8199.png\",\"name\":\"file0018\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRWiATYtZAABKy8vblZk432.png\",\"name\":\"file0002\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRWuAC3afAAA6FHs4fnI670.png\",\"name\":\"file0012\"},{\"path\":\"group1/M00/BC/EA/oYYBAF8VRW6AdyxWAABCSbZ6Oew984.png\",\"name\":\"file0052\"},{\"path\":\"group1/M00/37/18/oYYBAF37NRCAGBWQAFs4AFqaopk693.doc\",\"name\":\"2017110316004513021\"}]";
// 		JSONObject jsonOb = new JSONObject();
// 		jsonOb.put("name", "2003年北京市泥沙公报"); 
// 		jsonOb.put("resTranMode", "0");// 0  文本内容    1：文件资源-文件传输  
// 	    jsonOb.put("resTitle", "测试附件过多是否超时"); 
//// 	    jsonOb.put("resourceClassify", "6"); //资源分类   文档中该字段非必须
// 	    jsonOb.put("source", "测试接口"); //来源 
// 	    jsonOb.put("abstracts", "摘要"); //摘要
// 	    jsonOb.put("author", "作者");//作者
// 	    jsonOb.put("isOrig", "0");
// 	    jsonOb.put("origId", "trswbj1304740");// 文档id
// 	    jsonOb.put("dirId", "10668ab5cb6c4929b83574147ed2a3ac");//目录id 
// 	    jsonOb.put("status", "3"); 
// 	    jsonOb.put("content", "&nbsp&nbsp&nbsp&nbsp&nbsp正文1231231231232");//正文
// 	    jsonOb.put("attachments", aaa);//附件
// 	    
// 	    System.out.println("存入开普数据jsonOb==:"+jsonOb);
//
// 	   
// 	    String result=createRes2(json, jsonOb);
// 		System.out.println(result);
    	 
//=============================================================================================================
    	 
    	 //删除栏目
    	 
//		//存入开普数据库传参数
//		JSONObject json= new JSONObject();
//	    json.put("dirIds", "40852add4d85411db9d3f64f8f7a1911");//目录id 开普
//	    json.put("isOrig", "0");//isOrig=0时，dirIds为资源库的目录ID
//	    String result=delAllDirs(json);
//    	 
//	    System.out.println(result);
    	 

 //============================================================================================================
    	 //修改栏目
			//存入开普数据库传参数
//			JSONObject json= new JSONObject();
//	        json.put("dirName", "20200615修改栏目");//目录名称
//	        json.put("dirId", "40852add4d85411db9d3f64f8f7a1911");//目录id  开普的id
//	        json.put("isOrig", "0");//dirPid为资源库的目录ID 
//	        json.put("memo","测试修改接口");//栏目描述
//	        json.put("modifierId","hyq");//修改人
//	        json.put("metadataIds","");//直接挂元数据集id
//	        
//	        ResDirectory rd= new ResDirectory();
//	        String result=updateResDirectory(json);
//	        System.out.println(result);
    	 
 // =================================================================================   	 
    	 // 新增栏目
			//存入开普数据库传参数
//			JSONObject json= new JSONObject();
//	        json.put("dirName", "20200615领导简历");//目录名称  CHNLDESC
//	        json.put("dirCode", "ldjl0615");//目录代号
//	        json.put("seqNum", "1");//排序号
////	        json.put("dirType", "");//目录类型
//	        json.put("dirPid", "");//父目录id，空表示为根目录
//	        json.put("metadataIds","5bb8ebf76322d01de0097405");//直接挂元数据集id
//	        json.put("libId","dc7fb3c65e2a44ab892819fe9d7609f1");//目录所属资源库ID  
//	        json.put("isOrig", "0");//dirPid为资源库的目录ID 
//	        json.put("memo","20200609测试新接口栏目");//栏目描述
//	        json.put("creatorId","testV5");//创建人
//	        
//	        String result=null;
//	        System.out.println("调用开普新增接口：：");
//	        System.out.println(json);
//	        try {
//	        	result=addResDirectory(json);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//	        System.out.println(result);
    	 
    	 
    	 
    	 
    	 
 //==========================================================================================================================================   	 
    	 
    	 
//    	 AddDocumentControllerZCJD.addDocumentZCJD("76642","2659","zcjd","80");
//			StringBuffer zt=new StringBuffer();
//			String ztfl="";
//			if(!StringUtils.isBlank(ztfl)){
//				String [] ztflsz=ztfl.split("~");
//				for (int a=0;a<ztflsz.length;a++) {
//					String fl=ztflsz[a].substring(0,ztflsz[a].indexOf("`"));
//					zt.append(fl+";");
//				}
//				System.out.println(zt);
//			}else{
//				System.out.println(111);
//			}

//			int delete = jdbc.JDBCDriver("DELETE  FROM document where source='WD' and chnlid='39180' and siteid='124'");
//			System.out.println(delete);
    	 
			//调用海云接口查询文档详细数据
//			String sServiceId="gov_webdocument";
//			String sMethodName="findDocumentById";
//			Map savemap = new HashMap();
//			savemap.put("DocId","764706");
//			savemap.put("ChannelId","4408");
//			savemap.put("CurrUserName", USER); // 当前操作的用户
//			String hy11=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
//			System.out.println(hy11);
    	 
//			String sServiceId="gov_channel";
//			String sMethodName="findChannelById";
//			Map<String, String> savemap = new HashMap();
//			savemap.put("ChannelID","19678");
//			savemap.put("CurrUserName", USER); // 当前操作的用户
//			String hyLM=null;
//			try {
//				hyLM = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			System.out.println(hyLM);
    	 
    	 
// 		String sServiceId = "gov_site";
// 		String sMethodName = "queryChildrenChannelsOnEditorCenter";
// 		Map<String, String> mSE=new HashMap<String, String>();
// 		mSE.put("CurrUserName", "dev"); // 当前操作的用户
// 		//设置一次查询数量
// 		mSE.put("pageSize", "10000");
// 		mSE.put("pageindex", "1");
// 		mSE.put("SITEID", "80");
// 		mSE.put("ParentChannelId", "0");
// 		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
// 		System.out.println("结果"+hy1);
// ===================================================================================================================    	 
//			//查询新增栏目的视图id
//			String viewid=zfwzUtil.selectViewid("17715");
//			//查询开普元数据集id
//			if("".equals(viewid)){
//				System.out.println("咨询视图空字符串：："+viewid);
//				viewid="5";
//			}
//			String sql="SELECT * FROM viewid WHERE viewid = '"+viewid+"'";
//			String ysjjid=JDBC.selectYsjjid(sql);
//			System.out.println(ysjjid);

	}

     
     /**
      * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
      * 
      * @param nowTime 当前时间
      * @param startTime 开始时间
      * @param endTime 结束时间
      * @return
      * @author jqlin
      */
     public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
         if (nowTime.getTime() == startTime.getTime()
                 || nowTime.getTime() == endTime.getTime()) {
             return true;
         }

         Calendar date = Calendar.getInstance();
         date.setTime(nowTime);

         Calendar begin = Calendar.getInstance();
         begin.setTime(startTime);

         Calendar end = Calendar.getInstance();
         end.setTime(endTime);

         if (date.after(begin) && date.before(end)) {
             return true;
         } else {
             return false;
         }
     }
     
     
     public static String trims(String s){  
    	    String result = "";  
    	    if(null!=s && !"".equals(s)){  
    	        result = s.replaceAll("^[　*| *| *|//s*]*", "").replaceAll("[　*| *| *|//s*]*$", "");  
    	    }  
    	    return result;  
    	}
     
	public static String delHTMLTag(String htmlStr){ 
        String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式 
        String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式 
        String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式 
         
        Pattern p_script=Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE); 
        Matcher m_script=p_script.matcher(htmlStr); 
        htmlStr=m_script.replaceAll(""); //过滤script标签 
         
        Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE); 
        Matcher m_style=p_style.matcher(htmlStr); 
        htmlStr=m_style.replaceAll(""); //过滤style标签 
         
        Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE); 
        Matcher m_html=p_html.matcher(htmlStr); 
        htmlStr=m_html.replaceAll(""); //过滤html标签 

        return htmlStr.trim(); //返回文本字符串 
    } 
	
	public static  String getPath(){
		String abc="<div class='view TRS_UEDITOR trs_paper_default trs_web'><p>详见附件。</p>"
				+ "<p class='insertfileTag' style='line-height: 16px;'><img style='vertical-align: middle; margin-right: 2px;'"
				+ " src='/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_pdf.gif' />"
				+ "<a style='font-size:12px; color:#0066cc;' appendix='true' otheroperation='false' "
				+ "href='/protect/P0201901/P020190115/P020190115655681814519.pdf' title='市国资委2018年度绩效任务四季度落实情况汇总表.pdf' "
				+ "OLDSRC='/protect/P0201901/P020190115/P020190115655681814519.pdf'>市国资委2018年度绩效任务四季度落实情况汇总表.pdf</a>"
				+ "</p></div><div class='view TRS_UEDITOR trs_paper_default trs_web'><p>详见附件。</p><p class='insertfileTag' "
				+ "style='line-height: 16px;'><img style='vertical-align: middle; margin-right: 2px;' "
				+ "src='/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_pdf.gif' />"
				+ "<a style='font-size:12px; color:#0066cc;' appendix='true' otheroperation='false' "
				+ "href='/protect/P0201901/P020190115/P020190115655681814519.pdf' title='市国资委2018年度绩效任务四季度落实情况汇总表.pdf' "
				+ "OLDSRC='/protect/P0201901/P020190115/P020190115655681814519.pdf'>市国资委2018年度绩效任务四季度落实情况汇总表.pdf</a></p></div>"
				+"<p> <iframe class='edui-upload-video video-js vjs-default-skin' "
				+ "src='http://192.141.252.5/mas/openapi/pages.do?method=exPlay&appKey=gov&id=18&autoPlay=false'"
				+ " width='420' height='280' appendix='true'></iframe>测试视频库</p>";
//		
//		String abc="<p> <iframe class='edui-upload-video video-js vjs-default-skin' "
//				+ "src='http://192.141.252.5/mas/openapi/pages.do?method=exPlay&appKey=gov&id=18&autoPlay=false'"
//				+ " width='420' height='280' appendix='true'></iframe>测试视频库</p>";		
//		String aaa="iframe class='edui-upload-video video-js vjs-default-skin' "
//				+ "src='http://192.141.252.5/mas/openapi/pages.do?method=exPlay&appKey=gov&id=18&autoPlay=false'"
//				+ " width='420' height='280' appendix='true'></iframe";
//		String bb="video controls='controls' loop='loop' width='480' height='400' src='http://192.168.1.219:7002/repo-web/manager/getManagerFile?filePath=group1/M00/00/B4/wKgBxFxqgemAL1joADEBLOFwO7I159.mp4' autoplay='autoplay'></video";
//		abc=abc.replace(aaa, bb);
//		System.out.println(abc);
//		String a="a.a.a.a.a.a.a";
//		String b="bbbb";
//		
//		a.replace(".", b);
//		System.out.println(a);
//		
//		String s = "my.test.txt";
//		System.out.println(s.replace(".", "#"));
		
		//  '/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_pdf.gif'
		//  '/protect/P0201901/P020190115/P020190115655681814519.pdf'
//		String abc="    <img style='ertical-align: middle; margin-right: 2px;' src='/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_jpg.gif'/><a style='font-size:12px; color:#0066cc;' appendix='true' otheroperation='false' href='/protect/P0201902/P020190223/P020190223584663980378.png' title='Q量子银座.png'>Q量子银座.png</a>";
//		String name="icon_jpg";
//		String regExImg = "("+name+").*?(.jpg|.gif|.png|.jpeg|.bmp|.JPG|.GIF|.PNG|.JPEG|.BMP)";//正文中视频
////		String regExImg = "(icon_jpg).*?(.jpg|.gif|.png|.jpeg|.bmp|.JPG|.GIF|.PNG|.JPEG|.BMP)";//正文中视频
//		
		String regExImg = "(href=).*?(.pdf|.PDF|.RAR|.rar|.zip|.ZIP)";//正文中pdf  5  改成6
		Pattern patternImg = Pattern.compile(regExImg);
		Matcher matcherImg = patternImg.matcher(abc);
		List<String> list = new ArrayList<String>();
		while (matcherImg.find()) {
			String picPath = matcherImg.group();
			picPath = picPath.substring(8, picPath.length());
//			String fullPath = getFullPath(picPath);
			System.out.println("多少个path="+picPath);
			list.add(picPath);
		}
		return null;
	}



	/**
	 * 新增栏目接口
	 * @throws Exception
	 */
    public static String addResDirectory(JSONObject json) throws Exception {
        
        String url ="http://192.141.2.5:7002/zuul/repo-api/api/res/directory/createDir";
        System.out.println("新建站点："+url);
        Map<String, String> map = new HashMap<String, String>();
        map.put("appId", ZWBJ_APPID);//服务ID
        map.put("data", DESUtil.encrypt(json.toJSONString(), ZWBJ_STR_DEFAULT_KEY));
        String post = HttpUtil.doPost(url, map,json);
        System.out.println("开普新建返回：："+post);
//        JSONObject jsonKp = JSON.parseObject(post);
//		JSONArray kpLanmu=jsonKp.getJSONArray("data");
//		System.out.println(kpLanmu);
        return post;
    }
    
    
    public static String PResource(String  pPath,String pName) {
    	

    	String PKPpath=null;
    	String urlKp=null;
    	try {
			PKPpath=uploadFile(pPath, pName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//处理返回结果
		JSONObject jsonObject = JSON.parseObject(PKPpath);
		Object dataArr = jsonObject.get("data");
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		String code=jsonObject.getString("code");
		if("0".equals(code)){
			urlKp = jsonObjectHYGPSE.getString("fileURL");
		}else{
			System.out.println("开普上传失败："+jsonObject.get("MSG"));	
		}
		
    	return urlKp;
    }
    
    
	/**
	 * 上传文件 视频 图片 
	 * @throws Exception
	 */
	  public static String uploadFile(String path , String name) throws Exception {
		  
		  
	      JSONObject  json = new JSONObject();
	      File file = new File(path);
	      FileInputStream fileInputStream = new FileInputStream(file);
	      String encrypt = DESUtil.encrypt(json.toJSONString(), ZWBJ_APPID);
		  String url ="http://192.141.2.5:7002/zuul/repo-api/api/res/resource/uploadFile?data=" + encrypt + "&appId="+ZWBJ_STR_DEFAULT_KEY;
	      String s = HttpUtil.uploadFileByOkHttp(url,fileInputStream,name);
	
	      System.out.println(s);
	      return s;
	  }
  
  
		// resTranMode=2 
	  public static String  createRes2(JSONArray jsonArray,JSONObject json ) throws Exception {
			    
//			    json.put("attachments", jsonArray.toJSONString());//附件
			    
//			    String url = "http://192.140.199.187:7302/api/res/resource/createRes";
			    String url = "http://192.141.2.5:7002/zuul/repo-api/api/res/resource/createRes";
			    Map<String, String> map = new HashMap<String, String>();
			    System.out.println("新建文档内容json："+json);
			    System.out.println("新建文档内容url："+url);
			    map.put("appId", ZWBJ_APPID);
			    map.put("data", DESUtil.encrypt(json.toJSONString(),ZWBJ_STR_DEFAULT_KEY));
			    System.out.println("map"+map);
			    String post = HttpUtil.doPost(url, map,json);
			    System.out.println(post); 
			    return post;
	  	}
  
	/**
	 * 修改开普资源
	 * @throws Exception
	 */
	//  resTranMode=0
	public static String  updateRes0(JSONArray jsonArray,JSONObject json) throws Exception {
		  	
		  	json.put("attachments", jsonArray.toJSONString());//附件
		    
		    String url ="http://192.141.2.5:7002/zuul/repo-api/api/res/resource/updateRes";
		    System.out.println(url);
		    Map<String, String> map = new HashMap<String, String>();
		    map.put("appId",ZWBJ_APPID);
		        map.put("data", DESUtil.encrypt(json.toJSONString(),ZWBJ_STR_DEFAULT_KEY));
		    String post = HttpUtil.doPost(url, map,json);
		    System.out.println(post); 
		    return post;
	 }
	
	/**
	 * 删除开普资源
	 * @throws Exception
	 */
	public static String  deleRes(JSONObject json,String appid,String key) throws Exception {
		    
		    String encrypt = DESUtil.encrypt(json.toJSONString(), key);
		    String url ="http://192.141.2.5:7002/zuul/repo-api/api/res/resource/delRes?data=" + encrypt + "&appId="+appid;
		    String post = HttpUtil.doGet(url, "UTF-8");
//		    System.out.println(post); 
		    return post;
	 }
	
	/**
	 * 修改栏目
	 * @return 
	 * @throws Exception
	 */
    public static String updateResDirectory(JSONObject json) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("appId", ZWBJ_APPID);
        String url = "http://192.141.2.5:7002/zuul/repo-api/api/res/directory/updateDir";
        map.put("data", DESUtil.encrypt(json.toJSONString(), ZWBJ_STR_DEFAULT_KEY));
        String post = HttpUtil.doPost(url, map,json);
        System.out.println(post);
        return post;
    }
    
	/**
	 * 删除栏目
	 * @throws Exception
	 */
    public static String delAllDirs(JSONObject json) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("appId",ZWBJ_APPID);//8ea32629da3e4c7cb33ee96f71913e25
//        JSONObject json = new JSONObject();
//        json.put("isOrig","1");
//        json.put("libId", "111");
//        json.put("dirIds", "e69b4a5891c343c9b7ea079b4712b035");
        String url = "http://192.141.2.5:7002/zuul/repo-api/api/res/directory/delDirs";
        map.put("data", DESUtil.encrypt(json.toJSONString(), ZWBJ_STR_DEFAULT_KEY));
        String post = HttpUtil.doPost(url, map,json);
        System.out.println(post);
        return post;
    }
  
}
