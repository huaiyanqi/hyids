/*
 * Title: TRS ��ݷ����� Copyright: Copyright (c) 2004-2011, TRS��Ϣ�����ɷ����޹�˾. All rights reserved. License: see the license
 * file. Company: TRS��Ϣ�����ɷ����޹�˾(www.trs.com.cn)
 * 
 * Created: shixin@2011-3-2 ����08:18:45
 */
package com.trs.oauth;

import java.security.InvalidKeyException;
import java.security.Key;

import javax.crypto.SecretKey;
import javax.crypto.spec.DESKeySpec;


public class DesEncryptUtil {


	private final static String DES_ALGORITHM_NAME = "DES";


	private final static String DES_ALGORITHM_TRANSFORMATION_DEFAULT = "DES/ECB/PKCS5Padding";


	private static final String DES_KEY_DATA_PROPERTY_NAME = "desKeyData";


	private static final String SEC_CFG_FILE = "/security.ini";


	public static byte[] encrypt(byte[] toEncryptData) {
		return doEncrypt(getKey(), toEncryptData);
	}


	public static String encryptToHex(byte[] toEncryptData) {
		byte[] encryptedData = doEncrypt(getKey(), toEncryptData);
		if (null == encryptedData) {
			return "";
		}
		return StringHelper.bytesToHex(encryptedData, 0, encryptedData.length);
	}


	public static byte[] encrypt(byte[] toEncryptData, String keyResourceName, String propertyName) {
		return doEncrypt(getKeyFromResource(keyResourceName, propertyName), toEncryptData);
	}


	public static byte[] encrypt(byte[] toEncryptData, String keyData) {
		return doEncrypt(getKeyByData(keyData), toEncryptData);
	}


	public static String encryptToHex(byte[] toEncryptData, String keyResourceName, String propertyName) {
		byte[] encryptedData = doEncrypt(getKeyFromResource(keyResourceName, propertyName), toEncryptData);
		if (null == encryptedData) {
			return "";
		}
		return StringHelper.bytesToHex(encryptedData, 0, encryptedData.length);
	}


	public static String encryptToHex(byte[] toEncryptData, String keyData) {
		byte[] encryptedData = doEncrypt(getKeyByData(keyData), toEncryptData);
		if (null == encryptedData) {
			return "";
		}
		return StringHelper.bytesToHex(encryptedData, 0, encryptedData.length);
	}


	public static byte[] doEncrypt(Key key, byte[] toEncryptData, String transformation) {
		if (key == null) {
			return null;
		}
		return JceEncryptUtil.doEncrypt(key, toEncryptData, transformation);
	}


	public static byte[] doEncrypt(Key key, byte[] toEncryptData) {
		if (key == null) {
			return null;
		}
		return JceEncryptUtil.doEncrypt(key, toEncryptData, DES_ALGORITHM_TRANSFORMATION_DEFAULT);
	}

	public static byte[] decrypt(byte[] toDecryptData) {
		return JceEncryptUtil.doDecrypt(getKey(), toDecryptData, DES_ALGORITHM_TRANSFORMATION_DEFAULT);
	}


	public static String decryptToHex(byte[] toDecryptData) {
		byte[] decryptedData = JceEncryptUtil.doDecrypt(getKey(), toDecryptData, DES_ALGORITHM_TRANSFORMATION_DEFAULT);
		if (null == decryptedData) {
			return "";
		}
		return StringHelper.bytesToHex(decryptedData, 0, decryptedData.length);
	}


	public static byte[] decrypt(byte[] toDecryptData, String keyResourceName, String propertyName) {
		return JceEncryptUtil.doDecrypt(getKeyFromResource(keyResourceName, propertyName), toDecryptData,
				DES_ALGORITHM_TRANSFORMATION_DEFAULT);
	}


	public static byte[] decrypt(byte[] toDecryptData, String keyData) {
		return JceEncryptUtil.doDecrypt(getKeyByData(keyData), toDecryptData, DES_ALGORITHM_TRANSFORMATION_DEFAULT);
	}


	public static String decryptToHex(byte[] toDecryptData, String keyData) {
		byte[] decryptedData = JceEncryptUtil.doDecrypt(getKeyByData(keyData), toDecryptData,
				DES_ALGORITHM_TRANSFORMATION_DEFAULT);
		if (null == decryptedData) {
			return "";
		}
		return StringHelper.bytesToHex(decryptedData, 0, decryptedData.length);
	}


	public static String decryptToHex(byte[] toDecryptData, String keyResourceName, String propertyName) {
		byte[] decryptedData = JceEncryptUtil.doDecrypt(getKeyFromResource(keyResourceName, propertyName),
				toDecryptData, DES_ALGORITHM_TRANSFORMATION_DEFAULT);
		if (null == decryptedData) {
			return "";
		}
		return StringHelper.bytesToHex(decryptedData, 0, decryptedData.length);
	}


	public static byte[] doDecrypt(Key key, byte[] toDecryptData, String transformation) {
		if (key == null) {
			return null;
		}
		return JceEncryptUtil.doDecrypt(key, toDecryptData, transformation);
	}


	public static byte[] doDecrypt(Key key, byte[] toDecryptData) {
		if (key == null) {
			return null;
		}
		return JceEncryptUtil.doDecrypt(key, toDecryptData, DES_ALGORITHM_TRANSFORMATION_DEFAULT);
	}


	public static SecretKey getKey() {
		String keyData = getKeyDataFromResource(SEC_CFG_FILE, DES_KEY_DATA_PROPERTY_NAME);
		if (StringHelper.isEmpty(keyData)) {
			return null;
		}
		return getKeyByData(keyData);
	}


	public static SecretKey getKeyFromResource(String resName, String propertyName) {
		String keyData = getKeyDataFromResource(resName, propertyName);
		return getKeyByData(keyData);
	}


	public static SecretKey getKeyByData(String keyData) {
		DESKeySpec desKeySpec = null;
		try {
			desKeySpec = new DESKeySpec(keyData.getBytes());
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		return JceEncryptUtil.getKey(DES_ALGORITHM_NAME, desKeySpec);
	}


	public static String getKeyDataFromResource(String resName, String propertyName) {
//		return JceEncryptUtil.getKeyDataFromResource(resName, propertyName);
		return "";
	}


	public static String decrypt(String encryptedDataHex, String keyData) {
		if (StringHelper.isEmpty(encryptedDataHex)) {
			return "";
		}
		if (StringHelper.isEmpty(keyData)) {
			keyData = getKeyDataFromResource(SEC_CFG_FILE, DES_KEY_DATA_PROPERTY_NAME);
		}

		String decryptedHexStr = DesEncryptUtil.decryptToHex(StringHelper.toBytes(encryptedDataHex), keyData);
		return StringHelper.hexToStr(decryptedHexStr);
	}

	public static String decrypt(String encryptedDataHex) {
		return decrypt(encryptedDataHex, getKeyDataFromResource(SEC_CFG_FILE, DES_KEY_DATA_PROPERTY_NAME));
	}
}