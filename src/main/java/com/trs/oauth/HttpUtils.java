package com.trs.oauth;

/**
* http请求工具类
*/
public final class HttpUtils {
private static HttpRequester httpRequester = new HttpRequester();
	private HttpUtils() {
		
	}
	public static String sendGet(String url){
		try {
			httpRequester.setDefaultContentEncoding("UTF-8");
			HttpRespons hr = httpRequester.sendGet(url);
			return hr.getContent();
		} catch (Exception e) {
			e.printStackTrace();
		return null;
		}	 
	} 
}