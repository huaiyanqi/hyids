package com.trs.oauth.log;

import com.trs.kafka.Util;

/**
 * http请求工具类
 */
public final class HttpUtils {
	
	private static HttpRequester httpRequester = new HttpRequester();
	private HttpUtils() {}
	
	public static String sendGet(String url){
		try {
			httpRequester.setDefaultContentEncoding("UTF-8");
			HttpRespons hr = httpRequester.sendGet(url);
			return hr.getContent();
		} catch (Exception e) {
            System.out.println("网络故障");
            e.printStackTrace();
            return "网络故障,连接超时";
		}
	}
}
