package com.trs.oauth;

import java.util.ResourceBundle;


import java.util.ResourceBundle;

public class ConstantUtil {

	static ResourceBundle resource = ResourceBundle.getBundle("services");

	// 从配置文件中获取IDS的Url
	public static final String IDS_URL = resource.getString("IDS_URL");
	
	public static final String HYIDS_URL = resource.getString("HYIDS_URL");

	public static final String IDS_APPNAME = resource.getString("IDS_APPNAME");
	
	public static final String IDS_HYAPPNAME = resource.getString("IDS_HYAPPNAME");

	public static final String IDS_ENCRYPTPWD = resource.getString("IDS_ENCRYPTPWD");

	public static final String IDS_USER_DEFPWD = resource.getString("IDS_USER_PWD");

	public static final String CLIENT_ID = resource.getString("CLIENT_ID");

	public static final String CLIENT_SECRET = resource.getString("CLIENT_SECRET");

	public static final String REDIRECT_URI = resource.getString("REDIRECT_URI");

	public static final String ACCESSTOKEN_URI = resource.getString("ACCESSTOKEN_URI");

	public static final String PROFILE_URI = resource.getString("PROFILE_URI");
	
	public static final String USERNAMEKAFKA = resource.getString("USERNAMEKAFKA");
	
	public static final String PASSWORDKAFKA = resource.getString("PASSWORDKAFKA");
		
	public static final String GROUOID = resource.getString("GROUOID");
		
	public static final String CLIENTID = resource.getString("CLIENTID");
	
	public static final String P_CLIENTID = resource.getString("P_CLIENTID");
		
	public static final String R_QUEUE = resource.getString("R_QUEUE");

	public static final String UAS_TOPIC = resource.getString("UAS_TOPIC");
	
	public static final String UAS_TOPIC_LOGIN_OUT = resource.getString("UAS_TOPIC_LOGIN_OUT");
	
	public static final String SERVICE_UAS_TOPIC = resource.getString("SERVICE_UAS_TOPIC");
	
	public static final String BOOTSTRAP_SERVERS = resource.getString("BOOTSTRAP_SERVERS");
	
	public static final String LOCALLOG = resource.getString("LOCALLOG");
	
	public static final String COUNTLOCALLOG = resource.getString("COUNTLOCALLOG");
	
	public static final String COUNTLOCALLOGSITE = resource.getString("COUNTLOCALLOGSITE");
	//MQ
	public static final String MQ_ADDRESSES = resource.getString("MQ_ADDRESSES");
	
	public static final String MQ_PORT = resource.getString("MQ_PORT");
	
	public static final String MQ_USERNAME = resource.getString("MQ_USERNAME");
		
	public static final String MQ_PASSWORD = resource.getString("MQ_PASSWORD");

	public static final String MQ_VIRTUALHOST = resource.getString("MQ_VIRTUALHOST");
	
	public static final String MQ_EXCHANGENAME = resource.getString("MQ_EXCHANGENAME");
	
	public static final String MQ_EXCHANGETYPE = resource.getString("MQ_EXCHANGETYPE");
	
	public static final String MQ_EXCHANGEDURABLE = resource.getString("MQ_EXCHANGEDURABLE");
		
	public static final String MQ_ROUTINGKEY = resource.getString("MQ_ROUTINGKEY");

	public static final String MQ_QUEUENAME = resource.getString("MQ_QUEUENAME");
	
	public static final String MQ_CONSUMERTAG = resource.getString("MQ_CONSUMERTAG");
	
	public static final String MQ_ROUTINGKEY_L = resource.getString("MQ_ROUTINGKEY_L");
	
	public static final String MQ_EXCHANGENAME_L = resource.getString("MQ_EXCHANGENAME_L");

	//资源库

	public static final String Z_URL = resource.getString("Z_URL");
	
	public static final String Z_ID = resource.getString("Z_ID");

	public static final String Z_APPID = resource.getString("Z_APPID");
	
	public static final String Z_STR_DEFAULT_KEY = resource.getString("Z_STR_DEFAULT_KEY");
	
	public static final String HY_URL = resource.getString("HY_URL");
	
	public static final String Content_regular = resource.getString("Content_regular");
	
	public static final String Content_regular_p = resource.getString("Content_regular_p");
	
	public static final String Content_regular_v = resource.getString("Content_regular_v");
	
	public static final String Kp_conten_url = resource.getString("Kp_conten_url");
	
	public static final String resouse_path = resource.getString("resouse_path");
	
	//委办局
	
	public static final String ZWBJ_APPID = resource.getString("ZWBJ_APPID");
	
	public static final String ZWBJ_STR_DEFAULT_KEY = resource.getString("ZWBJ_STR_DEFAULT_KEY");
	
	
	//jdbc
	public static final String jdbc_driver = resource.getString("jdbc_driver");

	public static final String jdbc_url = resource.getString("jdbc_url");
	
	public static final String jdbc_username = resource.getString("jdbc_username");
	
	public static final String jdbc_password = resource.getString("jdbc_password");
	
	public static final String TABLE = resource.getString("TABLE");
	
	public static final String FIELDKP = resource.getString("FIELDKP");
	
	public static final String FIELDHY = resource.getString("FIELDHY");
	
	public static final String SOURCE = resource.getString("SOURCE");
	
	public static final String PARENTID = resource.getString("PARENTID");
	
	public static final String HYCHNLID = resource.getString("HYCHNLID");
	
	public static final String HYSITEID = resource.getString("HYSITEID");

	
	public static final String jdbc_urlids =resource.getString("jdbc_urlIIP");
	
	public static final String jdbc_usernameids = resource.getString("jdbc_usernameIIP");
	
	public static final String jdbc_passwordids = resource.getString("jdbc_passwordIIP");
	
	public static final String TABLEids =resource.getString("TABLEIIP");
	
	public static final String jdbc_urlIIP =resource.getString("jdbc_urlIIP");
	
	public static final String jdbc_usernameIIP = resource.getString("jdbc_usernameIIP");
	
	public static final String jdbc_passwordIIP = resource.getString("jdbc_passwordIIP");
	
	public static final String TABLEIIP = resource.getString("TABLEIIP");

	
	//操作用户
	public static final String USER = resource.getString("USER");
	public static final String pageSize = resource.getString("pageSize");
	
	//bpms对接
	public static final String bpms_url = resource.getString("bpms_url");
	public static final String bpms_userName = resource.getString("bpms_userName");
	public static final String bpms_password = resource.getString("bpms_password");
	public static final String bpms_file_url = resource.getString("bpms_file_url");
	public static final String bpms_from_url = resource.getString("bpms_from_url");
	public static final String bpms_from_update_url = resource.getString("bpms_from_update_url");
	
	//连接wcm数据库
	public static final String jdbc_urlwcm =resource.getString("jdbc_urlwcm");
	public static final String jdbc_usernamewcm = resource.getString("jdbc_usernamewcm");
	public static final String jdbc_passwordwcm = resource.getString("jdbc_passwordwcm");
	
	public static final String jdbc_urlmas =resource.getString("jdbc_urlmas");
	public static final String jdbc_usernamemas = resource.getString("jdbc_usernamemas");
	public static final String jdbc_passwordmas = resource.getString("jdbc_passwordmas");
	
	//海融ip
	public static final String HR_URL = resource.getString("HR_URL");
	public static final String BPMS_URL = resource.getString("BPMS_URL");
	public static final String BPMS_VIDEO = resource.getString("BPMS_VIDEO");
	
	//bpms接口参数配置 模板id
	public static final String bpms_templateCode_gr = resource.getString("bpms_templateCode_gr");
	public static final String bpms_templateCode_wz = resource.getString("bpms_templateCode_wz");
	public static final String bpms_templateCode_wx = resource.getString("bpms_templateCode_wx");
	public static final String bpms_templateCode_zm = resource.getString("bpms_templateCode_zm");
	//bpms表单id
	public static final String bpms_fromCode_gr = resource.getString("bpms_fromCode_gr");
	public static final String bpms_fromCode_wz = resource.getString("bpms_fromCode_wz");
	public static final String bpms_fromCode_wx = resource.getString("bpms_fromCode_wx");
	public static final String bpms_fromCode_zm = resource.getString("bpms_fromCode_zm");
	
	
	public static final String bpms_appName = resource.getString("bpms_appName");
	
	public static final String GET_TOKEN_URL = resource.getString("GET_TOKEN_URL");
	
	
}

