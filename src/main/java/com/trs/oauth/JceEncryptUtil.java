/*
 * Title: TRS ��ݷ����� Copyright: Copyright (c) 2004-2011, TRS��Ϣ�����ɷ����޹�˾. All rights reserved. License: see the license
 * file. Company: TRS��Ϣ�����ɷ����޹�˾(www.trs.com.cn)
 * 
 * Created: shixin@2011-3-2 ����08:18:45
 */
package com.trs.oauth;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class JceEncryptUtil {

	private static final Logger logger = LoggerFactory.getLogger(JceEncryptUtil.class);


	public static byte[] doEncrypt(Key key, byte[] toEncryptData, String transformation) {
		if (null == toEncryptData) {
			return null;
		}

		// Get a cipher object
		Cipher cipher = getCipher(key, Cipher.ENCRYPT_MODE, transformation);

		// Encrypt
		byte[] encryptedData = null;
		try {
			encryptedData = cipher.doFinal(toEncryptData);
		} catch (IllegalBlockSizeException e) {
			logger.error("get error while encrypt data by key[" + key + "] and transformation[" + transformation
					+ "], error info: " + e);
		} catch (BadPaddingException e) {
			logger.error("get error while encrypt data by key[" + key + "] and transformation[" + transformation
					+ "], error info: " + e);
		}

		return encryptedData;
	}


	public static byte[] doDecrypt(Key key, byte[] toDecryptData, String transformation) {
		if (null == toDecryptData) {
			return null;
		}

		// Get a cipher object
		Cipher cipher = getCipher(key, Cipher.DECRYPT_MODE, transformation);

		// Decrypt
		byte[] decryptedData = null;
		try {
			decryptedData = cipher.doFinal(toDecryptData);
		} catch (IllegalBlockSizeException e) {
			logger.error("get error while decrypt data by key[" + key + "] and transformation[" + transformation
					+ "], error info: " + e);
		} catch (BadPaddingException e) {
			logger.error("get error while decrypt data by key[" + key + "] and transformation[" + transformation
					+ "], error info: " + e);
		}
		return decryptedData;
	}



	public static Cipher getCipher(Key key, int cipherMode, String transformation) {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance(transformation);
		} catch (NoSuchAlgorithmException e) {
			logger.error("get error while getCipher by key[" + key + "], cipherMode[" + cipherMode
					+ "], and transformation[" + transformation + "], error info: " + e);
		} catch (NoSuchPaddingException e) {
			logger.error("get error while getCipher by key[" + key + "], cipherMode[" + cipherMode
					+ "], and transformation[" + transformation + "], error info: " + e);
		}

		try {
			cipher.init(cipherMode, key);
		} catch (InvalidKeyException e) {
			logger.error("get error while int Cipher by key[" + key + "], cipherMode[" + cipherMode + "], error info: "
					+ e);
		}

		return cipher;
	}

	public static SecretKey getKey(String algorithm, KeySpec keySpec) {
		SecretKeyFactory keyFactory = null;
		try {
			keyFactory = SecretKeyFactory.getInstance(algorithm);
		} catch (NoSuchAlgorithmException e) {
			logger.error("get error while get key by algorithm [" + algorithm + "] and keySpec [" + keySpec
					+ "], error info: " + e);
		}
		try {
			return keyFactory.generateSecret(keySpec);
		} catch (InvalidKeySpecException e) {
			logger.error("get error while generate key by algorithm [" + algorithm + "] and keySpec [" + keySpec
					+ "], error info: " + e);
		}
		logger.error("can not get key by by algorithm [" + algorithm + "] and keySpec [" + keySpec + "]");
		return null;
	}


}