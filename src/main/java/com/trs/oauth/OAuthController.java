package com.trs.oauth;

import java.io.IOException;

import com.alibaba.fastjson.JSON;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import com.trs.hy.HyUtil;
import com.trs.ids.IdsSSOUtil;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.kafka.JsonMapper;
import com.trs.kafka.KafkaUtil;
import com.trs.kafka.Producer;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;
import com.trs.oauth.HttpUtils;
import com.trs.oauth.log.TeaUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.security.MessageDigest;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.HttpClient;


/**
 * OAuth2.0Controller
 */
@Controller
@RequestMapping(value = "/oauth")
public class OAuthController{
	public static String coSessionId =null;
	public static String ssoSessionId =null;
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	// oauth
	public static final String  CLIENT_ID= ConstantUtil.CLIENT_ID;
    public static final String  CLIENT_SECRET=  ConstantUtil.CLIENT_SECRET;
    public static final String  REDIRECT_URI= ConstantUtil.REDIRECT_URI;
    public static final String  ACCESSTOKEN_URI=ConstantUtil.ACCESSTOKEN_URI+"?client_id="+CLIENT_ID+"&redirect_uri="+REDIRECT_URI+"&client_secret="+CLIENT_SECRET;
    public static final String  PROFILE_URI=ConstantUtil.PROFILE_URI;
    // ids
	private static String IDS_URL= ConstantUtil.IDS_URL;
	private static String UserName= null;
	private static String IDS_ENCRYPTPWD = ConstantUtil.IDS_ENCRYPTPWD;
	public static final String  HYIDS_URL= ConstantUtil.HYIDS_URL;
	private static String IDS_HYAPPNAME = ConstantUtil.IDS_HYAPPNAME;
	private static String IDS_APPNAME = ConstantUtil.IDS_APPNAME;
	//海云
	private static String USER = ConstantUtil.USER;
	
	
	private static String tokeids = null;
	private static  List<String> list = new ArrayList<>();
	
	
	
	/**
	 * ids通知认证接口
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = {"/idsrz"})
	public String  idsrz(HttpServletRequest request, HttpServletResponse response) throws IOException{

		return "hyids";
	
	}
	
	
	/**
	 * 开普调用的认证接口
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = {"/oauthLogin"})
	public void  oauthLogin(HttpServletRequest request, HttpServletResponse response) throws IOException{
//		System.out.println("查看ssoSessionId是否为空:"+ssoSessionId);
//		if(ssoSessionId != null){
//   		 	response.sendRedirect(REDIRECT_URI);
//		}else{
//			
//		}
		Util.log("*******************************登录开始******************************","loginUser",0);
		//获取授权码
		String code = request.getParameter("code");
		Util.log("获取令牌code="+code,"loginUser",0);
		if(code==null || "".equals(code.trim())){
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().print("获取code失败！");
			return;
		}
		System.out.println("code:::::::"+code);
		tokeids=code;
		System.out.println("code:::::::"+tokeids);
		//通过授权码获取令牌
		String token=HttpUtils.sendGet(ACCESSTOKEN_URI+"&code="+code);	
		System.out.println("记录得token：：：：：：：：：：：：：：：：：：：：：："+token);
		Util.log("通过授权码获取令牌access token="+token,"loginUser",0);
		System.out.println("access token="+token);
		if(token==null || "".equals(token.trim())){
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().print("获取access token失败！");
			return;
		}
		//通过令牌获取用户信息
		String profile=HttpUtils.sendGet(PROFILE_URI+"?"+token);
		Util.log("通过令牌获取用户信息profile="+profile,"log",0);
		System.out.println("profile="+profile);
		if(profile==null || "".equals(profile.trim())){
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().print("获取profile失败！");
			return;
		}
		// 获取到登录名
        JSONObject  jasonObject = JSONObject.fromObject(profile);
        Map map = (Map)jasonObject;
        String  value=(String ) map.get("id");
        UserName=value;
        // 判断是否有该用户
        String idsValue=getUserInfoByNameHY(value);
        if(idsValue.equals("true")&&idsValue!=null){
            //进行登录
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
            System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
            Util.log("==================================================","loginUser",0);
    		Util.log("当前登录用户判断，在海云中存在。。。。。。。","loginUser",0);
    		Util.log("用户名称：："+value,"loginUser",0);
    		Util.log("当前登录时间：：：：："+df.format(new Date()),"loginUser",0);
    		Util.log("==================================================","loginUser",0);
//    		userLoginRecord=1;
    		list.add(value);
        	LoginToken(request, response,value);
        	
        }else {
            //判断若果没有用户
        	response.setContentType("text/html;charset=UTF-8");
            response.getWriter().print(UserName+"，用户不存在：未对该用户进行同步，或同步失败，具体请查找原因！");
            return;
        }
		
	}

    /**
     * 根据用户名查询ids中是否存在该用户
     * @param userName
     * @return
     */
	@ResponseBody
	@RequestMapping(value = {"/getUserInfoByName"})
	public String getUserInfoByName(String userName){
//		userName="dev";
		String userValue = null;
		String serviceUrl = IDS_URL + "/service?idsServiceType=remoteapi&method=userQueryForManage";
		try {
			PostMethod methodPost = new PostMethod(serviceUrl);
			methodPost.addParameter("appName", IDS_HYAPPNAME);
			methodPost.addParameter("type", "json");
			String data = "userName=" + userName;
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data.getBytes("UTF-8"));
			byte[] digestByte = md.digest();
			String digest = StringHelper.toString(digestByte);
			String base64Encoded = new String(Base64.encode(data.getBytes("UTF-8")));
			String dataAfterDESEncode =  DesEncryptUtil.encryptToHex(base64Encoded.getBytes("UTF-8"), IDS_ENCRYPTPWD);
			String finalData = digest + "&" + dataAfterDESEncode;
			methodPost.addParameter("data", finalData);

			HttpClient client = new HttpClient();
			client.executeMethod(methodPost);

			String response = new String(methodPost.getResponseBody(), "utf-8");
			Util.log("认证接口获取本应用下的用户是否存在："+response,"log",0);
			// 拆分摘要和结果信息
			String[] digestAndResult = StringHelper.split(response, "&");
			String digestResult = digestAndResult[1];

			// 解密响应结果
			String afterDESResult = DesEncryptUtil.decrypt(digestResult, IDS_ENCRYPTPWD);
			String afterBase64Decode = new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
			Util.log("查询用户是否存在：解密响应结果==afterBase64Decode:"+afterBase64Decode,"log",0);
			System.out.println("==afterBase64Decode:"+afterBase64Decode);

			JSONObject  jasonObject = JSONObject.fromObject(afterBase64Decode);
			Map map = (Map)jasonObject;
			Map valueMap=(Map) map.get("entry");

			if(valueMap!=null){
				String  username=(String) valueMap.get("userName");
				userValue=username;
			}else{
				userValue="0";
			}
			Util.log("ids查询后的结果======"+userValue,"log",0);
			System.out.println("ids查询后的结果======"+userValue);
			methodPost.releaseConnection();

			return userValue;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userValue;
	}
	
	 /**
     * 根据用户名查询hy中是否存在该用户
     * @param userName
     * @return
     */
	@ResponseBody
	@RequestMapping(value = {"/getUserInfoByNameHY"})
	public String getUserInfoByNameHY(String userName){
		Map<String, String> map = new HashMap<String,String>();
		String sServiceIdSE = "gov_user";
		String sMethodNameSE = "checkUserExist";
		map.put("CurrUserName", USER); // 当前操作的用户
		map.put("SEARCHVALUE", userName); // 用户名称
		String hySE=null;
		try {
			hySE = HyUtil.dataMoveDocumentHyRbj(sServiceIdSE,sMethodNameSE,map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Util.log("认证接口查看用户是否存在："+hySE,"log",0);
		
		 JSONObject  jasonObject = JSONObject.fromObject(hySE);
	        Map map1 = (Map)jasonObject;
	        String  value=(String ) map1.get("DATA");
//		JSONObject jsonObjectSE = JSON.parseObject(hySE);
//		Object dataArrHYSE = jsonObjectSE.get("DATA");
		return value;
	}

	

	/**
	 * 退出登录  
	 * @return
	 */
	public void logout (String name){
		String userValue = "失败";
		String serviceUrl = IDS_URL + "/service?idsServiceType=httpssoservice&serviceName=logout";
		System.out.println("全部登录的用户ssoIDName："+ssoIDName);
		System.out.println("全部登录的用户coIDName："+coIDName);
		System.out.println("当前退出用户："+name);
		
		for(String key:ssoIDName.keySet()){
			System.out.println("ssoIDName中的数据："+key+"======"+ssoIDName.get(key));
			if(name.equals(ssoIDName.get(key))){
				String logoutSessionId=key;
				Util.log("退出logoutSessionId："+logoutSessionId,"log",0);
				System.out.println("退出logoutSessionId："+logoutSessionId);
				//存在登录用户 查找coSessionId
				for(String keyco:coIDName.keySet()){
					System.out.println("coIDName中的数据："+keyco+"======"+coIDName.get(keyco));
					if(name.equals(coIDName.get(keyco))){
						coSessionId=keyco;
						try {
							PostMethod methodPost = new PostMethod(serviceUrl);
//							methodPost.addParameter("coAppName", IDS_APPNAME);//ids的appname
							methodPost.addParameter("coAppName", IDS_HYAPPNAME);//中间应用的appname
							methodPost.addParameter("type", "json");
							System.out.println("data之前退出coSessionId="+coSessionId);
							System.out.println("data之前退出logoutSessionId="+logoutSessionId);
							String data ="coSessionId="+coSessionId+"&ssoSessionId="+logoutSessionId;
							
							System.out.println("退出data="+data);
							
							MessageDigest md = MessageDigest.getInstance("MD5");
							md.update(data.getBytes("UTF-8"));
							byte[] digestByte = md.digest();
							String digest = StringHelper.toString(digestByte);
							String base64Encoded = new String(Base64.encode(data.getBytes("UTF-8")));
							String dataAfterDESEncode =  DesEncryptUtil.encryptToHex(base64Encoded.getBytes("UTF-8"), IDS_ENCRYPTPWD);
							String finalData = digest + "&" + dataAfterDESEncode;
							methodPost.addParameter("data", finalData);

							HttpClient client = new HttpClient();
							client.executeMethod(methodPost);

							String response = new String(methodPost.getResponseBody(), "utf-8");
							// 拆分摘要和结果信息
							String[] digestAndResult = StringHelper.split(response, "&");
							String digestResult=null;
							if(digestAndResult.length>1){
								digestResult = digestAndResult[1];
							}else{
								digestResult = digestAndResult[0];
							}
							//关闭流
							methodPost.releaseConnection();
							// 解密响应结果
							String afterDESResult = DesEncryptUtil.decrypt(digestResult, IDS_ENCRYPTPWD);
							String afterBase64Decode = new String(Base64.decode(afterDESResult.getBytes("UTF-8")),"UTF-8");
							Util.log("退出:解密响应结果==afterBase64Decode:"+afterBase64Decode,"log",0);
							System.out.println("==afterBase64Decode:"+afterBase64Decode);
							JSONObject  jasonObject = JSONObject.fromObject(afterBase64Decode);
							Map map = (Map)jasonObject;
							Integer valueMap=(Integer) map.get("code");

							if(200==valueMap){
//								ssoIDName.remove(key,ssoIDName.get(key));
//								coIDName.remove(keyco, coIDName.get(keyco));
								System.out.println("退出后ssoIDName--key："+key);
								System.out.println("退出后coIDName--keyco："+keyco);
								System.out.println("退出后ssoIDName："+ssoIDName);
								System.out.println("退出后coIDName："+coIDName);
								System.out.println("退出code："+valueMap);
								userValue="成功";
							}
							
							Util.log("ids退出的结果======"+userValue,"log",0);
							System.out.println("ids退出的结果======"+userValue);
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	/**
	 * LoginToken 方法重定向后返回的方法  进行登录
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@RequestMapping(value = {"/IDSLogin"})
	public void IDSLogin(HttpServletRequest request,HttpServletResponse response) throws SQLException, Exception{
		//重定向后判断用户是否一致  0代表初始第一次登录   1 代表后续
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		String UNAME=request.getParameter("UN");
        Util.log("==================================================","loginUser",0);
		Util.log("重定向后日志。。。。。。","loginUser",0);
		Util.log("记录当前登录时间：：：：："+df.format(new Date()),"loginUser",0);
		Util.log("从地址中获取用户名：：："+UNAME,"loginUser",0);
		Util.log("==================================================","loginUser",0);
		list.remove(UNAME);
		if(list.size() == 0){
			
	      
	        System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
	        Util.log("==================================================","loginUser",0);
			Util.log("重定向后日志。。。。。。","loginUser",0);
			Util.log("记录当前登录时间：：：：："+df.format(new Date()),"loginUser",0);
			Util.log("==================================================","loginUser",0);
			
			
			IdsSSOUtil  util= new IdsSSOUtil();
			
			coSessionId = request.getSession().getId();
			ssoSessionId = request.getParameter("com.trs.idm.gSessionId");
			if(request.getParameter("com.trs.idm.gSessionId")==null){
				ssoSessionId = request.getParameter("trsidsssosessionid");
			}
			System.out.println("用来退出的参数ssoSessionId="+ssoSessionId);
			System.out.println("coSessionId::"+coSessionId);
			
			//登录之前需要判断是否已经登录
			boolean isLogon = false;
			isLogon=util.findUserBySSOID(request,ssoSessionId);
			if(isLogon){
				//已经登录，直接跳转
				//重定向到海云
				//通知开普云已经登录成功
				loginlog(UNAME);
				response.setHeader("Pragma","No-cache"); 
				response.setHeader("Cache-Control","no-cache"); 
				response.setDateHeader("Expires", 0);  
				System.out.println("登录后记录的Map值ssoIDName:::"+ssoIDName);
				System.out.println("重定向地址：：：http://192.141.252.5/govapp");
				response.sendRedirect("http://192.141.252.5/govapp");
			}else{
				//没有登录 进行登录
				//进行登录
				System.out.println("-------------------没有登录记录，登录开始-------------------");
				ssoIDName.put(ssoSessionId, UNAME);
				coIDName.put(coSessionId, UNAME);
				boolean userLogon = false;
				
		        Util.log("==================================================","loginUser",0);
				Util.log("========没有登录记录，登录开始。。。。======","loginUser",0);
				Util.log("用户名称："+UNAME,"loginUser",0);
				Util.log("记录当前登录时间：：：：："+df.format(new Date()),"loginUser",0);
				Util.log("==================================================","loginUser",0);
				
				userLogon = util.loginByUP(request, UNAME, ssoSessionId);
				Util.log("登录接口返回信息:"+userLogon,"log",0);
				System.out.println("登录接口返回信息:"+userLogon);
				if(userLogon){
					
					Util.log("----------------登录成功------------------","loginUser",0);
					Util.log("*******************************登录结束******************************","loginUser",0);
					//重定向到海云
					response.setHeader("Pragma","No-cache"); 
					response.setHeader("Cache-Control","no-cache"); 
					response.setDateHeader("Expires", 0);  
					System.out.println("登录后记录的Map值ssoIDName:::"+ssoIDName);
					System.out.println("重定向地址：：：http://192.141.252.5/govapp");
					//通知开普云已经登录成功
					loginlog(UNAME);
					response.sendRedirect("http://192.141.252.5/govapp");
				}else{
					response.setContentType("text/html;charset=UTF-8");
		            response.getWriter().print("登录失败");
				}
			}
			
		
		}else {
	        System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
	        Util.log("==================================================","loginUser",0);
			Util.log("用户集合:"+list,"loginUser",0);
			Util.log("记录当前登录时间：：：：："+df.format(new Date()),"loginUser",0);
			Util.log("==================================================","loginUser",0);
			//清除list
			list.clear();
			response.setContentType("text/html;charset=UTF-8");
            response.getWriter().print("登录失败，请从开普云处推出登录后，重新登录！！！");
		}
	}
	
	/**
	 * 登录成功后  调用开普接口  告诉开普登录成功了
	 */
	public static void loginlog (String username){
		
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String truename="初始值";
        String userid="初始id值";
		try {
			truename = JDBCIDS.JDBCDriverSelectUserName("select truename FROM wcmuser WHERE username = '"+username+"'");
			userid = JDBC.SelectKpuserid("select userid FROM kpuser WHERE username = '"+username+"'");
		} catch (SQLException e) {
			Util.log("==================================================","loginUser",0);
			Util.log("===============查询开普userid或者查询海云用户名失败=======================","loginUser",0);
			Util.log("==================================================","loginUser",0);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			Util.log("==================================================","loginUser",0);
			Util.log("===============查询开普userid或者查询海云用户名失败=======================","loginUser",0);
			Util.log("==================================================","loginUser",0);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		String ticid=tokeids.substring(13);
		String str = "loginName="+username+"_~_~_ticketId="+tokeids+"_~_~_accessedTime="+df.format(new Date())+"_~_~_userId="+userid+"_~_~_userName="+truename+"_~_~_appsId=0ff118e889c04fb5b886492ffc6f0135_~_~_isLoginLog=true";
		Util.log("==============================调用参数=================================","loginUser",0);
		Util.log(str,"loginUser",0);
		Util.log("==============================调用参数=======================================","loginUser",0);
		
		String encryptByTea = TeaUtil.encryptByTea(str);
		String sendGet = HttpUtils.sendGet("http://192.141.252.2:8080/uas-log/sys/log/oauthAccessLog"+"?data="+encryptByTea);
        Util.log("==================================================","loginUser",0);
		Util.log("登录日志，通知开普返回结果:"+sendGet,"loginUser",0);
		Util.log("记录当前时间：：：：："+df.format(new Date()),"loginUser",0);
		Util.log("==================================================","loginUser",0);
	}
	
	/**
	 * 清除登录记录方法
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@RequestMapping(value = {"/ssoDelete"})
	public void ssoDelete(HttpServletRequest request,HttpServletResponse response) throws SQLException, Exception{
		
		coSessionId = null;
		ssoSessionId = null;
		ssoIDName.clear();
		coIDName.clear();
		System.out.println("ssoIDName:"+ssoIDName);
		System.out.println("coIDName:"+coIDName);
	}
	
	/**
	 * 进行ids的302 重定向
	 * @param request
	 * @param response
	 */
	public void LoginToken(HttpServletRequest request, HttpServletResponse response,String value) {
		//http://192.141.7.55:8080/hyids/oauth/IDSLogin?username=
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		try {
			String cdxdz=HYIDS_URL+"?UN="+value;
			System.out.println("重定向返回地址：：："+cdxdz);
	        Util.log("==================================================","loginUser",0);
			Util.log("进入LoginToken，当前用户:"+value,"loginUser",0);
			Util.log("重定向返回地址：：：:"+cdxdz,"loginUser",0);
			Util.log("记录当前时间：：：：："+df.format(new Date()),"loginUser",0);
			
			String idsUrl =IDS_URL+"/LoginServlet?coAppName="+Base64Util.encode(IDS_HYAPPNAME)
						+"&coSessionId="+Base64Util.encode(request.getSession().getId())
						+"&surl="+Base64Util.encode(cdxdz);
			System.out.println("idsUrl:  "+idsUrl);
			response.sendRedirect(idsUrl);
			Util.log("记录当前idsUrl：：：：："+idsUrl,"loginUser",0);
			Util.log("==================================================","loginUser",0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	public void split(){
		String aaa="address";
		String [] SPN=aaa.split(","); 
		System.out.println(SPN+"------------------"+SPN.length);
		
		String aaaq="address,address";
		String [] SPNq=aaaq.split(",");
		System.out.println(SPNq+"------------------"+SPNq.length);
		
		String aaaqa="address,address,";
		String [] SPNqa=aaaqa.split(","); 
		System.out.println(SPNq+"------------------"+SPNq.length);
		 for (String string : SPNqa) {
             System.out.println(string);
         }
		for (int i = 0; i < SPNqa.length; i++) { 
			String aaaaaaa=SPNqa[i];
			System.out.println("aaaaaaa:"+aaaaaaa);
		}
		
	}
    public static void main(String[] args) {

        OAuthController oau=new OAuthController();
        oau.split();
//        System.out.println(oau.getUserInfoByName("dev"));
    }
}