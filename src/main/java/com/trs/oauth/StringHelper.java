
package com.trs.oauth;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


public class StringHelper {

	public static byte byteAt(String strHead, int index) {
		byte result = (byte) strHead.charAt(index);
		result -= '0';
		return result;
	}

	public static String[] split(String origin, String token) {
		if (isEmpty(origin))
			return null;

		final StringTokenizer st = new StringTokenizer(origin, token);
		final int countTokens = st.countTokens();
		if (countTokens <= 0) {
			return new String[] { origin };
		}
		String[] results = new String[countTokens];
		for (int i = 0; i < countTokens; i++) {
			results[i] = st.nextToken();
		}
		return results;
	}


	public static String[] mergeArrary(String[] firstArray, String[] secondArray) {
		if (isStringArrayEmpty(firstArray) && isStringArrayEmpty(secondArray))
			return null;
		if (isStringArrayEmpty(firstArray) && secondArray != null)
			return secondArray;
		if (firstArray != null && isStringArrayEmpty(secondArray))
			return firstArray;

		int firstLength = firstArray.length;
		int secondLength = secondArray.length;
		int resultLength = firstLength + secondLength;
		String[] resultArray = new String[resultLength];
		for (int i = 0; i < firstLength; i++) {
			resultArray[i] = firstArray[i];
		}
		for (int j = 0; j < secondLength; j++) {
			resultArray[firstLength + j] = secondArray[j];
		}
		return resultArray;
	}

	public static boolean isStringArrayEmpty(String[] strArray) {
		if (strArray == null || strArray.length == 0)
			return true;
		return false;
	}


	public static List splitToList(String origin, String token) {
		List result = new ArrayList();
		String[] arr = split(origin, token);
		if (arr != null) {
			for (int i = 0; i < arr.length; i++) {
				result.add(arr[i]);
			}
		}
		return result;
	}

	public static String[] splitAtFirstToken(String origin, String token) {
		if (isEmpty(origin))
			return null;

		if (isEmpty(token))
			return new String[] { origin };

		int firstTokenIndex = origin.indexOf(token, 0);
		if (firstTokenIndex == -1)
			return new String[] { origin };

		String firstString = origin.substring(0, firstTokenIndex);
		String secondString = origin.substring(firstTokenIndex + token.length(), origin.length());
		String[] arrayToReturn = new String[] { firstString, secondString };
		return arrayToReturn;
	}


	public static String reverse(String origin) {
		if (origin == null) {
			throw new NullPointerException("???????v?ll!");
		}
		return new StringBuffer(origin).reverse().toString();
	}


	public static String[] splitAlways(String origin, String token) {
		return split(origin, token);
	}

	public static String[] splitWithMutiChar(String origin, String token) {
		if (isEmpty(origin))
			return null;

		if (isEmpty(token))
			return new String[] { origin };

		if (token.length() == 1)
			return split(origin, token);

		int index = origin.indexOf(token);

		if (index < 0) {
			return new String[] { origin };
		}
		//
		return origin.split(token);
	}


	public static String toString(byte[] data) {
		if (data == null) {
			return "null!";
		}
		int l = data.length;

		char[] out = new char[l << 1];

		// two characters form the hex value.
		for (int i = 0, j = 0; i < l; i++) {
			out[j++] = DIGITS[(0xF0 & data[i]) >>> 4];
			out[j++] = DIGITS[0x0F & data[i]];
		}

		return new String(out);
	}


	public static byte[] toBytes(String str) {
		if (str == null) {
			return null;
		}
		char[] data = str.toCharArray();
		int len = data.length;

		if ((len & 0x01) != 0) {
			throw new RuntimeException("Odd number of characters!");
		}

		byte[] out = new byte[len >> 1];

		// two characters form the hex value.
		for (int i = 0, j = 0; j < len; i++) {
			int f = toDigit(data[j], j) << 4;
			j++;
			f = f | toDigit(data[j], j);
			j++;
			out[i] = (byte) (f & 0xFF);
		}

		return out;
	}


	public static String antiFilterForHTMLValue(String _sContent) {
		if (_sContent == null)
			return "";
		_sContent = _sContent.replaceAll("&amp;", "&");
		_sContent = _sContent.replaceAll("&lt;", "<");
		_sContent = _sContent.replaceAll("&gt;", ">");
		_sContent = _sContent.replaceAll("&quot;", "\"");

		return _sContent;
	}


	public static String toString(Object[] objs) {
		return toString(objs, false, ", ");
	}


	public static String toString(Object[] objs, boolean showOrder) {
		return toString(objs, showOrder, ",");
	}


	public static String toString(Object[] objs, boolean showOrder, String token) {
		if (objs == null) {
			return "null";
		}
		int len = objs.length;
		StringBuffer sb = new StringBuffer(10 * len);
		for (int i = 0; i < len; i++) {
			if (showOrder) {
				sb.append(i).append(':');
			}
			sb.append(objs[i]);
			if (i < len - 1) {
				sb.append(token);
			}
		}
		return sb.toString();
	}

	public static String avoidNull(String str) {
		if ("null".equals(str)) {
			return "";
		}
		return (str == null) ? "" : str;
	}

	public static String trim(String str) {
		return (str == null) ? "" : str.trim();
	}

	private static final char[] DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
			'f' };

	/**
	 * Converts a hexadecimal character to an integer.
	 * 
	 * @param ch
	 *            A character to convert to an integer digit
	 * @param index
	 *            The index of the character in the source
	 * @return An integer
	 * @throws RuntimeException
	 *             Thrown if ch is an illegal hex character
	 */
	static int toDigit(char ch, int index) {
		int digit = Character.digit(ch, 16);
		if (digit == -1) {
			throw new RuntimeException("Illegal hexadecimal charcter " + ch + " at index " + index);
		}
		return digit;
	}



	public static String replaceWithRegex(String templateStr, Map<String, ?> data, Pattern pattern) {
		if (isEmpty(templateStr))
			return null;

		if (data == null)
			data = Collections.EMPTY_MAP;

		StringBuffer newValue = new StringBuffer(templateStr.length());
		Matcher matcher = pattern.matcher(templateStr);
		while (matcher.find()) {
			String key = matcher.group(1);
			if (data.get(key) != null) {
				String r = data.get(key).toString();
				matcher.appendReplacement(newValue, r.replaceAll("\\\\", "\\\\\\\\"));
			}
		}

		matcher.appendTail(newValue);

		return newValue.toString();
	}


	public static String replaceStr(String _strSrc, String _strOld, String _strNew) {
		if (_strSrc == null)
			return null;


		char[] srcBuff = _strSrc.toCharArray();
		int nSrcLen = srcBuff.length;
		if (nSrcLen == 0)
			return "";

		char[] oldStrBuff = _strOld.toCharArray();
		int nOldStrLen = oldStrBuff.length;
		if (nOldStrLen == 0 || nOldStrLen > nSrcLen)
			return _strSrc;

		StringBuffer retBuff = new StringBuffer((nSrcLen * (1 + _strNew.length() / nOldStrLen)));

		int i, j, nSkipTo;
		boolean bIsFound = false;

		i = 0;
		while (i < nSrcLen) {
			bIsFound = false;


			if (srcBuff[i] == oldStrBuff[0]) {
				for (j = 1; j < nOldStrLen; j++) {
					if (i + j >= nSrcLen)
						break;
					if (srcBuff[i + j] != oldStrBuff[j])
						break;
				}
				bIsFound = (j == nOldStrLen);
			}


			if (bIsFound) {
				retBuff.append(_strNew);
				i += nOldStrLen;
			} else {
				if (i + nOldStrLen >= nSrcLen) {
					nSkipTo = nSrcLen - 1;
				} else {
					nSkipTo = i;
				}
				for (; i <= nSkipTo; i++) {
					retBuff.append(srcBuff[i]);
				}
			}
		}// end while
		srcBuff = null;
		oldStrBuff = null;
		return retBuff.toString();
	}


	public static String[] splitAndTrim(String origin, String token) {
		if (origin == null) {
			return null;
		}
		origin = origin.trim();
		final StringTokenizer st = new StringTokenizer(origin, token);
		final int countTokens = st.countTokens();
		if (countTokens <= 0) {
			return new String[] { origin };
		}
		List strs = new ArrayList(countTokens);
		String str;
		for (int i = 0; i < countTokens; i++) {
			str = st.nextToken().trim();
			if (str.length() > 0) {
				strs.add(str);
			}
		}
		return (String[]) strs.toArray(new String[0]);
	}

	public static String hexToStr(String hex) {
		return new String(toBytes(hex));
	}


	public static String truncateAndTrim(String str, String delim) {
		if (str == null || delim == null) {
			return str;
		}
		int nStart = str.indexOf(delim);
		if (nStart < 0) {
			return str;
		}
		return str.substring(nStart + delim.length()).trim();
	}

	public static String getStringByEncoding(String originalStr, String encoding) {

		String encodingStr = originalStr;
		try {
			encodingStr = new String(originalStr.getBytes(), encoding);
		} catch (UnsupportedEncodingException e) {
			return originalStr;
		}
		return encodingStr;

	}

	public static boolean isEmpty(String str) {
		return (str == null || str.trim().length() == 0);
	}


	public static String adjustLength(String str, int maxLength) {
		if (str == null) {
			return str;
		}
		if (maxLength <= 0) {
			throw new IllegalArgumentException("Illegal value of maxLength: " + maxLength
					+ "! It must be a positive integer.");
		}
		int strLength = str.length();
		if (maxLength > strLength) {
			return str;
		}
		final String DELIT = "...";
		StringBuffer sb = new StringBuffer(maxLength);
		int splitPos = (maxLength - DELIT.length()) / 2;
		sb.append(str.substring(0, splitPos));
		sb.append(DELIT);
		sb.append(str.substring(strLength - splitPos));
		return sb.toString();
	}

	public static String getURLSafe(String url) {
		if (url == null || "".equals(url))
			return "";

		StringBuffer strBuff = new StringBuffer();
		char[] charArray = url.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			if (charArray[i] == '<' || charArray[i] == '>')
				continue;

			strBuff.append(charArray[i]);
		}
		return strBuff.toString();
	}

	public static final String KEYVALUE_SPLITTER = "=";

	public static final String PROPERTY_SPLITTER = "&";

	/**
	 * 
	 * @param properties
	 * @return
	 */
	public static Map String2Map(String properties) {
		return String2Map(properties, PROPERTY_SPLITTER, KEYVALUE_SPLITTER);
	}


	public static Map String2Map(String properties, String outerSplitter, String innerSplitter) {
		if (isEmpty(properties)) {
			return null;
		}

		Map outProperties = new Hashtable();
		StringTokenizer tokenizerOuter = new StringTokenizer(properties, outerSplitter);
		while (tokenizerOuter.hasMoreTokens()) {
			String currProperty = tokenizerOuter.nextToken();
			int index = currProperty.indexOf(innerSplitter);
			if (index != -1) {
				String value = currProperty.substring(index + 1, currProperty.length());
				outProperties.put(currProperty.substring(0, index), value);
			}
		}
		return outProperties;
	}

	/**
	 * 
	 * @param inProperties
	 * @return
	 */
	public static String Map2String(Map inProperties) {
		return Map2String(inProperties, PROPERTY_SPLITTER, KEYVALUE_SPLITTER);
	}

	/**
	 * 
	 * @param inProperties
	 * @return
	 */
	public static String Map2String(Map inProperties, String outerSplitter, String innerSplitter) {
		StringBuffer propertiesBuffer = new StringBuffer();
		for (Iterator i = inProperties.keySet().iterator(); i.hasNext();) {
			String key = (String) i.next();
			Object valueObj = inProperties.get(key);
			propertiesBuffer.append(key).append(innerSplitter).append(valueObj).append(outerSplitter);
		}

		return propertiesBuffer.toString();
	}


	public static List StringArrayToList(String[] strArray) {
		if (strArray == null || strArray.length == 0)
			return new ArrayList();

		List list2Return = new ArrayList();
		for (int i = 0; i < strArray.length; i++) {
			String str = strArray[i];
			list2Return.add(str);
		}
		return list2Return;
	}


	public static Set StringArrayToSet(String[] strArray) {
		if (strArray == null || strArray.length == 0)
			return null;

		Set set2Return = new HashSet();
		for (int i = 0; i < strArray.length; i++) {
			String str = strArray[i];
			set2Return.add(str);
		}
		return set2Return;
	}


	public static int length(String str) {
		if (isEmpty(str))
			return 0;
		else
			return str.length();
	}

	public static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine() method. We iterate until the
		 * BufferedReader return null which means there's no more data to read. Each line will appended to a
		 * StringBuilder and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuffer sb = new StringBuffer();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sb.toString();
	}

	public static String convertStreamToString(InputStream is, String charSet) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine() method. We iterate until the
		 * BufferedReader return null which means there's no more data to read. Each line will appended to a
		 * StringBuilder and returned as String.
		 */

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(is, charSet));
		} catch (UnsupportedEncodingException e1) {
		}
		StringBuffer sb = new StringBuffer();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sb.toString();
	}

	/**
	 *
	 * @return
	 * @creator fx @ Aug 10, 2009
	 */
	public static String filterXSS(String value) {
		if (value == null || value.length() == 0) {
			return "";
		}
		value = value.replaceAll("<", "& lt;").replaceAll(">", "& gt;");
		value = value.replaceAll("\\(", "& #40;").replaceAll("\\)", "& #41;");
		value = value.replaceAll("'", "& #39;");
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		// value = value.replaceAll("script", "");
		return value;
	}

	public static String filterXSSWithoutQuotation(String value) {
		if (value == null || value.length() == 0) {
			return "";
		}
		value = value.replaceAll("<", "& lt;").replaceAll(">", "& gt;");
		value = value.replaceAll("\\(", "& #40;").replaceAll("\\)", "& #41;");
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		return value;
	}


	public static final String toString(Object[] number, String token) {
		StringBuffer sb = new StringBuffer();
		if (number != null) {
			int length = number.length;
			for (int i = 0; i < number.length; i++) {
				sb.append(number[i]);
				if (i != length - 1) {
					sb.append(token);
				}
			}
		}
		return sb.toString();
	}

	public static int parseInt(String value) {
		return parseInt(value, -1);
	}


	public static int parseInt(String value, int defValue) {
		if (StringHelper.isEmpty(value)) {
			return defValue;
		}
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException ex) {
			return -1;
		}
	}


	public static long parseLong(String value) {
		if (StringHelper.isEmpty(value)) {
			return -1;
		}
		try {
			return Long.parseLong(value);
		} catch (NumberFormatException ex) {
			return -1;
		}
	}


	public static double parseDouble(String value) {
		if (StringHelper.isEmpty(value)) {
			return -1;
		}
		try {
			return Double.parseDouble(value);
		} catch (NumberFormatException ex) {
			return -1;
		}
	}


	public static boolean parseBoolean(String value) {
		if (StringHelper.isEmpty(value)) {
			return false;
		}
		try {
			return Boolean.parseBoolean(value);
		} catch (Exception ex) {
			return false;
		}
	}


	public static String substringByFirstOccurance(String origin, String begin, String end) {
		if (isEmpty(origin)) {
			return null;
		}


		if (!isEmpty(begin) && !isEmpty(end) && begin.equals(end))
			return null;


		if ((!isEmpty(begin) && origin.indexOf(begin) < 0) || (!isEmpty(end) && origin.indexOf(end) < 0))
			return null;

		int beginIndex = 0;


		if (isEmpty(begin)) {
			beginIndex = 0;
		}

		if (!isEmpty(begin) && origin.indexOf(begin) >= 0) {
			beginIndex = origin.indexOf(begin) + begin.length();
		}

		int endIndex = 0;

		if (isEmpty(end)) {
			endIndex = origin.length();
		}

		if (!isEmpty(end) && origin.indexOf(end, beginIndex) >= 0) {
			endIndex = origin.indexOf(end, beginIndex);
		}
		if (endIndex <= beginIndex)
			return null;

		String result = origin.substring(beginIndex, endIndex);
		if (isEmpty(result))
			return null;
		return result;
	}

	/**
	 * @since v3.5
	 * @creator liushen @ Jan 15, 2010
	 */
	public static int parseIntUsingFormat(String source, String formatPattern) {
		DecimalFormat df = new DecimalFormat(formatPattern);
		try {
			return df.parse(source).intValue();
		} catch (ParseException e) {
			return -1;
		}
	}


	public static String parseIntAsStringUsingFormat(int intToFormat, String formatPattern) {
		DecimalFormat df = new DecimalFormat(formatPattern);
		long intOfLong = 0;
		try {
			intOfLong = new Integer(intToFormat).longValue();
		} catch (Exception e) {
			return null;
		}

		try {
			return df.format(intOfLong);
		} catch (Exception e) {
			return null;
		}
	}

	public static String excludeParamInQueryString(String queryString, String excludeParam) {
		if (isEmpty(queryString))
			return null;

		if (isEmpty(excludeParam))
			return queryString;

		String[] strArray = split(queryString, "&");
		StringBuffer str2Return = new StringBuffer();
		if (strArray == null)
			return null;
		for (int i = 0; i < strArray.length; i++) {
			String str = strArray[i];

			if (!isEmpty(str) && str.indexOf(excludeParam) < 0) {
				if (i > 0)
					str2Return.append("&");

				str2Return.append(str);
			}

		}
		String toReturnQueryString = str2Return.toString();
		if (!isEmpty(toReturnQueryString) && toReturnQueryString.indexOf("&") == 0)
			toReturnQueryString = substringByFirstOccurance(toReturnQueryString, "&", null);
		return toReturnQueryString;
	}

	public static String string2Json(String str) {
		StringBuffer sb = new StringBuffer(str.length() + 20);
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			switch (c) {
			case '\"':
				sb.append("\\\"");
				break;
			case '\\':
				sb.append("\\\\");
				break;
			case '/':
				sb.append("\\/");
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\t':
				sb.append("\\t");
				break;
			default:
				sb.append(c);
			}
		}
		return sb.toString();
	}

	public final static String bytesToHex(byte[] buf, int off, int len) {
		char[] out = new char[len * 2];

		for (int i = 0, j = 0; i < len; i++) {
			int a = buf[off++];

			out[j++] = DIGITS[(a >>> 4) & 0X0F];
			out[j++] = DIGITS[a & 0X0F];
		}
		return (new String(out));
	}

	public final static String removeFromStringWithSeperator(String stringWithSeperator, String stringToRemove,
			String seperator) {
		if (StringHelper.isEmpty(stringWithSeperator))
			return null;

		if (StringHelper.isEmpty(stringToRemove))
			return stringWithSeperator;

		String[] strArray = StringHelper.split(stringWithSeperator, seperator);
		StringBuffer str2Return = new StringBuffer();
		if (strArray == null)
			return null;
		for (int i = 0; i < strArray.length; i++) {
			String str = strArray[i];

			if (!StringHelper.isEmpty(str) && str.indexOf(stringToRemove) < 0) {
				if (i > 0)
					str2Return.append(seperator);

				str2Return.append(str);
			}

		}
		String toReturnString = str2Return.toString();
		if (!StringHelper.isEmpty(toReturnString) && toReturnString.indexOf(seperator) == 0)
			toReturnString = StringHelper.substringByFirstOccurance(toReturnString, seperator, null);
		return toReturnString;
	}
	public static String replaceVariableContentWithValue(String contentWithKeyValue, String variableName) {
		if (StringHelper.isEmpty(variableName) || variableName.indexOf("%") == -1) {
			return contentWithKeyValue;
		}
		if (StringHelper.isEmpty(contentWithKeyValue)) {
			return contentWithKeyValue;
		}
		String variableNameFront = variableName.substring(0, variableName.length() - 1);
		// System.out.println("variableNameFront=" + variableNameFront);
		if (contentWithKeyValue.indexOf(variableNameFront) == -1) {
			return contentWithKeyValue;
		}

		String variableMatchReg = variableNameFront + "=" + "([^%]*)" + "%";
		// System.out.println("variableMatchReg=" + variableMatchReg);
		Pattern p = Pattern.compile(variableMatchReg);
		Matcher m = p.matcher(contentWithKeyValue);
		while (m.find()) {
			// System.out.println("contentWithKeyValue=" + contentWithKeyValue);
			// int startIndex = m.start();
			// System.out.println("startIndex=" + startIndex);
			// int endIndex = m.end();
			// System.out.println("endIndex=" + endIndex);
			String matchGroup = m.group();
			matchGroup = matchGroup.replaceAll("\\?", "\\\\?");
			matchGroup = matchGroup.replaceAll("\\$", "\\\\\\$");
			matchGroup = matchGroup.replaceAll("\\{", "\\\\{");
			matchGroup = matchGroup.replaceAll("\\.", "\\\\."); // .
			matchGroup = matchGroup.replaceAll("\\^", "\\\\\\^"); // ^
			matchGroup = matchGroup.replaceAll("\\[", "\\\\["); // [
			matchGroup = matchGroup.replaceAll("\\(", "\\\\("); // (
			matchGroup = matchGroup.replaceAll("\\)", "\\\\)"); // )
			matchGroup = matchGroup.replaceAll("\\|", "\\\\|"); // |
			matchGroup = matchGroup.replaceAll("\\*", "\\\\*"); // *
			matchGroup = matchGroup.replaceAll("\\+", "\\\\+"); // +
			// matchGroup = matchGroup.replaceAll("\\", "\\\\\\"); // \

			// System.out.println("matchGroup=" + matchGroup);
			String replacedValue = matchGroup.substring(variableNameFront.length() + 1, matchGroup.length() - 1);
			// System.out.println("replacedValue=" + replacedValue);

			contentWithKeyValue = contentWithKeyValue.replaceFirst(matchGroup, replacedValue);
			// System.out.println("contentWithKeyValue=" + contentWithKeyValue);
		}

		return contentWithKeyValue.replaceAll(variableName, "");
	}

	public static String generateRandomCode(int length) {
		StringBuffer code = new StringBuffer();
		code.delete(0, code.capacity() - 1);
		Random random = new Random((new Date()).getTime());
		for (int i = 0; i < length; i++) {
			code.append(random.nextInt(length + 1));
		}
		return code.toString();
	}

	public static String avoidEmpty(String str, String defaultValue) {
		return isEmpty(str) ? defaultValue : str;
	}

	public static boolean isNotEmpty(String str) {
		return false == isEmpty(str);
	}

	public static String filterAndCharacter(String str) {
		if (isEmpty(str)) {
			return "";
		}
		return str.replaceAll("&", "&amp;");
	}

	public static String getRandomLower(int length) {
		if (length <= 0) {
			return "";
		}
		String str = "";
		for (int i = 0; i < length; i++) {
			str += String.valueOf((char) Math.round(Math.random() * 25 + 97));
		}
		return str;
	}

	public static String getRandomUpper(int length) {
		if (length <= 0) {
			return "";
		}
		String str = "";
		for (int i = 0; i < length; i++) {
			str += String.valueOf((char) Math.round(Math.random() * 25 + 65));
		}
		return str;
	}

	public static String getRandomNumber(int length) {
		if (length <= 0) {
			return "";
		}
		String str = "";
		Random r = new Random();
		for (int i = 0; i < length; i++) {
			str += r.nextInt(10);
		}
		return str;
	}

	public static String generateRandomCodeMixed(int length) {
		return generateRandomCodeMixed(length, true);
	}

	public static String generateRandomCodeMixed(int length, boolean limitMinLength) {
		if (limitMinLength && length < 8) {
			length = 8;
		}
		String str = "";
		for (int i = 0; i < length; i++) {
			str += String.valueOf((char) Math.round(Math.random() * 74 + 48));
		}
		return str;
	}

	public static boolean contains(String[] orignalStrs, String key) {
		if (null == orignalStrs || orignalStrs.length == 0) {
			return false;
		}

		if (isEmpty(key)) {
			return false;
		}
		for (String value : orignalStrs) {
			if (value.trim().equals(key.trim())) {
				return true;
			}
		}
		return false;
	}

	public static String replaceCRLF(String str) {
		if (str == null) {
			return str;
		}
		str = str.replaceAll("\r\n", " ");
		str = str.replaceAll("\n", " ");
		str = str.replaceAll("\r", " ");
		return str;
	}


	public static int getRandomNum(int maxValue) {
		if (maxValue == 0 || maxValue < 0) {
			return 0;
		}
		double randomNum = Math.random();
		int random = (int) (randomNum * maxValue);
		if (random == 0) {
			return 1;
		}
		return random;
	}


	public static String substring(String originalStr, int firstPlace, int lastPlace) {
		if (firstPlace < 0) {
			firstPlace = 0;
		}
		if (lastPlace < 0) {
			lastPlace = 0;
		}
		int length = length(originalStr);
		if (StringHelper.isEmpty(originalStr) || !(firstPlace < lastPlace) || firstPlace > length) {
			return "";
		}
		if (lastPlace > length) {
			lastPlace = length;
		}
		return originalStr.substring(firstPlace, lastPlace);
	}


	public static String compress(String str) {
		if (isEmpty(str)) {
			return str;
		}
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			GZIPOutputStream gzip = new GZIPOutputStream(out);
			gzip.write(str.getBytes());
			gzip.close();
			return out.toString("ISO-8859-1");
		} catch (Throwable e) {
			e.printStackTrace();
			return str;
		}
	}


	public static String uncompress(String str, String encoding) {
		if (isEmpty(str)) {
			return str;
		}
		if (isEmpty(encoding)) {
			encoding = "utf-8";
		}
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(str.getBytes("ISO-8859-1"));
			GZIPInputStream gunzip = new GZIPInputStream(in);
			byte[] buffer = new byte[4000];
			int n;
			while ((n = gunzip.read(buffer)) >= 0) {
				out.write(buffer, 0, n);
			}
			return out.toString(encoding);
		} catch (Throwable e) {
			e.printStackTrace();
			return str;
		}
	}


	public static String smartAppendSuffix(String str, String endingStr) {
		if (str == null) {
			return null;
		}
		return str.endsWith(endingStr) ? str : str + endingStr;
	}


	public static String csrfReplace(String source, String csrfToken) {
		StringBuffer csrfBuffer = new StringBuffer((int) (source.length() * 1.2));
		int index = 0, indexPrev = 0;
		do {
			index = source.indexOf(".jsp", indexPrev);
			if (index == -1) {
				break;
			}
			char questionMark = '-';
			try {
				questionMark = source.charAt(index + 4);
			} catch (Throwable e) {
			}
			if ('?' == questionMark) {
				csrfBuffer.append(source.substring(indexPrev, index + 5));
				csrfBuffer.append("csrftoken=").append(csrfToken).append("&");
				indexPrev = index + 5;
			} else {
				csrfBuffer.append(source.substring(indexPrev, index + 4));
				csrfBuffer.append("?csrftoken=").append(csrfToken);
				indexPrev = index + 4;
			}
		} while (index < source.length());
		csrfBuffer.append(source.substring(indexPrev, source.length()));
		return csrfBuffer.toString();
	}

	public static String buildUrl(String url, String params) {
		if (StringHelper.isEmpty(params)) {
			return url;
		}
		String concat = "?";
		if (url.contains("?")) {
			concat = "&";
		}
		return url + concat + params;
	}
}