package com.trs.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.trs.jdbc.JDBCIDS;

public class JDBCUpdateCountnull {
	static JDBCIDS  jdbcids=new JDBCIDS();
	
	public static void main(String[] args) throws SQLException, Exception {
		//政策文件栏目id
		String channlid="12824";
		//组织机构代码 名称
		String organName="市国资委";
		upIdxidAndOrgancat(channlid,organName);
	}
	
	public static void upIdxidAndOrgancat(String channelID,String organ){
		try {
			ResultSet  rs=jdbcids.JDBCSelectAllView("select * from wcmmetatablewebzcwj zcwj where zcwj.ChannelId in("+channelID+")");
			
			 while(rs.next()){  // 遍历结果集ResultSet
				 int id = rs.getInt("MetaDataId");  // 数据id
				 String idxid = rs.getString("idxid");// 索引号  
				 ResultSet  rsOrg=jdbcids.JDBCSelectAllView("select * from xwcmclassinfo info where info.CNAME ='"+organ+"'"); 
				 while(rsOrg.next()){  // 遍历结果集ResultSet
					 String CLASSINFOID = rsOrg.getString("CLASSINFOID");  //写入organcat 字段  机构id
					 String CCODE = rsOrg.getString("CCODE");  // 与 idxid拼接   
					 
					 Integer  uppsd=jdbcids.JDBCDriver("update wcmmetatablewebzcwj zcwj set zcwj.organcat='"+CLASSINFOID+"' , zcwj.idxid='"+CCODE+idxid+"' where zcwj.MetaDataId='"+id+"'");
					 if(uppsd == 1){
						 System.out.println("更新成功！！！");
					 }else {
						 System.out.println("=================================================================================================");
						 System.out.println("================================MetaDataId为："+id+" 更新失败====================================");
						 System.out.println("=================================================================================================");
					 }
				 }
			 }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
