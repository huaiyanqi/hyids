package com.trs.addDocWBJK;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIIP;





/**
 * OAuth2.0Controller 
 */
@Controller
@RequestMapping(value = "/wbjk")
public class addDocController{
//	private static String Z_APPID = ConstantUtil.Z_APPID;
//	private static String USER = ConstantUtil.USER;
//	private static String TABLE = ConstantUtil.TABLE;
//	private static String FIELDKP = ConstantUtil.FIELDKP;
//	private static String FIELDHY = ConstantUtil.FIELDHY;
//	private static String SOURCE = ConstantUtil.SOURCE;
//	private static String PARENTID = ConstantUtil.PARENTID;
//	private static String Content_regular = ConstantUtil.Content_regular;
//	private static String Content_regular_p = ConstantUtil.Content_regular_p;
//	private static String Content_regular_v = ConstantUtil.Content_regular_v;
//	private static String resouse_path = ConstantUtil.resouse_path;
//	private static String pageSize = ConstantUtil.pageSize;
	
	ResResource  re= new ResResource();
	ChannelReceiver cR=new ChannelReceiver();
	HttpFileUpload hfl=new HttpFileUpload();
	JDBCIIP iip=new JDBCIIP();
	JDBC  jdbc=new JDBC();
	
	
	

	/**
	 * 外部调用新增接口
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@ResponseBody
	@RequestMapping(value = {"/webaddDoc"},method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String  webaddDoc(@RequestParam(value ="data") String data) throws Exception {
		
		data = new String (data.getBytes("ISO8859-1"),"utf-8");
		System.out.println("------------------外部推送文档开始---------------");
		JSONObject jsondata = JSON.parseObject(data);
		String type=jsondata.getString("TYPE");//判断推送视图
		Map<String, Object> result = new HashMap<String, Object>();
		if(data != null ){
			if("1".equals(type)){//1 默认视图
				//默认视图
				String zxresult=documentZX(data);
				System.out.println("外部默认视图新增结果："+zxresult);
				System.out.println("------------------外部推送默认视图文档结束---------------");
				return zxresult;
			}else{
				//自定义视图
				String zdyresult=documentZDY(data);
				System.out.println("外部自定义视图返回结果："+zdyresult);
				System.out.println("------------------外部推送自定义视图文档结束---------------");
				return zdyresult;
//				return "自定义视图无法测试";
			}
			
		}else{
			result.put("code", "-1");
			result.put("data", "外部新增接收数据失败!");
			System.out.println("外部新增接收数据失败!");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------外部推送文档结束---------------");
			return jsonResult.toString();
		}
		
	}
	
	
	/*
	 * 咨询视图同步数据
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String  documentZX(String data) throws Exception {
		
		JSONObject jsonObject = JSON.parseObject(data);
		
		
		String userName=jsonObject.getString("USERNAME");
		Map result = new HashMap();
		Map oPostData = new HashMap();
		String  dataArr =jsonObject.get("DATA").toString();//根据json对象中数组的名字解析出其所对应的值
		Map maps = (Map)JSON.parse(dataArr);  
		for (Object map : maps.entrySet()){  
			
			System.out.println(((Map.Entry)map).getKey()+"   =====推送默认数据=====   " + ((Map.Entry)map).getValue()); 
			if("fj".equals(((Map.Entry)map).getKey())){
				//处理附件
				Object ObjFj= jsonObject.get("DATA");//根据json对象中数组的名字解析出其所对应的值
				JSONObject jsonObjectHYGPSE = JSON.parseObject(ObjFj.toString());
				JSONObject valobj = jsonObjectHYGPSE.getJSONObject("fj");
				for (Object fjVfj : valobj.entrySet()){  
					oPostData.put(((Map.Entry)fjVfj).getKey(), ((Map.Entry)fjVfj).getValue());
				}
			}else{
				oPostData.put(((Map.Entry)map).getKey(), ((Map.Entry)map).getValue());	
			}
			
	    } 
		oPostData.put("CurrUserName", userName); // 当前操作的用户
		String sServiceId = "gov_webdocument";
		String sMethodName = "saveDocumentInWeb";
		System.out.println("调用海云之前的参数："+oPostData);
		String hy=null;
		try {
			hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("code", "-1");
			result.put("data", "调用新增接口失败！！！！！");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------开普推送文档结束---------------");
			return jsonResult.toString();
		}

		return hy;
	}
	
	
	/*
	 * 自定义视图同步数据
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String  documentZDY(String data) throws Exception {
		
		System.out.println("------------------外部自定义视图推送文档开始---------------");
		JSONObject jsonObject = JSON.parseObject(data);
		String userName=jsonObject.getString("USERNAME");
		Map result = new HashMap();
		Map oPostData = new HashMap();
		String  dataArr =jsonObject.get("DATA").toString();//根据json对象中数组的名字解析出其所对应的值
		Map maps = (Map)JSON.parse(dataArr); 
		for (Object map : maps.entrySet()){  
			
			System.out.println(((Map.Entry)map).getKey()+"   =====外部自定义视图推送默认数据=====   " + ((Map.Entry)map).getValue()); 
			if("fj".equals(((Map.Entry)map).getKey())){
				//处理附件
				Object ObjFj= jsonObject.get("DATA");//根据json对象中数组的名字解析出其所对应的值
				JSONObject jsonObjectHYGPSE = JSON.parseObject(ObjFj.toString());
				JSONObject valobj = jsonObjectHYGPSE.getJSONObject("fj");
				for (Object fjVfj : valobj.entrySet()){  
					oPostData.put(((Map.Entry)fjVfj).getKey(), ((Map.Entry)fjVfj).getValue());
				}
			}else{
				oPostData.put(((Map.Entry)map).getKey(), ((Map.Entry)map).getValue());	
			}
			
	    } 
		oPostData.put("CurrUserName", userName); // 当前操作的用户
		String sServiceId = "gov_webdocument";
		String sMethodName = "saveDocumentInOpenData";
		System.out.println("外部自定义视图调用海云之前的参数："+oPostData);
		String hy=null;
		try {
			hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("code", "-1");
			result.put("data", "外部自定义视图调用新增接口失败！！！！！");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------外部自定义视图推送文档结束---------------");
			return jsonResult.toString();
		}

		return hy;
	}
	
	
	/**
	 * html标签去除
	 * @param htmlStr
	 * @return
	 */
	public static String delHTMLTag(String htmlStr){ 
        String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式 
        String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式 
        String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式 
         
        Pattern p_script=Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE); 
        Matcher m_script=p_script.matcher(htmlStr); 
        htmlStr=m_script.replaceAll(""); //过滤script标签 
         
        Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE); 
        Matcher m_style=p_style.matcher(htmlStr); 
        htmlStr=m_style.replaceAll(""); //过滤style标签 
         
        Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE); 
        Matcher m_html=p_html.matcher(htmlStr); 
        htmlStr=m_html.replaceAll(""); //过滤html标签 

        return htmlStr.trim(); //返回文本字符串 
    } 
}