package com.trs.wechat;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.bpmsutil.BPMSUtil;
import com.trs.bpmsutil.HttpController;
import com.trs.bpmsutil.JDBCSelectDoc;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;


@Controller
@RequestMapping(value = "/tobpmswx")
public class BpmsWeChatController{
	
	private static String bpms_url= ConstantUtil.bpms_url;
	public static final String  bpms_userName= ConstantUtil.bpms_userName;
	private static String bpms_password = ConstantUtil.bpms_password;
	private static String bpms_templateCode_wx = ConstantUtil.bpms_templateCode_wx;
	private static String bpms_appName = ConstantUtil.bpms_appName;
	private static String bpms_from_url = ConstantUtil.bpms_from_url;


		
	
	/**
	 * 正式获取token
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ResponseBody
	@RequestMapping(value = {"/wxtobpms"},produces = "application/json; charset=utf-8")
	public  String wxToBpms(HttpServletRequest request,HttpServletResponse response) throws SQLException, Exception{
		
		System.out.println("--------------------------------------微信同步bpms开始--------------------------------------------------");
		Util.log("--------------------------------------微信同步bpms开始--------------------------------------------------","bpms",0);
		
//		String docid ="179"; 
//		String qd ="wx";
//		String loginname ="zhaolichao";
//		String sig="1";
		
		String docid = request.getParameter("docid");
		String sig = request.getParameter("sig");
		String qd = request.getParameter("qd");
		String loginname = request.getParameter("loginname");//bpms_loginName
		String lgname=StringUtils.substringBefore(loginname,"%40"); 
		lgname=StringUtils.substringBefore(lgname,"@");
		lgname=StringUtils.substringBefore(lgname,"_");
		
		System.out.println("当前登录用户处理@，用于获取token：："+lgname);
		System.out.println("当前登录用户，用于获取token：："+loginname);
		//先查询是否设置封面图
//		boolean isfmt=JDBCSelectDoc.selectFengMian(docid);
//		if(!isfmt){
//			Map hrMap =new HashMap<String, String>();
//			//返回海融数据
//			hrMap.put("code", "-1");	
//			hrMap.put("message", "稿件没有设置封面图，无法上传！！稿件id为["+docid+"]");	
//			JSONObject hrJson =new JSONObject(hrMap);
//			System.out.println("微信同步bpms返回给海融："+hrJson);
//			Util.log("--------------------------------------微信同步bpms结束--------------------------------------------------","bpms",0);
//			System.out.println("--------------------------------------微信同步bpms结束--------------------------------------------------");
//			return hrJson.toString();
//		}
		
		//查询bpmslog  判断是否需要调用修改接口
		int bpmssig=JDBCSelectDoc.selectBpmslog(docid,"wx");// 1推送过未结束流程  2推送了已经结束流程  其他未推送
		if(bpmssig == 1){  // 调用bpms修改接口
			String bpmsupresult =BPMSUtil.bpmsUpdateWX(docid,qd,lgname);

    		Util.log("--------------------------------------微信同步修改bpms结束--------------------------------------------------","bpms",0);
    		System.out.println("--------------------------------------微信同步修改bpms结束--------------------------------------------------");
			return bpmsupresult;
			
		}else if(bpmssig == 2){
			Map hrMap =new HashMap<String, String>();
			//返回海融数据
			hrMap.put("code", "-1");	
			hrMap.put("message","流程已经结束,需要特殊处理！！！");
			JSONObject hrJson =new JSONObject(hrMap);
			System.out.println("bpms无法唤起已经结束流程的稿件，需要特殊处理！！！");
			Util.log("--------------------------------------微信同步bpms结束--------------------------------------------------","bpms",0);
			System.out.println("--------------------------------------微信同步bpms结束--------------------------------------------------");
			return hrJson.toString();
		}

		//获取token
		String result=HttpController.testGetToken(bpms_userName,bpms_password,lgname,bpms_url);
		//解析返回结果
		JSONObject jsonObjectHYSE = JSON.parseObject(result);
		String token = jsonObjectHYSE.get("id").toString();//获取到token
        
		//查询到需要传给bpms接口的书文档数据
		HashMap<String,String> bpmsmap=JDBCSelectDoc.selectWeChatDoc(docid);//基础数据

		bpmsmap.put("稿件id", docid);
		bpmsmap.put("现状态方式", sig);
		bpmsmap.put("流程分支判断", sig);
		bpmsmap.put("渠道标识", qd);
		System.out.println("基础数据：："+bpmsmap);
		//查询附件 并进行上传
		HashMap<String,String> bpmsfile=JDBCSelectDoc.selectFile(bpmsmap,docid,token,qd);//基础数据

		
		//定义返回map  第一层data
		Map resma =new HashMap<String, String>();
		resma.put("data", bpmsfile);
		resma.put("subject", bpmsmap.get("文章标题"));
		resma.put("draft", "0");
		
		resma.put("templateCode", bpms_templateCode_wx);
		
		//定义返回map  最二层data
		Map resmap =new HashMap<String, String>();
		resmap.put("data", resma);
		resmap.put("appName", bpms_appName);

		//转换成json数据
		JSONObject json =new JSONObject(resmap);
		
		//调用上传表单接口
		//设置表单头
	    Map<String, String> maphead = new HashMap<String, String>();
	    maphead.put("token", token);
	    Util.log("bpms调用json："+json.toJSONString(),"bpms",0);
	    System.out.println("bpms调用json："+json.toJSONString());
	    //调用接口
//	    String post = doPost1("fuserest","609011ab-8662-4678-9280-a8eff259a288","seeyonas","http://123.121.155.161:801/seeyon/rest/token");

	    String postfrom = sendPost(bpms_from_url,json.toJSONString(),maphead);
	    Util.log("bpms发起表单接口返回结果："+postfrom,"bpms",0);
	    System.out.println("bpms发起表单接口返回结果：："+postfrom);
	    //解析结果
		JSONObject jsonform = JSON.parseObject(postfrom);
		String formcode = jsonform.get("code").toString();//获取到token
		
		// 2推送了已经结束流程   0 没有查到数据  未推送
    	Date date = new Date();
    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String last_time = dtf.format(date);
    	System.out.println("推送时的时间：："+last_time);
		String bpmslogsql="INSERT INTO xwcmmetadatabpmslog (`recid`, `channel`, `pushtime`, `pushtitle`, `pushsig`, `returnjson`, `bpmsopinion`, `bpmspushtime`) VALUES "
				+ "('"+docid+"', '"+qd+"', '"+last_time+"', '"+bpmsmap.get("文章标题")+"', '1', '"+jsonform+"', '', NULL);";
		int resu=JDBCSelectDoc.insertBpmsLog(bpmslogsql);
		if(resu>0){
			Util.log("添加记录表成功："+bpmslogsql,"bpms",0);
			System.out.println("添加记录表成功："+bpmslogsql);
		}else{
			Util.log("添加记录失败："+bpmslogsql,"bpms",0);
			System.out.println("添加记录表失败："+bpmslogsql);
		}
		
		//修改稿件状态
		String updateUnderreView=" UPDATE WCMChnlDoc  SET underreview = '1' WHERE  RECID = '"+docid+"' ";
		int uuv=JDBCSelectDoc.updataSql(updateUnderreView);
		if(uuv>0){
			Util.log("修改稿件状态成功："+updateUnderreView,"bpms",0);
			System.out.println("修改稿件状态成功："+updateUnderreView);
		}else{
			Util.log("修改稿件状态失败："+bpmslogsql,"bpms",0);
			System.out.println("修改稿件状态失败："+updateUnderreView);
		}
		
		Map hrMap =new HashMap<String, String>();
		//返回海融数据
		if("0".equals(formcode)){
			
			hrMap.put("code", "0");	
			hrMap.put("message","上传成功!");	
			
		}else{
			hrMap.put("code", "-1");	
			hrMap.put("message", "上传失败！！稿件id为["+docid+"]");	
		}
		JSONObject hrJson =new JSONObject(hrMap);
		System.out.println("微信同步bpms返回给海融："+hrJson);
		Util.log("--------------------------------------微信同步bpms结束--------------------------------------------------","bpms",0);
		System.out.println("--------------------------------------微信同步bpms结束--------------------------------------------------");
		return hrJson.toString();
        
	}
	
	
	/**
	 * 获取微信预览账号
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ResponseBody
	@RequestMapping(value = {"/getWxUser"},produces = "application/json; charset=utf-8")
	public  String getWxUserName() throws SQLException, Exception{
		
		System.out.println("--------------------------------------获取微信预览账号开始--------------------------------------------------");
		Util.log("--------------------------------------获取微信预览账号开始--------------------------------------------------","bpms",0);

		String  resu=JDBCSelectDoc.selectWXUser();
		System.out.println("微信预览账号：：：："+resu);
		

		
		Util.log("--------------------------------------获取微信预览账号结束--------------------------------------------------","bpms",0);
		System.out.println("--------------------------------------获取微信预览账号结束--------------------------------------------------");
		return resu;
        
	}
	
	
	 /**
	  * @param url 访问地址
	  *  @param param 需要传输参数参数；对象可以通过json转换成String
	  * @param header header 参数；可以通过下面工具类将string类型转换成map
	  * @return 返回网页返回的数据
	  */
	 public static String sendPost(String url, String param, Map<String, String> header)  {
		 
//		 	System.out.println("微信进入sendPost：-url："+url);
//		 	System.out.println("微信进入sendPost-param："+param);
//		 	System.out.println("微信进入sendPost-header："+header);
		 
	        HttpURLConnection conn = null;
	        String responseContent = null;
	        try {
	         OutputStreamWriter out;
	         URL realUrl = new URL(url);
	         // 打开和URL之间的连接
	         conn = (HttpURLConnection) realUrl.openConnection();
	         //设置超时时间
	         conn.setConnectTimeout(5000);
	         conn.setReadTimeout(15000);
	         // 设置通用的请求属性
	         if (header!=null) {
	             for (Entry<String, String> entry : header.entrySet()) {
	                 conn.setRequestProperty(entry.getKey(), entry.getValue());
	             }
	         }
	         conn.setRequestMethod("POST");
	         conn.addRequestProperty("Content-Type", "application/json");
	         conn.setRequestProperty("accept", "*/*");
	         conn.setRequestProperty("connection", "Keep-Alive");
	         conn.setRequestProperty("user-agent",
	                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	         // 发送POST请求必须设置如下两行
	         conn.setDoOutput(true);
	         conn.setDoInput(true);
	         // 获取URLConnection对象对应的输出流
	         out = new OutputStreamWriter( conn.getOutputStream(),"UTF-8");// utf-8编码
	         // 发送请求参数
	         out.write(param);

	         // flush输出流的缓冲
	         out.flush();
	               
	         
	         int responseCode = conn.getResponseCode();  
	         InputStream in1=null;
	         // 定义BufferedReader输入流来读取URL的响应
			 if (responseCode == 200) {  
				 in1 = new BufferedInputStream(conn.getInputStream());  
			 } else {  
				 in1 = new BufferedInputStream(conn.getErrorStream());  
			 } 
             BufferedReader rd = new BufferedReader(new InputStreamReader(in1,"utf8"));
             String tempLine = rd.readLine();
             StringBuffer tempStr = new StringBuffer();
             String crlf = System.getProperty("line.separator");
             while (tempLine != null) {
                tempStr.append(tempLine);
                tempStr.append(crlf);
                tempLine = rd.readLine();
             }
             responseContent = tempStr.toString();
	         if(out!=null){
	             out.close();
	         }
	        } catch (IOException e) {
	        	Util.log(param,"网络故障WD",0); 
	            System.out.println("网络故障");
	            e.printStackTrace();
	        } finally {
	        	System.out.println("进入finally");
	            if (conn != null) {
	            	conn.disconnect();
	            }
	        }
	         return responseContent;
	     }

	 
    public static void main(String[] args) throws Exception {
    	
    	HttpServletRequest request=null;
    	HttpServletResponse response=null;
//    	BpmsWeChatController.wxToBpms(request,response);
    	
//    	HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
//        //加载html模版
//        imageGenerator.loadHtml("D:\\data\\ZGRT_Resources\\lgn.html");
//        //把html写入到图片
//        imageGenerator.saveAsImage("D:\\hello-world.png");
    	
//    	String ZhuBanSet="{\"PICUPLOADTYPE\":1,\"ISCREATEXIAOYANG\":true,\"SPACENUMTYPE\":43,\"ISADDTITLEINXIAOYANG\":false,\"XIAOYANGFILEPATHFORWIN\":\"/TRS/paper/zgrtb\",\"PAGETEMPLATEPATH\":\"/TRS/paper/zgrtb\",\"XIAOYANGFILEPATH\":\"/TRS/paper/zgrtb\",\"FITFILEBAKPATH\":\"/TRS/paper/zgrtb\",\"FITFILEPATH\":\"/TRS/paper/zgrtb\",\"COLORIMAGEXIAOYANGPATH\":\"/TRS/paper/zgrtb\",\"COLORIMAGEXIAOYANGPATHFORWIN\":\"/TRS/paper/zgrtb\",\"DAYANGMONITORPATH\":\"/TRS/paper/zgrtb\",\"DAYANGMONITORPATHFORWIN\":\"/TRS/paper/zgrtb\",\"GRAYIMAGEXIAOYANGPATH\":\"/TRS/paper/zgrtb\",\"GRAYIMAGEXIAOYANGPATHFORWIN\":\"/TRS/paper/zgrtb\",\"CHANGLIUMONITORPATH\":\"/TRS/paper/zgrtb\",\"ADFILEPATH\":\"/TRS/paper/zgrtb\",\"DPI\":\"240\",\"ISCREATEPS\":false,\"ISCREATEPDF\":false,\"ISCOPYPSTOCL\":false,\"ISCOPYVFTTOCL\":false,\"ISCOPYPDFTOCL\":false,\"ISCOPYICONPICTOCL\":false,\"ISCOPYBRIEFPICTOCL\":false,\"ISCOPYACTUALPICTOCL\":false,\"ISCREATEPAGEICONPIC\":false,\"ICONPICINFO\":{\"MAXWIDTH\":\"1024\",\"MAXHEIGHT\":\"1024\",\"QUALITY\":\"0\",\"DPI\":\"2048\"},\"ISCREATEPAGEBRIEFPIC\":true,\"BRIEFPICINFO\":{\"MAXWIDTH\":\"1024\",\"MAXHEIGHT\":\"1024\",\"QUALITY\":\"0\",\"DPI\":\"2048\"},\"ISCREATEPAGEACTUALPIC\":true,\"ACTUALPICINFO\":{\"MAXWIDTH\":\"1024\",\"MAXHEIGHT\":\"1024\",\"QUALITY\":\"0\",\"DPI\":\"2048\"},\"SIGNUSES\":[],\"EDITORS\":[],\"PRINTFTP\":{},\"FACTORYFTP\":{},\"DYFILEFTP\":{},\"FITFILEFTP\":{},\"WATERMARKIMGPATHFORWIN\":\"/TRS/paper/zgrtb\",\"WATERMARKIMGPATH\":\"/TRS/paper/zgrtb\",\"FTFILEPATH\":\"/TRS/paper/zgrtb\",\"DAYANGPATH\":\"/TRS/paper/zgrtb\",\"FTVERSION\":\"/TRS/paper/zgrtb\",\"READYDAYANGPATH\":\"/TRS/paper/zgrtb\",\"XYPICTEMPORARYDIR\":\"/TRS/paper/zgrtb\"}";
//    	JSONObject oPaperZhunBanSet= JSONObject.parseObject(ZhuBanSet);
//    	String a=oPaperZhunBanSet.getString("FITFILEPATH");
//
//        String [] pt=a.split("paper");
//        String repath=pt[1];
//    	System.out.println(repath);
    	
    	
    	String a="2022-07-29-?o??a???????-1???";
    	String b=BPMSUtil.encodePathVariable(a);
    	
    }
    

}