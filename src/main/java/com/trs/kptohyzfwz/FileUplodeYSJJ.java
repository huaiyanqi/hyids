package com.trs.kptohyzfwz;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.trs.hy.HttpFileUpload;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

public class FileUplodeYSJJ {
	
	private static String resouse_path = ConstantUtil.resouse_path;
	
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	
	/**
	 * 处理政府网站 政策文件元数据集
	 * @param htmlStr
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public static Map fujianYSJJZCWJ(Map oPostData,String dowurl) throws Exception{ 
		
		if(dowurl.indexOf("group1")>=0){
			String[] url=dowurl.split(",");
			List flist=new ArrayList<>();
			for(int s=0;s<url.length;s++){

				  byte[] file=zfwzUtil.downloadFile(url[s]);
				  String fjName=url[s].substring(url[s].lastIndexOf("/")+1);
				  System.out.println("元数据集附件处理:"+fjName);
				  FileOutputStream downloadFile = new FileOutputStream(resouse_path+fjName);
			      downloadFile.write(file, 0, file.length);
			      downloadFile.flush();
			      downloadFile.close();
				  //上传海云
				  System.out.println("--------------下载之后上传到海云开始-----------------");
				  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+fjName);
				  String fileName = oDispatch.getUploadShowName();
				  System.out.println("上传附件之后的附件名称"+fileName);

				  Map pmap=new HashMap<>();
				  pmap.put("SrcFile", fjName);
				  pmap.put("AppFile", fileName);
				  pmap.put("AppDesc", fjName.substring(0,fjName.lastIndexOf(".")));
				  pmap.put("AppendixId", "0");
				  flist.add(pmap);
				  Util.log("处理附件："+pmap+"------------------"+flist,"kplog",0);
				  System.out.println("处理附件："+pmap+"------------------"+flist);
			
			}
			oPostData.put("fj",JSON.toJSONString(flist));
		}else{
			Util.log("<fj>附件格式存在问题！！！==！"+dowurl,"kplog",0);
			System.out.println("<fj>附件格式存在问题！！！==！"+dowurl);	
		}
		return oPostData;
	}
	
	/**
	 * 处理政府网站  领导照片 元数据集
	 * @param htmlStr
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public static Map fujianFJJLDZP(Map oPostData,String dowurl) throws Exception{ 
		
		if(dowurl.indexOf("group1")>=0){
			String[] url=dowurl.split(",");
			List flist=new ArrayList<>();
			for(int s=0;s<url.length;s++){
				
				  byte[] file=zfwzUtil.downloadFile(url[s]);
				  String fjName=url[s].substring(url[s].lastIndexOf("/")+1);
				  Util.log("元数据集附件处理:"+fjName,"kplog",0);
				  System.out.println("元数据集附件处理:"+fjName);
				  FileOutputStream downloadFile = new FileOutputStream(resouse_path+fjName);
			      downloadFile.write(file, 0, file.length);
			      downloadFile.flush();
			      downloadFile.close();
				  //上传海云
				  System.out.println("--------------下载之后上传到海云开始-----------------");
				  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+fjName);
				  String fileName = oDispatch.getUploadShowName();
				  System.out.println("上传附件之后的附件名称"+fileName);

				  Map pmap=new HashMap<>();
				  pmap.put("SrcFile", fjName);
				  pmap.put("AppFile", fileName);
				  pmap.put("AppDesc", fjName.substring(0,fjName.lastIndexOf(".")));
				  pmap.put("AppendixId", "0");
				  flist.add(pmap);
				  Util.log("处理附件："+pmap+"------------------"+flist,"kplog",0);
				  System.out.println("处理附件："+pmap+"------------------"+flist);
			
			}
			oPostData.put("fjldzp",JSON.toJSONString(flist));
		}else{
			Util.log("<fjldzp>领导照片附件格式存在问题！！！==！"+dowurl,"kplog",0);
			System.out.println("<fjldzp>领导照片附件格式存在问题！！！==！"+dowurl);
		}

		
		return oPostData;
	}
	
	
	/**
	 * 处理政府网站 元数据集
	 * @param htmlStr
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public static Map fujianYSJJFJ(Map oPostData,String dowurl,String fjkeyName) throws Exception{
		if(dowurl.indexOf("group1")>=0){
			String[] url=dowurl.split(",");
			List flist=new ArrayList<>();
			for(int s=0;s<url.length;s++){
				  byte[] file=zfwzUtil.downloadFile(url[s]);
				  String fjName=url[s].substring(url[s].lastIndexOf("/")+1);
				  System.out.println("元数据集附件处理:"+fjName);
				  FileOutputStream downloadFile = new FileOutputStream(resouse_path+fjName);
			      downloadFile.write(file, 0, file.length);
			      downloadFile.flush();
			      downloadFile.close();
				  //上传海云
				  System.out.println("--------------下载之后上传到海云开始-----------------");
				  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+fjName);
				  String fileName = oDispatch.getUploadShowName();
				  System.out.println("上传附件之后的附件名称"+fileName);

				  Map pmap=new HashMap<>();
				  pmap.put("SrcFile", fjName);
				  pmap.put("AppFile", fileName);
				  pmap.put("AppDesc", fjName.substring(0,fjName.lastIndexOf(".")));
				  pmap.put("AppendixId", "0");
				  flist.add(pmap);
				  Util.log("处理附件："+pmap+"------------------"+flist,"kplog",0);
				  System.out.println("处理附件："+pmap+"------------------"+flist);
			}
			oPostData.put(fjkeyName,JSON.toJSONString(flist));
		}else{
			Util.log(fjkeyName+"==附件格式存在问题！！！==！"+dowurl,"kplog",0);
			System.out.println(fjkeyName+"==附件格式存在问题！！！==！"+dowurl);	
		}
		
		return oPostData;
	}

}
