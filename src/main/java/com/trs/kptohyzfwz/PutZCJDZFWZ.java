package com.trs.kptohyzfwz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class PutZCJDZFWZ {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map putZCJDzfwz(Map oPostData,JSONObject jsondata){ 
		
		//处理非默认字段
		for(String str:jsondata.keySet()){
			
			if(str.indexOf("@@")>=0){
				String valueKp=jsondata.get(str).toString();
				String[] nameStrArray=str.split("@@");
				String vName=nameStrArray[0];//视图短名
				String zName=nameStrArray[1];//字段名称
				//开始处理字段
				if("webzcjd".equals(vName.toLowerCase())){
					System.out.println("oPostData@@处理:"+oPostData);
					System.out.println("开普推送过来的数据政府网站政策解读键值对:"+str + "==========:===========" +jsondata.get(str));
					
					if(zName.indexOf("_NAME")>=0){
						oPostData.put(zName.split("_")[0],"");
					}else{
						//处理包含字段 特殊字符
						oPostData.put(zName, valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}
					
				}
			}
		}
		
		return oPostData;
	}
	
}
