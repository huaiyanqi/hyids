package com.trs.kptohyzfwz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.kafka.Util;

public class PutLDJLZFWZ {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map putLDJLzfwj(Map oPostData,JSONObject jsondata) throws Exception{ 
		
		//处理非默认字段
		for(String str:jsondata.keySet()){
			
			if(str.indexOf("@@")>=0){
				String valueKp=jsondata.get(str).toString();
//				System.out.println("获取到的值："+valueKp);
				String[] nameStrArray=str.split("@@");
				String vName=nameStrArray[0];//视图短名
				String zName=nameStrArray[1];//字段名称
				//开始处理字段
				if("webldjl".equals(vName.toLowerCase())){
					Util.log("开普推送过来的政府网站政策文件数据政策文件键值对:"+str+ "==========:===========" +jsondata.get(str),"kplog",0);
					System.out.println("开普推送过来的政府网站政策文件数据政策文件键值对:"+str+ "==========:===========" +jsondata.get(str));
					
					if("xb".equals(zName.toLowerCase())){
						//处理有效性
						if("女".equals(valueKp)){
							oPostData.put("xb", "1"); 	
						}else if("男".equals(valueKp)){
							oPostData.put("xb", "0");
						}
					}else if("fjldzp".equals(zName.toLowerCase())){
						if(valueKp!=null){
							Util.log("元数据集附件：："+valueKp,"kplog",0);
							System.out.println("元数据集附件：："+valueKp);
							//处理元数据集附件
							oPostData=FileUplodeYSJJ.fujianFJJLDZP(oPostData, valueKp);
						}
						
					}else{
						//处理其他字段
						oPostData.put(zName, valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}
				}
			}
		}
		
		return oPostData;
	}
	
}
