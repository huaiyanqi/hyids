package com.trs.kptohyzfwz;

import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

public class PutZXZFWZ {

	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = "document";
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String pageSize = ConstantUtil.pageSize;
	
	ResResource  re= new ResResource();
	ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	
	/*
	 * 咨询视图同步数据
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String  documentZX(String jsonData) throws Exception {
		
		Util.log("默认视图：：："+jsonData,"kplog",0);
		System.out.println("------------------开普推送文档开始处理数据---------------");
		Map result = new HashMap();
		Map oPostData = new HashMap();
		JSONObject jsondata = JSON.parseObject(jsonData);
//		JSONObject jsondata = jsonObjectSE.getJSONObject("data");
		if(jsonData != null){
			//接收数据成功
			String siteId=jsondata.getString("siteId");//站点id
			String channelId=jsondata.getString("channelId");//栏目id
			String resID=jsondata.getString("resId");//文档资源id
			
			String DOCTITLE=jsondata.getString("name");//标题
//			String SUBDOCTITLE=jsondata.getString("GOVDOCNEWS@@SUBDOCTITLE");//副标题
//			String DOCKEYWORDS=jsondata.getString("GOVDOCNEWS@@DOCKEYWORDS");//关键词
			String DOCABSTRACT=jsondata.getString("abstracts");//摘要
			String DOCSOURCENAME=jsondata.getString("source");//来源
			String DOCRELTIME=jsondata.getString("pubDate");//撰写时间
			String DOCAUTHOR=jsondata.getString("author");//作者
//			String DOCPUBTIME=jsondata.getString("GOVDOCNEWS@@DOCPUBTIME");//发布时间
			String content=jsondata.getString("content");//正文
			
			String type=jsondata.getString("type");//1图片类型；2网页类型;3音频类型；4视频类型;5文档类型
			
			oPostData.put("ChannelId", channelId);
			oPostData.put("ObjectId", "0");
			oPostData.put("DocTitle", DOCTITLE);
			
			//处理正文中附件
			String pcontent=null;
			String fcontent=null;
			Util.log("=============================默认视图正文============================"+content,"kplog",0);
			System.out.println("=============================默认视图正文============================");
			System.out.println(content);
			System.out.println("=============================默认视图正文============================");
			if(content==null){
				oPostData.put("DocHtmlCon", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			}else{
				//处理图片
				pcontent=ZWuplod.contentResourceUpload(content,5,"(src=\").*?(\")");
				//处理附件
				fcontent=ZWuplod.contentResourceUpload(pcontent,6,"(href=\").*?(\")");
				oPostData.put("DocHtmlCon", fcontent);
			}
			//去除html标签
			String txtContent=delHTMLTag(content);
			Util.log("去除html正文：：："+txtContent,"kplog",0);
			System.out.println("去除html标签txtContent："+txtContent);
			oPostData.put("DocContent", txtContent);
			if(DOCABSTRACT != null){
				oPostData.put("DocAbstract", DOCABSTRACT);
			}
			if(DOCAUTHOR != null){
				oPostData.put("DOCAUTHOR", DOCAUTHOR);
			}
//			if(SUBDOCTITLE != null){
//				oPostData.put("SUBDOCTITLE", SUBDOCTITLE);
//			}
			if(DOCSOURCENAME != null){
				oPostData.put("DOCSOURCENAME", DOCSOURCENAME);
			}
//			if(DOCKEYWORDS != null){
//				oPostData.put("DOCKEYWORDS", DOCKEYWORDS);
//			}
			if(DOCRELTIME != null){
				oPostData.put("DOCRELTIME", DOCRELTIME);
			}
//			if(DOCPUBTIME != null){
//				oPostData.put("DOCPUBTIME", DOCPUBTIME);
//			}
			if("2".equals(type)){ 
				System.out.println("---------------------------开普推送文档类型进入--------------------");
				oPostData.put("DOCTYPE",20);  //文字
				//正文不做处理，直接处理附件
				JSONArray attach=jsondata.getJSONArray("attachments");//相附件
				System.out.println("附件：：：："+attach);
				if(attach!=null){
					 List plist=new ArrayList<>();
					 List flist=new ArrayList<>();
					for(int i = 0; i < attach.size(); i++){
						  String  kuURL="http://192.141.1.10:7002/repo-web";
						  String  fjName=attach.getJSONObject(i).getString("name");
						  String  fjpath= attach.getJSONObject(i).getString("path");
						  String  fjtype=attach.getJSONObject(i).getString("type");
						  System.out.println("fjtype:"+fjtype);
						  if(fjpath != null){
							  //截取后调用开普下载接口
							  String[] dowpath=fjpath.split("filePath=");
							  String writName=fjpath.substring(fjpath.lastIndexOf("/")+1);
							  //下载开普资源
							  Util.log("咨询视图下载地址：：：："+dowpath[1],"kplog",0);
							  ZWuplod.upload(dowpath[1],writName);
							  //上传海云
							  System.out.println("--------------下载之后上传到海云开始-----------------");

							  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+writName);
							  String fileName = oDispatch.getUploadShowName();
							  Util.log("咨询视图上传附件之后的附件名称：：：："+fileName,"kplog",0);
							  System.out.println("上传附件之后的附件名称"+fileName);
							  System.out.println("fjtype:"+fjtype);
							  //图片处理
							  if("1".equals(fjtype)){
								  Map fmap=new HashMap<>();
								  fmap.put("SrcFile", fjName);
								  fmap.put("AppFile", fileName);
								  fmap.put("AppDesc", fjName.substring(0,fjName.lastIndexOf(".")));
								  fmap.put("AppendixId", "0");
								  plist.add(fmap);
								  System.out.println("处理图片："+fmap+"------------------"+plist);
							  }else if("5".equals(fjtype)||"3".equals(fjtype)||"4".equals(fjtype)){//处理附件
								  System.out.println("==============处理附件开始=================="); 
								  Map pmap=new HashMap<>();
								  pmap.put("SrcFile", fjName);
								  pmap.put("AppFile", fileName);
								  pmap.put("AppDesc", fjName);
								  pmap.put("AppendixId", "0");
								  flist.add(pmap);
								  System.out.println("处理附件："+pmap+"------------------"+flist);
							  }
							
						}
					}
					Util.log("图片附件处理：：：："+JSON.toJSONString(plist),"kplog",0);
					Util.log("附件处理："+JSON.toJSONString(flist),"kplog",0);
					System.out.println("图片附件处理："+JSON.toJSONString(plist));
					System.out.println("附件处理："+JSON.toJSONString(flist));
					if(plist.size()!=0) {
						oPostData.put("DocRelPic",JSON.toJSONString(plist));
					}
					if(flist.size()!=0) {
						oPostData.put("docRelFile",JSON.toJSONString(flist));
					}
				}else{
					System.out.println("========================开普推送的资源无附件需要处理=====================");
				}
				oPostData.put("CurrUserName", USER); // 当前操作的用户
				String sServiceId = "gov_webdocument";
				String sMethodName = "saveDocumentInWeb";
				Util.log("调用海云之前的参数："+oPostData,"kplog",0);
				System.out.println("调用海云之前的参数："+oPostData);
				String hy=null;
				try {
					hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					result.put("code", "-1");
					result.put("data", "调用新增接口失败！！！！！");
					JSONObject jsonResult =new JSONObject(result);
					System.out.println("------------------开普推送文档结束---------------");
					return jsonResult.toString();
				}
				Util.log("海云返回结果："+hy,"kplog",0);
				JSONObject jsonObjectHYSE = JSON.parseObject(hy);
				Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
				JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
				
				Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
				System.out.println("海云返回结果："+jsonObjectHYSE.get("MSG"));
				if("true".equals(dataArrHYSave)){
					
					String	docid =jsonObjectHYGPSE.getString("DOCID");//标题
					Util.log("文档新建到海云存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')","log",0);
					System.out.println("开普同步到海云存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')");
					Integer jd=null;
					try {
						jd = jdbc.JDBCDriver("INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						result.put("code", "-1");
						result.put("data", "中间表插入失败！！");
						JSONObject jsonResult =new JSONObject(result);
						System.out.println("------------------开普推送文档结束---------------");
						return jsonResult.toString();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						result.put("code", "-1");
						result.put("data", "中间表插入失败！！");
						JSONObject jsonResult =new JSONObject(result);
						System.out.println("------------------开普推送文档结束---------------");
						return jsonResult.toString();
					}
					if(jd!=-1){
						result.put("code", "0");
						result.put("data", "已成功接收推送数据");
						Util.log("开普同步到海云中间表存入成功!","log",0);
						System.out.println("开普同步到海云中间表存入成功!");
						JSONObject jsonResult =new JSONObject(result);
						System.out.println("------------------开普推送文档结束---------------");
						return jsonResult.toString();
					}else{
						result.put("code", "-1");
						result.put("data", "开普同步到海云中间表存入失败!");
						Util.log("开普同步到海云中间表存入失败!","log",0);
						System.out.println("开普同步到海云中间表存入失败!");
						System.out.println("------------------开普推送文档结束---------------");
						JSONObject jsonResult =new JSONObject(result);
						return jsonResult.toString();
					}
					
				}else{
					result.put("code", "-1");
					result.put("data", jsonObjectHYSE.get("MSG"));
					Util.log(jsonObjectHYSE.get("MSG"),"log",0);
					System.out.println(jsonObjectHYSE.get("MSG"));
					System.out.println("------------------开普推送文档结束---------------");
					JSONObject jsonResult =new JSONObject(result);
					return jsonResult.toString();
				}
				
			}else if(type == null||"".equals(type)||type.isEmpty()){
				result.put("code", "-1");
				result.put("data", "数据格式中，没有返回文章类型，无法进行解析");
				Util.log("数据格式中，没有返回文章类型，无法进行解析","log",0);
				System.out.println("数据格式中，没有返回文章类型，无法进行解析");
				System.out.println("------------------开普推送文档结束---------------");
				JSONObject jsonResult =new JSONObject(result);
				return jsonResult.toString();
			}else {
				result.put("code", "-1");
				result.put("data", "非网页类型文章，不做处理");
				Util.log( "非网页类型文章，不做处理","log",0);
				System.out.println( "非网页类型文章，不做处理");
				System.out.println("------------------开普推送文档结束---------------");
				JSONObject jsonResult =new JSONObject(result);
				return jsonResult.toString();
			}
			
			
		}else{
			result.put("code", "-1");
			result.put("data", "接收开普数据失败!");
			System.out.println("接收开普数据失败!");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------开普推送文档结束---------------");
			return jsonResult.toString();
		}
	}
	
	
	
	
	/**
	 * html标签去除
	 * @param htmlStr
	 * @return
	 */
	public static String delHTMLTag(String htmlStr){ 
        String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式 
        String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式 
        String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式 
         
        Pattern p_script=Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE); 
        Matcher m_script=p_script.matcher(htmlStr); 
        htmlStr=m_script.replaceAll(""); //过滤script标签 
         
        Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE); 
        Matcher m_style=p_style.matcher(htmlStr); 
        htmlStr=m_style.replaceAll(""); //过滤style标签 
         
        Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE); 
        Matcher m_html=p_html.matcher(htmlStr); 
        htmlStr=m_html.replaceAll(""); //过滤html标签 

        return htmlStr.trim(); //返回文本字符串 
    } 
	
}
