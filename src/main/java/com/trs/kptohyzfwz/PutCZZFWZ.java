package com.trs.kptohyzfwz;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.trs.kafka.Util;

public class PutCZZFWZ {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map putCZzfwj(Map oPostData,JSONObject jsondata) throws Exception{ 
		
		//处理非默认字段
		for(String str:jsondata.keySet()){
			
			if(str.indexOf("@@")>=0){
				String valueKp=jsondata.get(str).toString();
//				System.out.println("获取到的值："+valueKp);
				String[] nameStrArray=str.split("@@");
				String vName=nameStrArray[0];//视图短名
				String zName=nameStrArray[1];//字段名称
				//开始处理字段
				if("webcz".equals(vName.toLowerCase())){
					Util.log("开普推送过来的政府网站财政数据政策文件键值对:"+str+ "==========:===========" +jsondata.get(str),"kplog",0);
					System.out.println("开普推送过来的政府网站财政数据政策文件键值对:"+str+ "==========:===========" +jsondata.get(str));
					oPostData.put(zName, valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
				}
			}
		}
		
		return oPostData;
	}
	
}
