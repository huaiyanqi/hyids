package com.trs.kptohyzfwz;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;

public class PutZFGBZFWZ {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map putZFGBzfwj(Map oPostData,JSONObject jsondata) throws Exception{ 
		
		//处理非默认字段
		for(String str:jsondata.keySet()){
			
			if(str.indexOf("@@")>=0){
				String valueKp=jsondata.get(str).toString();
//				System.out.println("获取到的值："+valueKp);
				String[] nameStrArray=str.split("@@");
				String vName=nameStrArray[0];//视图短名
				String zName=nameStrArray[1];//字段名称
				//开始处理字段
				if("webzfgb".equals(vName.toLowerCase())){
					System.out.println("开普推送过来的政府网站政府公报数据政策文件键值对:"+str+ "==========:===========" +jsondata.get(str));
					if("fjgbfy".equals(zName.toLowerCase())||"fjgbqzb".equals(zName.toLowerCase())){
						System.out.println("元数据集附件：："+valueKp);
						//处理元数据集附件
						oPostData=FileUplodeYSJJ.fujianYSJJFJ(oPostData, valueKp,zName.toLowerCase());
					}else {
						oPostData.put(zName, valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}

				}
			}
		}
		
		return oPostData;
	}
	
}
