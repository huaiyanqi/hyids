package com.trs.kptohyzfwz;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

public class ZWuplod {
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	
	/**
	 * 下载开普流文件
	 * @param url
	 * @param fjName
	 * @param resousePath
	 * @throws Exception
	 */
	public static void upload(String url,String fjName) throws Exception{
		
		System.out.println("开普下载附件地址："+url);
		byte[] file=zfwzUtil.downloadFile(url);
		FileOutputStream downloadFile = new FileOutputStream(resouse_path+fjName);
		downloadFile.write(file, 0, file.length);
		downloadFile.flush();
		downloadFile.close();
		
	}
	
	/**
	 * 处理正文方法
	 * @param content
	 * @param sub
	 * @param regular
	 * @return
	 * @throws Exception 
	 */
    @SuppressWarnings("static-access")
	public static String contentResourceUpload(String content,int sub,String regular) throws Exception {
    	
    	System.out.println("处理正文开始："+content);
    	
    	String regExImg = regular;//正文中图片 
    	
		List<String> list=getContentResource(regExImg, sub, content);
		
		for(int i = 0 ; i < list.size() ; i++) {
			if(list.get(i).indexOf("filePath=")>=0){

				System.out.println(list.get(i));
				//获取名称
				String [] splitName=list.get(i).split("/");
				String name=splitName[splitName.length-1];
				
				//获取下载地址
				String [] splitUrl=list.get(i).split("filePath=");
				String url=splitUrl[1];
				//开始下载
				upload(url,name);
				Util.log("正文附件上传之前地址：：：："+resouse_path+name,"content",0);
				System.out.println("正文附件上传之前地址：：：："+resouse_path+name);
				Dispatch upDispatch = WCMServiceCaller.UploadFile(resouse_path+name);
				// 获取上传后返回的文件名
				String file = upDispatch.getUploadShowName();
				if(file.length()>0) {
					if(regular.indexOf("src")>=0){
						String bq="src";
						String oldUrl=""+bq+"=\""+list.get(i)+"\"";
						String newUrl=""+bq+"=\"/upload/"+file.substring(0,8)+"/"+file.substring(0,10)+"/"+file+"\""+" uploadpic=\""+file+"\""+" title=\""+name+"\"";
						Util.log("开普地址："+oldUrl,"content",0);
						Util.log("海云地址："+newUrl,"content",0);
						System.out.println("海云地址："+oldUrl);
						System.out.println("海云地址："+newUrl);
						content=content.replace(oldUrl,newUrl);
						Util.log("替换后得正文：：：：：：：：：：：：：：："+content,"content",0);
						System.out.println("替换后得正文：：：：：：：：：：：：：：："+content);
					}else{
						String bq="href";
						String oldUrl=""+bq+"=\""+list.get(i)+"\"";
						String newUrl=""+bq+"=\"/upload/"+file.substring(0,8)+"/"+file.substring(0,10)+"/"+file+"\""+" appendix='true'"+"  title=\""+name+"\"";
						Util.log("开普地址："+oldUrl,"content",0);
						Util.log("海云地址："+newUrl,"content",0);
						System.out.println("海云地址："+oldUrl);
						System.out.println("海云地址："+newUrl);
						content=content.replace(oldUrl,newUrl);
						Util.log("替换后得正文：：：：：：：：：：：：：：："+content,"content",0);
						System.out.println("替换后得正文：：：：：：：：：：：：：：："+content);
					}
				}
			
			}else{
				Util.log("无法处理路径："+list.get(i),"content",0);
				System.out.println("无法处理路径："+list.get(i));
			}
		}
		return content;
	
    }
	/**
	 * 获取正文内资源
	 * List<String> list = new ArrayList<String>();
	 */
	public static List<String>  getContentResource(String regExImg,int subStart,String content){
		List<String> list = new ArrayList<String>();
		Pattern patternImg = Pattern.compile(regExImg);
		Matcher matcherImg = patternImg.matcher(content);
		while (matcherImg.find()) {
			String picPath = matcherImg.group();
			picPath = picPath.substring(subStart, picPath.length());
//			String fullPath = getFullPath(picPath);
			Util.log("匹配到的路径："+picPath.replace("\"", ""),"content",0);
			System.out.println("匹配到的路径："+picPath.replace("\"", ""));
			list.add(picPath.replace("\"", ""));
		}
		List<String> remove=removeDuplicate(list);
		return remove;
	}
	
	/**
	 * 去除重复数据
	 */
	public static List<String> removeDuplicate(List<String> list) {   
	    HashSet<String> h = new HashSet<String>(list);   
	    list.clear();   
	    list.addAll(h);   
	    return list;   
	}
}
