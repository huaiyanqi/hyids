package com.trs.kptohyzfwz;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;

public class PutZCWJZFWZ {
	
	/**
	 * 政府网站-政策文件
	 * @param oPostData
	 * @param jsondata
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map putZCWJHYzfwz(Map oPostData,JSONObject jsondata) throws Exception{ 
		
		//处理非默认字段
		for(String str:jsondata.keySet()){
			
			if(str.indexOf("@@")>=0){
				String valueKp=jsondata.get(str).toString();
//				System.out.println("获取到的值："+valueKp);
				String[] nameStrArray=str.split("@@");
				String vName=nameStrArray[0];//视图短名
				String zName=nameStrArray[1];//字段名称
				//开始处理字段
				if("webzcwj".equals(vName.toLowerCase())){
					
					System.out.println("开普推送过来的政府网站政策文件数据政策文件键值对:"+str+ "==========:===========" +jsondata.get(str));
					
					if("wjyxx".equals(zName.toLowerCase())){
						//处理有效性
						if("是".equals(valueKp)||"有效".equals(valueKp)){
							oPostData.put("wjyxx", "0"); // 有效	
						}else if("否".equals(valueKp)||"无效".equals(valueKp)){
							oPostData.put("wjyxx", "1"); // 无效	
						}
					}else if("fj".equals(zName.toLowerCase())){
						if(valueKp!=null){
							System.out.println("元数据集附件：："+valueKp);
							//处理元数据集附件
							oPostData=FileUplodeYSJJ.fujianYSJJZCWJ(oPostData, valueKp);
						}
					}else if("idxid".equals(zName.toLowerCase())){
						if(valueKp!=null){
							oPostData.put("idxid",valueKp.replace("[\"", "").replace("\"]", "").replace("[]", "")); //索引
							oPostData.put("organcat",valueKp.split("/")[0]); // 有效
						}

					}else{
						//处理其他字段
						oPostData.put(zName, valueKp.replace("[\"", "").replace("\"]", "").replace("[]", ""));
					}
				}
			}
		}
		
		return oPostData;
	}

	
	
}
