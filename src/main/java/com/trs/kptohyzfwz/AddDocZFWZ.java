package com.trs.kptohyzfwz;

import java.sql.SQLException;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;

public class AddDocZFWZ {
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = "document";
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String addHyDoc(Map oPostData,Map result,String resID){
		
		String sServiceId = "gov_webdocument";
		String sMethodName = "saveDocumentInOpenData";
		Util.log("===================================调用海云之前的参数===================================","kplog",0);
		Util.log(oPostData,"kplog",0);
		Util.log("===================================调用海云之前的参数===================================","kplog",0);
		System.out.println("调用海云之前的参数："+oPostData);
		String hy=null;
		try {
			hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("code", "-1");
			result.put("data", "调用新增接口失败！！！！！");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------开普推送文档结束---------------");
			return jsonResult.toString();
		}
		Util.log("===================================结果===================================","kplog",0);
		Util.log(hy,"kplog",0);
		Util.log("===================================结果===================================","kplog",0);
		JSONObject jsonObjectHYSE = JSON.parseObject(hy);
		Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		
		Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
		System.out.println("海云返回结果："+jsonObjectHYSE.get("MSG"));
		if("true".equals(dataArrHYSave)){
			
			String	docid =jsonObjectHYGPSE.getString("DOCID");//标题
			Util.log("文档新建到海云存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')","log",0);
			System.out.println("开普同步到海云存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')");
			Integer jd=null;
			try {
				jd = jdbc.JDBCDriver("INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result.put("code", "-1");
				result.put("data", "中间表插入失败！！");
				JSONObject jsonResult =new JSONObject(result);
				System.out.println("------------------开普推送文档结束---------------");
				return jsonResult.toString();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result.put("code", "-1");
				result.put("data", "中间表插入失败！！");
				JSONObject jsonResult =new JSONObject(result);
				System.out.println("------------------开普推送文档结束---------------");
				return jsonResult.toString();
			}
			if(jd!=-1){
				result.put("code", "0");
				result.put("data", "已成功接收推送数据");
				Util.log("开普同步到海云中间表存入成功!","log",0);
				System.out.println("开普同步到海云中间表存入成功!");
				JSONObject jsonResult =new JSONObject(result);
				System.out.println("------------------开普推送文档结束---------------");
				return jsonResult.toString();
			}else{
				result.put("code", "-1");
				result.put("data", "开普同步到海云中间表存入失败!");
				Util.log("开普同步到海云中间表存入失败!","log",0);
				System.out.println("开普同步到海云中间表存入失败!");
				System.out.println("------------------开普推送文档结束---------------");
				JSONObject jsonResult =new JSONObject(result);
				return jsonResult.toString();
			}
			
		}
		return result.toString();
		
	}
	

}
