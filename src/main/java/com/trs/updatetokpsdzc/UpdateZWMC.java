package com.trs.updatetokpsdzc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.SelectSiteName;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

public class UpdateZWMC {
	
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	
	private static String HYCHNLID = ConstantUtil.HYCHNLID;
	private static String HYSITEID = ConstantUtil.HYSITEID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String resouse_path = ConstantUtil.resouse_path;
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	
	 /**
	 * 修改文档-首都之窗-------------政务名词
	 * @param jsondata
	 * @throws Exception 
	 */
	@SuppressWarnings("static-access")
	public static void upDOcZWNC(String channelid,String docid,String viewname,String SITEID) throws Exception{
		
		System.out.println("------------修改首都之窗-------------政务名词视图文档开始------------");
		
		//开普接口json
		JSONObject jsonOb = new JSONObject();
		JSONArray json = new JSONArray();// 默认附件
		StringBuffer fujianString=new StringBuffer();
		StringBuffer fujianPic=new StringBuffer();
		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  开普同步到海云id SITEID  父栏目id  PARENTID
		String CHANNELID=channelid;//id
		String DOCID=docid;//文档id
		//默认字段
		String zw=null;//正文
		String pContent=null;
		String otherContent=null;
		String bt=null;//文件名称  标题
		String zy=null;//文档摘要
		String kaipuid=null;
		try {
				kaipuid=cR.doCheck(DOCID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(kaipuid==null){
			System.out.println("开普中不存在该文档！");
		}else{
			//查询站点名称 打印日志需要使用
			String sitename=SelectSiteName.selectSiteName(SITEID);
			System.out.println("----------------------------处理文档开始-----------------------------");
			//调用海云接口查询文档详细数据
			String sServiceId="gov_webdocument";
			String sMethodName="findOpenDataDocumentById";
			Map<String, String> savemap = new HashMap<String, String>();
			savemap.put("DocId",DOCID);
			savemap.put("ChannelId",CHANNELID);
			savemap.put("CurrUserName", USER); // 当前操作的用户
			String hy11= HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
			JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
			Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
			
			Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
			System.out.println("id:"+DOCID+"--海云返回结果："+jsonObjectHYSE.get("MSG"));
			
			if("true".equals(dataArrHYSave)){
				
				//处理获取结果
				//默认字段处理
				bt =jsonObjectHYGPSE.getString("CTM");//文件名称  标题
				zy=jsonObjectHYGPSE.getString("JJ");//内容摘要
				SITEID=jsonObjectHYGPSE.getString("SITEID");//开普同步到海云id
				
				//循环处理其他字段
				for(String str:jsonObjectHYGPSE.keySet()){
					
					if("DOCATTACHFILEFIELD".equals(str)){ // 默认附件
						if(fujianString.length()!=0){
							fujianString.append(",");
						}
						//开普元数据集中附件处理 
					    JSONArray DOCRELFILE=jsonObjectHYGPSE.getJSONArray(str);//相关附件
					    String docPath=null;
					    String dName=null;
					    if(DOCRELFILE!=null){
					    	for(int i = 0; i < DOCRELFILE.size(); i++){
						    	String url="http://192.141.252.5/gov/file/read_file.jsp?DownName=DOCUMENT&FileName=";
						    	docPath=DOCRELFILE.getJSONObject(i).getString("APPFILE");
						    	dName=DOCRELFILE.getJSONObject(i).getString("APPDESC");
						    	//处理附件名称带特殊字符
						    	dName=zfwzUtil.filterSpecialChar(dName);
						    	String Pv=null;
						    	if(docPath.indexOf(".")>0){
						    		Pv=docPath.substring(docPath.lastIndexOf("."),docPath.length());
						    	}else{
									Util.log("文章名称："+bt,"附件没有后缀"+sitename+SITEID,0);
									Util.log("栏目id："+CHANNELID,"附件没有后缀"+sitename+SITEID,0);
						    		Util.log("=========================================="+SITEID,"附件没有后缀"+sitename+SITEID,0);
						    		Pv=".doc";
						    	}
						    	//上传之前先下载
								try {
									HttpFileUpload.downLoadFromUrl(url+docPath,dName+Pv,resouse_path);
								} catch (IOException e) {
									Util.log("文章名称："+bt,"erro"+sitename+SITEID,0);
									Util.log("栏目id："+CHANNELID,"erro"+sitename+SITEID,0);
									Util.log("错误信息：附件下载错误"+e,"erro"+sitename+SITEID,0);
									Util.log("=============================================================================","erro"+sitename+SITEID,0);
									e.printStackTrace();
								}
						    	String pKPpath=zfwzUtil.uploadFile(resouse_path+docPath,dName+Pv,bt,CHANNELID,sitename,SITEID);
						    	//返回的开普上传路径，需要拼接成开普需要的格式
						    	System.out.println("返回的上传路径"+pKPpath);
						    	if(i!=0){
						    		fujianString.append(",");
						    	}
						    	fujianString.append(Kp_conten_url+pKPpath);
						    }
					    	System.out.println("首都之窗---政务名词附件："+fujianString);
					    }
						
					}else if("DOCATTACHPICFIELD".equals(str)){ //默认图片
						if(fujianPic.length()!=0){
							fujianPic.append(",");
						}
						//开普元数据集中附件处理 
					    JSONArray DOCRELFILE=jsonObjectHYGPSE.getJSONArray(str);//相关附件
					    String docPath=null;
					    String dName=null;
					    if(DOCRELFILE!=null){
					    	for(int i = 0; i < DOCRELFILE.size(); i++){
						    	String url="http://192.141.252.5/gov/file/read_file.jsp?DownName=DOCUMENT&FileName=";
						    	docPath=DOCRELFILE.getJSONObject(i).getString("APPFILE");
						    	dName=DOCRELFILE.getJSONObject(i).getString("APPDESC");
						    	//处理附件名称带特殊字符
						    	dName=zfwzUtil.filterSpecialChar(dName);
						    	String Pv=null;
						    	if(docPath.indexOf(".")>0){
						    		Pv=docPath.substring(docPath.lastIndexOf("."),docPath.length());
						    	}else{
									Util.log("文章名称："+bt,"附件没有后缀"+sitename+SITEID,0);
									Util.log("栏目id："+CHANNELID,"附件没有后缀"+sitename+SITEID,0);
						    		Util.log("=========================================="+SITEID,"附件没有后缀"+sitename+SITEID,0);
						    		Pv=".doc";
						    	}
						    	//上传之前先下载
								try {
									HttpFileUpload.downLoadFromUrl(url+docPath,dName+Pv,resouse_path);
								} catch (IOException e) {
									Util.log("文章名称："+bt,"erro"+sitename+SITEID,0);
									Util.log("栏目id："+CHANNELID,"erro"+sitename+SITEID,0);
									Util.log("错误信息：附件下载错误"+e,"erro"+sitename+SITEID,0);
									Util.log("=============================================================================","erro"+sitename+SITEID,0);
									e.printStackTrace();
								}
						    	String pKPpath=zfwzUtil.uploadFile(resouse_path+docPath,dName+Pv,bt,CHANNELID,sitename,SITEID);
						    	//返回的开普上传路径，需要拼接成开普需要的格式
						    	System.out.println("返回的上传路径"+pKPpath);
						    	if(i!=0){
						    		fujianPic.append(",");
						    	}
						    	fujianPic.append(Kp_conten_url+pKPpath);
						    }
					    	System.out.println("首都之窗---政务名词附件："+fujianPic);
					    }
						
					}else if("FWDW_NAME".equals(str)){
						//发文单位
						String value=jsonObjectHYGPSE.get(str).toString();
						jsonOb.put(viewname+"@@"+str, value.replace("[\"", "").replace("\"]", "").replace("\"", "").replace("[", "").replace("]", ""));
							
					}else if("ZTFL_NAME".equals(str)){
						//主题分类  
						String value=jsonObjectHYGPSE.get(str).toString();
						if(value!=null){
							jsonOb.put(viewname+"@@"+str, value.replace("[\"", "").replace("\"]", "").replace("\"", "").replace("[", "").replace("]", ""));
						}
							
					}else if("ZW".equals(str)){
						zw=jsonObjectHYGPSE.get(str).toString();
						// 处理正文 内容  
						//图片 Content_regular_p    截取开始 src='  5个字符串
						pContent = re.contentResource(zw, 5, Content_regular_p,bt,CHANNELID,sitename,SITEID);
						//处理 zip  pdf  rar   截取开始  href=' 6个字符串
						otherContent = re.contentResource(pContent, 6, Content_regular,bt,CHANNELID,sitename,SITEID);
							
					}else{
						String value=jsonObjectHYGPSE.get(str).toString();
						if(value!=null){
							jsonOb.put(viewname+"@@"+str, value.replace("[\"", "").replace("\"]", "").replace("\"", "").replace("[", "").replace("]", ""));
						}
					}
					
				}
			    
				//修改文档固定参数   
				jsonOb.put("resTranMode", "0");// 0  文本内容    1：文件资源-文件传输  
				jsonOb.put("isOrig", "1");
				jsonOb.put("resId",ZWBJ_APPID+ DOCID); //  资源id
				
			    //默认字段
			    jsonOb.put("zwmc_docattachpicfield",fujianPic);//图片
			    jsonOb.put("zwmc_docattachfilefield",fujianString);//附件
			    System.out.println("正文数据是否为空："+zw);
			    if(zw==null||"".equals(zw)){
			    	jsonOb.put("content", "&nbsp&nbsp&nbsp&nbsp&nbsp");//正文
			    }else{
			    	jsonOb.put("content", otherContent);//正文
			    }
			    if(zy!=null){
			    	jsonOb.put("abstracts", zy);	
			    }
			    jsonOb.put("name", bt); 

			    //发布地址
			    String pubUrl=JDBCIIP.JDBCDriverSelectPubUrl(DOCID);
			    if(pubUrl!=null){
			    	jsonOb.put("online", pubUrl);
			    }
			    
				String fbrq=jsonObjectHYGPSE.getString("GXSJ");//发布日期
			    if(fbrq!=null){
			    	jsonOb.put("pubDate", fbrq);
			    }
			    
				
				System.out.println("首都之窗-----政务名词视图存入开普数据jsonOb==:"+jsonOb);
				System.out.println("首都之窗-----政务名词视图存入开普数据json==:"+json);
				String result=null;
				try {
				   result=re.updateRes0(json, jsonOb);
				} catch (Exception e) {
					// TODO Auto-generated catch block
						e.printStackTrace();
				}
				    //处理开普新增返回结果
				JSONObject jsonresult = JSON.parseObject(result);
				Integer  dataresult = (Integer) jsonresult.get("code"); 
				if(dataresult==0){
					System.out.println("修改资源成功！");
				}else{
					Util.log("文章名称："+bt,"erroUpdate"+sitename+SITEID,0);
					Util.log("栏目id："+CHANNELID,"erroUpdate"+sitename+SITEID,0);
					Util.log("错误信息：修改首都之窗-----政务名词视图资源失败-----"+jsonresult.get("msg"),"erroUpdate"+sitename+SITEID,0);
					Util.log("=============================================================================","erroUpdate"+sitename+SITEID,0);
					System.out.println("修改首都之窗-----政务名词视图资源失败："+jsonresult.get("msg"));
				}
				
			}else{
				System.out.println("海云查询文档详细信息接口返回信息:"+jsonObjectHYSE.get("MSG"));
			}
		
		}
		System.out.println("------------修改首都之窗-----政务名词视图文档结束------------");	
	
	}

}
