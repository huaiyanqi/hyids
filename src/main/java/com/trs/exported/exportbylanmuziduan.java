package com.trs.exported;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mysql.jdbc.Connection;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;


public class exportbylanmuziduan {

	
    public static final String  jdbc_idsdriver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_idsurl= ConstantUtil.jdbc_urlids;
    
    public static final String  jdbc_idsusername=  ConstantUtil.jdbc_usernameids;
    public static final String  jdbc_idspassword= ConstantUtil.jdbc_passwordids;
	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	//以下三个参数不做同步
//	private static String bmwd = ChannelidUtil.bmwd;
//	private static String zwmc = ChannelidUtil.zwmc;
//	private static String zwzsdbp = ChannelidUtil.zwzsdbp;
	
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	static int count=0;
	/**
	 * 历史栏目同步
	 * 历史数据同步
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		//站点id
		String siteid="80"; 
		//站点名称
		String sitename="首都之窗";
		//链接栏目id
		String channlid="51139";
		
		Util.log("站点名称"+"	"+"栏目id"+"	"+"标题"+"	"+"发布地址"+"	"+"关键词"+"	"+"内容摘要"+"	"+"有效性"+"	"+"机关代字"+"	"+"发文年份"+"	"+"发文序号"+"	"+"发文单位"+"	"+"文体分类"+"	"+"联合发文单位"+"	"+"主题分类"+"	"+"公报年份"+"	"+"公报序号"+"	"+"公报总序号"
				+"	"+"排序"+"	"+"组织机构代码"+"	"+"索引号"+"	"+"成文日期"+"	"+"实施日期"+"	"+"废止日期"+"	"+"发布日期"+"	"+"相关政策标题"+"	"+"相关解读标题",sitename, 0);

		//查询数据
		selectDoc(sitename,siteid,channlid);

	}
	
	
	public static void selectDoc(String SITENAME,String SITEID,String CHANNELID) throws SQLException, Exception{
		
		//根据栏目id查询到栏目下的已发实体数据数量
//		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027)";
		//栏目下全部数据，包括引用数据
		String sqlcount="SELECT count(*) con FROM wcmchnldoc dc WHERE  dc.DOCSTATUS in (10,8,12,1027) AND dc.CHNLID = '"+CHANNELID+"' ";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		int con=0;
		while(resultSet.next()) {
			con=resultSet.getInt("con");
//			Util.log("查询到的条数："+con,SITENAME+CHANNELID+"文章总数", 0);
			System.out.println("查询到的文章数量："+con);
		}
		
		if(con!=0){
//			resultSet.close();
			con=con/1000+1;
			for(int i=0;i<con;i++) {
				count++;
				selectdocwords(SITENAME,SITEID,CHANNELID);
				
			}	
		}
//		else{
//			Util.log("该栏目下无数据！"+con, SITENAME+CHANNELID, 0);
//		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
		
		
	}
	
	
	@SuppressWarnings("unused")
	public static void selectdocwords(String SITENAME,String SITEID,String CHANNELID) throws Exception{
		
		//根据栏目id查询到栏目下的已发实体数据id
		
		String sqlcount="SELECT * FROM wcmchnldoc WHERE CHNLID = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027) ORDER BY DOCORDER  LIMIT "+(count-1)*1000+",1000 ; ";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		
		while(resultSet.next()) {
			//必要参数    文档 id  
			String ID = resultSet.getString("DOCID"); 
			System.out.println(ID);
//			System.out.println(res.getString("CHNLID") );
//			System.out.println(res.getString("DOCPUBURL") );
			Doc(CHANNELID,SITEID,ID,SITENAME);
			
		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
	}
	
	private static void Doc(String CHANNELID,String sitid,String docid,String SITENAME) throws Exception {
		
		String sql= "SELECT * FROM wcmdocument WHERE  DOCID = '"+docid+"' ";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet res = stat.executeQuery(sql);
		
		while(res.next()) {
			//开始获取字段进行打印
			
			String doclinkObj = (String) res.getObject("DOCLINK");// 链接   
			 
			
			String [] link=doclinkObj.split("_");
			String dID = link[1];
			String id=dID.substring(0,dID.length()-5);
			//查询政策文件类数据进行打印
			seectZCDoc(CHANNELID,sitid,id,SITENAME,doclinkObj);
		}
		//关闭连接
		res.close();
		stat.close();
		conn.close();
	}
	
	private static void seectZCDoc(String CHANNELID,String sitid,String docid,String SITENAME,String doclinkObj) throws Exception {
		
		String sql= "SELECT * FROM wcmdocument WHERE  DOCID = '"+docid+"' ";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet res = stat.executeQuery(sql);
		
		while(res.next()) {
			//开始获取字段进行打印
			
			String zclanmuid = res.getString("DOCCHANNEL");// 原栏目id   
			
			//查询政策文件类数据进行打印
			selectZFWZZCWJ(docid,CHANNELID,sitid,zclanmuid,doclinkObj,SITENAME);
		}
		//关闭连接
		res.close();
		stat.close();
		conn.close();
	}
	
	@SuppressWarnings("unused")
	public static void  selectZFWZZCWJ(String docid,String chnlid,String SITEID,String zclanmuid,String doclinkObj,String SITENAME) throws Exception {
		
		//调用海云接口查询文档详细数据
		String sServiceId="gov_webdocument";
		String sMethodName="findOpenDataDocumentById";
		Map<String, String> savemap = new HashMap<String, String>();
		savemap.put("DocId",docid);
		savemap.put("ChannelId",zclanmuid);
		savemap.put("CurrUserName", USER); // 当前操作的用户
		String hy11= HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
		Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		
		Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
		System.out.println("id:"+docid+"--海云返回结果："+jsonObjectHYSE.get("MSG"));
		
		if("true".equals(dataArrHYSave)){
			String WJMC =jsonObjectHYGPSE.getString("WJMC");//文件名称
			String GJC =jsonObjectHYGPSE.getString("GJC");//关键词
			String NRZY=jsonObjectHYGPSE.getString("NRZY");//内容摘要
			String yxx =jsonObjectHYGPSE.getString("YXX");//有效性
			String youxiaoxing="";
			if("0".equals(yxx)){
				//有效
				youxiaoxing= "有效";
			}else{
				youxiaoxing= "无效";;
			}
			String jgdz =jsonObjectHYGPSE.getString("JGDZ");//机关代字
			String fwnf =jsonObjectHYGPSE.getString("FWNF");//发文年份、
			String fwxh =jsonObjectHYGPSE.getString("FWXH");//发文序号、
			String fwdw =jsonObjectHYGPSE.getString("FWDW_NAME").replace("[\"", "").replace("\"]", "");//发文单位、
			String wtfl =jsonObjectHYGPSE.getString("WTFL_NAME").replace("[\"", "").replace("\"]", "");//文体分类、
			String lhfwdw =jsonObjectHYGPSE.getString("LHFWDW_NAME");//联合发文单位、
			if(lhfwdw.equals("[]")){
				lhfwdw="";
			}else{
				lhfwdw=lhfwdw.replace("[\"", "").replace("\"]", "");
			}
			String ztfl =jsonObjectHYGPSE.getString("ZTFL_NAME").replace("[\"", "").replace("\"]", "");//主题分类
			String gbnf =jsonObjectHYGPSE.getString("GBNF");//公报年份
			if(gbnf==null){
				gbnf="";
			}
			String gbxh =jsonObjectHYGPSE.getString("GBXH");//公报序号
			if(gbxh==null){
				gbxh="";
			}
			String gbzxh =jsonObjectHYGPSE.getString("GBZXH");//公报总序号
			if(gbzxh==null){
				gbzxh="";
			}
			String px =jsonObjectHYGPSE.getString("PX");//排序
			if(px==null){
				px="";
			}
			String organcat =jsonObjectHYGPSE.getString("ORGANCAT_NAME");//组织机构代码
			if(organcat==null){
				organcat="";
			}else{
				organcat=organcat.replace("[\"", "").replace("\"]", "");
			}
			String idxid =jsonObjectHYGPSE.getString("IDXID");//索引号
			if(idxid==null){
				idxid="";
			}
			String cwrq =jsonObjectHYGPSE.getString("CWRQ");//成文日期
			String ssrq =jsonObjectHYGPSE.getString("SSRQ");//实施日期
			String fzrq =jsonObjectHYGPSE.getString("FZRQ");//废止日期
			String fbrq =jsonObjectHYGPSE.getString("FBRQ");//发布日期
			String xgzcbt =jsonObjectHYGPSE.getString("XGZCBT");//相关政策标题
			String xgjdbt =jsonObjectHYGPSE.getString("XGJDBT");//相关解读标题

			Util.log(SITENAME+"	"+chnlid+"	"+WJMC+"	"+doclinkObj+"	"+GJC+"	"+NRZY+"	"+youxiaoxing+"	"+jgdz+"	"+fwnf+"	"+fwxh+"	"+fwdw+"	"+wtfl+"	"+lhfwdw+"	"+ztfl+"	"+gbnf+"	"+gbxh+"	"+gbzxh
			+"	"+px+"	"+organcat+"	"+idxid+"	"+cwrq+"	"+ssrq+"	"+fzrq+"	"+fbrq+"	"+xgzcbt+"	"+xgjdbt,SITENAME, 0);
			
			
		}
	}
}
