package com.trs.exported;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mysql.jdbc.Connection;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.AddDocByView;
import com.trs.mqadddoc.GovPu;
import com.trs.mqadddoc.SelectSiteName;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;


public class exportCountByOperator {

	
    public static final String  jdbc_idsdriver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_idsurl= ConstantUtil.jdbc_urlids;
    
    public static final String  jdbc_idsusername=  ConstantUtil.jdbc_usernameids;
    public static final String  jdbc_idspassword= ConstantUtil.jdbc_passwordids;
	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
    public static final String  jdbc_driver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_url= ConstantUtil.jdbc_url;
    
    public static final String  jdbc_username=  ConstantUtil.jdbc_username;
    public static final String  jdbc_password= ConstantUtil.jdbc_password;
    static final String DB_URL = jdbc_url;
    // MySQL的JDBC URL编写方式：jdbc:mysql://主机名称：连接端口/数据库的名称
    static final String JDBCUSER = jdbc_username;
    static final String JDBCPASS = jdbc_password;
	
	//以下三个参数不做同步
//	private static String bmwd = ChannelidUtil.bmwd;
//	private static String zwmc = ChannelidUtil.zwmc;
//	private static String zwzsdbp = ChannelidUtil.zwzsdbp;
	
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	
	/**
	 * 历史栏目同步
	 * 历史数据同步
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		//站点id
//		String siteid="124"; 
		String siteid="135"; 
		//站点名称
		String sitename="北京市规划和自然资源委员会";
//		String sitename="拓尔思政府网站测试";
		//查询委办局用户后缀
//		String usuerBy="_kpy";   
		String usuerBy="zhangfaliang_ghzrzyw";   
		
		//同步历史数据 
		selectUserCont(usuerBy,siteid,sitename);
		
	}
	
	
	/**
	 * 查询用户数量总数
	 * @throws SQLException
	 * @throws Exception
	 */
	public static void selectUserCont(String usuerBy,String SITEID,String sitename) throws SQLException, Exception{
		int count=0;
		//根据栏目id查询到栏目下的已发实体数据数量
//		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027)";
		String sqlcount="SELECT COUNT(*) con FROM kpuser WHERE username like '%"+usuerBy+"%'";
    	Connection conn = null;
        Statement stat = null;

        System.out.println(sqlcount);
        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,JDBCUSER,JDBCPASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		int allcon=0;
		while(resultSet.next()) {
			allcon=resultSet.getInt("con");
//			Util.log("查询到的条数："+con,SITENAME+CHANNELID+"文章总数", 0);
			System.out.println("查询到的用户数量："+allcon);
		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
		
		if(allcon!=0){
//			resultSet.close();
			int con=allcon/100+1;
			for(int i=0;i<con;i++) {
//				Util.log("循环次数："+con, SITENAME+CHANNELID, 0);
				if(i == 0){
					Util.log("用户名称"+"	"+"已发数据数量"+"	"+"发布后进行操作再次发布数据数量",sitename+i, 0);
					selectuser(usuerBy,SITEID,sitename,count,i);
					count++;
				}else{
					
					Util.log("用户名称"+"	"+"已发数据数量"+"	"+"发布后进行操作再次发布数据数量",sitename+i, 0);
					selectuser(usuerBy,SITEID,sitename,count,i);
					count++;
					
				}

			}	
		}

		
	}
	

	
	
	public static void selectuser(String usuerBy,String siteid,String sitename,int count,int con) throws SQLException, Exception{
		
		int start=count*100;
		//根据后缀查询用户
		String sqlcount="SELECT username FROM kpuser WHERE username like '%"+usuerBy+"%' LIMIT "+start+",100";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,JDBCUSER,JDBCPASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		String username=null;
//		resultSet.last(); 
//		int size = resultSet.getRow(); 
//		System.out.println(size);
//		resultSet.beforeFirst();
		while(resultSet.next()) {
			username=resultSet.getString("username");
//			Util.log("查询到的条数："+con,SITENAME+CHANNELID+"文章总数", 0);
			System.out.println("查询到的用户名："+username);
			//查询到用户后 根据用户名查询文章数量
//			if(!username.equals("zhangfaliang_ghzrzyw")){
//				selectDoc(username,siteid,sitename,con);
//			}
			selectDoc(username,siteid,sitename,con);
			
//			//输出最总结果日志
//			Util.log(username+"	"+allCount+"	"+countResult,sitename, 0);
		}

		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
		
		
	}
	
	/**
	 * 根据用户名称查询到文章的具体信息
	 * @param username
	 * @param SITEID
	 * @param count  
	 * @param allCount
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static int selectdocCount(String username,String SITEID,int count,int allCount,String sitename) throws Exception{
		
		//根据栏目id查询到栏目下的已发实体数据id
		int start=count*1000;
//		String sqlcount="SELECT DOCTITLE,DOCCONTENT,DOCKEYWORDS,DOCABSTRACT,DOCPUBURL FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027) ORDER BY DOCID LIMIT "+start+",1000";
		String sqlcount="SELECT docid,doctitle,docchannel FROM wcmdocument WHERE OPERUSER = '"+username+"'  AND SITEID = '"+SITEID+"'  AND DOCSTATUS in (10,12,1027)  ORDER BY DOCID LIMIT "+start+",1000";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		int countResult=0;
		while(resultSet.next()) {
			int DOCID=resultSet.getInt("DOCID");
//			String title=resultSet.getString("DOCTITLE");
//			String DOCCONTENT=resultSet.getString("DOCCHANNEL");
//			System.out.println("查询到的文章title："+title);
			// 根据文章id 去操作日志查询数据 判断该文章是否属于 发布后进行其他操作后再次发布
			System.out.println("=================================================");
			System.out.println("文章id："+DOCID);
			int logResult=selectDocLog(DOCID);
			System.out.println("文章是否是二次发布结果 0为多次发布：："+logResult);
			if(logResult == 0){
				countResult++;
			}
		}
		System.out.println("当前文章条数返回发布二次数据：：："+countResult);
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();

		return countResult;
	}
	
	
	/**
	 * 查询数量总数
	 * @throws SQLException
	 * @throws Exception
	 */
	public static void selectDoc(String username,String SITEID,String sitename,int djccon) throws SQLException, Exception{
		int count=0;
		//根据栏目id查询到栏目下的已发实体数据数量
//		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027)";
		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE OPERUSER = '"+username+"'  AND SITEID = '"+SITEID+"'  AND DOCSTATUS in (10,8,12,1027)";
    	Connection conn = null;
        Statement stat = null;

        System.out.println(sqlcount);
        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		int allcon=0;
		while(resultSet.next()) {
			allcon=resultSet.getInt("con");
//			Util.log("查询到的条数："+con,SITENAME+CHANNELID+"文章总数", 0);
			System.out.println("查询到的文章数量："+allcon);
		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
		
		int resultCount=0;
		if(allcon!=0){
//			resultSet.close();
			int con=allcon/1000+1;
			for(int i=0;i<con;i++) {
//				Util.log("循环次数："+con, SITENAME+CHANNELID, 0);
				resultCount=resultCount+selectdocCount(username,SITEID,count,allcon,sitename);
				System.out.println("返回结果二次发布数量：："+resultCount);
				count++;
			}	
		}
		//开始添加 状态委8得数据数量
		int count8=selectDocStatus(username,SITEID);
		int endcount=count8+resultCount;
		System.out.println("多次发布数据数量最终结果：：："+endcount);
		System.out.println("当前用处查询完毕，开始输出结果");
//		//输出最总结果日志
		Util.log(username+"	"+allcon+"	"+endcount,sitename+djccon, 0);

		
	}
	
	
	/**
	 * 查询数量总数状态为8
	 * @throws SQLException
	 * @throws Exception
	 */
	public static int selectDocStatus(String username,String SITEID) throws SQLException, Exception{
		
		//根据栏目id查询到栏目下的已发实体数据数量
//		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027)";
		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE OPERUSER = '"+username+"'  AND SITEID = '"+SITEID+"'  AND DOCSTATUS = 8 ";
    	Connection conn = null;
        Statement stat = null;

        System.out.println("查询状态为8得数据数量：：："+sqlcount);
        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		int allcon=0;
		while(resultSet.next()) {
			allcon=resultSet.getInt("con");
//			Util.log("查询到的条数："+con,SITENAME+CHANNELID+"文章总数", 0);
			System.out.println("查询到状态为8得文章数量："+allcon);
		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
		
		return allcon;
	}
	
	/**
	 * 根据文档id 查询文章的操作日志
	 * @throws SQLException
	 * @throws Exception
	 */
	public static int selectDocLog(int DOCID) throws SQLException, Exception{
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		// 0  表示二次发布  1 标识不是
		int count=0;
		//根据id 查询文章操作日志
		String sqlcount="SELECT OPERDESC FROM xwcmmetadatalog WHERE METADATAID = '"+DOCID+"'";
    	Connection conn = null;
        Statement stat = null;
        System.out.println("查询sql："+sqlcount);
        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);
        System.out.println("===============开始查询==============");
        System.out.println(df.format(new Date()));
        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
        System.out.println(df.format(new Date()));
        System.out.println("================结束查询==============");
        
		String OPERDESC="";
		int contFB=0;
		while(resultSet.next()) {
			OPERDESC=resultSet.getString("OPERDESC");
//			Util.log("查询到的条数："+con,SITENAME+CHANNELID+"文章总数", 0);
			System.out.println("查询到的操作日志："+OPERDESC);
			if(OPERDESC.contains("发布")){
				contFB++;
			}
		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
		
		System.out.println("-------------查询到的操作日志结束------------");
		if(contFB>1){//代表着该数据为多次发布数据
			return count;
		}else{
			return count+1;
		}
		

		
		
	}

}
