package com.trs.exported;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mysql.jdbc.Connection;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.AddDocByView;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;


public class exportByHttps {

	
    public static final String  jdbc_idsdriver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_idsurl= ConstantUtil.jdbc_urlids;
    
    public static final String  jdbc_idsusername=  ConstantUtil.jdbc_usernameids;
    public static final String  jdbc_idspassword= ConstantUtil.jdbc_passwordids;
	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	//以下三个参数不做同步
//	private static String bmwd = ChannelidUtil.bmwd;
//	private static String zwmc = ChannelidUtil.zwmc;
//	private static String zwzsdbp = ChannelidUtil.zwzsdbp;
	
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	
	/**
	 * 查询正文 带外连的数据数量  标题 发布地址  正文链接地址
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		//站点id
//		String siteid="124"; 
		String siteid="112"; 
		//站点名称
//		String sitename="拓尔思政府网站测试";
		String sitename="北京市发展和改革委员会";
		
		//栏目父id
		String pid="0";   
		//操作用户
		String userName="dev";
		
		Util.log("标题"+"	"+"栏目id"+"	"+"正文外链地址"+"	"+"发布地址",sitename, 0);
		//处理开始
		selectDocBychannel(siteid,pid,userName,sitename);
		
		

	}
	
	
	
	
	/**
	 * 获取一级栏目
	 * @throws Exception
	 */
	public static void selectDocBychannel(String sitId,String pid,String userName ,String sitename) throws Exception{
		
		// 根据站点id查询下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", userName); // 当前操作的用户
		//设置一次查询数量
		mSE.put("pageSize", "15000");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", sitId);
		mSE.put("ParentChannelId", pid);
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		System.out.println("结果"+hy1);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2.size()>0){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					
					String SITEID=dataBean.getString("SITEID");
					String CHNLDESC=dataBean.getString("CHNLDESC");//栏目名称
					String CHANNELID=dataBean.getString("CHANNELID");//栏目id
					//查询新增栏目的视图id
					String viewid=zfwzUtil.selectViewid(CHANNELID);
					
					String HASCHILDREN=dataBean.getString("HASCHILDREN");//是否存在下级栏目
					
					System.out.println(SITEID+"-------------------"+CHNLDESC+"----------------"+CHANNELID);
					if("true".equals(HASCHILDREN)){
						//代表存在下级栏目   同步到开普该栏目  并且查询下级栏目
						selectDoc(sitename,SITEID,CHANNELID,CHNLDESC,viewid);
						//继续查询下级栏目
						selectDocBychannel(SITEID,CHANNELID,userName,sitename);
					}else{
						//无下级栏目 ，仅仅同步该栏目下得数据到开普
						selectDoc(sitename,SITEID,CHANNELID,CHNLDESC,viewid);
					}
					
				}
			}else{
				System.out.println("没有查询到站点下得栏目");
			}
		}
	}

	
	
	public static void selectDoc(String SITENAME,String SITEID,String CHANNELID,String CHNLNAME,String viewid) throws SQLException, Exception{
		int count=0;
		//根据栏目id查询到栏目下的已发实体数据数量
		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCTYPE = '20' AND DOCSTATUS in (10,8,12,1027)";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		int con=0;
		while(resultSet.next()) {
			con=resultSet.getInt("con");
			System.out.println("查询到的文章数量："+con);
		}
		
		if(con!=0){
//			resultSet.close();
			con=con/1000+1;
			for(int i=0;i<con;i++) {
				addByView(SITENAME,SITEID,CHANNELID,CHNLNAME,viewid,count);
				count++;
			}	
		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
		
		
	}
	
	
	public static void addByView(String SITENAME,String SITEID,String CHANNELID,String CHNLNAME,String viewid,int count) throws Exception{
		
		//根据栏目id查询到栏目下的已发实体数据id
		int start=count*1000;
		String sqlcount="SELECT * FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCTYPE = '20'  AND DOCSTATUS in (10,8,12,1027) ORDER BY DOCID LIMIT "+start+",1000";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		
		while(resultSet.next()) {
			int DOCID=resultSet.getInt("DOCID");
			int DOCKIND=resultSet.getInt("DOCKIND");//视图id
			int DOCCHANNEL=resultSet.getInt("DOCCHANNEL");//栏目id
			String DOCTITLE =resultSet.getString("DOCTITLE");//标题
			String fbdi =resultSet.getString("DOCPUBURL");//发布地址
			System.out.println("查询到的文章id："+DOCID);
			
			if(DOCKIND == 5){
				String htmlContent=resultSet.getString("DOCHTMLCON");
				
				contentResource(htmlContent,6, "(href=\").*?(\")",String.valueOf(DOCCHANNEL),DOCTITLE,SITENAME,fbdi);
			}else{
//				//根据视图id查询视图短名
//				String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+DOCKIND+"'";
//				String viewName=jdbcids.JDBCVIEWIDSELECT(sql);
//				System.out.println("视图短名："+viewName.toUpperCase());
//				//其他视图  拼接表名 查询数据
//				String tablename="wcmmetatable"+viewName;
				selectZDYST(String.valueOf(DOCID),String.valueOf(DOCCHANNEL),SITENAME,DOCTITLE,SITENAME,fbdi);
			}

			
		}
		//关闭连接
		resultSet.close();
		stat.close();
		conn.close();
	}
	
	/**
	 * 处理自定义视图
	 * @return
	 * @throws Exception
	 */
	public static void selectZDYST(String docid,String chnlid,String viewname,String DOCTITLE,String SITENAME,String fbdi) throws Exception {
		
		//调用海云接口查询文档详细数据
		String sServiceId="gov_webdocument";
		String sMethodName="findOpenDataDocumentById";
		Map<String, String> savemap = new HashMap<String, String>();
		savemap.put("DocId",docid);
		savemap.put("ChannelId",chnlid);
		savemap.put("CurrUserName", USER); // 当前操作的用户
		String hy11= HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
		Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		
		Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
		System.out.println("id:"+docid+"--海云返回结果："+jsonObjectHYSE.get("MSG"));
		
		if("true".equals(dataArrHYSave)){
			String zw=jsonObjectHYGPSE.getString("ZW");//其他视图正文
			if(zw!=null){//正文不为空 则处理 否则是正文没有内容或者 视图没有正文字段
				contentResource(zw,6, "(href=\").*?(\")",chnlid,DOCTITLE,SITENAME,fbdi);
			}
		}
		
	}
	

	private static boolean exist(String url) {
	        try {
	            URL u = new URL(url);
	            HttpURLConnection huc = (HttpURLConnection) u.openConnection();
	            huc.setRequestMethod ("HEAD");
	            huc.setConnectTimeout(5000); //视情况设置超时时间
	            huc.connect();
	            return huc.getResponseCode() == HttpURLConnection.HTTP_OK;
	        } catch (Exception e) {
	           return false;
	        }
	
	}
	
	//处理正文
	public static void contentResource(String content,int sub,String regular,String channelid,String title,String SITENAME,String fbdi) throws IOException {
		int six=0;
		String regExImg = regular;//正文中图片 
		List<String> list=getContentResource(regExImg, sub, content);
		List<String> listLogs=new ArrayList();
		for(int i = 0 ; i < list.size() ; i++) {
			System.out.println("正文外链路径路径：：：：：："+list.get(i));
			if(list.get(i).contains("http")||list.get(i).contains("https")){
				six++;
				//排查死链
//				boolean re=exist(list.get(i));
//				if(!re){
//					Util.log(title+"	"+channelid+"		"+list.get(i)+"		"+fbdi,"死链地址", 0);
//				}
				listLogs.add(list.get(i));
			}
		}
		if(six!=0){
			//外链地址
			Util.log(title+"	"+channelid+"	"+listLogs+"	"+fbdi,SITENAME, 0);
		}
		
	}
	
	/**
	 * 获取正文内资源
	 * List<String> list = new ArrayList<String>();
	 */
	public static List<String>  getContentResource(String regExImg,int subStart,String content){
		List<String> list = new ArrayList<String>();
		Pattern patternImg = Pattern.compile(regExImg);
		Matcher matcherImg = patternImg.matcher(content);
		while (matcherImg.find()) {
			String picPath = matcherImg.group();
			System.out.println("匹配到的路径："+picPath);
			String picPath1=picPath.substring(6,picPath.length()-1);
			list.add(picPath1);

		}
		List<String> remove=removeDuplicate(list);
		return remove;
	}

	/**
	 * 去除重复数据
	 */
	public static List<String> removeDuplicate(List<String> list) {   
	    HashSet<String> h = new HashSet<String>(list);   
	    list.clear();   
	    list.addAll(h);   
	    return list;   
	}
}
