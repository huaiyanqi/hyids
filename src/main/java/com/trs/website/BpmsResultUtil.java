package com.trs.website;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.bpmsutil.HttpController;
import com.trs.bpmsutil.JDBCSelectDoc;
import com.trs.oauth.ConstantUtil;
import com.trs.oauth.log.HttpUtils;

public class BpmsResultUtil {
	
	private static String GET_TOKEN_URL = ConstantUtil.GET_TOKEN_URL;
	private static String HR_URL = ConstantUtil.HR_URL;
	private static String urlmak =HR_URL+"wcm/message/sendmessage";//请求wcm消息中心接口地址
	
	
	/**
	 * 变更bpms记录表稿件状态
	 * @param code
	 * @throws Exception 
	 * @throws SQLException 
	 */
	public static void updateDocStatus(String opUn,String docid,String code) throws SQLException, Exception{

		//获取当前时间
    	Date date = new Date();
    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	dtf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
    	String last_time = dtf.format(date);
    
        DateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf1.setTimeZone(TimeZone.getTimeZone("GMT"));
        String GMTdate = sdf1.format(new Date());
        
        Date date3 = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        String BeijingTime = dateFormat.format(date3);
        System.out.println("\n=========\n" + BeijingTime);
        
        System.out.println("GMT+8----推送时的时间：：" + BeijingTime);
        System.out.println("GMT----推送时的时间：：" + GMTdate);
    	System.out.println("Asia/Shanghai----推送时的时间：："+last_time);
    	
    	//结束记录表中流程
    	if("0".equals(code)||"1".equals(code)){// 成功，终止
        	//原定 2 是审核结束流程
    		String sql=" UPDATE xwcmmetadatabpmslog	SET bpmsopinion = '"+opUn+"',bpmspushtime = '"+last_time+"',pushsig = '2' WHERE  recid = '"+docid+"' ";
    		int status=JDBCSelectDoc.insertBpmsLog(sql);

    		if(status>0){
    			System.out.println("xwcmmetadatabpmslog>"+docid+"---结束流程成功！！");
    		}else {
    			System.out.println("xwcmmetadatabpmslog>"+docid+"---结束流程失败！！");
    		}
    	}


	}
	
	
	/**
	 * 变更稿件状态
	 * @param code
	 * @throws Exception 
	 * @throws SQLException 
	 */
	public static void updateChannlDocStatus(String docid,String code) throws SQLException, Exception{


		//更新bpms审核状态
    	String bpmsstatus="2";
    	if("0".equals(code)){
    		bpmsstatus="2";//成功
    	}else if("1".equals(code)){
    		bpmsstatus="3";//终止
    	}else if("2".equals(code)){
    		bpmsstatus="4";//回退
    	}
		String sqlrhsq=" UPDATE wcmchnldoc	SET UnderReview = '"+bpmsstatus+"'  WHERE  recid = '"+docid+"' ";
		int rhsq=JDBCSelectDoc.insertBpmsLog(sqlrhsq);
		if(rhsq>0){
			System.out.println("wcmchnldoc>"+docid+"---wcmchnldoc状态变更成功！！");
		}else {
			System.out.println("wcmchnldoc>"+docid+"---wcmchnldoc状态变更失败！！");
		}

	}
	
	
	/**
	 * 插入流程记录
	 * @throws Exception 
	 * @throws SQLException 
	 */
	public static int  insterBpmsLogs(String docid,String opUn) throws SQLException, Exception{
		//操作轨迹插入一条新的操作轨迹
		//根据文章id 查询最近一条操作轨迹
		String selectsql=" SELECT ls.* from xwcmmetadatalog ls ,wcmchnldoc chnl where chnl.RECID='"+docid+"' and ls.MetaDataId =chnl.docid   order by OPERTIME DESC limit 1 ";
		//获取插入日志sql
		String inssql=JDBCSelectDoc.getMetaDataLogSql(selectsql,opUn);
		//执行插入日志sql
		int resu=JDBCSelectDoc.updataOpinion(inssql);
		
		return resu;
	}
	
	/**
	 * 根据recid查询数据 判断是否属于微信，只处理微信
	 * @param code
	 * @param recid
	 * @throws Exception 
	 * @throws SQLException 
	 */
	public static void  bpmsResul(String code,String recid,String qd) throws SQLException, Exception{
		
		//查询操作人id 并且判断是不是微信稿件
		HashMap<String,String> userid=null;
		try {
			userid=JDBCSelectDoc.stWeChatChannel(recid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("查询稿件操作人id失败！！！稿件id为["+recid+"]");
		}
		
		
		//成功触发自动签发
		if("0".equals(code)){
			System.out.println("---------------------------------------------稿件自动签发开始---------------------------------------------");
			//自动签发
			userid=qianfa(userid,code,recid, qd);	
			
			System.out.println("---------------------------------------------稿件自动签发结束---------------------------------------------");
		}

		//新增通知公告
		String notid=creatTZGGWX(userid,code);
		if(!"007".equals(notid)){
			//发布新增的通知公告
			publishTZGG(userid,notid);
						
		}		
	}

	/**
	 * 创建通知公告
	 * @param userid
	 * @param code
	 * @param recid
	 * @return
	 */
	public static String  creatTZGG(HashMap<String,String> userid,String code,String recid ) {
		
    	//创建通知公告
		Map<String, String> map = new HashMap<String, String>();
		map.put("typeid", "3");
		map.put("title",userid.get("title"));
		map.put("dispatchunit","OA审核结果");
		map.put("titlecolor","");
		map.put("content","<p>微信<"+userid.get("title")+">稿件审核失败！！</p>");
		map.put("appendixinfo","[]");
		map.put("alluser","false");
		map.put("tenantuser", "false");
		map.put("serviceid","notice");
		map.put("method","saveNotice");
		map.put("userids",userid.get("userid"));
		String postmak = HttpController.doPost(urlmak, map);
    	System.out.println("创建OA审核失败的通知公告"+postmak);
    	  
    	//判断是否创建成功 	
		JSONObject jsonObjectHYSE = JSON.parseObject(postmak);
		Object dataArr = jsonObjectHYSE.get("data");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		String noticeid=jsonObjectHYGPSE.getString("noticeid");
		String issuccess=jsonObjectHYSE.getString("issuccess");

		System.out.println("新建通知公告id["+noticeid+"]");
		
		if("true".equals(issuccess)){//创建通知公告成功
			System.out.println("新建通知公告成功！！！id["+noticeid+"]");
			return noticeid;
		}else{
			System.out.println("通知公告创建失败！！！！");	
			return "007";
		}
		
	}
	
	/**
	 * 创建通知公告  微信
	 * @param code
	 */
	public static String  creatTZGGWX(HashMap<String,String> userid,String code) {
		
		//判断创建成功还是失败的通知公告
		if("0".equals(code)||"gr".equals(code)){

			//新增通知公告  统一新增到zhaolichao账号
	    	//创建通知公告
			Map<String, String> map = new HashMap<String, String>();
			map.put("typeid", "3");
			map.put("title","稿件<"+userid.get("title")+">审核结果");
			map.put("dispatchunit","OA审核结果");
			map.put("titlecolor","");
			map.put("content","<p>您的稿件<"+userid.get("title")+">稿件审核已通过！！</p>");
			map.put("appendixinfo","[]");
			map.put("alluser","false");
			map.put("tenantuser", "false");
			map.put("serviceid","notice");
			map.put("method","saveNotice");
			map.put("userids",userid.get("userid"));
			String postmak = HttpController.doPost(urlmak, map);
	    	System.out.println("创建OA审核通过的通知公告"+postmak);
	    	
	    	
	    	//判断是否创建成功 	
			JSONObject jsonObjectHYSE = JSON.parseObject(postmak);
			Object dataArr = jsonObjectHYSE.get("data");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
			String noticeid=jsonObjectHYGPSE.getString("noticeid");
			String issuccess=jsonObjectHYSE.getString("issuccess");

			System.out.println("新建通知公告id["+noticeid+"]");
			if("true".equals(issuccess)){//创建通知公告成功
				System.out.println("新建通知公告成功！！！id["+noticeid+"]");
				return noticeid;
			}else{
				System.out.println("通知公告创建失败！！！！");	
				return "007";
			}
			
	    	
		}else{
			
			String opin="未通过";
			if("1".equals(code)){//终止
				opin="已终止";
			}else{//回退
				opin="已回退";
			}
			//只发通知公告 不做自动签发
	    	//创建通知公告
			Map<String, String> map = new HashMap<String, String>();
			map.put("typeid", "3");
			map.put("title","稿件<"+userid.get("title")+">审核结果");
			map.put("dispatchunit","OA审核结果");
			map.put("titlecolor","");
			map.put("content","<p>您的稿件<"+userid.get("title")+">稿件审核"+opin+"！！</p>");
			map.put("appendixinfo","[]");
			map.put("alluser","false");
			map.put("tenantuser", "false");
			map.put("serviceid","notice");
			map.put("method","saveNotice");
			map.put("userids",userid.get("userid"));
			String postmak = HttpController.doPost(urlmak, map);
	    	System.out.println("创建OA审核终止或回退的通知公告"+postmak);
	    	  
	    	//判断是否创建成功 	
			JSONObject jsonObjectHYSE = JSON.parseObject(postmak);
			Object dataArr = jsonObjectHYSE.get("data");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
			String noticeid=jsonObjectHYGPSE.getString("noticeid");
			String issuccess=jsonObjectHYSE.getString("issuccess");

			System.out.println("新建通知公告id["+noticeid+"]");
			
			if("true".equals(issuccess)){//创建通知公告成功
				System.out.println("新建通知公告成功！！！id["+noticeid+"]");
				return noticeid;
			}else{
				System.out.println("通知公告创建失败！！！！");	
				return "007";
			}
	    	
		}	
		
	}
	
	
	/**
	 * 微信自动签发 ，修改状态即可
	 * 网站自动签发需要调用海融接口，数据同步到海云中
	 * @param code
	 * @param recid
	 */
	public static HashMap<String,String>  qianfa(HashMap<String,String> userid,String code,String recid,String qd) {
		
		
		if("wx".equals(qd)){
			
        	Date date = new Date();
        	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	String last_time = dtf.format(date);
        	System.out.println("微信稿件自动签发，修改最后版本时间：："+last_time);
        	
			//更新wcmchnldoc状态
			String sqlrhsq=" UPDATE wcmchnldoc	SET DOCSTATUS = '10',OPERTIME = '"+last_time+"'  WHERE  recid = '"+recid+"' ";
			int rhsq=0;
			try {
				rhsq = JDBCSelectDoc.insertBpmsLog(sqlrhsq);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(rhsq>0){
				System.out.println("wcmchnldoc>"+recid+"---微信修改稿件为已签成功！！");
				userid.put("qianfa", "成功!!!!");
			}else {
				System.out.println("wcmchnldoc>"+recid+"---微信修改稿件为已签失败！！");
				userid.put("qianfa", "失败！！！");
			}
			
		}else if("wz".equals(qd)){
			
			String ChannelId=userid.get("chnlid");
			String MetaDataId=userid.get("docid");
			
	    	//自动签发网站数据
			String urlget =HR_URL+"wcm/websiteqianfa.do?";
			String canshu="ChannelId="+ChannelId+"&ChnlDocIds="+recid+"&CurrChnlId="+ChannelId+"&"
					+ "MetaDataId="+MetaDataId+"&MetaDataIds="+MetaDataId+"&methodname=webDaiShenPublish&serviceid=mlf_websiteoper";
			System.out.println("网站待审自动签发接口地址：："+urlget+canshu);
			String get=HttpUtils.sendGet(urlget+canshu);
			
			System.out.println("自动签发网站数据返回结果：："+get);
	    	//判断是否自动签发成功 	
			JSONObject json = JSON.parseObject(get);
			String ISSUCCESS=json.getString("ISSUCCESS");
			
			if("true".equals(ISSUCCESS)){
				userid.put("qianfa", "成功！！！");		
				System.out.println("网站设置自动签发成功！！！");
			}else{
				JSONArray jsonarray=json.getJSONArray("REPORTS");
				for(int i = 0; i < jsonarray.size(); i++){
				   String  DETAIL=jsonarray.getJSONObject(i).getString("DETAIL");
				   System.out.println("网站设置自动签发失败！！！失败原因：：："+DETAIL);
				   userid.put("qianfa",DETAIL);								   
				}
			}
		}

		return userid;	
	}
	
	/**
	 * 发布通知公告
	 */
	@SuppressWarnings("unchecked")
	public static void  publishTZGG(HashMap<String,String> userid,String noticeids) {
		
		//获取发布通知公告需要的token
		String urlget =HR_URL+GET_TOKEN_URL;
		String get=HttpUtils.sendGet(urlget);
		
		JSONObject jsonObjectHYSE = JSON.parseObject(get);
		Object dataArr = jsonObjectHYSE.get("properties");//根据json对象中数组的名字解析出其所对应的值
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		String token=jsonObjectHYGPSE.getString("TOKEN");//办理事项  标题
		System.out.println("通知公告获取到的 token：："+token);
		if(!"".equals(token)||token!=null){
			//调用发布接口
			Map mappub = new HashMap<String, String>();
			mappub.put("noticeids",noticeids);
			mappub.put("serviceid","notice");
			mappub.put("method","publishNotice");
			mappub.put("token",token);

			String post =HttpController.doPost(urlmak, mappub);
	    	System.out.println(post);
	    	
	    	//判断是否发布成功 	
	    	if(post.contains("公告已发布，请耐心等待！")){
	    		System.out.println("公告已发布给["+userid.get("truename")+"]，请耐心等待！");
	    	}else{
	    		System.out.println("公告发布给["+userid.get("truename")+"]失败！！！");
	    	}
	    	
		}else{
			System.out.println("发布通知公告获取到的 token失败！！");
			System.out.println("发布通知公告获取到的urlget::"+urlget);
		}

		
	}
}
