package com.trs.website;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.bpmsutil.GetPostMan;
import com.trs.bpmsutil.HttpController;
import com.trs.bpmsutil.JDBCSelectDoc;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.oauth.ConstantUtil;


@Controller
@RequestMapping(value = "/tobpms")
public class BpmsController{
	
	private static String bpms_url= ConstantUtil.bpms_url;
	public static final String  bpms_userName= ConstantUtil.bpms_userName;
	private static String bpms_password = ConstantUtil.bpms_password;
	private static String bpms_templateCode_gr = ConstantUtil.bpms_templateCode_gr;
	private static String bpms_templateCode_wz = ConstantUtil.bpms_templateCode_wz;
	private static String bpms_templateCode_wx = ConstantUtil.bpms_templateCode_wx;
	private static String bpms_appName = ConstantUtil.bpms_appName;
	private static String bpms_from_url = ConstantUtil.bpms_from_url;


	
	/**
	 * 测试获取token
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@RequestMapping(value = {"/testbpmstoken"})
	public void getTokenTest(HttpServletRequest request,HttpServletResponse response) throws SQLException, Exception{
//		String bpms_loginName = request.getParameter("loginname");//bpms_loginName
		String bpms_loginName="seeyonas";
 		HttpController HttpController = new HttpController();
		String result=HttpController.testGetToken(bpms_userName,bpms_password,bpms_loginName,bpms_url);
    	response.setContentType("text/html;charset=UTF-8");
        response.getWriter().print("以下为测试结果：");
        response.getWriter().print(result);
	}
	
	
	/**
	 * 审核返回结果
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/auditingBack"},method = RequestMethod.POST,produces = "application/json;charset=utf-8")
	@ResponseBody
	public JSONObject  auditingBack(HttpServletRequest request,HttpServletResponse response) throws SQLException, Exception{
		
		System.out.println("==============================审核返回数据接收开始====================================");
		
		String result = GetPostMan.readRaw(request.getInputStream());  //result就是raw中的数据
		JSONObject jsonData = JSON.parseObject(result);
		System.out.println("接收的参数：："+jsonData);
		String code = jsonData.get("code").toString();//审核成功失败code 0成功  1失败
		String opinion = jsonData.get("opinion").toString();//审核意见
		String docid = jsonData.get("docid").toString();//稿件的id
		String qd = jsonData.get("qd").toString();//稿件的id
		String opUn=DESUtil.decrypt(opinion, "bpms");
		System.out.println("解码前的意见："+opinion);
		System.out.println("解码后的意见："+opUn);
		
		Map hrMap =new HashMap<String, String>();
		if(docid.isEmpty()||opinion.isEmpty()||qd.isEmpty()||code.isEmpty()){
			hrMap.put("code", "-1");	
			hrMap.put("message",DESUtil.encrypt("参数不全，请核对参数！！！！","bpms"));

			JSONObject hrJson =new JSONObject(hrMap);
			System.out.println("返回bpms结果："+hrMap);
			System.out.println("==============================审核返回数据接收结束====================================");
			return hrJson;
		}
		
		//修改 bpmslog 记录表中数据状态
		BpmsResultUtil.updateDocStatus(opUn,docid,code);
		
		//微信，网站自动签发，发送通知消息
		if("wx".equals(qd)||"wz".equals(qd)){
			//处理自动签发，并且触发通知消息通知编辑bpms结束审核
			BpmsResultUtil.bpmsResul(code,docid,qd);
			
			//变更wcmchnldoc中bpms审核状态、
			BpmsResultUtil.updateChannlDocStatus(docid,code);
			
		}else{
			//触发通知消息通知编辑bpms结束审核
			BpmsResultUtil.bpmsResul("gr",docid,qd);

		}

		//插入流程记录
		int insterBpmsLogs=BpmsResultUtil.insterBpmsLogs(docid,opUn);
		
		
		//返回海融数据
		if(insterBpmsLogs>0){
			hrMap.put("code", "0");	
			hrMap.put("message",DESUtil.encrypt("接收成功","bpms"));	
		}else{
			hrMap.put("code", "-1");	
			hrMap.put("message",DESUtil.encrypt("接受数据失败！！插入意见失败！","bpms"));	
		}

		JSONObject hrJson =new JSONObject(hrMap);
		System.out.println("返回bpms结果："+hrMap);
		System.out.println("==============================审核返回数据接收结束====================================");
		return hrJson;
		
	

	}
	
	/**
	 * 正式获取token
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = {"/grgjtobpms"},produces = "application/json; charset=utf-8")
	public  String grgjTobpms(HttpServletRequest request,HttpServletResponse response) throws SQLException, Exception{
		
		System.out.println("--------------------------------------子公司同步bpms开始--------------------------------------------------");
		Util.log("--------------------------------------子公司同步bpms开始--------------------------------------------------","bpms",0);
		String docid = request.getParameter("docid");
		String qd = request.getParameter("qd");
		String loginname = request.getParameter("loginname");//bpms_loginName
		
//		String docid ="124"; 
//		String qd ="gr";
//		String loginname ="zhaolichao";
		
		String lgname=StringUtils.substringBefore(loginname,"@"); 
		lgname=StringUtils.substringBefore(lgname,"%40"); 
		lgname=StringUtils.substringBefore(lgname,"_");
		System.out.println("当前登录用户，用于获取token：："+loginname);
		
		//查询bpmslog  
		int sig=JDBCSelectDoc.selectBpmslog(docid,"gr");// 1推送过未结束流程  2推送了已经结束流程  其他未推送
		if(sig == 1){
			Map hrMap =new HashMap<String, String>();
			hrMap.put("code", "0");	
			hrMap.put("message","已经送审过!");
			JSONObject hrJson =new JSONObject(hrMap);
			System.out.println("稿件已经推送到bpms，未结束流程！！！！");
    		Util.log("--------------------------------------子公司同步bpms结束--------------------------------------------------","bpms",0);
    		System.out.println("--------------------------------------子公司同步bpms结束--------------------------------------------------");
			return hrJson.toJSONString();
		}
		
		//获取token
		String result=HttpController.testGetToken(bpms_userName,bpms_password,lgname,bpms_url);
		//解析返回结果
		JSONObject jsonObjectHYSE = JSON.parseObject(result);
		String token = jsonObjectHYSE.get("id").toString();//获取到token
        if("".equals(token)||token==null){
        	
    		Map hrMap =new HashMap<String, String>();
    		hrMap.put("code", "-1");	
			hrMap.put("message", "当用用户无法送审！！稿件id为["+docid+"]");
    		JSONObject hrJson =new JSONObject(hrMap);
    		Util.log("--------------------------------------子公司同步bpms结束--------------------------------------------------","bpms",0);
    		System.out.println("--------------------------------------子公司同步bpms结束--------------------------------------------------");
    		return hrJson.toString();
        }
		
		//查询到需要传给bpms接口的书文档数据
		HashMap<String,String> bpmsmap=JDBCSelectDoc.selectGrDoc(docid);//基础数据
		bpmsmap.put("稿件id", docid);
		bpmsmap.put("渠道标识", qd);
		//查询附件 并进行上传
		HashMap<String,String> bpmsfile=JDBCSelectDoc.selectFile(bpmsmap,docid,token,qd);//基础数据

		
		//定义返回map  第一层data
		Map resma =new HashMap<String, String>();
		resma.put("data", bpmsfile);
		resma.put("subject", bpmsmap.get("文章标题"));
		resma.put("draft", "0");
		
		resma.put("templateCode", bpms_templateCode_gr);
		
		//定义返回map  最二层data
		Map resmap =new HashMap<String, String>();
		resmap.put("data", resma);
		resmap.put("appName", bpms_appName);

		//转换成json数据
		JSONObject json =new JSONObject(resmap);
		
		//调用上传表单接口
		//设置表单头
	    Map<String, String> maphead = new HashMap<String, String>();
	    maphead.put("token", token);
	    Util.log("bpms调用json："+json.toJSONString(),"bpms",0);
	    System.out.println("bpms调用json："+json.toJSONString());
	    //调用接口
//	    String post = doPost1("fuserest","609011ab-8662-4678-9280-a8eff259a288","seeyonas","http://123.121.155.161:801/seeyon/rest/token");
	    String postfrom = sendPost(bpms_from_url,json.toJSONString(),maphead);
	    Util.log("bpms发起表单接口返回结果："+postfrom,"bpms",0);
	    System.out.println("bpms发起表单接口返回结果：："+postfrom);
	    //解析结果
		JSONObject jsonform = JSON.parseObject(postfrom);
		String formcode = jsonform.get("code").toString();//获取到token
		
		// 2推送了已经结束流程   0 没有查到数据  未推送
		if(sig == 2){
	    	Date date = new Date();
	    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	String last_time = dtf.format(date);
	    	System.out.println("推送时的时间：："+last_time);
			String bpmslogsql=" UPDATE xwcmmetadatabpmslog	SET  pushsig = '1' WHERE  recid = '"+docid+"' and channel = '"+qd+"'  ";
			int resu=JDBCSelectDoc.insertBpmsLog(bpmslogsql);
			if(resu>0){
				Util.log("添加记录表成功："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表成功："+bpmslogsql);
			}else{
				Util.log("添加记录失败："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表失败："+bpmslogsql);
			}
			
		}else{
			
	    	Date date = new Date();
	    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	String last_time = dtf.format(date);
	    	System.out.println("推送时的时间：："+last_time);
			String bpmslogsql="INSERT INTO xwcmmetadatabpmslog (`recid`, `channel`, `pushtime`, `pushtitle`, `pushsig`, `returnjson`, `bpmsopinion`, `bpmspushtime`) VALUES "
					+ "('"+docid+"', '"+qd+"', '"+last_time+"', '"+bpmsmap.get("文章标题")+"', '1', '"+postfrom+"', '', NULL);";
			int resu=JDBCSelectDoc.insertBpmsLog(bpmslogsql);
			if(resu>0){
				Util.log("添加记录表成功："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表成功："+bpmslogsql);
			}else{
				Util.log("添加记录失败："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表失败："+bpmslogsql);
			}
		}

		
		//修改稿件状态
		String updateUnderreView=" UPDATE WCMChnlDoc  SET UnderReview = '1' WHERE  RECID = '"+docid+"' ";
		int uuv=JDBCSelectDoc.updataSql(updateUnderreView);
		if(uuv>0){
			Util.log("修改稿件状态成功："+updateUnderreView,"bpms",0);
			System.out.println("修改稿件状态成功："+updateUnderreView);
		}else{
			Util.log("修改稿件状态失败："+updateUnderreView,"bpms",0);
			System.out.println("修改稿件状态失败："+updateUnderreView);
		}
		
		
		Map hrMap =new HashMap<String, String>();
		//返回海融数据
		if("0".equals(formcode)){
			
			hrMap.put("code", "0");	
			hrMap.put("message","同步bpms审核成功!");
			
		}else{
			hrMap.put("code", "-1");	
			hrMap.put("message", "送审失败！！稿件id为["+docid+"]");	
		}
		
		JSONObject hrJson =new JSONObject(hrMap);
		Util.log("--------------------------------------子公司同步bpms结束--------------------------------------------------","bpms",0);
		System.out.println("--------------------------------------子公司同步bpms结束--------------------------------------------------");
		return hrJson.toString();
        
	}
	
	
	
	
	/**
	 * 网站端同步bpms
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = {"/getbpmstoken"},produces = "application/json; charset=utf-8")
	public  String tobpms(HttpServletRequest request,HttpServletResponse response) throws SQLException, Exception{
		
		System.out.println("--------------------------------------网站同步bpms开始--------------------------------------------------");
		Util.log("--------------------------------------网站同步bpms开始--------------------------------------------------","bpms",0);
		
//		String docid = "146";
//		String chanl = "测试";
//		String qd = "wz";
//		String sig = "0";
//		String loginname = "zhaolichao";//bpms_loginName
		
		String docid = request.getParameter("docid");
		String chanl = request.getParameter("chanl");
		String qd = request.getParameter("qd");
		String sig = request.getParameter("sig");
		String loginname = request.getParameter("loginname");//bpms_loginName
		String lgname=StringUtils.substringBefore(loginname,"%40"); 
		lgname=StringUtils.substringBefore(lgname,"@");
		lgname=StringUtils.substringBefore(lgname,"_");
		System.out.println("当前登录用户处理@，用于获取token：："+lgname);
		System.out.println("网站bpms审核流程标识：："+sig);
		System.out.println("当前登录用户，用于获取token：："+loginname);
		
		//查询bpmslog
		int bpmssig=JDBCSelectDoc.selectBpmslog(docid,"wz");// 1推送过未结束流程  2推送了已经结束流程  其他未推送
		if(bpmssig == 1){
			Map hrMap =new HashMap<String, String>();
			hrMap.put("code", "0");	
			hrMap.put("message","已经送审过，流程未结束!");
			JSONObject hrJson =new JSONObject(hrMap);
			System.out.println("稿件已经推送到bpms，未结束流程！！！！");
    		Util.log("--------------------------------------网站同步bpms结束--------------------------------------------------","bpms",0);
    		System.out.println("--------------------------------------网站同步bpms结束--------------------------------------------------");
			return hrJson.toJSONString();
		}
		
		//获取token
		String result=HttpController.testGetToken(bpms_userName,bpms_password,lgname,bpms_url);
		//解析返回结果
		JSONObject jsonObjectHYSE = JSON.parseObject(result);
		String token = jsonObjectHYSE.get("id").toString();//获取到token
        
		//查询到需要传给bpms接口的书文档数据
		HashMap<String,String> bpmsmap=JDBCSelectDoc.selectWebSiteDoc(docid);//基础数据
		bpmsmap.put("栏目", chanl);
		bpmsmap.put("稿件id", docid);
		bpmsmap.put("现状态方式", sig);
		bpmsmap.put("流程分支判断", sig);
		bpmsmap.put("渠道标识", qd);
		//查询附件 并进行上传
		HashMap<String,String> bpmsfile=JDBCSelectDoc.selectFile(bpmsmap,docid,token,qd);//基础数据

		
		//定义返回map  第一层data
		Map resma =new HashMap<String, String>();
		resma.put("data", bpmsfile);
		resma.put("subject", bpmsmap.get("文章标题"));
		resma.put("draft", "0");
		
		resma.put("templateCode", bpms_templateCode_wz);
		
		//定义返回map  最二层data
		Map resmap =new HashMap<String, String>();
		resmap.put("data", resma);
		resmap.put("appName", bpms_appName);

		//转换成json数据
		JSONObject json =new JSONObject(resmap);
		
		//调用上传表单接口
		//设置表单头
	    Map<String, String> maphead = new HashMap<String, String>();
	    maphead.put("token", token);
	    Util.log("bpms调用json："+json.toJSONString(),"bpms",0);
	    System.out.println("bpms调用json："+json.toJSONString());
	    //调用接口
//	    String post = doPost1("fuserest","609011ab-8662-4678-9280-a8eff259a288","seeyonas","http://123.121.155.161:801/seeyon/rest/token");
	    String postfrom = sendPost(bpms_from_url,json.toJSONString(),maphead);
	    Util.log("bpms发起表单接口返回结果："+postfrom,"bpms",0);
	    System.out.println("bpms发起表单接口返回结果：："+postfrom);
	    //解析结果
		JSONObject jsonform = JSON.parseObject(postfrom);
		String formcode = jsonform.get("code").toString();//获取到token
		
		// 2推送了已经结束流程   0 没有查到数据  未推送
		if(bpmssig == 2){
	    	Date date = new Date();
	    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	String last_time = dtf.format(date);
	    	System.out.println("推送时的时间：："+last_time);
			String bpmslogsql=" UPDATE xwcmmetadatabpmslog	SET  pushsig = '1' WHERE  recid = '"+docid+"' and channel = '"+qd+"'  ";
			int resu=JDBCSelectDoc.insertBpmsLog(bpmslogsql);
			if(resu>0){
				Util.log("添加记录表成功："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表成功："+bpmslogsql);
			}else{
				Util.log("添加记录失败："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表失败："+bpmslogsql);
			}
			
		}else{
	    	Date date = new Date();
	    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	String last_time = dtf.format(date);
	    	System.out.println("推送时的时间：："+last_time);
			String bpmslogsql="INSERT INTO xwcmmetadatabpmslog (`recid`, `channel`, `pushtime`, `pushtitle`, `pushsig`, `returnjson`, `bpmsopinion`, `bpmspushtime`) VALUES "
					+ "('"+docid+"', '"+qd+"', '"+last_time+"', '"+bpmsmap.get("文章标题")+"', '1', '"+postfrom+"', '', NULL);";
			int resu=JDBCSelectDoc.insertBpmsLog(bpmslogsql);
			if(resu>0){
				Util.log("添加记录表成功："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表成功："+bpmslogsql);
			}else{
				Util.log("添加记录失败："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表失败："+bpmslogsql);
			}
		}
		
		//修改稿件状态
		String updateUnderreView=" UPDATE WCMChnlDoc  SET UnderReview = '1' WHERE  RECID = '"+docid+"' ";
		int uuv=JDBCSelectDoc.updataSql(updateUnderreView);
		if(uuv>0){
			Util.log("修改稿件状态成功："+updateUnderreView,"bpms",0);
			System.out.println("修改稿件状态成功："+updateUnderreView);
		}else{
			Util.log("修改稿件状态失败："+updateUnderreView,"bpms",0);
			System.out.println("修改稿件状态失败："+updateUnderreView);
		}
		
		Map hrMap =new HashMap<String, String>();
		//返回海融数据
		if("0".equals(formcode)){
			hrMap.put("code", "0");	
			hrMap.put("message","同步bpms审核成功!");	
		}else{
			hrMap.put("code", "-1");	
			hrMap.put("message", "送审失败！！稿件id为["+docid+"]");	
		}
		JSONObject hrJson =new JSONObject(hrMap);
		Util.log("--------------------------------------网站同步bpms结束--------------------------------------------------","bpms",0);
		System.out.println("--------------------------------------网站同步bpms结束--------------------------------------------------");
		return hrJson.toString();
        
	}
	
	
	 /**
	  * @param url 访问地址
	  *  @param param 需要传输参数参数；对象可以通过json转换成String
	  * @param header header 参数；可以通过下面工具类将string类型转换成map
	  * @return 返回网页返回的数据
	  */
	 public static String sendPost(String url, String param, Map<String, String> header) throws UnsupportedEncodingException, IOException {
	         OutputStreamWriter out;
	         URL realUrl = new URL(url);
	         // 打开和URL之间的连接
	         HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
	         //设置超时时间
	         conn.setConnectTimeout(5000);
	         conn.setReadTimeout(15000);
	         // 设置通用的请求属性
	         if (header!=null) {
	             for (Entry<String, String> entry : header.entrySet()) {
	                 conn.setRequestProperty(entry.getKey(), entry.getValue());
	             }
	         }
	         conn.setRequestMethod("POST");
	         conn.addRequestProperty("Content-Type", "application/json");
	         conn.setRequestProperty("accept", "*/*");
	         conn.setRequestProperty("connection", "Keep-Alive");
	         conn.setRequestProperty("user-agent",
	                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	         // 发送POST请求必须设置如下两行
	         conn.setDoOutput(true);
	         conn.setDoInput(true);
	         // 获取URLConnection对象对应的输出流
	         out = new OutputStreamWriter( conn.getOutputStream(),"UTF-8");// utf-8编码
	         // 发送请求参数
	         out.write(param);

	         // flush输出流的缓冲
	         out.flush();
	         int responseCode = conn.getResponseCode();  
	         InputStream in1=null;
	         // 定义BufferedReader输入流来读取URL的响应
			 if (responseCode == 200) {  
				 in1 = new BufferedInputStream(conn.getInputStream());  
			 } else {  
				 in1 = new BufferedInputStream(conn.getErrorStream());  
			 } 
             BufferedReader rd = new BufferedReader(new InputStreamReader(in1,"utf8"));
             String tempLine = rd.readLine();
             StringBuffer tempStr = new StringBuffer();
             String crlf = System.getProperty("line.separator");
             while (tempLine != null) {
                tempStr.append(tempLine);
                tempStr.append(crlf);
                tempLine = rd.readLine();
             }
             String responseContent = tempStr.toString();
             
//			 
//	         in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf8"));
//	         String line;
//	         while ((line = in.readLine()) != null) {
//	             result += line;
//	         }
//	         
	         if(out!=null){
	             out.close();
	         }
	         return responseContent;
	     }

	 
    public static void main(String[] args) throws Exception {
    	
//    	HashMap<String , String> res=new HashMap<>();
//    	
//    	res.put("来源", "来源");
//    	res.put("关键词", "关键词");
//    	res.put("作者", "作者");
//    	res.put("状态", "BPMS审核");
//    	res.put("责任编辑", "责任编辑");
//    	res.put("备注", "备注");
//    	res.put("摘要", "摘要");
//    	res.put("最后版本时间","2022-04-26 14:49:10");
//    	res.put("作者信息-单位", "融通");
//    	res.put("作者信息-部门","融通");
//    	res.put("作者信息-姓名", "张三");
//    	res.put("文章标题", "文章的标题111111");
//
//    	
//    	
//		//定义返回map  第一层data
//		Map resma =new HashMap<String, String>();
//		resma.put("data", res);
//		resma.put("subject", res.get("文章标题"));
//		resma.put("draft", "0");
//		resma.put("templateCode", bpms_templateCode_wx);
//		
//		//定义返回map  最二层data
//		Map resmap =new HashMap<String, String>();
//		resmap.put("data", resma);
//		resmap.put("appName", bpms_appName);
//
//		//转换成json数据
//		JSONObject json =new JSONObject(resmap);
//		
//		//调用上传表单接口
//		//设置表单头
//	    Map<String, String> maphead = new HashMap<String, String>();
//	    maphead.put("token", "faa3f4a3-1b6b-4a25-861d-4be72314315e");
//
//    	 String postfrom = sendPost(bpms_from_url,json.toJSONString(),maphead);
//
//    	
//        System.out.println(postfrom);
    	
    	
//		String bpms_loginName="seeyonas";
// 		HttpController HttpController = new HttpController();
//		String result=HttpController.testGetToken(bpms_userName,bpms_password,bpms_loginName,bpms_url);

//    	HttpServletRequest request=null;
//    	HttpServletResponse response=null;
//    	BpmsController.tobpms(request, response);
    	
//    	Date date = new Date();
//    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    	dtf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
//    	String last_time = dtf.format(date);
//    	System.out.println(last_time);

//        DateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        sdf1.setTimeZone(TimeZone.getTimeZone("GMT"));
//        String date = sdf1.format(new Date());
//        //20210409T113025.625+0800
//        System.out.println("\n=========\n" + date);
        
        
//        Date date = new Date(System.currentTimeMillis());
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
//        String BeijingTime = dateFormat.format(date);
//        System.out.println("\n=========\n" + BeijingTime);

		Map<String, String> params = new HashMap();
		params.put("uid", "123");
		params.put("access_token", "token");

		String postfrom = sendPost("http://dh.trs.cn/datas/api/cas/uploaddoc/uploadDoc","",params);
		System.out.println(postfrom);
    }
}