package com.trs.addhytokpsdzc;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.mqadddoc.SelectSiteName;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;





/**
 * OAuth2.0Controller
 */
@Controller
public class AddDocumentControllerQXZMCX{
	 /**
     * 请求编码
     */
    public static String requestEncoding = "UTF-8";
    /**
     * 连接超时
     */
    private static int connectTimeOut = 5000;
    /**
     * 读取数据超时
     */
    private static int readTimeOut = 10000;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	
	private static String HYCHNLID = ConstantUtil.HYCHNLID;
	private static String HYSITEID = ConstantUtil.HYSITEID;
	
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	
	 /**
	 * 添加文档-开普-首都之窗=---取消证明查询视图
	 * @param docid
	 * @param chnlid
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static boolean addDocumentQXZMCX(String docid,String chnlid,String viewname,String SITEID) throws Exception {
		
		Util.log("==================================首都之窗-----取消证明查询视图新增数据=========================","addDoc===="+SITEID,0);
		Util.log(chnlid+"=========="+viewname+"===================="+docid,"addDoc===="+SITEID,0);
		Util.log("==================================首都之窗-----取消证明查询视图新增数据=========================","addDoc===="+SITEID,0);
		System.out.println("------------首都之窗-----取消证明查询自定义视图下文章同步到开普------------");
		
		boolean returnResult=false;	
		//开普接口json
		JSONObject jsonOb = new JSONObject();
		JSONArray json = new JSONArray();// 默认附件
		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  开普同步到海云id SITEID  父栏目id  PARENTID
		String CHANNELID=chnlid;//id
		String DOCID=docid;//文档id
		//默认字段
		String bt=null;//文件名称  标题
		String ly=null;//来源

		
		String kaipuid=null;
//		String kaipupush=null;
		try {
			kaipuid = cR.doCheck(DOCID);
//			kaipupush=cR.docCheckWD(DOCID);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return returnResult=false;	
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return returnResult=false;	
		}
		System.out.println(kaipuid+"------------查询是否已经新建---------------");
		if(kaipuid != null){
			Util.log("已经存在与开普，修改，不新建！","addDoc===="+SITEID,0);
			System.out.println("已经存在与开普，修改，不新建！");	
		}else{
//			if(kaipupush!=null){
//				Util.log("文档是开普推送来的,不做重新新建文档!!!!!!!!","addDoc===="+SITEID,0);
//				System.out.println("文档是开普推送来的,不做重新新建文档!!!!!!!!");
//			}else{}
			//查询站点名称 打印日志需要使用
			String sitename=SelectSiteName.selectSiteName(SITEID);
			Util.log("----------------------------处理文档开始-----------------------------","addDoc===="+SITEID,0);
			System.out.println("----------------------------处理文档开始-----------------------------");
			//调用海云接口查询文档详细数据
			String sServiceId="gov_webdocument";
			String sMethodName="findOpenDataDocumentById";
			Map<String, String> savemap = new HashMap<String, String>();
			savemap.put("DocId",DOCID);
			savemap.put("ChannelId",CHANNELID);
			savemap.put("CurrUserName", USER); // 当前操作的用户
			String hy11= HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
			JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
			Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
			
			Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
			Util.log("id:"+DOCID+"--海云返回结果："+jsonObjectHYSE.get("MSG"),"addDoc===="+SITEID,0);
			System.out.println("id:"+DOCID+"--海云返回结果："+jsonObjectHYSE.get("MSG"));
			System.out.println(hy11);
			Util.log(hy11,"addDoc===="+SITEID,0);
			if("true".equals(dataArrHYSave)){
				
				//处理获取结果
				//默认字段处理
				bt =jsonObjectHYGPSE.getString("BLSX");//办理事项  标题
				ly =jsonObjectHYGPSE.getString("LY");//来源
				SITEID=jsonObjectHYGPSE.getString("SITEID");//开普同步到海云id
				
				//循环处理其他字段
				for(String str:jsonObjectHYGPSE.keySet()){
					
					if("BLR".equals(str)){
						System.out.println("BLR===="+jsonObjectHYGPSE.get(str).toString());
						//主题分类  
						StringBuffer zt=new StringBuffer();
						String ztfl=jsonObjectHYGPSE.get(str).toString();
						if(!StringUtils.isBlank(ztfl)){
							String [] ztflsz=ztfl.split("~");
							for (int a=0;a<ztflsz.length;a++) {
								String fl=ztflsz[a].substring(0,ztflsz[a].indexOf("`"));
								zt.append(fl+";");
							}
							jsonOb.put(viewname+"@@"+str,zt);
						}

							
					}else if("ZTFL_NAME".equals(str)){
						//主题分类  
						String value=jsonObjectHYGPSE.get(str).toString();
						if(value!=null){
							jsonOb.put(viewname+"@@"+str, value.replace("[\"", "").replace("\"]", "").replace("\"", "").replace("[", "").replace("]", ""));
						}
							
					}else{
						String value=jsonObjectHYGPSE.get(str).toString();
						if(value!=null){
							jsonOb.put(viewname+"@@"+str, value.replace("[\"", "").replace("\"]", "").replace("\"", "").replace("[", "").replace("]", ""));
						}
					}
					
				}
				
			    //查询对应栏目的开普id  
			    //根据开普同步到海云id 获取到开普对应的资源库id  SITEID
				String kaipuidLM=null;
				try {
					if(CHANNELID == null){
						kaipuidLM=cR.docCheck(SITEID,1);
					}else {
						//查询栏目是否存在开普
						kaipuidLM=cR.docCheck(CHANNELID,2);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Util.log("----------------------------处理文档结束开始新建-----------------------------","addDoc===="+SITEID,0);
				System.out.println("----------------------------处理文档结束开始新建-----------------------------");
			    
				//关联的其他文档  目前开普不支持接口 关联  不做处理
				//新增之前 检查是否已经存在该文档  文档可以重复 不需要该操作
			    // 处理正常字段同步
			    jsonOb.put("isOrig", "0");
			    jsonOb.put("resTranMode", "0");// 0  文本内容    1：文件资源-文件传输  
			    jsonOb.put("dirId", kaipuidLM);//目录id 
//			    jsonOb.put("dirId", "");//目录id 
			    jsonOb.put("origId",ZWBJ_APPID+ DOCID);// 文档id -- 资源ID   修改文档需要使用该id 
			    jsonOb.put("status", "3");
			    //默认字段
			    jsonOb.put("content", "&nbsp&nbsp&nbsp&nbsp&nbsp");//正文
			    jsonOb.put("name", bt); 
			    jsonOb.put("source", ly); 

			    //发布地址
			    String pubUrl=JDBCIIP.JDBCDriverSelectPubUrl(DOCID);
			    if(pubUrl!=null){
			    	jsonOb.put("online", pubUrl);
			    }
			    
				String fbrq=jsonObjectHYGPSE.getString("GBSJ");//公布日期
			    if(fbrq!=null){
			    	jsonOb.put("pubDate", fbrq);
			    }
			    
			    Util.log("首都之窗-----取消证明查询视图存入开普数据jsonOb==:"+jsonOb,"addDoc===="+SITEID,0);
			    Util.log("首都之窗-----取消证明查询视图存入开普数据json==:"+json,"addDoc===="+SITEID,0);
			    System.out.println("首都之窗-----取消证明查询视图存入开普数据jsonOb==:"+jsonOb);
			    System.out.println("首都之窗-----取消证明查询视图存入开普数据json==:"+json);
			    
			    String result=createRes2(json, jsonOb);
			    
			    //处理开普新增返回结果
			    JSONObject jsonresult = JSON.parseObject(result);
			    Util.log("首都之窗-----取消证明查询视图开普返回结果数据："+jsonresult,"addDoc===="+SITEID,0);
			    System.out.println("首都之窗-----取消证明查询视图开普返回结果数据："+jsonresult);
			    Integer  dataresult = (Integer) jsonresult.get("code"); 
				if(0 == dataresult){
					System.out.println("首都之窗-----取消证明查询视图新增资源成功！");
					 returnResult=true;
					String kpid=jsonresult.getString("data");
					JDBC  jdbc=new JDBC();
					Util.log("首都之窗-----取消证明查询视图文档同步到开普存入sql："+"INSERT INTO document ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+","+HYCHNLID+","+HYSITEID+") VALUES ('"+kpid+"','"+DOCID+"','WD','0','"+CHANNELID+"','"+SITEID+"')","log",0);
					System.out.println("首都之窗-----取消证明查询视图开普同步到海云存入sql："+"INSERT INTO document ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+","+HYCHNLID+","+HYSITEID+") VALUES ('"+kpid+"','"+DOCID+"','WD','0','"+CHANNELID+"','"+SITEID+"')");
					try {
						Integer jd=jdbc.JDBCDriver("INSERT INTO document ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+","+HYCHNLID+","+HYSITEID+") VALUES ('"+kpid+"','"+DOCID+"','WD','0','"+CHANNELID+"','"+SITEID+"')");
						if(jd!=-1){
							Util.log("首都之窗-----取消证明查询视图开普同步到海云中间表存入成功!","log",0);
							System.out.println("首都之窗-----取消证明查询视图开普同步到海云中间表存入成功!");
						}else{
							Util.log("首都之窗-----取消证明查询视图开普同步到海云中间表存入失败!","log",0);
							System.out.println("首都之窗-----取消证明查询视图开普同步到海云中间表存入失败!");
						}
					} catch (SQLException e) {
						
						// TODO Auto-generated catch block
						e.printStackTrace();
						return returnResult=false;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return returnResult=false;
					}
				}else{
					
					Util.log("文章名称："+bt,"erro"+sitename+SITEID,0);
					Util.log("栏目id："+CHANNELID,"erro"+sitename+SITEID,0);
					Util.log("错误信息：新增首都之窗-----取消证明查询资源失败-----"+jsonresult.get("msg"),"erro"+sitename+SITEID,0);
					Util.log("=============================================================================","erro"+sitename+SITEID,0);
					System.out.println("新增首都之窗-----取消证明查询资源失败："+jsonresult.get("msg"));
					return returnResult=false;
				}
			
				
			}else{
				
				Util.log("文章名称："+bt,"erro"+sitename+SITEID,0);
				Util.log("栏目id："+CHANNELID,"erro"+sitename+SITEID,0);
				Util.log("错误信息：调用海云接口查询文章失败-----"+jsonObjectHYSE.get("MSG"),"erro"+sitename+SITEID,0);
				Util.log("=============================================================================","erro"+sitename+SITEID,0);
				System.out.println("海云查询文档详细信息接口返回信息:"+jsonObjectHYSE.get("MSG"));
				return returnResult=false;
			}
		
		
		}
		
		System.out.println("==================================首都之窗-----取消证明查询视图新增数据结束=================================");
		return returnResult;
	}
	
	
	
	 public static  String  createRes2(JSONArray jsonArray,JSONObject json ) throws Exception {
		 	System.out.println("createRes2");
		    json.put("attachments", jsonArray.toJSONString());//附件
		    
		    String url =Z_URL+ "/resource/createRes";
		    Map<String, String> map = new HashMap<String, String>();
		    System.out.println("新建文档内容json："+json);
		    System.out.println("新建文档内容url："+url);
		    map.put("appId", ZWBJ_APPID);
		    map.put("data", DESUtil.encrypt(json.toJSONString(),ZWBJ_STR_DEFAULT_KEY));
//		    System.out.println("map"+map);
		    String post = doPost(url, map,json);
//		    System.out.println(post); 
		    return post;
	} 
	
	 
	 public static String doPost(String reqUrl, Map parameters,JSONObject json) {
//	    	System.out.println("------reqUrl开始------"+reqUrl);
//	    	System.out.println("------parameters开始------"+parameters);
//	    	System.out.println("------dopost开始------");
	        HttpURLConnection url_con = null;
	        String responseContent = null;
	        try {
	        	System.out.println("进入try：");
	            String params = getMapParamsToStr(parameters, requestEncoding);
//	            System.out.println("dopost里的params"+params);
	            URL url = new URL(reqUrl);
//	            System.out.println("dopost里的url"+url);
	            url_con = (HttpURLConnection) url.openConnection();
	            url_con.setRequestMethod("POST");
	            System.setProperty("sun.net.client.defaultConnectTimeout", String.valueOf(connectTimeOut));// （单位：毫秒）jdk1.4换成这个,连接超时
	            System.setProperty("sun.net.client.defaultReadTimeout", String.valueOf(readTimeOut)); // （单位：毫秒）jdk1.4换成这个,读操作超时
	            url_con.setRequestProperty("User-agent","Mozilla/4.0");
	            url_con.setDoOutput(true);
	            byte[] b = params.toString().getBytes();
	            url_con.getOutputStream().write(b, 0, b.length);
	            url_con.getOutputStream().flush();
	            url_con.getOutputStream().close();
	            int responseCode = url_con.getResponseCode();  
	            System.out.println("responseCode:"+responseCode);
	            InputStream in=null;
		    	if (responseCode == 200) {  
		    		in = new BufferedInputStream(url_con.getInputStream());  
		    	} else {  
		    		in = new BufferedInputStream(url_con.getErrorStream());  
		    	} 
	            BufferedReader rd = new BufferedReader(new InputStreamReader(in, requestEncoding));
	            String tempLine = rd.readLine();
	            StringBuffer tempStr = new StringBuffer();
	            String crlf = System.getProperty("line.separator");
	            while (tempLine != null) {
	                tempStr.append(tempLine);
	                tempStr.append(crlf);
	                tempLine = rd.readLine();
	            }
	            responseContent = tempStr.toString();
	            rd.close();
	            in.close();
	        } catch (IOException e) {
	            Util.log(json,"网络故障WD",0); 
	            System.out.println("网络故障");
	            e.printStackTrace();
	        } finally {
	        	System.out.println("进入finally");
	            if (url_con != null) {
	                url_con.disconnect();
	            }
	        }
	        return responseContent;
	    }

	    public static void test(){
	    	System.out.println("调用测试方法调用");
	    }

	    private static String getMapParamsToStr(Map paramMap, String requestEncoding) throws IOException {
	        StringBuffer params = new StringBuffer();
	        // 设置边界
	        for (Iterator iter = paramMap.entrySet().iterator(); iter.hasNext(); ) {
	            Map.Entry element = (Map.Entry) iter.next();
	            params.append(element.getKey().toString());
	            params.append("=");
	            params.append(URLEncoder.encode(element.getValue().toString(), requestEncoding));
	            params.append("&");
	        }

	        if (params.length() > 0) {
	            params = params.deleteCharAt(params.length() - 1);
	        }

	        return params.toString();
	    }
	    
	    
	   


}