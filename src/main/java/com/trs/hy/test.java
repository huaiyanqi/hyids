package com.trs.hy;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZFWZLDJL;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.JsonMapper;
import com.trs.kafka.KafkaUtil;
import com.trs.kafka.Producer;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.kptohyzfwz.ZWuplod;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class test {

	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	//以下三个参数不做同步
//	private static String bmwd = ChannelidUtil.bmwd;
//	private static String zwmc = ChannelidUtil.zwmc;
//	private static String zwzsdbp = ChannelidUtil.zwzsdbp;
	
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	
    private final static String USER_AGENT = "Mozilla/5.0";

    /**
     * 请求编码
     */
     static String requestEncoding = "UTF-8";
    /**
     * 连接超时
     */
     static int connectTimeOut = 5000;
    /**
     * 读取数据超时
     */
     static int readTimeOut = 10000;
	
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public static void main(String[] args) throws Exception {
//		.toLowerCase();//转成小写
//		.toUpperCase();//转成大写

		
//		StringBuffer zt=new StringBuffer();
//		String ztfl="个人`个人~企业`企业~企业、个人`企业、个人";
//		String [] ztflsz=ztfl.split("~");
//		for (int a=0;a<ztflsz.length;a++) {
//			String fl=ztflsz[a].substring(0,ztflsz[a].indexOf("`"));
//			zt.append(fl+";");
//		}
//		System.out.println(zt);
//====================================================================================================================================
		
//		String sServiceId="gov_webdocument";
//		String sMethodName="findOpenDataDocumentById";
//		Map<String, String> savemap = new HashMap<String, String>();
//		savemap.put("DocId","1908929");
//		savemap.put("ChannelId","39644");
//		savemap.put("CurrUserName", USER); // 当前操作的用户
//		String hy11= HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
//		System.out.println(hy11);
// =============================================================================================================================================
//		Map<String, Object> oPostData = new HashMap<>();
//		oPostData.put("ChannelId","36863");
//		String sServiceId = "gov_webdocument";// 必填，固定参数
//		String sMethodName = "saveDocumentInOpenData";// 必填，固定参数
//		oPostData.put("CurrUserName", "dev");// 必填，固定参数
//		oPostData.put("DOCTYPE", "20");// 必填，固定参数
//		oPostData.put("ObjectId", "0");// 必填，固定参数
//		
//		System.out.println("=========================================================开始调用海云接口=========================================================");
//		System.out.println(JSON.toJSONString(oPostData));
//		Dispatch oDispatch = WCMServiceCaller.Call(sServiceId, sMethodName, oPostData, true);
//		String aaaa=oDispatch.getResponseText();
//		System.out.println(aaaa);
		//=============================================================
		
//		String sql="SELECT * FROM viewid WHERE viewid = '74'";
//		String  ysjjid=JDBC.selectYsjjid(sql);
//		System.out.println("jieguo :"+ysjjid);
		
		
		
//===============================================================================
		
//		String  name="测试附件名*aaa?称特殊字符";
//    	//处理附件名称带特殊字符
//		name=zfwzUtil.filterSpecialChar(name);
//		System.out.println(name);
		
		
//==============================================================================================================================		
		
//		String aaa="http://192.141.252.5/gov/file/read_file.jsp?DownName=DOCUMENT&FileName=P020191219585739024282.pdf";
//		System.out.println(aaa.substring(aaa.lastIndexOf("=")+1));
		
//		String pKPpath=zfwzUtil.uploadFile("D:\\data\\Hycloud_Resources\\北京一卡通电子发票.pdf","北京一卡通电子发票","asdasd","1111","asdas","111");
//		System.out.println(pKPpath);
		
		//去除 文件名称得特殊字符
//		String name=zfwzUtil.filterSpecialChar("正/文-外/网/信\\息/\\'.a'aodaksl;das!@#$^&*(*^&%$#^&|}{}|::?>更新（沟域经济）");
//		System.out.println(name);
//====================================================================================================		
//		String sServiceId="gov_site";
//		String sMethodName="whetherOpenData";
//		Map<String, String> savemap = new HashMap();
//		savemap.put("ChannelID","15208");
//		savemap.put("CurrUserName", USER); // 当前操作的用户
//		String hyLM = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
//		
//		System.out.println(hyLM);
		
//====================================================================================================		
//		String aaa="京发改（审）〔2017〕11号";
//		String[] a=aaa.split("〔");
//		System.out.println(a[0]);
		
		
//		ResourceBundle bundle = ResourceBundle.getBundle("services");
//		String aaa="北京市文化和旅游局";
//		Z_ID= new String(bundle.getString("Z_ID").getBytes("ISO-8859-1"), "UTF8");
//		String [] zid=Z_ID.split(";");
//		for(int z=0;z<zid.length;z++){
//			String zid1=zid[z];
//			Util.log("第"+z+"组资源库id名称："+zid1,"addLMlog",0);
//			System.out.println("第"+z+"组资源库id名称："+zid1);
//			
//			String [] zsting=zid1.split(",");
//			
//			Util.log("第"+z+"次资源库得名称："+zsting[1],"addLMlog",0);
//			System.out.println("第"+z+"次资源库得名称："+zsting[1]);
//			
//			if(aaa.equals(zsting[1])){
//				System.out.println(zsting[1]+"========================"+aaa);
//			}
//		}
		
//		String dowurl="group1/M00/46/3C/oYYBAF4F0XyAKCWvACpDLuO_pRg701.jpg";
//		String url=dowurl.substring(dowurl.lastIndexOf("/")+1,dowurl.length());
//		System.out.println(url);
		
//		String url="/repo/fs/repository/metadatas/110003/775343/3e4f8c110cbf4185aead26fd62795ff2/9a12994764bc4c20bc0fdd3528e30e9f.shtml";
//		String[] fja=url.split("filePath");
////		System.out.println(fja[0]+"--------11-------"+fja[1]);
//		if(fja[1].indexOf(".")>=0){
//			System.out.println("1111");
//		}else{
//			System.out.println(fja[0]+"---------------"+fja[1]);
//		}
		
		
//		//根据栏目id查询到视图
//		String sServiceIdView="gov_site";
//		String sMethodNameView="whetherOpenData";
//		Map<String, String> savemapView = new HashMap();
//		savemapView.put("ChannelID","18818");
//		savemapView.put("CurrUserName", USER); // 当前操作的用户
//		String hyLMView = HyUtil.dataMoveDocumentHyRbj(sServiceIdView,sMethodNameView,savemapView);
//		System.out.println(hyLMView);
		
		
		
		
		
		
//		System.out.println(Z_ID);
//		String paramValue=StringEscapeUtils.unescapeHtml("/upload/File/&#65533;&#65533;&#65533;&#65533;2_&#65533;&#65533;&#65533;&#65533;&#65533;.doc");
////		String paramValue="/upload/File/&#65533;&#65533;&#65533;&#65533;2_&#65533;&#65533;&#65533;&#65533;&#65533;.doc";
//        System.out.println("paramValue = " + new String(paramValue.getBytes("GB2312"), "UTF-8"));
//        System.out.println("paramValue = " + new String(paramValue.getBytes("GB2312"), "GBK"));
//        System.out.println("paramValue = " + new String(paramValue.getBytes("GB2312"), "ISO8859-1"));
//        String str = new String(paramValue.getBytes("GB2312"), "UTF-8");
//        System.out.println("paramValue = " + "a    a".replace(" ", "%20"));
//		String a="src=\"filePath=";
//		String[] fja=a.split("filePath");
//		System.out.println(fja[1].indexOf("."));
		
		
		
//		根据站点id查询站点名称
		String sServiceId="gov_classifiedinfo";
		String sMethodName="getCompleteClassInfo";
		Map savemap = new HashMap();
		savemap.put("fieldName","SSQY");
		savemap.put("objectId","14240");
		savemap.put("ChannelId","48737");
		savemap.put("CurrUserName", "sunjiwei_kpy"); // 当前操作的用户
		String hy11=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		System.out.println(hy11);
//		// 根据站点id查询下级栏目
//		String sServiceId = "gov_site";
//		String sMethodName = "queryChildrenChannelsOnEditorCenter";
//		Map<String, String> mSE=new HashMap<String, String>();
//		mSE.put("CurrUserName", "wangyu_www"); // 当前操作的用户
//		//设置一次查询数量
//		mSE.put("pageSize", "1500");
//		mSE.put("pageindex", "1");
//		mSE.put("SITEID", "128");
//		mSE.put("ParentChannelId", "0");
//		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
//		System.out.println("结果"+hy1);
		
//		//根据栏目id查询栏目信息
//		String sServiceId="gov_site";
//		String sMethodName="whetherOpenData";
//		Map<String, String> savemap = new HashMap();
//		savemap.put("ChannelID","3933");
//		savemap.put("CurrUserName", USER); // 当前操作的用户
//		String hyLM = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
//		System.out.println(hyLM);
		
//		String sServiceId="gov_webdocument";
//		String sMethodName="findOpenDataDocumentById";
//		Map<String, String> savemap = new HashMap<String, String>();
//		savemap.put("DocId","1013412");
//		savemap.put("ChannelId","3921");
//		savemap.put("CurrUserName", USER); // 当前操作的用户
//		String hy11= HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
//		System.out.println(hy11);
		
		
//		String [] aaa= "/upload/File/59969-2(1).doc".split("/upload");
//		System.out.println(aaa[0]+"==============="+aaa[1]);
		
//		String FWZH="怀政文〔2018〕25号";
//		String[] sp=FWZH.toString().split("〔");
//		String mz=sp[0];
//		String other=sp[1];
//		
//		String fwnf=FWZH.toString().split("〔")[1].split("〕")[0];
//		String fwxh=FWZH.toString().split("〔")[1].split("〕")[1];
//		System.out.println();
		
		//更新到发布平台数据库
//		String sServiceIdSave = "gov_group";
//		String sMethodNameSave = "findGroupById";
//		HashMap<String, String> oPostDataSave = new HashMap<String, String>();
//		oPostDataSave.put("CurrUserName",USER); // 当前操作的用户
//		oPostDataSave.put("GROUPID","89");
//		String Gprole=HyUtil.dataMoveDocumentHyRbj(sServiceIdSave,sMethodNameSave,oPostDataSave);
//		System.out.println(Gprole);
//		StringBuffer roelsgroup=new StringBuffer();
//		JSONObject jsonObjectrole = JSON.parseObject(Gprole);
//		Object dataArrrole = jsonObjectrole.get("DATA");//根据json对象中数组的名字解析出其所对应的值
//		if(dataArrrole!=null){
//			JSONObject jsonObjectHYrole = JSON.parseObject(dataArrrole.toString());
//			//获取到角色
//			JSONArray DOCRELPIC=jsonObjectHYrole.getJSONArray("ROLES");//相角色
//			for(int i = 0; i < DOCRELPIC.size(); i++){
//		    	String role=DOCRELPIC.getJSONObject(i).getString("ROLEID");
//		    	if(i != 0){
//		    		roelsgroup.append(",");	
//		    	}
//		    	roelsgroup.append(role);
//			}
//		}
		
		
//		String str="http://192.141.1.11:7002/repo-web/manager/getManagerFile?filePath=group1/M00/08/81/oYYBAF3SkwOAdiqkAAHGMu-Xy4A58.docx";
//		String [] splitName=str.split("/");
//		String name=splitName[splitName.length-1];
//		System.out.println(name);
		
		
//		String aaa="<div class=\"view TRS_UEDITOR trs_paper_default trs_web\"><p style=\"text-align: center\"><img src=\"http://192.141.1.10:7002/zuul/repo-web/manager/getManagerFile?filePath=group1/M00/07/11/oYYBAF3FFcOAPz15ABpszsbAa6E502.jpg\" width=\"1920\" height=\"1200\" title=\"2.jpg\" alt=\"2.jpg\" OLDSRC=\"W020191118832734035902.jpg\" /></p><p class=\"insertfileTag\" style=\"line-height: 16px;\"><img style=\"vertical-align: middle; margin-right: 2px;\" src=\"http://192.141.1.10:7002/zuul/repo-web/manager/getManagerFile?filePath=group1/M00/00/62/wI0BClxw89eAcZ0EAAAD9PvfRvQ854.gif\" /><a style=\"font-size:12px; color:#0066cc;\" appendix=\"true\" otheroperation=\"false\" href=\"http://192.141.1.10:7002/zuul/repo-web/manager/getManagerFile?filePath=group1/M00/03/60/oYYBAF2DRQWASmHxAAsGRGJBd_Q74.docx\" title=\"市级统一信息资源库应用接入说明.docx\" OLDSRC=\"http://192.141.1.10:7002/zuul/repo-web/manager/getManagerFile?filePath=group1/M00/03/60/oYYBAF2DRQWASmHxAAsGRGJBd_Q74.docx\">市级统一信息资源库应用接入说明.docx</a></p><p>工作动态内容工作动态内容工作动态内容工作动态内容工作动态内容<br/></p></div>";
//		String pcontent=ZWuplod.contentResourceUpload(aaa,5,"(src=\").*?(\")");
//		System.out.println(pcontent);
		
//		Map map=new HashMap<>();
//		String sServiceId = "gov_user";
//		String sMethodName = "saveUser";
//		map.put("CurrUserName", USER); // 当前操作的用户
//		map.put("USERID", "0");
//		map.put("USERNAME", "cheyan_swj");
//		map.put("PASSWORD", "Awr*&8%kT1");
//		map.put("EMAIL", "cheyan@capinfo.com.cn");
//		map.put("TRUENAME", "车燕");
//		map.put("MOBILE", "15801373395");
//		String hy=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,map);
//		System.out.println(hy);
		
//		Dispatch upDispatch = WCMServiceCaller.UploadFile("D:\\TestDataBridge\\oYYBAF2DRQWASmHxAAsGRGJBd_Q74.docx");
//		// 获取上传后返回的文件名
//		String file = upDispatch.getUploadShowName();
//		System.out.println(file);
		
//		String sServiceIdView="gov_site";
//		String sMethodNameView="whetherOpenData";
//		Map<String, String> savemapView = new HashMap();
//		savemapView.put("ChannelID","15212");
//		savemapView.put("CurrUserName", USER); // 当前操作的用户
//		String hyLMView = HyUtil.dataMoveDocumentHyRbj(sServiceIdView,sMethodNameView,savemapView);
//		JSONObject jsonObjectHYSEView = JSON.parseObject(hyLMView);
//		Object dataArrView = jsonObjectHYSEView.get("DATA");//根据json对象中数组的名字解析出其所对应的值
//		JSONObject jsonObjectHYGPSEView = JSON.parseObject(dataArrView.toString());
//		System.out.println("政府网站：开普推送数据到海云查询视图短名返回结果=================："+jsonObjectHYSEView.get("MSG"));
//		//视图id
//		String viewid =jsonObjectHYGPSEView.getString("VIEWID");
//		System.out.println(jsonObjectHYSEView);
		
//		AddDocumentControllerLDJL.addDocumentLDJL("483723","14201","WEBLDJL");
		
		
		
		
		
//		String aa="LY_NAME";
//		System.out.println(aa.split("_")[0]);
//		String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='74'";
//		String viewName=jdbcids.JDBCVIEWIDSELECT(sql);
//		System.out.println("视图短名："+viewName.toUpperCase());
		
		
//		Map<Integer,String> map = new HashMap<Integer,String>();
//
//		 map.put(1, "星期一");
//
//		map.put(2, "星期一");
//
//		 map.put(3, "星期一");
//
//		 map.put(4, "星期一");
//
//		 map.put(5, "星期一");
//
//		map.put(6, "星期一");
//
//		map.put(7, "星期一");
		
//		String url=zfwzUtil.uploadFile("D:\\TestDataBridge\\图片2.jpg", "图片2.jpg");
//		System.out.println(url);
//		byte[] file=zfwzUtil.downloadFile("group1/M00/08/20/oYYBAF3LxTCASUKvAAAAJ1ENaVc94.docx");
//		System.out.println(file);
//		byte[] file=null;
//		FileOutputStream downloadFile = new FileOutputStream("D:\\1111.docx");
//        downloadFile.write(file, 0, file.length);
//        downloadFile.flush();
//        downloadFile.close();
//        System.out.println(file);
//        System.out.println(file);
		
//		zfwzUtil.downloadFile2();
		
//		//成功后添加中间表
//		JDBC  jdbc=new JDBC();
//		
//		Integer jd=jdbc.JDBCDriver("update kpuser set password='Trsadmin!@#$5678' where username='devadmin'");	
//		System.out.println(jd);
		 
		 
		 
//		StringBuffer roelsids=new StringBuffer();
//		Map<String, String> userRole=new HashMap<>();
//		String sServiceJS = "gov_user";
//		String sMethodJS = "findUserById";
//		userRole.put("CurrUserName", USER); // 当前操作的用户
//		userRole.put("USERID", "277");
//		System.out.println("更新用户前map："+userRole);
//		String hyrole=HyUtil.dataMoveDocumentHyRbj(sServiceJS,sMethodJS,userRole);
//		System.out.println("更新用户返回结果：："+hyrole);
//		JSONObject jsonObjectrole = JSON.parseObject(hyrole);
//		Object dataArrrole = jsonObjectrole.get("DATA");//根据json对象中数组的名字解析出其所对应的值
//		JSONObject jsonObjectHYrole = JSON.parseObject(dataArrrole.toString());
//		//获取到角色
//		JSONArray DOCRELPIC=jsonObjectHYrole.getJSONArray("ROLES");//相角色
//		for(int i = 0; i < DOCRELPIC.size(); i++){
//	    	String role=DOCRELPIC.getJSONObject(i).getString("ROLEID");
//	    	if(i != 0){
//	    		roelsids.append(",");	
//	    	}
//	    	roelsids.append(role);
//		}
//		System.out.println(roelsids);
		
//		String sServiceIdDE = "gov_user";
//		String sMethodNameDE = "saveUser";
//		map.put("CurrUserName", USER); // 当前操作的用户
//		map.put("USERID", userid);
//		System.out.println("更新用户前map："+map);
//		String hyUP=HyUtil.dataMoveDocumentHyRbj(sServiceIdDE,sMethodNameDE,map);
		
//		String sServiceId = "gov_user";
//		String sMethodName = "queryUsersNoAuth";
//		Map<String, String> oPostData = new HashMap<String, String>();
//		oPostData.put("CurrUserName", USER); // 当前操作的用户
//		oPostData.put("pageSize", pageSize);//
//		oPostData.put("pageindex", "1");
//		String hySE = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
//		System.out.println(hySE);
		
		
//		  Dispatch oDispatch = WCMServiceCaller.UploadFile("D:\\huaiyanqi\\testpng20191017.png");
//		  String fileName = oDispatch.getUploadShowName();
//		  System.out.println("上传附件之后的附件名称"+fileName);
		


////		String sMethodName = "saveDocumentInWeb"; // 咨询视图
//		String sServiceId = "gov_webdoc.ument";
//		String sMethodName = "saveDocumentInOpenData";
//		Map<String, String> oPostData = new HashMap<String, String>();
////		oPostData.put("DocTitle", "北京市行政许可和行政处罚等信用信息公示工作方案");
////		oPostData.put("MetaDataId", "0");//新增 0 但是可以不传这个参数
//		oPostData.put("ObjectId", "0");//新增 0 固定参数
//		oPostData.put("CurrUserName", "bjg_scjgj"); // 当前操作的用户
//		oPostData.put("ChannelId", "15195"); //政策文件
//		oPostData.put("DocType", "20");// 文章类型  需要自己去区分 
//		
//		oPostData.put("zw", "&nbsp;&nbsp;&nbsp;&nbsp;"); 
//		oPostData.put("fbt","");
//		oPostData.put("zz","");
//		oPostData.put("bt","测试标题文章");
//		oPostData.put("ZY","");
//		oPostData.put("gjc","");
//		oPostData.put("ly","");
//		oPostData.put("organcat","市投资促进局");
//		oPostData.put("ztfl","");
//		oPostData.put("wtfl","");
//		oPostData.put("fwdw","");
//		oPostData.put("lhfwdw","");
//		oPostData.put("xgdz","");
//		oPostData.put("fwnf","");
//		oPostData.put("fwxh","");
//		oPostData.put("cwrq","");
//		oPostData.put("ssrq","");
//		oPostData.put("fzrq","");
//		oPostData.put("fbrq","");
//		Dispatch dispatch=WCMServiceCaller.Call(sServiceId, sMethodName,oPostData, true);
//		String aaaa=dispatch.getResponseText();
////		String result=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
//		System.out.println("返回结果：："+aaaa);
//		
//      处理附件上传接口		
		//附件名称
//		String fjName="testpng20191017.png";
//		Map<String, Object> mapfujian=new HashMap<>();
//		//附件存放路径
//		mapfujian.put("FJURL", "/data/Hycloud_Resources/");
//		mapfujian.put("FJNAME", fjName);
//		JSONObject jsonup =new JSONObject(mapfujian);
//		System.out.println("附件的最終上傳參數JSON："+jsonup.toString());
//		//开始上传附件
//		String fileName=upFile(jsonup.toString());
//		//返回附件名称
//		System.out.println(fileName);
////		
//		List plist=new ArrayList<>();
//		Map fmap=new HashMap<>();
//		fmap.put("SrcFile", fjName);
//		fmap.put("AppFile", fileName);
//		fmap.put("AppDesc", fjName.substring(0,fjName.lastIndexOf(".")));
//		fmap.put("AppendixId", "0");
//		plist.add(fmap);
//		HashMap<Object, Object> oPostData=new HashMap<>();
//		oPostData.put("DocRelPic",JSON.toJSONString(plist));
//		
//		Map<String , Object> mapDate = new HashMap<>();
//		mapDate.put("CHANNELID", "5689");
//		mapDate.put("ObjectId", "0");
//		mapDate.put("DocType", "20");
//		mapDate.put("DocContent", "wjmc测试标题");
//		mapDate.put("DocTitle", "这是标题");
//		mapDate.put("DocHtmlCon", "<p>正文<br/></p>");
//		mapDate.put("fj", JSON.toJSONString(oPostData));
//		Map<String, Object> map = new HashMap<>();
//		map.put("USERNAME", "dev");
//		map.put("TYPE", "1");
//		map.put("DATA", mapDate);
//		JSONObject json =new JSONObject(map);
//		String result=createDoc(json.toString());
//		System.out.println(result);
		
		
		
		
//  其他栏目数据		
		
//		Map<String , Object> mapDate = new HashMap<>();
//		mapDate.put("CHANNELID", "13471");
////		mapDate.put("ObjectId", "0");
//		mapDate.put("ObjectId", "0");
//		mapDate.put("DocType", "20");
//		mapDate.put("zw", "wjmc测试标题");
//		mapDate.put("gjc", "正文");
//		mapDate.put("ly", "这是个来源");
//		mapDate.put("zy", "正文");
//		mapDate.put("zxsj", "1990-10-22 11:07");
//		mapDate.put("wjyxx", "0");
//		List<Object> listztfl=new ArrayList<>();
//		Map<Object, Object>  ztflMap= new HashMap<Object, Object>();
//		ztflMap.put("type", "content");
//		ztflMap.put("value", "主题1`1~主题2`2");
//		listztfl.add(ztflMap);
//		Map<Object, Object>  ztflMap1= new HashMap<Object, Object>();
//		ztflMap1.put("type", "value");
//		ztflMap1.put("value","主题1`1");
//		listztfl.add(ztflMap1);						
//		mapDate.put("ztfl", JSON.toJSONString(listztfl)); 
//		
//		
//		List<Object> listwtfl=new ArrayList<>();
//		Map<Object, Object>  wtflMap= new HashMap<Object, Object>();
//		wtflMap.put("type", "content");
//		wtflMap.put("value", "文体1`1~文体2`2");
//		listwtfl.add(wtflMap);
//		Map<Object, Object>  wtflMap1= new HashMap<Object, Object>();
//		wtflMap1.put("type", "value");
//		wtflMap1.put("value","文体1`1");
//		listwtfl.add(wtflMap1);						
//		mapDate.put("wtfl", JSON.toJSONString(listwtfl)); 
//		
//		
//		List<Object> listfwdw=new ArrayList<>();
//		Map<Object, Object>  fwdwMap= new HashMap<Object, Object>();
//		fwdwMap.put("type", "content");
//		fwdwMap.put("value", "单位1`1~单位2`2~全国人民代表大会常务委员会`全国人民代表大会常务委员会");
//		listfwdw.add(fwdwMap);
//		Map<Object, Object>  fwdwMap1= new HashMap<Object, Object>();
//		fwdwMap1.put("type", "value");
//		fwdwMap1.put("value","单位1`1");
//		listfwdw.add(fwdwMap1);						
//		mapDate.put("fwdw", JSON.toJSONString(listfwdw)); 
//		
//		
//		List<Object> listlhfwdw=new ArrayList<>();
//		Map<Object, Object>  lhfwdwMap= new HashMap<Object, Object>();
//		lhfwdwMap.put("type", "content");
//		lhfwdwMap.put("value", "单位1`1~单位2`2");
//		listfwdw.add(lhfwdwMap);
//		Map<Object, Object>  lhfwdwMap1= new HashMap<Object, Object>();
//		lhfwdwMap1.put("type", "value");
//		lhfwdwMap1.put("value","单位1`1");
//		listlhfwdw.add(lhfwdwMap1);						
//		mapDate.put("lhfwdw", JSON.toJSONString(listlhfwdw)); 
//		mapDate.put("organcat", "市地税局");
//		
////		mapDate.put("DocHtmlCon", "<p>正文<br/></p>");
////		mapDate.put("fj", JSON.toJSONString(oPostData));
//		Map<String, Object> map = new HashMap<>();
//		map.put("USERNAME", "dev");
//		map.put("TYPE", "0");
//		map.put("DATA", mapDate);
//		
//		JSONObject json =new JSONObject(map);
//		System.out.println("最终调用参数json："+json);
//		for(int s=0;s<5;s++){
//			mapDate.put("bt", "测试标题"+s);
//			String result=createDoc(json.toString());
//			System.out.println(result);
//		}
		
		
		
		
//  默认视图的		
		
		
//		String fjName="testpng20191017.png";
//		Map<String, Object> mapfujian=new HashMap<>();
//		mapfujian.put("FJURL", "/data/Hycloud_Resources/");
//		mapfujian.put("FJNAME", fjName);
//		JSONObject jsonup =new JSONObject(mapfujian);
//		System.out.println("附件的最終上傳參數："+jsonup.toString());
//		String fileName=upFile(jsonup.toString());
//		System.out.println(fileName);
//		
//		List plist=new ArrayList<>();
////		List flist=new ArrayList<>();
//		Map fmap=new HashMap<>();
//		fmap.put("SrcFile", fjName);
//		fmap.put("AppFile", fileName);
//		fmap.put("AppDesc", fjName.substring(0,fjName.lastIndexOf(".")));
//		fmap.put("AppendixId", "0");
//		plist.add(fmap);
//		HashMap<Object, Object> oPostData=new HashMap<>();
//		oPostData.put("DocRelPic",JSON.toJSONString(plist));
//		
//		Map<String , String> mapDate = new HashMap<>();
//		mapDate.put("CHANNELID", "5689");
////		mapDate.put("ObjectId", "0");
//		mapDate.put("ObjectId", "0");
//		mapDate.put("DocType", "20");
//		mapDate.put("DocTitle", "测试标题");
//		mapDate.put("ShortTitle", "测试标题");
//		mapDate.put("DocContent", "正文");
//		mapDate.put("DocHtmlCon", "<p>正文<br/></p>");
//		mapDate.put("fj", JSON.toJSONString(oPostData));
//		Map<String, Object> map = new HashMap<>();
//		map.put("USERNAME", "dev");
//		map.put("TYPE", "1");
//		map.put("DATA", mapDate);
//		
//		JSONObject json =new JSONObject(map);
//		System.out.println("最终调用参数json："+json);
//		String result=createDoc(json.toString());
//		System.out.println(result);		
		
		
//		解码
		
//		 String strTest = "æ­£æ\u0096\u0087";
//		 strTest = URLDecoder.decode(strTest,"UTF-8");//解码
//		 System.out.println(strTest);	
//		String fileName = new String (strTest.getBytes("ISO8859-1"),"utf-8");
//		System.out.println(fileName);
		
		
//		桥银强本地调用新增		
		
//		JSONArray json = new JSONArray();// 默认附件
//		String pKPpath=PResource("D:\\TestDataBridge2\\P020151225617037279230.doc","P020151225617037279230.doc");
// 	    JSONObject fpCon= new JSONObject();
// 	    fpCon.put("name", "P020151225617037279230.doc");//附件名称
// 	    fpCon.put("path", pKPpath);// 附件路径
// 	    json.add(fpCon);
// 	    
// 	    
// 	    
// 	    
//		JSONObject jsonOb = new JSONObject();
//		jsonOb.put("name", "测试标题"); 
//		jsonOb.put("resTranMode", "0");// 0  文本内容    1：文件资源-文件传输  
//	    jsonOb.put("resTitle", "测试标题"); 
////	    jsonOb.put("resourceClassify", "6"); //资源分类   文档中该字段非必须
//	    jsonOb.put("source", "测试标题"); //来源 
//	    jsonOb.put("abstracts", "测试标题"); //摘要
//	    jsonOb.put("author", "测试标题");//作者
//	    jsonOb.put("isOrig", "0");
//	    jsonOb.put("origId", "90909899");// 文档id
//	    jsonOb.put("dirId", "f4a30417473648e79d941e7cd93195d8");//目录id 
//	    jsonOb.put("status", "3"); 
//	    jsonOb.put("content", "&nbsp&nbsp&nbsp&nbsp&nbsp");//正文
//	    jsonOb.put("fj", json.toJSONString());//附件
//	    
//	    System.out.println("存入开普数据jsonOb==:"+jsonOb);
//	    System.out.println("存入开普数据json==:"+json);
//	   
//	    String result=createRes2(json, jsonOb);
//		System.out.println(result);

	}
	
	


	public static String delHTMLTag(String htmlStr){ 
        String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式 
        String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式 
        String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式 
         
        Pattern p_script=Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE); 
        Matcher m_script=p_script.matcher(htmlStr); 
        htmlStr=m_script.replaceAll(""); //过滤script标签 
         
        Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE); 
        Matcher m_style=p_style.matcher(htmlStr); 
        htmlStr=m_style.replaceAll(""); //过滤style标签 
         
        Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE); 
        Matcher m_html=p_html.matcher(htmlStr); 
        htmlStr=m_html.replaceAll(""); //过滤html标签 

        return htmlStr.trim(); //返回文本字符串 
    } 
	
	public static  String getPath(){
		String abc="<div class='view TRS_UEDITOR trs_paper_default trs_web'><p>详见附件。</p>"
				+ "<p class='insertfileTag' style='line-height: 16px;'><img style='vertical-align: middle; margin-right: 2px;'"
				+ " src='/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_pdf.gif' />"
				+ "<a style='font-size:12px; color:#0066cc;' appendix='true' otheroperation='false' "
				+ "href='/protect/P0201901/P020190115/P020190115655681814519.pdf' title='市国资委2018年度绩效任务四季度落实情况汇总表.pdf' "
				+ "OLDSRC='/protect/P0201901/P020190115/P020190115655681814519.pdf'>市国资委2018年度绩效任务四季度落实情况汇总表.pdf</a>"
				+ "</p></div><div class='view TRS_UEDITOR trs_paper_default trs_web'><p>详见附件。</p><p class='insertfileTag' "
				+ "style='line-height: 16px;'><img style='vertical-align: middle; margin-right: 2px;' "
				+ "src='/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_pdf.gif' />"
				+ "<a style='font-size:12px; color:#0066cc;' appendix='true' otheroperation='false' "
				+ "href='/protect/P0201901/P020190115/P020190115655681814519.pdf' title='市国资委2018年度绩效任务四季度落实情况汇总表.pdf' "
				+ "OLDSRC='/protect/P0201901/P020190115/P020190115655681814519.pdf'>市国资委2018年度绩效任务四季度落实情况汇总表.pdf</a></p></div>"
				+"<p> <iframe class='edui-upload-video video-js vjs-default-skin' "
				+ "src='http://192.141.252.5/mas/openapi/pages.do?method=exPlay&appKey=gov&id=18&autoPlay=false'"
				+ " width='420' height='280' appendix='true'></iframe>测试视频库</p>";
//		
//		String abc="<p> <iframe class='edui-upload-video video-js vjs-default-skin' "
//				+ "src='http://192.141.252.5/mas/openapi/pages.do?method=exPlay&appKey=gov&id=18&autoPlay=false'"
//				+ " width='420' height='280' appendix='true'></iframe>测试视频库</p>";		
//		String aaa="iframe class='edui-upload-video video-js vjs-default-skin' "
//				+ "src='http://192.141.252.5/mas/openapi/pages.do?method=exPlay&appKey=gov&id=18&autoPlay=false'"
//				+ " width='420' height='280' appendix='true'></iframe";
//		String bb="video controls='controls' loop='loop' width='480' height='400' src='http://192.168.1.219:7002/repo-web/manager/getManagerFile?filePath=group1/M00/00/B4/wKgBxFxqgemAL1joADEBLOFwO7I159.mp4' autoplay='autoplay'></video";
//		abc=abc.replace(aaa, bb);
//		System.out.println(abc);
//		String a="a.a.a.a.a.a.a";
//		String b="bbbb";
//		
//		a.replace(".", b);
//		System.out.println(a);
//		
//		String s = "my.test.txt";
//		System.out.println(s.replace(".", "#"));
		
		//  '/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_pdf.gif'
		//  '/protect/P0201901/P020190115/P020190115655681814519.pdf'
//		String abc="    <img style='ertical-align: middle; margin-right: 2px;' src='/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_jpg.gif'/><a style='font-size:12px; color:#0066cc;' appendix='true' otheroperation='false' href='/protect/P0201902/P020190223/P020190223584663980378.png' title='Q量子银座.png'>Q量子银座.png</a>";
//		String name="icon_jpg";
//		String regExImg = "("+name+").*?(.jpg|.gif|.png|.jpeg|.bmp|.JPG|.GIF|.PNG|.JPEG|.BMP)";//正文中视频
////		String regExImg = "(icon_jpg).*?(.jpg|.gif|.png|.jpeg|.bmp|.JPG|.GIF|.PNG|.JPEG|.BMP)";//正文中视频
//		
		String regExImg = "(href=).*?(.pdf|.PDF|.RAR|.rar|.zip|.ZIP)";//正文中pdf  5  改成6
		Pattern patternImg = Pattern.compile(regExImg);
		Matcher matcherImg = patternImg.matcher(abc);
		List<String> list = new ArrayList<String>();
		while (matcherImg.find()) {
			String picPath = matcherImg.group();
			picPath = picPath.substring(8, picPath.length());
//			String fullPath = getFullPath(picPath);
			System.out.println("多少个path="+picPath);
			list.add(picPath);
		}
		return null;
	}

	public static  void  getpwd(){
		String sServiceId="gov_webdocument";
		String sMethodName="findOpenDataDocumentById";
		Map savemap = new HashMap();
		savemap.put("DocId","39");
		savemap.put("ChannelId","18");
		savemap.put("CurrUserName", "dev"); // 当前操作的用户
		String hy11=null;
		try {
			hy11 = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObjectHYSave = JSON.parseObject(hy11);
		Object dataArrHYSave = jsonObjectHYSave.get("ISSUCCESS");
		System.out.println("--海云返回结果："+jsonObjectHYSave.get("MSG"));
	}
	
	public static String hyQuerySites(){
		String sServiceId = "gov_site";
		String sMethodName = "querySites";
		Map oPostData=new HashMap();
		oPostData.put("CurrUserName", "dev"); // 当前操作的用户
		oPostData.put("MEDIATYPE", 1);
//		oPostData.put("SEARCHFIELDS", "SITEDESC");
		oPostData.put("MODULEID", 10);
		String hy = null;
		try {
			hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(hy);
		return hy;
	}
	
	public static String hyuser() throws Exception{
		String sServiceIdSE = "gov_user";
		String sMethodNameSE = "checkUserExist";
		Map map=new HashMap();
		map.put("CurrUserName", "dev"); // 当前操作的用户
		map.put("SEARCHVALUE", "abbb"); // 用户名称
		String hySE=HyUtil.dataMoveDocumentHyRbj(sServiceIdSE,sMethodNameSE,map);
		return hySE;
	}
	
	public static void  hylanmu() throws Exception{
		String sServiceId = "gov_site";
		String sMethodName = "querySitesAndChnls";
		Map oPostData = new HashMap();
		oPostData.put("CurrUserName", "dev"); // 当前操作的用户
		oPostData.put("MEDIATYPE",1);
		oPostData.put("OBJTYPE",1);
		oPostData.put("OBJID",1);
		String hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		System.out.println(hy);
	}
	
	public static void  hylanmu1() throws Exception{
		String sServiceId = "gov_channel";
		String sMethodName = "queryChannels";
		Map oPostData = new HashMap();
		oPostData.put("CurrUserName", "dev"); // 当前操作的用户
		oPostData.put("SITEID",1);
//		oPostData.put("OBJTYPE",1);
//		oPostData.put("OBJID",1);
		String hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		System.out.println(hy);
	}
	
	  public static String  deleRes(JSONObject json) throws Exception {
		    
		    String encrypt = DESUtil.encrypt(json.toJSONString(), Z_APPID);
		    String url =Z_URL+ "/resource/delRes?data=" + encrypt + "&appId="+Z_APPID;
		    String post = HttpUtil.doGet(url, "UTF-8");
		    System.out.println(post); 
		    return post;
	  }
	  
	public static void test123() {
	    ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.141.7.28");
        factory.setPort(5672);
        factory.setUsername("trs_rabbitmq");
        factory.setPassword("trsadmin!@#$5678");
        Connection connection = null;
		try {
			connection = factory.newConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Channel channel = null;
		try {
			channel = connection.createChannel();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			channel.queueDeclare("site", false, false, false, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("Customer Waiting Received messages");
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Customer Received '" + message + "'");
            }
        };
        try {
			channel.basicConsume("site", true, consumer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static List<Object> selectCh(String sitId,String pId) throws Exception{
		List<Object> list=new ArrayList<>();
		// 根据站点id查询下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", USER); // 当前操作的用户
//		mSE.put("SEARCHFIELDS", userName); // 用户名称
		//设置查询多少个用户
		mSE.put("pageSize", "1500");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", sitId);
		mSE.put("ParentChannelId", pId);
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		System.out.println("结果"+hy1);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2.size()>0){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					String SITEID=dataBean.getString("SITEID");
					String CHNLNAME=dataBean.getString("CHNLNAME");
					String CHANNELID=dataBean.getString("CHANNELID");
					String HASCHILDREN=dataBean.getString("HASCHILDREN");//是否存在下级栏目
					System.out.println(SITEID+"-------------------"+CHNLNAME+"----------------"+CHANNELID);
					Map<String, Object> mapChnl=new HashMap<>();
					mapChnl.put("siteId", SITEID);
					mapChnl.put("channelName", CHNLNAME);
					mapChnl.put("channelId", CHANNELID);
					mapChnl.put("pid", pId);//上一级栏目id,0表示一级栏目
					mapChnl.put("isLeaf", 0);
					list.add(mapChnl);
					if("true".equals(HASCHILDREN)){
						List<Object> listch=selectCh(SITEID,CHANNELID);
						list.addAll(listch);
					}
					
				}
			}
		}
		return list;
	}
	
	
	public static String  pushDocument(String data) throws Exception {
		
		System.out.println("文档接收到的数据:"+data);
		Map result = new HashMap();
		Map oPostData = new HashMap();
		JSONObject jsondata = JSON.parseObject(data.replace("'", "\""));
//		JSONObject jsondata = jsonObjectSE.getJSONObject("data");
		if(data != null){
			//接收数据成功
			String siteId=jsondata.getString("siteId");//站点id
			String channelId=jsondata.getString("channelId");//栏目id
			String resID=jsondata.getString("resID");//文档资源id
			String abstracts=jsondata.getString("abstracts");//摘要
			String resTitle=jsondata.getString("resTitle");//标题
			String name=jsondata.getString("name");//名称
			String author=jsondata.getString("author");//作者
			String content=jsondata.getString("content");//正文
			String source=jsondata.getString("idPoC");//来源
			String attachments=jsondata.getString("attachments");//附件
			String type=jsondata.getString("type");//1图片类型；2网页类型;3音频类型；4视频类型;5文档类型
			String DocRelTime=jsondata.getString("createdTime");//创建时间
			
			oPostData.put("DocRelTime", DocRelTime);
			oPostData.put("ChannelId", channelId);
			oPostData.put("ObjectId", "0");
			oPostData.put("DocTitle", resTitle);
			oPostData.put("DocHtmlCon", content);
			//去除html标签
			String txtContent=delHTMLTag(content);
			System.out.println("去除html标签txtContent："+txtContent);
			oPostData.put("DocHtmlCon", txtContent);
			oPostData.put("DocAbstract", abstracts);
			oPostData.put("SUBDOCTITLE", name);
			oPostData.put("DOCAUTHOR", author);
			oPostData.put("DOCSOURCENAME", source);
			//查询文档是否已经同步
			String kpid=null;
			try {
				kpid = cR.docCheckWD(resID);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result.put("code", "1");
				result.put("data", "中间表查询失败！");
				JSONObject jsonResult =new JSONObject(result);
				return jsonResult.toString();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result.put("code", "1");
				result.put("data", "中间表查询失败！");
				JSONObject jsonResult =new JSONObject(result);
				return jsonResult.toString();
			}
			if(kpid!=null){
				System.out.println("已经同步过该文章!!!!!");
				result.put("code", "1");
				result.put("data", "已经同步过该文章!!!!!");
				JSONObject jsonResult =new JSONObject(result);
				return jsonResult.toString();
			}else{
				if("1".equals(type)){  
					oPostData.put("DOCTYPE",50);  //图片
				}else if("2".equals(type)){
					
					oPostData.put("DOCTYPE",20);  //文字
					//正文不做处理，直接处理附件
					JSONArray attach=jsondata.getJSONArray("attachments");//相附件
					if(attach.size()>0){
						//图片
						 StringBuilder docRelPic = new StringBuilder();
						 docRelPic.append("[");
						 //附件
						 StringBuilder docRelFile = new StringBuilder();
						 docRelFile.append("[");
						for(int i = 0; i < attach.size(); i++){
							  String  kuURL="http://192.141.1.10:7002/repo-web";
							  String  fjName=attach.getJSONObject(i).getString("name");
							  String  fjpath= attach.getJSONObject(i).getString("path");
							  String  fjtype=attach.getJSONObject(i).getString("type");
							  if(fjpath != null){
								  //代表存在附件，处理上传
								  //先下载再上传
								  try {
									hfl.downLoadFromUrl(kuURL+fjpath,fjName,resouse_path);
								  } catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									result.put("code", "1");
									result.put("remarks", "下载附件失败！！");
									JSONObject jsonResult =new JSONObject(result);
									return jsonResult.toString();
								  }
								  //上传海云
								  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+fjName);
								  String fileName = oDispatch.getUploadShowName();
								  //图片处理
								  if("1".equals(fjtype)){
									  docRelPic.append("{\"SrcFile\":\"" + fjName+ "\",\"AppFile\":\"" + fileName + "\",\"AppDesc\":\""
													+ fjName.substring(0,fjName.lastIndexOf(".")) + "\", \"AppendixId\":\"0\" }");
								  }else if("5".equals(fjtype)||"3".equals(fjtype)||"4".equals(fjtype)){//处理附件
									  docRelFile.append("{\"SrcFile\":\"" + fjName+ "\",\"AppFile\":\"" + fileName + "\",\"AppDesc\":\""
												+ fjName.substring(0,fjName.lastIndexOf(".")) + "\", \"AppendixId\":\"0\" }");
								  }
								  
								}
						}
						docRelPic.append("]");
						docRelFile.append("]");
						
						if(!"".equals(docRelPic)&&docRelPic!=null) {
							oPostData.put("DocRelPic",docRelPic);
						}
						if(!"".equals(docRelFile)&&docRelFile!=null) {
							oPostData.put("docRelFile",docRelFile);
						}
					}else{
						System.out.println("同步文档不存在附件！！！");
					}
					oPostData.put("CurrUserName", USER); // 当前操作的用户
					String sServiceId = "gov_webdocument";
					String sMethodName = "saveDocumentInWeb";
					String hy=null;
					try {
						hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						result.put("code", "1");
						result.put("data", "添加文档失败！！！！！");
						JSONObject jsonResult =new JSONObject(result);
						return jsonResult.toString();
					}
					JSONObject jsonObjectHYSE = JSON.parseObject(hy);
					Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
					JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
					
					Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
					System.out.println("海云返回结果："+jsonObjectHYSE.get("MSG"));
					if("true".equals(dataArrHYSave)){
						
						String	docid =jsonObjectHYGPSE.getString("DOCID");//标题
						Util.log("文档新建到海云存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')","log",0);
						System.out.println("开普同步到海云存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')");
						Integer jd=null;
						try {
							jd = jdbc.JDBCDriver("INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							result.put("code", "1");
							result.put("remarks", "中间表插入失败！！");
							JSONObject jsonResult =new JSONObject(result);
							return jsonResult.toString();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							result.put("code", "1");
							result.put("remarks", "中间表插入失败！！");
							JSONObject jsonResult =new JSONObject(result);
							return jsonResult.toString();
						}
						if(jd!=-1){
							result.put("code", "0");
							result.put("data", "已成功接收推送数据");
							Util.log("开普同步到海云中间表存入成功!","log",0);
							System.out.println("开普同步到海云中间表存入成功!");
							JSONObject jsonResult =new JSONObject(result);
							return jsonResult.toString();
						}else{
							result.put("code", "1");
							result.put("remarks", "开普同步到海云中间表存入失败!");
							Util.log("开普同步到海云中间表存入失败!","log",0);
							System.out.println("开普同步到海云中间表存入失败!");
							JSONObject jsonResult =new JSONObject(result);
							return jsonResult.toString();
						}
						
					}
					
				}else if("30".equals(type)){
					oPostData.put("DOCTYPE",30);  //链接型文档
				}else if("40".equals(type)){
					oPostData.put("DOCTYPE",40);  //外部文件文档
				}
				
			}
			
			
		}else{
			result.put("code", "1");
			result.put("remarks", "接收开普数据失败!");
			System.out.println("接收开普数据失败!");
			JSONObject jsonResult =new JSONObject(result);
			return jsonResult.toString();
		}
		result.put("code", "1");
		result.put("remarks", "操作失败！！！");
		JSONObject jsonResult =new JSONObject(result);
		return jsonResult.toString();
	}
	
	
	public  static void tksoid(){
		System.out.println(coIDName);
	}
	
	
	/**
	 * 修改文档-政策文件
	 * @param jsondata
	 */
	public static void upDOcZCWJTEST(String DOCID,String CHANNELID){
		
		System.out.println("------------修改政策文件视图文档开始------------");
		//MQ消息队列数据获取
		
		String wjmc=null;//文件名称
		String zcywgjc=null;//关键词
		String zw=null;//正文
		String zy=null;//文档摘要
		String cwrq=null;//成文日期
		String ssrq=null;//实施日期
		String fzrq=null;//废止日期
		String fbrq=null;//发布日期
		String yxx=null;//文件有效性
		String zcywwh=null;//发文序号
		String zcywzrbm=null;//发文单位
		String zcywlhfw=null;//联合发文单位
		String zcywgwzl=null;//主题分类
		
		String kaipuid=null;
		try {
				kaipuid=cR.doCheck(DOCID);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(kaipuid==null){
			System.out.println("开普中不存在该文档！");
		}else{

			//调用海云接口查询文档详细数据
			String sServiceId="gov_webdocument";
			String sMethodName="findOpenDataDocumentById";
			Map savemap = new HashMap();
			savemap.put("DocId",DOCID);
			savemap.put("ChannelId",CHANNELID);
			savemap.put("CurrUserName", USER); // 当前操作的用户
			String hy11=null;
			try {
				hy11 = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
			Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
			
			Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
			System.out.println("id:"+DOCID+"--海云返回结果："+jsonObjectHYSE.get("MSG"));
			
			if("true".equals(dataArrHYSave)){
				//处理获取结果
				wjmc =jsonObjectHYGPSE.getString("WJMC");//文件名称
				zcywgjc=jsonObjectHYGPSE.getString("GJC");//关键词
				zw=jsonObjectHYGPSE.getString("ZW");//正文
				zy=jsonObjectHYGPSE.getString("NRZY");//内容摘要
				cwrq =jsonObjectHYGPSE.getString("CWRQ");//成文日期
				ssrq =jsonObjectHYGPSE.getString("SSRQ");//实施日期
				fzrq =jsonObjectHYGPSE.getString("FZRQ");//废止日期
				fbrq =jsonObjectHYGPSE.getString("FBRQ");//发布日期
				yxx =jsonObjectHYGPSE.getString("YXX");//有效性
				zcywwh =jsonObjectHYGPSE.getString("FWXH");//发文序号
				zcywzrbm =jsonObjectHYGPSE.getString("FWDW_NAME");//发文单位
				zcywlhfw =jsonObjectHYGPSE.getString("LHFWDW_NAME");//联合发文单位
				zcywgwzl =jsonObjectHYGPSE.getString("ZTFL_NAME");//主题分类
				
				// 处理正文 内容  
				
				String pContent=null;
				String otherContent=null;
				String videoContent=null;
				//正文不为空处理
				if(zw != null){
					try {
						//图片 Content_regular_p    截取开始 src='  5个字符串
						pContent = re.contentResource(zw, 5, Content_regular_p,"标题","13141","站点名称","127");
						//处理 zip  pdf  rar   截取开始  href=' 6个字符串
						otherContent = re.contentResource(pContent, 6, Content_regular,"标题","13141","站点名称","127");
						//处理正文中的视频标签
						String videoNmae=CHANNELID+"_"+wjmc+"_"+DOCID;
						videoContent = re.contentResourceVideo(otherContent, 1, Content_regular_v,videoNmae);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				
				//处理附件
				JSONArray json = new JSONArray();// 默认附件
			    JSONArray DOCRELFILE=jsonObjectHYGPSE.getJSONArray("FJ");//相关附件
			    String docPath=null;
			    String dName=null;
			    
			    if(DOCRELFILE!=null){
			    	
			    	for(int i = 0; i < DOCRELFILE.size(); i++){
				    	String url="http://192.141.252.5/gov/file/read_file.jsp?DownName=DOCUMENT&FileName=";
				    	docPath=DOCRELFILE.getJSONObject(i).getString("APPFILE");
				    	dName=DOCRELFILE.getJSONObject(i).getString("APPDESC");
				    	String Pv=docPath.substring(docPath.lastIndexOf("."),docPath.length());
				    	//上传之前先下载
						try {
							hfl.downLoadFromUrl(url+docPath,dName+Pv,resouse_path);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
				    	String pKPpath=re.PResource(resouse_path+dName+Pv,dName+Pv);
				    	//返回的开普上传路径，需要拼接成开普需要的格式
				    	//循环添加到JSONArray中
				 	    JSONObject fpCon= new JSONObject();
				 	    fpCon.put("name", dName);//附件名称
				 	    fpCon.put("path", pKPpath);// 附件路径
				 	    json.add(fpCon);
				    }
			    }
			    
				//查询对应栏目的开普id  
			    //根据站点id 获取到开普对应的资源库id  SITEID
					
					
				// 处理正常字段同步
				JSONObject jsonOb = new JSONObject();
				//修改文档固定参数   
				jsonOb.put("name", wjmc); 
				jsonOb.put("resTranMode", "0");// 0  文本内容    1：文件资源-文件传输  
				jsonOb.put("isOrig", "1");
				jsonOb.put("resId", "103797"); //  资源id
				//默认字段
				jsonOb.put("content", videoContent);//正文
				jsonOb.put("resTitle", wjmc); 
				jsonOb.put("abstracts", zy);
//				jsonOb.put("keyword", zcywgjc); 
				//元数据集字段
				jsonOb.put("cwrq", cwrq);
				jsonOb.put("ssrq", ssrq); 
				jsonOb.put("fzrq", fzrq);
				jsonOb.put("fbrq", fbrq); 
				jsonOb.put("status", "3"); 
				if(yxx!=null){
				    if(yxx.equals("1")){
					    jsonOb.put("zcywxxyxx", "是");
				    }else if(yxx.equals("0")){
					    jsonOb.put("zcywxxyxx", "否");
					}
				  }
				
				jsonOb.put("zcywwh", zcywwh); 
			    if(zcywzrbm!=null){
				    if(!zcywzrbm.equals("[]")){
				    	jsonOb.put("zcywzrbm", zcywzrbm.replace("[\"", "").replaceAll("\",\"", ";").replaceAll("\"]", ""));
				    }	
			    }
			    if(zcywlhfw!=null){
			    	 if(!zcywlhfw.equals("[]")){
					    jsonOb.put("zcywlhfw", zcywlhfw.replace("[\"", "").replaceAll("\",\"", ";").replaceAll("\"]", "")); 
					 }
			    }
			    if(zcywgwzl!=null){
				    if(!zcywgwzl.equals("[]")){
				    	 jsonOb.put("zcywgwzl", zcywgwzl.replace("[\"", "").replaceAll("\",\"", ";").replaceAll("\"]", ""));
				    }
			    }
				jsonOb.put("zcywgjc", zcywgjc);
				jsonOb.put("wjmc", wjmc);
				
				System.out.println("存入开普数据jsonOb==:"+jsonOb);
				System.out.println("存入开普数据json==:"+json);
				String result=null;
				try {
				   result=re.updateRes0(json, jsonOb);
				} catch (Exception e) {
					// TODO Auto-generated catch block
						e.printStackTrace();
				}
				    //处理开普新增返回结果
				JSONObject jsonresult = JSON.parseObject(result);
				Integer  dataresult = (Integer) jsonresult.get("code"); 
				if(dataresult==0){
					System.out.println("修改政策文件视图资源成功！");
				}else{
					System.out.println("修改政策文件视图资源失败："+jsonresult.get("msg"));
				}
				
			}else{
				System.out.println("海云查询文档详细信息接口返回信息:"+jsonObjectHYSE.get("MSG"));
			}
			
		
		}
		System.out.println("------------修改政策文件文档结束------------");	
	
	}
	
	
	// resTranMode=2 
    public static String  createRes2(JSONArray jsonArray,JSONObject json ) throws Exception {
	    
	    json.put("attachments", jsonArray.toJSONString());//附件
	    
	    String url = "http://192.140.199.194:7302/api/res/resource/createRes";
	    Map<String, String> map = new HashMap<String, String>();
	    System.out.println("新建文档内容json："+json);
	    System.out.println("新建文档内容url："+url);
	    map.put("appId", Z_APPID);
	    map.put("data", DESUtil.encrypt(json.toJSONString(),Z_APPID));
	    System.out.println("map"+map);
	    String post = HttpUtil.doPost(url, map,json);
	    System.out.println(post); 
	    return post;
}
    
    
    /**
     * <pre>
     * 发送带参数的POST的HTTP请求
     * </pre>
     *
     * @param reqUrl     HTTP请求URL
     * @param parameters 参数映射表
     * @return HTTP响应的字符串
     */
    public static String doPost(String reqUrl, Map parameters) {
    	System.out.println("------reqUrl开始------"+reqUrl);
    	System.out.println("------parameters开始------"+parameters);
    	System.out.println("------dopost开始------");
        HttpURLConnection url_con = null;
        String responseContent = null;
        try {
            String params = getMapParamsToStr(parameters, requestEncoding);
            System.out.println("dopost里的params"+params);
            
            params = URLDecoder.decode(params,"UTF-8");//解码
            System.out.println("dopost里的params解码后"+params);
            URL url = new URL(reqUrl);
            System.out.println("dopost里的url"+url);
            url_con = (HttpURLConnection) url.openConnection();
            url_con.setRequestMethod("POST");
            System.setProperty("sun.net.client.defaultConnectTimeout", String.valueOf(connectTimeOut));// （单位：毫秒）jdk1.4换成这个,连接超时
            System.setProperty("sun.net.client.defaultReadTimeout", String.valueOf(readTimeOut)); // （单位：毫秒）jdk1.4换成这个,读操作超时
            url_con.setRequestProperty("User-agent","Mozilla/4.0");
            url_con.setDoOutput(true);
            
            System.out.println("url_con++++"+url_con);
            byte[] b = params.toString().getBytes();
            url_con.getOutputStream().write(b, 0, b.length);
            url_con.getOutputStream().flush();
            url_con.getOutputStream().close();
            int responseCode = url_con.getResponseCode();  
            System.out.println("responseCode:"+responseCode);
            InputStream in=null;
	    	if (responseCode == 200) {  
	    		in = new BufferedInputStream(url_con.getInputStream());  
	   	} else {  
	    		in = new BufferedInputStream(url_con.getErrorStream());  
	    	} 
            BufferedReader rd = new BufferedReader(new InputStreamReader(in, HttpUtil.requestEncoding));
            String tempLine = rd.readLine();
            StringBuffer tempStr = new StringBuffer();
            String crlf = System.getProperty("line.separator");
            while (tempLine != null) {
                tempStr.append(tempLine);
                tempStr.append(crlf);
                tempLine = rd.readLine();
            }
            responseContent = tempStr.toString();
            rd.close();
            in.close();
        } catch (IOException e) {
            System.out.println("网络故障");
            e.printStackTrace();
        } finally {
            if (url_con != null) {
                url_con.disconnect();
            }
        }
        return responseContent;
    }

    private static String getMapParamsToStr(Map paramMap, String requestEncoding) throws IOException {
        StringBuffer params = new StringBuffer();
        // 设置边界
        for (Iterator iter = paramMap.entrySet().iterator(); iter.hasNext(); ) {
            Map.Entry element = (Map.Entry) iter.next();
            params.append(element.getKey().toString());
            params.append("=");
            params.append(URLEncoder.encode(element.getValue().toString(), requestEncoding));
            params.append("&");
        }

        if (params.length() > 0) {
            params = params.deleteCharAt(params.length() - 1);
        }

        return params.toString();
    }
    
    
    
    public static void posttest(HashMap<String, String> map){  //这里没有返回，也可以返回string

    	
    	String url = "http://192.141.7.55:8080/hyids/wbjk/webaddDoc";
        OkHttpClient mOkHttpClient = new OkHttpClient();

        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        Set<String> keySet = map.keySet();
        for(String key:keySet) {
            String value = map.get(key);
            formBodyBuilder.add(key,value);
        }
        FormBody formBody = formBodyBuilder.build();

        Request request = new Request
                .Builder()
                .post(formBody)
                .url(url)
                .build();

        try (Response response = mOkHttpClient.newCall(request).execute()) {
            System.out.println(response.body().string());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static String  documentZX(String data) throws Exception {
		
		JSONObject jsonObject = JSON.parseObject(data);
		
		
		String userName=jsonObject.getString("USERNAME");
		Map result = new HashMap();
		Map oPostData = new HashMap();
		String  dataArr =jsonObject.get("DATA").toString();//根据json对象中数组的名字解析出其所对应的值
		Map maps = (Map)JSON.parse(dataArr);  
		for (Object map : maps.entrySet()){  
			
			System.out.println(((Map.Entry)map).getKey()+"   =====推送默认数据=====   " + ((Map.Entry)map).getValue()); 
			if("fj".equals(((Map.Entry)map).getKey())){
				//处理附件
				Map fjV = (Map) ((Map.Entry)map).getValue(); 
				for (Object fjVfj : fjV.entrySet()){  
					oPostData.put(((Map.Entry)fjVfj).getKey(), ((Map.Entry)fjVfj).getValue());
				}
			}else{
				oPostData.put(((Map.Entry)map).getKey(), ((Map.Entry)map).getValue());	
			}
			
	    } 
		oPostData.put("CurrUserName", userName); // 当前操作的用户
		String sServiceId = "gov_webdocument";
		String sMethodName = "saveDocumentInWeb";
		System.out.println("调用海云之前的参数："+oPostData);
		String hy=null;
		try {
			hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("code", "-1");
			result.put("data", "调用新增接口失败！！！！！");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------开普推送文档结束---------------");
			return jsonResult.toString();
		}

		return hy;
	}
    
    /**
     * 处理默认附件 图片 方法
     */
    public static String PResource(String  pPath,String pName) {
    	

    	String PKPpath=null;
    	String urlKp=null;
    	try {
			PKPpath=uploadFile(pPath, pName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//处理返回结果
		JSONObject jsonObject = JSON.parseObject(PKPpath);
		Object dataArr = jsonObject.get("data");
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		String code=jsonObject.getString("code");
		if("0".equals(code)){
			urlKp = jsonObjectHYGPSE.getString("fileURL");
		}else{
			System.out.println("开普上传失败："+jsonObject.get("MSG"));	
		}
		
    	return urlKp;
    }
    
	/**
	 * 上传文件 视频 图片 
	 * @throws Exception
	 */
  public static String uploadFile(String path , String name) throws Exception {
	  
	  
      JSONObject  json = new JSONObject();
      File file = new File(path);
      FileInputStream fileInputStream = new FileInputStream(file);
      String encrypt = DESUtil.encrypt(json.toJSONString(), Z_APPID);
	  String url =Z_URL + "/resource/uploadFile?data=" + encrypt + "&appId="+Z_APPID;
      String s = HttpUtil.uploadFileByOkHttp(url,fileInputStream,name);

      System.out.println(s);
      return s;
  }
    
    
	/**
	 * 修改开普资源
	 * @throws Exception
	 */
	//  resTranMode=0
	public static String  updateRes0(JSONArray jsonArray,JSONObject json) throws Exception {
		  	
		  	json.put("attachments", jsonArray.toJSONString());//附件
		    
		    String url =Z_URL+ "/resource/updateRes";
		    System.out.println(url);
		    Map<String, String> map = new HashMap<String, String>();
		    map.put("appId",ZWBJ_APPID);
		        map.put("data", DESUtil.encrypt(json.toJSONString(),ZWBJ_STR_DEFAULT_KEY));
		    String post = HttpUtil.doPost(url, map,json);
		    System.out.println(post); 
		    return post;
	 }
    
}
