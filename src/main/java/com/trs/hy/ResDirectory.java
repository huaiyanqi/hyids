package com.trs.hy; /**
 * Created by ZM on 2018/9/26.
 */
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.HttpUtil;
import com.trs.kptohysdzc.DESUtil;
import com.trs.oauth.ConstantUtil;


import java.util.HashMap;
import java.util.Map;


/**
 * 开普栏目，站点调用
 * @author epro1
 *
 */
public class ResDirectory {
	
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Z_STR_DEFAULT_KEY = ConstantUtil.Z_STR_DEFAULT_KEY;
	
	
	/**
	 * 删除栏目
	 * @throws Exception
	 */
    public String delAllDirs(JSONObject json) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("appId",Z_APPID);//8ea32629da3e4c7cb33ee96f71913e25
//        JSONObject json = new JSONObject();
//        json.put("isOrig","1");
//        json.put("libId", "111");
//        json.put("dirIds", "e69b4a5891c343c9b7ea079b4712b035");
        String url = Z_URL+"/directory/delDirs";
        map.put("data", DESUtil.encrypt(json.toJSONString(), Z_STR_DEFAULT_KEY));
        String post = HttpUtil.doPost(url, map,json);
        System.out.println(post);
        return post;
    }
	
	/**
	 * 修改栏目
	 * @return 
	 * @throws Exception
	 */
    public String updateResDirectory(JSONObject json) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("appId", Z_APPID);
        String url = Z_URL+"/directory/updateDir";
        map.put("data", DESUtil.encrypt(json.toJSONString(), Z_STR_DEFAULT_KEY));
        String post = HttpUtil.doPost(url, map,json);
        System.out.println(post);
        return post;
    }
	/**
	 * 新增栏目接口
	 * @throws Exception
	 */
    public String addResDirectory(JSONObject json) throws Exception {
        
        String url =Z_URL+"/directory/createDir";
        System.out.println("新建目录："+url);
        Map<String, String> map = new HashMap<String, String>();
        map.put("appId", Z_APPID);//服务ID
        map.put("data", DESUtil.encrypt(json.toJSONString(), Z_STR_DEFAULT_KEY));
        String post = HttpUtil.doPost(url, map,json);
        System.out.println("开普新建返回：："+post);
//        JSONObject jsonKp = JSON.parseObject(post);
//		JSONArray kpLanmu=jsonKp.getJSONArray("data");
//		System.out.println(kpLanmu);
        return post;
    }
    
    /**
     * 1.2.6.【分页】根据资源库ID查询顶级目录
     * @throws Exception
     */
    public void getTopDirsByLibId() throws Exception {
        JSONObject json = new JSONObject();
//        json.put("isOrig","0");
//        json.put("libId", "d808ab76c01443f1b0cc515bbc2d2921");
//        json.put("isOrig","1");
        json.put("libId", "7ab67b77f20f4931a614372e4719ffe5");
        json.put("pageSize","111111");
        json.put("pageIndex","1");
        String encrypt = DESUtil.encrypt(json.toJSONString(), Z_STR_DEFAULT_KEY);

        String url = Z_URL+"/directory/getTopDirsByLibId?data=" + encrypt + "&appId="+Z_APPID;
        String get = HttpUtil.doGet(url, "UTF-8");
        System.out.println(get);

    }
    
    /**
     * 1.2.8.【分页】根据库ID查询所有目录
     * @throws Exception
     */
    public void getDirsByLibId() throws Exception {
        JSONObject json = new JSONObject();
//        json.put("isOrig","0");
//        json.put("libId", "d808ab76c01443f1b0cc515bbc2d2921");
//        json.put("isOrig","1");
        json.put("libId", "7ab67b77f20f4931a614372e4719ffe5");
        json.put("pageSize","111111");
        json.put("pageIndex","1");
        String encrypt = DESUtil.encrypt(json.toJSONString(), Z_STR_DEFAULT_KEY);

        String url = Z_URL+"/directory/getDirsByLibId?data=" + encrypt + "&appId="+Z_APPID;
        String get = HttpUtil.doGet(url, "UTF-8");
        System.out.println(get);

    }


 
    
    public void moveDirs() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("appId", "default");//8ea32629da3e4c7cb33ee96f71913e25
        JSONObject json = new JSONObject();
        json.put("isOrig","0");
        json.put("libId", "ca0f51dc0b6344fca1bd56cc9376132b");
        json.put("dirIds", "5da3d73d57104c4f80207e92c4b0bafd");
        json.put("toDirId", "1c904650a48b4e489b31f5c2b7e57659");
        String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/directory/moveDirs";
        map.put("data", DESUtil.encrypt(json.toJSONString(), "qwer1234"));
        String post = HttpUtil.doPost(url, map,json);
        System.out.println(post);
    }
    
    

    
//  1.2.5.	根据目录ID 查询目录

  public void getDirById() throws Exception {
      JSONObject json = new JSONObject();
      json.put("isOrig","1");
      json.put("dirId", "1c904650a48b4e489b31f5c2b7e57659");
      String encrypt = DESUtil.encrypt(json.toJSONString(), "qwer1234");

      String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/directory/getDirById?data=" + encrypt + "&appId=default";
      String get = HttpUtil.doGet(url, "UTF-8");
      System.out.println(get);

  }
  //原查询资源库顶级目录接口

  public void findResDirectorysByLibId() throws Exception {
      JSONObject json = new JSONObject();
      json.put("repId", "9ce10e446dfc48d78fb4043bdfa98f05");
      json.put("pageIndex", "1");
      json.put("pageSize", "3");
      String encrypt = DESUtil.encrypt(json.toJSONString(), "qwer1234");

      String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/directory/findResDirectorysByLibId?data=" + encrypt + "&strategyCode=default";
      String get = HttpUtil.doGet(url, "UTF-8");
      System.out.println(get);

  }
  
  
 

  public void getSubDirs() throws Exception {
      JSONObject json = new JSONObject();
      json.put("isOrig","1");
      json.put("dirId", "123421341234");
      json.put("pageSize","2");
      json.put("pageIndex","1");
      String encrypt = DESUtil.encrypt(json.toJSONString(), "qwer1234");

      String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/directory/getSubDirs?data=" + encrypt + "&appId=default";
      String get = HttpUtil.doGet(url, "UTF-8");
      System.out.println(get);

  }

  
	  public static void main(String[] args) {
		  ResDirectory aaa=new ResDirectory();
		  try {
			aaa.getTopDirsByLibId();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }

}
