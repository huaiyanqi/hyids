package com.trs.hy;



import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Address;
import com.trs.deletedoc.DeleteDocByView;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.GovPu;
import com.trs.mqadddoc.MQaddDocument;
import com.trs.mqupdatedoc.UpdateController;
import com.trs.oauth.ConstantUtil;

import allmq.MQConfig;
import allmq.MQFactory;
import allmq.ReceiveMsgChannel;
import allmq.rabbitmq.config.RabbitMQChannelConfig;
import allmq.rabbitmq.config.ServerConfig;


/**
 * 海云文档接收器---对应开普资源   新增  修改
 * @author epro1
 *
 */
public class HycloudResource implements MQReceiver {


	private static String  host=ConstantUtil.MQ_ADDRESSES;
	private static String  port=ConstantUtil.MQ_PORT;
//	private static Address[] addresses= ConstantUtil.MQ_ADDRESSES;
	private static String password = ConstantUtil.MQ_PASSWORD;
	private static String virtualHost = ConstantUtil.MQ_VIRTUALHOST;
	private static String username = ConstantUtil.MQ_USERNAME;
	
	private static String exchangeName= ConstantUtil.MQ_EXCHANGENAME;
	private static String exchangeType = ConstantUtil.MQ_EXCHANGETYPE;
	private static Boolean exchangeDurable =true;
	private static String routingKey= ConstantUtil.MQ_ROUTINGKEY;
	private static String queueName = ConstantUtil.MQ_QUEUENAME;
	private static String consumerTag = ConstantUtil.MQ_CONSUMERTAG;
	
	
	private static String pageSize = ConstantUtil.pageSize;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String resouse_path = ConstantUtil.resouse_path;
	ResResource  re= new ResResource();
	ChannelReceiver cR=new ChannelReceiver();
	HttpFileUpload hfl=new HttpFileUpload();
	JDBCIIP iip=new JDBCIIP();
	GovPu gp=new GovPu();
		/**
		 * 这个方法，需要有一个地方调用，最好是在这个中间件启动的过程中就被调用。相当于一个初始化一个MQ的接收端
		 * 里边儿需要的各种参数，你可以直接写死，或者用配置文件的方式都可以。看你具体怎么实现。
		 */
		public  void initMQClient (){
			
			try {
				String[] hosts;
				String[] ports;
				if (host.contains(";")){
					hosts = host.split(";");
					ports = port.split(";");
				} else {
					hosts = host.split(",");
					ports = port.split(",");
				}
				if (hosts.length != ports.length && ports.length != 1) {
					
				}
				Address[] addresses = new Address[hosts.length];
				for (int i = 0; i < hosts.length; i++) {
					Address address;
					if (ports.length == 1){
						address = new Address(hosts[i], Integer.parseInt(ports[0]));
					} else {
						address = new Address(hosts[i], Integer.parseInt(ports[i]));
					}
					addresses[i] = address;
				}
//				ServerConfig serverConfig = new ServerConfig(addresses, username, password, virtualHost);
//				MQFactory.initConnection(serverConfig);
//				Util.log("MQ元数据：新增，修改消费监听启动!","log",0);
//				System.out.println("MQ元数据：新增，修改消费监听启动!");
			} catch (Exception e) {
				  //这个地方记录一下日志，连接MQ服务器失败！  
				Util.log("MQ元数据：新增，修改连接MQ服务器失败:"+e,"log",0);
				System.out.println("MQ元数据：新增，修改连接MQ服务器失败"+e);
			}  
		}


		@Override
		public void doWork(String msg) {
			Util.log(msg,"MQDOClog",0);
			//todo 这个是你们自己实现的逻辑
			JSONObject jsonObject = JSON.parseObject(msg);
			Object dataArrHYSE = jsonObject.get("DATA");
			
			if("true".equals(jsonObject.get("ISSUCCESS"))){
				
				JSONObject jsonObjectHYName = JSON.parseObject(dataArrHYSE.toString());
				
				
				if(jsonObjectHYName.get("TYPE").toString().equals("51")||jsonObjectHYName.get("TYPE").toString().equals("52")){
					
					System.out.println("----------------------------------mq消息进入创建文档------------------------------");
					JSONObject jsondata = jsonObjectHYName.getJSONObject("DATA");
					//添加文档
					try {
						MQaddDocument.addDoc(jsondata);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}else if(jsonObjectHYName.get("TYPE").toString().equals("12")){
					
					JSONObject jsondata = jsonObjectHYName.getJSONObject("DATA");
					//修改文档
					try {
						UpdateController.upDocment(jsondata);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}else if(jsonObjectHYName.get("TYPE").toString().equals("71")){
					
					//删除文档
					try {
						DeleteDocByView.deleteByView(jsonObjectHYName);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else if(jsonObjectHYName.get("TYPE").toString().equals("53")){
					System.out.println("----------------------------------mq撤销文档消息进开始------------------------------");
					JSONObject jsondata = jsonObjectHYName.getJSONObject("DATA");
					//撤销发布  同步删除文档
					try {
						DeleteDocByView.delDoc(jsondata);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				
			}else{
				System.out.println("mq接口，文档操作失败!");
			}
			
		}

		@Override
		public void initReceiver() throws Exception {
			
//			MQConfig mqConfig = new RabbitMQChannelConfig(exchangeName, exchangeType, exchangeDurable, routingKey, queueName, consumerTag);
			MQConfig mqConfig = new RabbitMQChannelConfig("document", "topic", true, "#.document.*", "documentkp.gov", "gov");
			try{
//				ReceiveMsgChannel receiveChannel = MQFactory.createReceiveChannel(mqConfig);//获取消费者通道		
//				receiveChannel.onReceive(this);
//				System.out.println("初始化接收器成功");
			}catch(Exception e){
				throw new Exception("初始化接收器失败!");
			}
			                          
		
			
		}


	
}