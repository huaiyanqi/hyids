package com.trs.hy;



import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Address;
import com.trs.jdbc.JDBC;
import com.trs.kafka.Util;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;

import allmq.MQConfig;
import allmq.MQFactory;
import allmq.ReceiveMsgChannel;
import allmq.rabbitmq.config.RabbitMQChannelConfig;
import allmq.rabbitmq.config.ServerConfig;


/**
 * 海云栏目接收器---对应开普目录  新增 修改
 * @author epro1
 *
 */
public class HycloudChannelReceiver implements MQReceiver {


	private static String  host=ConstantUtil.MQ_ADDRESSES;
	private static String  port=ConstantUtil.MQ_PORT;
//	private static Address[] addresses= ConstantUtil.MQ_ADDRESSES;
	private static String password = ConstantUtil.MQ_PASSWORD;
	private static String virtualHost = ConstantUtil.MQ_VIRTUALHOST;
	private static String username = ConstantUtil.MQ_USERNAME;
	
	private static String exchangeName= ConstantUtil.MQ_EXCHANGENAME_L;
	private static String exchangeType = ConstantUtil.MQ_EXCHANGETYPE;
	private static Boolean exchangeDurable =true;
	private static String routingKey= ConstantUtil.MQ_ROUTINGKEY_L;
	private static String queueName = ConstantUtil.MQ_QUEUENAME;
	private static String consumerTag = ConstantUtil.MQ_CONSUMERTAG;
	
	private static String pageSize = ConstantUtil.pageSize;
	private static String USER = ConstantUtil.USER;
	
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String Z_ID = ConstantUtil.Z_ID;
	ChannelReceiver cR=new ChannelReceiver();
	Reslibrary  rl= new Reslibrary();
	GovPu gp=new GovPu();
	
		/**
		 * 这个方法，需要有一个地方调用，最好是在这个中间件启动的过程中就被调用。相当于一个初始化一个MQ的接收端
		 * 里边儿需要的各种参数，你可以直接写死，或者用配置文件的方式都可以。看你具体怎么实现。
		 */
		public  void initMQClient (){
			
			try {
				String[] hosts;
				String[] ports;
				if (host.contains(";")){
					hosts = host.split(";");
					ports = port.split(";");
				} else {
					hosts = host.split(",");
					ports = port.split(",");
				}
				if (hosts.length != ports.length && ports.length != 1) {
					
				}
				Address[] addresses = new Address[hosts.length];
				for (int i = 0; i < hosts.length; i++) {
					Address address;
					if (ports.length == 1){
						address = new Address(hosts[i], Integer.parseInt(ports[0]));
					} else {
						address = new Address(hosts[i], Integer.parseInt(ports[i]));
					}
					addresses[i] = address;
				}
//				ServerConfig serverConfig = new ServerConfig(addresses, username, password, virtualHost);
//				MQFactory.initConnection(serverConfig);
//				Util.log("MQ栏目消费监听启动!","log",0);
//				System.out.println("MQ栏目消费监听启动!");
			} catch (Exception e) {
				  //这个地方记录一下日志，连接MQ服务器失败！  
				Util.log("连接MQ服务器失败","log",0);
				System.out.println("连接MQ服务器失败"+e);
			}  
		}


		@Override
		public void doWork(String msg) {
			//todo 这个是你们自己实现的逻辑
			JSONObject jsonObject = JSON.parseObject(msg);
			Object dataArrHYSE = jsonObject.get("DATA");
			if("true".equals(jsonObject.get("ISSUCCESS"))){
				
				JSONObject jsonObjectHYName = JSON.parseObject(dataArrHYSE.toString());
				JSONObject jsondata = jsonObjectHYName.getJSONObject("DATA");
				Util.log("栏目mq：：："+msg,"addLMlog",0);
				if(jsonObjectHYName.get("TYPE").toString().equals("11")){
					Util.log("新建栏目jsondata：：："+jsondata,"addLMlog",0);
					//添加栏目
					gp.addChannelHY(jsondata);
					
				}else if(jsonObjectHYName.get("TYPE").toString().equals("12")){
					
					//修改栏目
					gp.upChannelHY(jsondata);
					
				}else if(jsonObjectHYName.get("TYPE").toString().equals("71")){
					
					//删除栏目
					gp.deChannelHY(jsondata);
					
				}
			}else{
				Util.log("栏目操作失败！","log",0);
				System.out.println("栏目操作失败！");
			}
			
			
		}



		@Override
		public void initReceiver() throws Exception {
			
//			MQConfig mqConfig = new RabbitMQChannelConfig(exchangeName, exchangeType, exchangeDurable, routingKey, queueName, consumerTag);
			MQConfig mqConfig = new RabbitMQChannelConfig("channel", "topic", true, "#.channel.*", "channelkp.gov", "gov");
			try{
//				ReceiveMsgChannel receiveChannel = MQFactory.createReceiveChannel(mqConfig);//获取消费者通道		
//				receiveChannel.onReceive(this);
//				Util.log("初始化接收器成功","log",0);
//				System.out.println("初始化接收器成功");
			}catch(Exception e){
				Util.log("初始化接收器失败!"+e,"log",0);
				throw new Exception("初始化接收器失败!");
			}
			                          
		
			
		}



	
}