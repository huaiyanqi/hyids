package com.trs.hy;

import com.alibaba.fastjson.JSONObject;
import com.trs.hy.HttpUtil;
import com.trs.kptohysdzc.DESUtil;
import com.trs.oauth.ConstantUtil;

import java.util.HashMap;
import java.util.Map;


/**
 * 开普资源库调用
 * @author epro1
 *
 */
public class Reslibrary {

//	@Before
//	public void setUp() throws Exception {
//		//map.put("strategyCode", "default");
//		json = new JSONObject();
//	}
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Z_STR_DEFAULT_KEY = ConstantUtil.Z_STR_DEFAULT_KEY;

	/**
	 * 开普云  资源库修改名称接口
	 * @throws Exception
	 */
	public  String updateLib(JSONObject json) throws Exception {
		
		String url ;
		Map<String, String> map = new HashMap<String, String>();
		url = Z_URL+"/library/updateLib";
//		map.put("data", DESUtil.encrypt(json.toJSONString(), "trsadmin2018"));
		map.put("appId",Z_APPID);
		map.put("data", DESUtil.encrypt(json.toJSONString(), Z_APPID));
		String post = HttpUtil.doPost(url, map,json);
		System.out.println("站点修改返回结果"+post);
		return post;
		
	}
	
	/**
	 * 根据应用 ID 查询所有授权资源库
	 * Z_APPID  =  trsadmin
	 * @throws Exception
	 */
	public String getLibsByStrategyCode() throws Exception {
		JSONObject json = new JSONObject();
		String url ;
		json.put("appId", Z_APPID);
		String encrypt = DESUtil.encrypt(json.toJSONString(), Z_APPID);
		url = Z_URL+"/library/getLibsByAppId?data=" + encrypt + "&appId="+Z_APPID;
		String get = HttpUtil.doGet(url, "UTF-8");

		System.out.println("开普查询所有站点返回结果:"+get);
		return get;
	}
	

	/**
	 * 根据资源库 ID 查询资源库
	 * @throws Exception
	 */
	public void getLibById() throws Exception {
		JSONObject json = new JSONObject();
		String url ;
		Map<String, String> map = new HashMap<String, String>();
		json.put("libId", "7ab67b77f20f4931a614372e4719ffe5");
		System.out.println(json.toJSONString());
		String encrypt = DESUtil.encrypt(json.toJSONString(), "trsadmin");
		url = Z_URL+"/library/getLibById?data=" + encrypt + "&appId=trsadmin";
		String get = HttpUtil.doGet(url, "UTF-8");
		System.out.println(get);
	}
	
	
	public static void main(String[] args) {
		Reslibrary aaa=new Reslibrary();
		try {
			aaa.getLibsByStrategyCode();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
