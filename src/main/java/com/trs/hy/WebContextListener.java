package com.trs.hy;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class WebContextListener implements ServletContextListener{

    @Override 
    public void contextInitialized(ServletContextEvent arg0) { 
        // TODO Auto-generated method stub 
        //这里可以放你要执行的代码或方法
    	//开启站点监听
    	try {
    	HycloudNodesReceiver hyc=new HycloudNodesReceiver();
    	hyc.initMQClient();
    	hyc.initReceiver();
	
    	//开启栏目监听
    	HycloudChannelReceiver hyccr=new HycloudChannelReceiver();
    	hyccr.initMQClient();
    	hyccr.initReceiver();
    	//开启文档监听
    	
    	HycloudResource hyr=new HycloudResource();
    	hyr.initMQClient();
    	hyr.initReceiver();
    	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    } 
       
    @Override 
    public void contextDestroyed(ServletContextEvent arg0) { 
        // TODO Auto-generated method stub 
           
    } 
}
