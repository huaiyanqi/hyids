package com.trs.hy;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * 开普文档相关调用
 * @author epro1
 *
 */
public class ResResource {
	
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.ZWBJ_APPID;
	private static String Z_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	
	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String USER = ConstantUtil.USER;
	static HttpFileUpload hfl=new HttpFileUpload();
	
	/**
	 * 处理正文中的视频  regVideo = "(<iframe).*?(/iframe)"
	 * @param content
	 * @param sub
	 * @param regular
	 * @return
	 * @throws IOException 
	 */
    @SuppressWarnings("static-access")
	public String contentResourceVideo(String content,int sub,String regular,String videoName) throws IOException {
    	 String regVideoSrc = "(<img class=\"trsresize\" type=\"personal\").*?(.jpg)";
    	String regVideo = regular;//正文中视频
		List<String> list=getContentResource(regVideo, sub, content);
		for(int i = 0 ; i < list.size() ; i++) {
			System.out.println(list.get(i));
			
			//拼接海云资源路径
			String iframContent=list.get(i);
			List<String> listurl=getContentResource(regVideoSrc, 5, iframContent);
			if(listurl.size()!= 0){
				String urlPtah="";
				for(int k = 0 ; k < listurl.size() ; k++) {
					urlPtah=listurl.get(k);
				}
				//上传之前先下载
				hfl.downLoadFromUrl(urlPtah,videoName+"_"+i,resouse_path);
				System.out.println("视频名称："+videoName+"_"+i);
				//调用上传接口上传到开普
				String hyReturn=null;
				try {
					hyReturn=uploadFile(resouse_path, videoName+"_"+i);
					//处理返回结果
					JSONObject jsonObject = JSON.parseObject(hyReturn);
					Object dataArrHYSave = jsonObject.get("code");
					JSONArray dataArr = jsonObject.getJSONArray("data");
					if(dataArrHYSave.equals("0")){
						String urlKp=null;
						for (int j = 0; j < dataArr.size(); j++) {
							JSONObject dataBean = (JSONObject) dataArr.get(j);
							// 拼接  开普上传后路径
							urlKp = Kp_conten_url+dataBean.getString("fileURL");
						}
						//替换正文中的海云路径
						String kpUrl="video controls='controls' loop='loop' width='480' height='400' src='"
								+ urlKp + " autoplay='autoplay'></video";
						content=content.replace(list.get(i), urlKp);
						System.out.println("旧路径："+list.get(i)+"------新路径："+kpUrl);
					}else{
						System.out.println("正文视频处理开普上传失败："+jsonObject.get("msg"));	
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				System.out.println("正文中不存在视频："+iframContent);
			}
			
		}
    	return content;
    }
	
	/**
	 * 删除开普资源
	 * @throws Exception
	 */
  public String  deleRes(JSONObject json) throws Exception {
	    
	    String encrypt = DESUtil.encrypt(json.toJSONString(), Z_STR_DEFAULT_KEY);
	    String url =Z_URL+ "/resource/delRes?data=" + encrypt + "&appId="+Z_APPID;
	    String post = HttpUtil.doGet(url, "UTF-8");
	    System.out.println(post); 
	    return post;
  }
	
	
	/**
	 * 修改开普资源
	 * @throws Exception
	 */
//  resTranMode=0
  public String  updateRes0(JSONArray jsonArray,JSONObject json) throws Exception {
	  	
	  	json.put("attachments", jsonArray.toJSONString());//附件
	    
	    String url =Z_URL+ "/resource/updateRes";
	    System.out.println(url);
	    Map<String, String> map = new HashMap<String, String>();
	    map.put("appId", Z_APPID);
	        map.put("data", DESUtil.encrypt(json.toJSONString(),Z_STR_DEFAULT_KEY));
	    String post = HttpUtil.doPost(url, map,json);
	    System.out.println(post); 
	    return post;
  }
	
	   /**
	    * 新增资源
	    * @param jsonArray
	    * @param json
	    * @throws Exception
	    */
	// resTranMode=2 
	    public String  createRes2(JSONArray jsonArray,JSONObject json ) throws Exception {
		    
		    json.put("attachments", jsonArray.toJSONString());//附件
		    
		    String url =Z_URL+ "/resource/createRes";
		    Map<String, String> map = new HashMap<String, String>();
		    System.out.println("新建文档内容json："+json);
		    System.out.println("新建文档内容url："+url);
		    map.put("appId", Z_APPID);
		    map.put("data", DESUtil.encrypt(json.toJSONString(),Z_STR_DEFAULT_KEY));
		    System.out.println("map"+map);
		    String post = HttpUtil.doPost(url, map,json);
		    System.out.println(post); 
		    return post;
	} 
	
	/**
	 * 获取正文内资源
	 * List<String> list = new ArrayList<String>();
	 */
	public static List<String>  getContentResource(String regExImg,int subStart,String content){
		List<String> list = new ArrayList<String>();
		Pattern patternImg = Pattern.compile(regExImg);
		Matcher matcherImg = patternImg.matcher(content);
		while (matcherImg.find()) {
			String picPath = matcherImg.group();
			if(picPath.contains("protect")||picPath.contains("webpic")){
				picPath = picPath.substring(subStart, picPath.length()-1);
//				String fullPath = getFullPath(picPath);
				System.out.println("匹配到的路径："+picPath);
				list.add(picPath);
			}

		}
		List<String> remove=removeDuplicate(list);
		return remove;
	}
	
	/**
	 * 去除重复数据
	 */
	public static List<String> removeDuplicate(List<String> list) {   
	    HashSet<String> h = new HashSet<String>(list);   
	    list.clear();   
	    list.addAll(h);   
	    return list;   
	}
	
	
	
//  1.3.9.	上传文件（只上传文件，不会转为资源对象） 
	/**
	 * 上传文件 视频 图片 
	 * @throws Exception
	 */
  public static String uploadFile(String path , String name) throws Exception {
	  
//	    JSONObject  json = new JSONObject();
//	      File file = new File("D:\\reText\\00-11.txt");
//	      FileInputStream fileInputStream = new FileInputStream(file);
//	      String encrypt = DESUtil.encrypt(json.toJSONString(), "trsadmin");
//	      String url = "http://jyhgl.beijing.gov.cn:7002/zuul/repo-api/api/res/resource/uploadFile?data=" + encrypt + "&appId=trsadmin";
//	      String s = HttpUtil.uploadFileByOkHttp(url,fileInputStream,"00-11.txt");
//	      System.out.println(s);
	  
	  
      JSONObject  json = new JSONObject();
      //  "F:\\cms最新接口升级文件.zip"
      File file = new File(path);
      FileInputStream fileInputStream = new FileInputStream(file);
      String encrypt = DESUtil.encrypt(json.toJSONString(), Z_STR_DEFAULT_KEY);
	  String url =Z_URL + "/resource/uploadFile?data=" + encrypt + "&appId="+Z_APPID;
	  //  cms最新接口升级文件.zip
//      String s = HttpUtil.uploadFile(url,fileInputStream,name);
      String s = HttpUtil.uploadFileByOkHttp(url,fileInputStream,name);

      System.out.println(s);
      return s;
  }
	
	
// resTranMode=0
	/**
	 * 新增  资源
	 * @throws Exception
	 */
   public void createRes0() throws Exception {
     JSONObject json = new JSONObject();
     json.put("isOrig", "0");//
     json.put("name", "创建资源测试接口0");//资源名称
     json.put("dirId", "1c904650a48b4e489b31f5c2b7e57659");//目录的id
//     json.put("origId", "adsfqertrhg23233434wqeqwer4jghjsddfadsfad");// 原始资源id 不知道干嘛的
     json.put("title", "测试创建资源测试接口是不是好用标题"); //标题
     json.put("abstract", "摘要测试"); //摘要
     json.put("author", "张三wewewe");// 作者
     json.put("source", "资源引用测试");//来源
//     json.put("resClassify", "18");//资源分类     海云没有该字段
     json.put("resTranMode", "0");//0：文本资源-字符串传输 1：文件资源-文件传输 2：url资源地址-接口内部下载   使用 2   1为直接上传文件  zip    
     json.put("content", "http://gks.mof.gov.cn/zhengfucaigouguanli/201808/P020180810368961610728.pdf");//
//     json.put("extdata", "232323");//没有这个 字段
     JSONArray jsonArray = new JSONArray();
     JSONObject object = new JSONObject();
     object.put("name", ""); // 附件名称
     object.put("path", "group1/M00/00/3B/wKgBxFtxbuKAHI3dAAGXRzq5fkw53911111.jpg"); // 图片地址  调用上传接口返回的 url
     
     jsonArray.add(object);
     
     json.put("filePath", jsonArray.toJSONString());//fujian
     String data = DESUtil.encrypt(json.toJSONString(),  Z_APPID);
     Map<String, String> map = new HashMap<>();
     map.put("appId",  Z_APPID);//策略
     map.put("data", data);
    String url = Z_URL+"/resource/createRes";
    String post = HttpUtil.doPost(url, map,json);
    System.out.println(post);
    System.out.println(data);
}     
 
   
// resTranMode=1
    public void createRes1() throws Exception {
       JSONObject json = new JSONObject();
       json.put("name", "w资源123");
       json.put("resTranMode", "1"); //json.put("resTranMode", "2 ");
       json.put("content", "测试接口是否好用");
       json.put("title", "w资源123");
       json.put("resClassify", "6");
       json.put("abstract", "进出口商品检验法");
       json.put("source", "中国政府网");
       json.put("author", "政府网编辑部");
       json.put("lj_url", "http://www.chinalaw.gov.cn/art/2018/9/28/art_43_209277.html");//项目属性
       json.put("isOrig", "0");
       json.put("dirId", "1c904650a48b4e489b31f5c2b7e57659");//wjq测试目录1
//       File file = new File("F:\\a.png");
       File file = new File("F:\\cms最新接口升级文件.zip");
       FileInputStream fileInputStream = new FileInputStream(file);
       String data = DESUtil.encrypt(json.toJSONString(), "qwer1234");
       String url="http://192.168.1.181:7002/zuul/repo-api/api/res/resource/createRes?appId=default&data="+data;
       String post = HttpUtil.uploadFile(url,fileInputStream,"cms最新接口升级文件.zip");
       System.out.println(post);
//       System.out.println(data);
    }
   
   



// resTranMode=1    执行不成功
 public void updateRes1() throws Exception {
     JSONObject json = new JSONObject();
     json.put("name", "w资源123");
     json.put("resTranMode", "1"); //json.put("resTranMode", "2 ");
     json.put("content", "测试接口是否好用");
     json.put("title", "w资源123");
     json.put("resClassify", "6");
     json.put("abstract", "进出口商品检验法");
     json.put("source", "中国政府网");
     json.put("author", "政府网编辑部");
     json.put("lj_url", "http://www.chinalaw.gov.cn/art_11_208523.html");//项目属性
     json.put("isOrig", "0");
     json.put("dirId", "1c904650a48b4e489b31f5c2b7e57659");//wjq测试目录1
     File file = new File("F:\\a.png");
     FileInputStream fileInputStream = new FileInputStream(file);
     String data = DESUtil.encrypt(json.toJSONString(), "qwer1234");
     String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/resource/updateRes";
     String post = HttpUtil.uploadFile(url,fileInputStream,"a1.png");
     System.out.println(post);
     System.out.println(data);

 }   
   
// resTranMode=2
    public void updateRes2() throws Exception {
       	JSONObject json = new JSONObject();
        json.put("resId","5badd9c38a3529921f13e78a");
        json.put("name", "resTranMode=2修改资源测试接口");//目录名称
        json.put("resTranMode", "2");
        json.put("content", "http://www.gov.cn");
        json.put("title", "中华人民共和国进出口商品检验法"); 
        json.put("resourceClassify", "6"); 
        json.put("abstract", "进出口商品检验法"); 
        json.put("source", "中国政府网"); 
        json.put("author", "政府网编辑部");
        json.put("isOrig", "0");
        json.put("dirId", "1c904650a48b4e489b31f5c2b7e57659");//wjq测试目录1
	    JSONArray jsonArray = new JSONArray();
	    JSONObject fpCon= new JSONObject();
	    fpCon.put("name", "政策性文件附件");
	    fpCon.put("path", "/group1/M00/02/08/oYYBAFuhqwKAcg7kAAmDUYVni5Q869.pdf");
	    jsonArray.add(fpCon);
	    json.put("attachments", jsonArray.toJSONString());//附件
	    String url = "http://192.168.1.181:7002/repo-api/api/res/resource/updateRes";
	    Map<String, String> map = new HashMap<String, String>();
	    map.put("appId", "default");
	        map.put("data", DESUtil.encrypt(json.toJSONString(),"qwer1234"));
	    String post = HttpUtil.doPost(url, map,json);
	    System.out.println(post); 
    }

    
    public void moveRes() throws Exception {
        JSONObject json = new JSONObject();
        json.put("isOrig", "0");
        json.put("resIds", "5bade709a25b7d117d75f375");
        json.put("toDirId", "1c904650a48b4e489b31f5c2b7e57659");
        String data = DESUtil.encrypt(json.toString(), "qwer1234");
        String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/resource/moveRes";
        Map<String, String> map = new HashMap<>();
        map.put("appId", "default");
        map.put("data", data);
        String post = HttpUtil.doPost(url, map,json);
        System.out.println(post); 
        System.out.println(data);                    
    }

    public void delRes() throws Exception {
//   ok
       	
        JSONObject json = new JSONObject();
      json.put("isOrig", "0");
      json.put("resIds", "5bade709a25b7d117d75f375");
      String data = DESUtil.encrypt(json.toString(), "qwer1234");
//      System.out.println(data);
      String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/resource/delRes?data=" + data + "&appId=default";
      String get = HttpUtil.doGet(url, "UTF-8");
      System.out.println(get);
    }

    public void getResById() throws Exception {
        JSONObject json = new JSONObject();
        json.put("isOrig", "1");
        json.put("resId", "3333333333333333333");
        String data = DESUtil.encrypt(json.toString(), "default");
        System.out.println(data);
    }

    public void getResByDirId() throws Exception {
        JSONObject json = new JSONObject();
        json.put("isOrig", "1");
        json.put("pageIndex", "0");
        json.put("pageSize", "10");
        json.put("dirId", "2222-222,1111-111");
        String data = DESUtil.encrypt(json.toString(), "default");
        String url = "http://192.168.2.180:7002/zuul/repo-api/api/res/resource/getResByDirId?data=" + data + "&appId=default";
        String get = HttpUtil.doGet(url, "UTF-8");
        System.out.println(get);
    }

    public void getResByLibId() throws Exception {
        JSONObject json = new JSONObject();
        json.put("isOrig", "1");
        json.put("pageIndex", "0");
        json.put("pageSize", "2");
        json.put("libId", "9ce10e446dfc48d78fb4043bdfa98f05");
        String data = DESUtil.encrypt(json.toString(), "default");
        String url = "http://192.168.2.55:7002/zuul/repo-api/api/res/resource/getResByLibId?data=" + data + "&appId=default";
        String get = HttpUtil.doGet(url, "UTF-8");
        System.out.println(get);
    }

    public void getResByConditions() throws Exception {
//        JSONObject json = new JSONObject();
//        json.put("isOrig", "1");
//        json.put("pageIndex", "1");
//        json.put("pageSize", "2");
//        json.put("appId", "default");//策略
//            JSONObject object = new JSONObject();
//            object.put("relevance","or");
//                JSONArray jsonArray = new JSONArray();
//                    JSONObject arrayObj = new JSONObject();
//                    arrayObj.put("key","title");
//                    arrayObj.put("mode","1");
//                    arrayObj.put("val","李克强");
//
//                    JSONObject arrayObj1 = new JSONObject();
//                    arrayObj1.put("key","keyword");
//                    arrayObj1.put("mode","1");
//                    arrayObj1.put("val","国务院");
//
//                    JSONObject arrayObj2 = new JSONObject();
//                    arrayObj2.put("key","ysj");
//                    arrayObj2.put("mode","1");
//                    arrayObj2.put("val","郭老板");
//
//                    JSONObject arrayObj3 = new JSONObject();
//                    arrayObj3.put("key","dirId");
//                    arrayObj3.put("mode","1");
//                    arrayObj3.put("val","111111111111,22222222222222222");
//                jsonArray.add(arrayObj);
////                jsonArray.add(arrayObj1);
////                jsonArray.add(arrayObj2);
////                jsonArray.add(arrayObj3);
//            object.put("data",jsonArray);
//        json.put("conditions",object);
//        String data = DESUtil.encrypt(json.toString(), "qwer1234");
//
////        String data = DESUtil.encrypt("{\"pageIndex\":1,\"pageSize\":\"10\",\"isOrig\":\"0\",\"conditions\":{\"relevance\":\"or\",\"data\":[{\"key\":\"name\",\"mode\":\"3\",\"val\":\"测试\"},{\"key\":\"title\",\"mode\":\"3\",\"val\":\"管理\"},{\"key\":\"content\",\"mode\":\"3\",\"val\":\"顶级\"},{\"key\":\"author\",\"mode\":\"3\",\"val\":\"顶级\"},{\"key\":\"dirId\",\"mode\":\"4\",\"val\":\"\"},{\"key\":\"libId\",\"mode\":\"4\",\"val\":\"\"},{\"key\":\"type\",\"mode\":\"3\",\"val\":\"\"},{\"key\":\"resClassify\",\"mode\":\"3\",\"val\":\"\"}]}}", "default");
//        String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/resource/getResByConditions?data=" + data + "&appId=default";
//        String get = HttpUtil.doGet(url, "UTF-8");
//        System.out.println(get);
    	
    	JSONObject json = new JSONObject();
        json.put("isOrig", "0");
        json.put("pageIndex", "1");
        json.put("pageSize", "100");
        json.put("appId", "default");//策略
            JSONObject object = new JSONObject();
            object.put("relevance","or");
                JSONArray jsonArray = new JSONArray();
                    JSONObject arrayObj = new JSONObject();
                    arrayObj.put("key","title");
                    arrayObj.put("mode","1");
                    arrayObj.put("val","测试");
//                    JSONObject arrayObj3 = new JSONObject();
//                    arrayObj3.put("key","dirId");
//                    arrayObj3.put("mode","2");
//                    arrayObj3.put("val","6d1d5b3274374f6b8ff9e2669678dab7");
                jsonArray.add(arrayObj);
//                jsonArray.add(arrayObj3);
            object.put("data",jsonArray);
        json.put("conditions",object);
        String encrypt = DESUtil.encrypt(json.toJSONString(), "qwer1234");
        String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/resource/getResByConditions?data=" + encrypt + "&appId=default";
        String get = HttpUtil.doGet(url, "UTF-8");
        System.out.println(get);
    	
    }

    public void uploadRes() throws Exception {
        JSONObject json = new JSONObject();
        json.put("isOrig", "1");
        json.put("dirId", "555555555555555");
        json.put("origId", "343434343434343");
        json.put("name1", "张三");//项目属性
        json.put("name2", "李四");//项目属性
        json.put("lj_url", "http://www.chinalaw.gov.cn /art_11_208523.html");//项目属性
        json.put("bj_url ", " http://zfxxgk.beijing.gov.cn ");//项目属性
        json.put("gov_url ", " http://www.gov.cn");//项目属性
        String data = DESUtil.encrypt(json.toString(), "default");
        System.out.println(data);

    }


////    1.3.10.	下载文件
//    public void downloadFile() throws Exception {
////        JSONObject json = new JSONObject();
////        json.put("fileURL", "group1/M00/00/3B/wKgBxFtzx4OAClRzAACZRb--c-M591.jpg");
////        String encrypt = DESUtil.encrypt(json.toJSONString(), "qwer1234");
////        System.out.println(encrypt);
//	    JSONObject json = new JSONObject();
//	    json.put("fileURL", "group1/M00/02/0A/oYYBAFuknTKAI2FeAAAjLSt5gjo565.txt");
//	    String encrypt = DESUtil.encrypt(json.toJSONString(), "qwer1234");
//	    String url = "http://192.168.1.181:7002/zuul/repo-api/api/res/resource/downloadFile?data=" + encrypt + "&appId=default";
//		byte[] data = HttpUtil.downloadFile(url);
//		String str =new String(data);
//	    System.out.println(str);
//    }

	/**
	 * 处理正文方法
	 * @param content
	 * @param sub
	 * @param regular
	 * @return
	 * @throws IOException 
	 */
    @SuppressWarnings("static-access")
	public static String contentResource(String content,int sub,String regular,String bt,String CHANNELID,String sitename,String SITEID) throws IOException {
    	String regExImg = regular;//正文中图片 
		List<String> list=getContentResource(regExImg, sub, content);
		for(int i = 0 ; i < list.size() ; i++) {
			System.out.println("正文附件路径：：：：：："+list.get(i));
			if(list.get(i).contains("protect")||list.get(i).contains("webpic")){

				//调用上传接口上传到开普
				//拼接海云资源路径
				String urlPtah=HY_URL+list.get(i);
				//获取名称
				String name=list.get(i).substring(list.get(i).lastIndexOf("/")+1,list.get(i).length());
		    	//处理附件名称带特殊字符
				name=zfwzUtil.filterSpecialChar(name);
				//判断附件名称是不是符合海云规则 也可能存在 包含  W P 得 其他附件 概率比较低
				if(name.contains("W")||name.contains("P")){
					HttpFileUpload.downLoadFromUrl(urlPtah.replace("\"", ""),name.replace("\"", ""),resouse_path);
					String hyReturn=null;
					try {
						hyReturn=uploadFile(resouse_path+name.replace("\"", ""), name.replace("\"", ""));
						//处理返回结果
						JSONObject jsonObject = JSON.parseObject(hyReturn);
						String dataArrHYSave =jsonObject.getString("code");
						Object dataArr = jsonObject.get("data");
						JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
						if("0".equals(dataArrHYSave)){
							String urlKp = Kp_conten_url+jsonObjectHYGPSE.getString("fileURL");
							if(regular.indexOf("src")>=0){
								String bq="src";
								String oldUrl=""+bq+"=\""+list.get(i)+"\"";
								String newUrl=""+bq+"=\""+urlKp+"\"";
								System.out.println("海云地址："+oldUrl);
								System.out.println("海云地址："+newUrl);
								content=content.replace(oldUrl,newUrl);
								System.out.println("替换后得正文：：：：：：：：：：：：：：："+content);
							}else{
								String bq="href";
								String oldUrl=""+bq+"=\""+list.get(i)+"\"";
								String newUrl=""+bq+"=\""+urlKp+"\"";
								System.out.println("海云地址："+oldUrl);
								System.out.println("海云地址："+newUrl);
								content=content.replace(oldUrl,newUrl);
								System.out.println("替换后得正文：：：：：：：：：：：：：：："+content);
							}
						}else{
							Util.log("文章名称："+bt,"erro"+sitename+SITEID,0);
							Util.log("栏目id："+CHANNELID,"erro"+sitename+SITEID,0);
							Util.log("错误信息：正文附件上传错误---"+jsonObject.get("msg"),"erro"+sitename+SITEID,0);
							Util.log("=============================================================================","erro"+sitename+SITEID,0);
							System.out.println("开普上传失败："+jsonObject.get("msg"));	
						}
						
						
						
					} catch (Exception e) {
						Util.log("文章名称："+bt,"erro"+sitename+SITEID,0);
						Util.log("栏目id："+CHANNELID,"erro"+sitename+SITEID,0);
						Util.log("错误信息：正文附件上传错误---"+e,"erro"+sitename+SITEID,0);
						Util.log("=============================================================================","erro"+sitename+SITEID,0);
						System.out.println("开普附件上传报错！！！！！！！");
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					
					Util.log("文章名称："+bt,"正文附件错误=="+sitename+SITEID,0);
					Util.log("栏目id："+CHANNELID,"正文附件错误=="+sitename+SITEID,0);
					Util.log("正文附件错误，不是海云附件地址："+list.get(i),"正文附件错误=="+sitename+SITEID,0);
					Util.log("=============================================================================","正文附件错误=="+sitename+SITEID,0);
					
				}
			
			}else{
				Util.log("文章名称："+bt,"erro"+sitename+SITEID,0);
				Util.log("栏目id："+CHANNELID,"erro"+sitename+SITEID,0);
				Util.log("错误信息：正文附件上传错误---附件不是海运中附件地址","erro"+sitename+SITEID,0);
				Util.log("=============================================================================","erro"+sitename+SITEID,0);
			}
		
		}
    	return content;
    }
    
    /**
     * 处理默认附件 图片 方法
     */
    public String PResource(String  pPath,String pName) {
    	
//    	
//    	//根据名称查询出图片下载的路径
//	    //调用海云接口查询文档详细数据
//		String sServiceIdP="gov_appendix";
//		String sMethodNameP="getAppendixUrl";
//		Map savemapP = new HashMap();
//		savemapP.put("FILENAME",pName);
//		savemapP.put("CurrUserName", USER); // 当前操作的用户
//		String hyP=null;
//		try {
//			hyP = HyUtil.dataMoveDocumentHyRbj(sServiceIdP,sMethodNameP,savemapP);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	System.out.println("开普上传附件接口pPath："+pPath);
    	System.out.println("开普上传附件接口pName："+pName);
    	String PKPpath=null;
    	String urlKp=null;
    	try {
			PKPpath=uploadFile(pPath, pName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//处理返回结果
		JSONObject jsonObject = JSON.parseObject(PKPpath);
		Object dataArr = jsonObject.get("data");
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		String code=jsonObject.getString("code");
		if("0".equals(code)){
			urlKp = jsonObjectHYGPSE.getString("fileURL");
		}else{
			System.out.println("开普上传失败："+jsonObject.get("MSG"));	
		}
		
    	return urlKp;
    }
    
    public String videoUrl(String id){
    	
    	
		String sServiceId="gov_mas";
		String sMethodName="getDownLoadVideoURL";
		Map savemap = new HashMap();
		savemap.put("videoIds",id);
//		savemap.put("CurrUserName", USER); // 当前操作的用户
		
		String hy11=null;
		try {
			hy11 = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String url=null;
		JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
		Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
		
    	return null;
    }
    
    /**
	 * 处理正文方法
	 * @param content
	 * @param sub
	 * @param regular
	 * @return
	 * @throws IOException 
	 */
    @SuppressWarnings("static-access")
	public String contentResourceHy(String content,int sub,String regular) throws IOException {
    	String regExImg = regular;//正文中图片 
		List<String> list=getContentResource(regExImg, sub, content);
		for(int i = 0 ; i < list.size() ; i++) {
			System.out.println(list.get(i));
			
			//开普资源路径
			String urlPtah=list.get(i);
			
			//获取名称
			String name=urlPtah.substring(urlPtah.lastIndexOf("/", urlPtah.length()));

			//上传之前先下载
			hfl.downLoadFromUrl(urlPtah,name,resouse_path);
			
			//调用海云上传接口
			
		
			
			
		}
    	return content;
    }
    
    public static void main(String[] args) {
    	String regVideoSrc = "(src=).*?(autoPlay=false)";
    	String iframContent="iframe width='1200'  src='http://ybj.beijing.gov.cn/jdjb/postNew.htm'></iframe";
    	ResResource aaa=new ResResource();
		List<String> listurl=aaa.getContentResource(regVideoSrc, 5, iframContent);
		System.out.println(listurl.size()+"----------------------"+listurl);
		String urlPtah =null;
		for(int k = 0 ; k < listurl.size() ; k++) {
			 urlPtah=listurl.get(k);
		}
		System.out.println(urlPtah);
	}
}