package com.trs.kptohy;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.HyUtil;
import com.trs.jdbc.JDBCIDS;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.AddDocToHY;
import com.trs.kptohysdzc.DESUtil;
import com.trs.kptohysdzc.PutZCWJ;
import com.trs.kptohyzfwz.AddDocZFWZ;
import com.trs.kptohyzfwz.FileUplodeZFWZ;
import com.trs.kptohyzfwz.PutCZZFWZ;
import com.trs.kptohyzfwz.PutGHZFWZ;
import com.trs.kptohyzfwz.PutHDJLYJZJZFWZ;
import com.trs.kptohyzfwz.PutHDJLZXDCZFWZ;
import com.trs.kptohyzfwz.PutHDJLZXFTZFWZ;
import com.trs.kptohyzfwz.PutHDJLZXJYTSZFWZ;
import com.trs.kptohyzfwz.PutHYZFWZ;
import com.trs.kptohyzfwz.PutJGGS;
import com.trs.kptohyzfwz.PutLDJLZFWZ;
import com.trs.kptohyzfwz.PutRQGSZFWZ;
import com.trs.kptohyzfwz.PutRSRMZFWZ;
import com.trs.kptohyzfwz.PutZCJDZFWZ;
import com.trs.kptohyzfwz.PutZCWJZFWZ;
import com.trs.kptohyzfwz.PutZFGBZFWZ;
import com.trs.kptohyzfwz.PutZXZFWZ;
import com.trs.kptohyzfwz.ZWuplod;
import com.trs.oauth.ConstantUtil;

public class putDocumentZFWZ {
	
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String pageSize = ConstantUtil.pageSize;
	
	static JDBCIDS  jdbcids=new JDBCIDS();

	/*
	 * 自定义视图同步数据
	 */
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public static String  documentZFWZ(String data) throws Exception {
		
		String jsonData=DESUtil.decrypt(data, ZWBJ_STR_DEFAULT_KEY);
		Map result = new HashMap();
		Map oPostData = new HashMap();
		JSONObject jsondata = JSON.parseObject(jsonData);
		
		if(data != null){
			//处理默认字段正文，摘要，标题，附件
			String channelId=jsondata.getString("channelId");//栏目id
			String resID=jsondata.getString("resId");//文档资源id
			String abstracts=jsondata.getString("abstracts");//摘要
			String name=jsondata.getString("name");//名称 政策文件的文件名称
			String resTitle=jsondata.getString("resTitle");//文件名称  政策解读的文件名称
			String author=jsondata.getString("author");//作者
			String content=jsondata.getString("content");//正文
			String source=jsondata.getString("source");//来源
			String pubDate=jsondata.getString("pubDate");//撰写时间
			String type=jsondata.getString("type");//1图片类型；2网页类型;3音频类型；4视频类型;5文档类型
			System.out.println("获取到的栏目id：：：：：：：：：：：："+channelId);
			System.out.println("获取到的栏目name：：：：：：：：：：：："+name);
			//根据栏目id查询到视图
			String sServiceIdView="gov_site";
			String sMethodNameView="whetherOpenData";
			Map<String, String> savemapView = new HashMap();
			savemapView.put("ChannelID",channelId);
			savemapView.put("CurrUserName", USER); // 当前操作的用户
			String hyLMView = HyUtil.dataMoveDocumentHyRbj(sServiceIdView,sMethodNameView,savemapView);
			JSONObject jsonObjectHYSEView = JSON.parseObject(hyLMView);
			Object dataArrView = jsonObjectHYSEView.get("DATA");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSEView = JSON.parseObject(dataArrView.toString());
			Util.log("政府网站：开普推送数据到海云查询视图短名返回结果=================："+hyLMView,"kplog",0);
			System.out.println("查询栏目视图结果"+hyLMView);
			System.out.println("政府网站：开普推送数据到海云查询视图短名返回结果=================："+jsonObjectHYGPSEView);
			
			//视图id
			String viewid =jsonObjectHYGPSEView.getString("VIEWID");
			System.out.println("viewid:"+viewid);
			System.out.println("viewid:"+viewid.isEmpty());
			System.out.println("viewid:"+viewid.equals(""));
			if(viewid==null||viewid.equals("")||viewid.isEmpty()){
				Util.log("政府网站工作动态name："+name,"kplog",0);
				System.out.println("政府网站工作动态name："+name);
				String zxresult=PutZXZFWZ.documentZX(jsonData);
				return zxresult;
				
			}else{
				//根据视图id查询视图短名
				String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+viewid+"'";
				String viewName=jdbcids.JDBCVIEWIDSELECT(sql);
				Util.log("政府网站：转换为大写的视图短名================："+viewName.toUpperCase(),"kplog",0);
				System.out.println("政府网站：转换为大写的视图短名================："+viewName.toUpperCase());
				
				if("WEBZCJD".equals(viewName.toUpperCase())){
					System.out.println("政府网站政策解读name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutZCJDZFWZ.putZCJDzfwz(oPostData, jsondata);
					//处理附件
					Util.log("---------------政策解读处理附件开始-------------","kplog",0);
					System.out.println("---------------政策解读处理附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCJD(type,oPostData,jsondata);
					
				}else if("WEBZCWJ".equals(viewName.toUpperCase())){
					System.out.println("政府网站政策文件name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutZCWJZFWZ.putZCWJHYzfwz(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------政策文件处理附件开始-------------","kplog",0);
					System.out.println("---------------政策文件处理附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBLDJL".equals(viewName.toUpperCase())){
					System.out.println("政府网站领导简历name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutLDJLZFWZ.putLDJLzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------领导简历处理附件开始-------------","kplog",0);
					System.out.println("---------------领导简历处理附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBRSRM".equals(viewName.toUpperCase())){
					System.out.println("政府网站人事任免name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutRSRMZFWZ.putRSRMzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------人事任免处理附件开始-------------","kplog",0);
					System.out.println("---------------人事任免处理附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}
				else if("WEBCZ".equals(viewName.toUpperCase())){
					System.out.println("政府网站财政name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutCZZFWZ.putCZzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------财政处理默认附件开始-------------","kplog",0);
					System.out.println("---------------财政处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBRQGS".equals(viewName.toUpperCase())){
					System.out.println("政府网站 任前公示   name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutRQGSZFWZ.putRQGSzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------任前公示 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------任前公示 处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBGH".equals(viewName.toUpperCase())){
					System.out.println("政府网站 规划   name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutGHZFWZ.putGHzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------规划 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------规划 处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBHY".equals(viewName.toUpperCase())){
					System.out.println("政府网站 会议   name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutHYZFWZ.putHYzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------会议 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------会议 处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBZFGB".equals(viewName.toUpperCase())){
					System.out.println("政府网站 政府公报   name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutZFGBZFWZ.putZFGBzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------政府公报 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------政府公报 处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBHDJLZXJYTS".equals(viewName.toUpperCase())){
					System.out.println("政府网站 互动交流咨询建议投诉   name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutHDJLZXJYTSZFWZ.putHDJLZXJYTSzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------互动交流咨询建议投诉 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------互动交流咨询建议投诉 处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBHDJLZXDC".equals(viewName.toUpperCase())){
					System.out.println("政府网站 互动交流在线调查   name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutHDJLZXDCZFWZ.putZXDCzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------互动交流在线调查 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------互动交流在线调查 处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBHDJLZXFT".equals(viewName.toUpperCase())){
					System.out.println("政府网站 互动交流在线访谈   name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutHDJLZXFTZFWZ.putHDJLZXFTzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------互动交流在线访谈 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------互动交流在线访谈 处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBHDJLYJZJ".equals(viewName.toUpperCase())){
					System.out.println("政府网站 互动交流意见征集   name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutHDJLYJZJZFWZ.putYJZJzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------互动交流意见征集 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------互动交流意见征集 处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}else if("WEBJGGS".equals(viewName.toUpperCase())){
					System.out.println("政府网站 结果公示   name："+name);
					oPostData.put("bt", name);
					//处理其他字段开始
					oPostData=PutJGGS.putJGGSzfwj(oPostData, jsondata);
					//处理默认附件
					Util.log("---------------结果公示 处理默认附件开始-------------","kplog",0);
					System.out.println("---------------结果公示 处理默认附件开始-------------");
					oPostData=FileUplodeZFWZ.fujianZCWJ(type,oPostData,jsondata);
					
				}
				
			
			}
			

			
			//封装数据
			oPostData.put("ChannelId", channelId);
			oPostData.put("ObjectId", "0");
			oPostData.put("CurrUserName", USER); // 当前操作的用户
			//未处理的正文
			if(content!=null){
				String pcontent=null;
				String fcontent=null;
				//处理图片
				pcontent=ZWuplod.contentResourceUpload(content,5,"(src=\").*?(\")");
				//处理附件
				fcontent=ZWuplod.contentResourceUpload(pcontent,6,"(href=\").*?(\")");
				oPostData.put("zw", fcontent);
				System.out.println("====================处理后的正文==================");
				System.out.println(fcontent);
				System.out.println("====================处理后的正文==================");
				Util.log("====================处理后的正文==================","kplog",0);
				Util.log(fcontent,"kplog",0);
				Util.log("====================处理后的正文==================","kplog",0);
			}else {
				oPostData.put("zw", "&nbsp&nbsp&nbsp&nbsp&nbsp");
			}
			if(resTitle!=null){
				oPostData.put("fbt", resTitle);
			}
			if(abstracts!=null){
				oPostData.put("zy", abstracts);
			}
			if(author!=null){
				oPostData.put("zz", author);
			}
			if(source!=null){
				oPostData.put("ly", source);
			}
			if(pubDate!=null){
				oPostData.put("zxsj", pubDate);
			}
			Util.log("=====================================oPostData==================================","kplog",0);
			Util.log(oPostData,"kplog",0);
			Util.log("=====================================oPostData==================================","kplog",0);
			System.out.println("=====================================oPostData==================================："+oPostData);
			//开始添加到海云
			String addDocument=AddDocZFWZ.addHyDoc(oPostData,result,resID);
			
			return addDocument;
		}else{
			result.put("code", "-1");
			result.put("data", "接收开普数据失败!");
			System.out.println("接收开普数据失败!");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------政府网站：开普推送文档结束---------------");
			return jsonResult.toString();
		}
		
	}

	
}
