package com.trs.paper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.bpmsutil.BPMSUtil;
import com.trs.bpmsutil.FileUploadUtil;
import com.trs.bpmsutil.HttpController;
import com.trs.bpmsutil.HttpFileUploadUtil;
import com.trs.bpmsutil.JDBCSelectDoc;
import com.trs.kafka.Util;
import com.trs.oauth.ConstantUtil;


@Controller
@RequestMapping(value = "/tobpmszm")
public class BpmsPaperController{
	
	private static String bpms_url= ConstantUtil.bpms_url;
	public static final String  bpms_userName= ConstantUtil.bpms_userName;
	private static String bpms_password = ConstantUtil.bpms_password;
	private static String bpms_templateCode_zm = ConstantUtil.bpms_templateCode_zm;
	private static String bpms_appName = ConstantUtil.bpms_appName;
	private static String bpms_from_url = ConstantUtil.bpms_from_url;
	private static String resouse_path = ConstantUtil.resouse_path;
	private static String HR_URL = ConstantUtil.HR_URL;
	private static String bpms_fromCode_zm = ConstantUtil.bpms_fromCode_zm;	
	
	/**
	 * 正式获取token
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = {"/zmtobpms"},produces = "application/json; charset=utf-8")
	public   String zmToBpms(HttpServletRequest request,HttpServletResponse response) throws SQLException, Exception{
		
		System.out.println("--------------------------------------纸媒同步bpms开始--------------------------------------------------");
		Util.log("--------------------------------------纸媒同步bpms开始--------------------------------------------------","bpms",0);
		
//		String docid ="85"; 
//		String qd ="zm";
//		String loginname ="zhaolichao";
		
		String docid = request.getParameter("docid");
		String qd = request.getParameter("qd");
		String loginname = request.getParameter("loginname");//bpms_loginName
		String lgname=StringUtils.substringBefore(loginname,"%40"); 
		lgname=StringUtils.substringBefore(lgname,"@");
		lgname=StringUtils.substringBefore(lgname,"_");
		System.out.println("当前登录用户处理@，用于获取token：："+lgname);
		System.out.println("当前登录用户，用于获取token：："+loginname);

		//查询bpmslog
		int sig=JDBCSelectDoc.selectBpmslog(docid,"zm");// 1推送过未结束流程  2推送了已经结束流程  0 其他未推送
		if(sig == 1){
			Map hrMap =new HashMap<String, String>();
			hrMap.put("code", "0");	
			hrMap.put("message","已经送审过!");
			JSONObject hrJson =new JSONObject(hrMap);
			System.out.println("稿件已经推送到bpms！！！！！！");
    		Util.log("--------------------------------------纸媒同步bpms结束--------------------------------------------------","bpms",0);
    		System.out.println("--------------------------------------纸媒同步bpms结束--------------------------------------------------");
			return hrJson.toJSONString();
		}
		
		//获取token
		String result=HttpController.testGetToken(bpms_userName,bpms_password,lgname,bpms_url);
		//解析返回结果
		JSONObject jsonObjectHYSE = JSON.parseObject(result);
		String token = jsonObjectHYSE.get("id").toString();//获取到token
        
		//查询到需要传给bpms接口的书文档数据
		HashMap<String,String> bpmsmap=JDBCSelectDoc.selectPaperDoc(docid);//基础数据

		bpmsmap.put("稿件id", docid);
		bpmsmap.put("渠道标识", qd);
		//查询附件 并进行上传
		HashMap<String,String> bpmsfile=JDBCSelectDoc.selectFile(bpmsmap,docid,token,qd);//基础数据

		
		//定义返回map  第一层data
		Map resma =new HashMap<String, String>();
		resma.put("data", bpmsfile);
		resma.put("subject", bpmsmap.get("文章标题"));
		resma.put("draft", "0");
		
		resma.put("templateCode", bpms_templateCode_zm);
		
		//定义返回map  最二层data
		Map resmap =new HashMap<String, String>();
		resmap.put("data", resma);
		resmap.put("appName", bpms_appName);

		//转换成json数据
		JSONObject json =new JSONObject(resmap);
		
		//调用上传表单接口
		//设置表单头
	    Map<String, String> maphead = new HashMap<String, String>();
	    maphead.put("token", token);
	    Util.log("bpms调用json："+json.toJSONString(),"bpms",0);
	    System.out.println("bpms调用json："+json.toJSONString());
	    //调用接口
//	    String post = doPost1("fuserest","609011ab-8662-4678-9280-a8eff259a288","seeyonas","http://123.121.155.161:801/seeyon/rest/token");
	    String postfrom = sendPost(bpms_from_url,json.toJSONString(),maphead);
	    Util.log("bpms发起表单接口返回结果："+postfrom,"bpms",0);
	    System.out.println("bpms发起表单接口返回结果：："+postfrom);
	    //解析结果
		JSONObject jsonform = JSON.parseObject(postfrom);
		String formcode = jsonform.get("code").toString();//获取到token
		
		// 2推送了已经结束流程   0 没有查到数据  未推送
		if(sig == 2){
	    	Date date = new Date();
	    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	String last_time = dtf.format(date);
	    	System.out.println("推送时的时间：："+last_time);
			String bpmslogsql=" UPDATE xwcmmetadatabpmslog	SET  pushsig = '1' WHERE  recid = '"+docid+"' and channel = '"+qd+"'  ";
			int resu=JDBCSelectDoc.insertBpmsLog(bpmslogsql);
			if(resu>0){
				Util.log("修改记录表成功："+bpmslogsql,"bpms",0);
				System.out.println("修改记录表成功："+bpmslogsql);
			}else{
				Util.log("修改记录失败："+bpmslogsql,"bpms",0);
				System.out.println("修改记录表失败："+bpmslogsql);
			}
			
		}else{
			
	    	Date date = new Date();
	    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	String last_time = dtf.format(date);
	    	System.out.println("推送时的时间：："+last_time);
			String bpmslogsql="INSERT INTO xwcmmetadatabpmslog (`recid`, `channel`, `pushtime`, `pushtitle`, `pushsig`, `returnjson`, `bpmsopinion`, `bpmspushtime`) VALUES "
					+ "('"+docid+"', '"+qd+"', '"+last_time+"', '"+bpmsmap.get("文章标题")+"', '1', '"+postfrom+"', '', NULL);";
			int resu=JDBCSelectDoc.insertBpmsLog(bpmslogsql);
			if(resu>0){
				Util.log("添加记录表成功："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表成功："+bpmslogsql);
			}else{
				Util.log("添加记录失败："+bpmslogsql,"bpms",0);
				System.out.println("添加记录表失败："+bpmslogsql);
			}
		}

		
		//修改稿件状态
		String updateUnderreView=" UPDATE WCMChnlDoc  SET underreview = '1' WHERE  RECID = '"+docid+"' ";
		int uuv=JDBCSelectDoc.updataSql(updateUnderreView);
		if(uuv>0){
			Util.log("修改稿件状态成功："+updateUnderreView,"bpms",0);
			System.out.println("修改稿件状态成功："+updateUnderreView);
		}else{
			Util.log("修改稿件状态失败："+updateUnderreView,"bpms",0);
			System.out.println("修改稿件状态失败："+updateUnderreView);
		}
		
		Map hrMap =new HashMap<String, String>();
		//返回海融数据
		if("0".equals(formcode)){
			
			hrMap.put("code", "0");	
			hrMap.put("message","同步bpms审核成功!");	
			
		}else{
			hrMap.put("code", "-1");	
			hrMap.put("message", "送审失败！！稿件id为["+docid+"]");	
		}
		JSONObject hrJson =new JSONObject(hrMap);
		Util.log("--------------------------------------纸媒同步bpms结束--------------------------------------------------","bpms",0);
		System.out.println("--------------------------------------纸媒同步bpms结束--------------------------------------------------");
		return hrJson.toString();
        
	}
	
	
	/**
	 * 正式获取token
	 * @param request
	 * @param response
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = {"/dytobpms"},produces = "application/json; charset=utf-8")
	public   String dayangToBpms(HttpServletRequest request,HttpServletResponse response) throws SQLException, Exception{
		
		System.out.println("--------------------------------------纸媒大样同步bpms开始--------------------------------------------------");
		Util.log("--------------------------------------纸媒大样同步bpms开始--------------------------------------------------","bpms",0);

//		String websiteid ="18";//站点的id
//		String composeId ="8";//大样的id
//		String IsPdf ="1";//1是图片 其他是pdf
//		String PubDate ="2022-07-29";//发布的时间  2022-07-29
//		String jbdc = "纸媒叠次";//见报叠次   -----纸媒叠次 
//		String jbbm ="1版";//见报版名 ---- 1版		
//		String qd = "dy";//dy
//		String loginname ="zhaolichao";//bpms_loginName
		
		String websiteid = request.getParameter("paperid");//站点的id
		String composeId = request.getParameter("composeId");//大样的id
		String IsPdf = request.getParameter("IsPdf");//是图片还是pdf  false
		String PubDate = request.getParameter("PubDate");//发布的时间  2022-07-29
		String jbdc = request.getParameter("jbdc");//见报叠次   -----纸媒叠次 
		String jbbm = request.getParameter("jbbm");//见报版名 ---- 1版		
		String qd = request.getParameter("qd");//dy
		String loginname = request.getParameter("loginname");//bpms_loginName
		
		
		if(!BPMSUtil.checkParameterDY(websiteid,composeId,IsPdf,PubDate,jbdc,jbbm,qd,loginname)){
			Util.log("参数错误，请核对！！！！", "dayang", 0);
			Map hrMap =new HashMap<String, String>();
			hrMap.put("code", "-1");	
			hrMap.put("message","上传报纸参数错误！！!");
			JSONObject hrJson =new JSONObject(hrMap);
			System.out.println("大样审阅上传参数错误！！！！！！");
    		Util.log("--------------------------------------纸媒大样同步bpms结束--------------------------------------------------","bpms",0);
    		System.out.println("--------------------------------------纸媒大样同步bpms结束--------------------------------------------------");
			return hrJson.toJSONString();
		}
		
		//查询bpmslog 是否推送过
//		int bpmssig=JDBCSelectDoc.selectBpmslog(composeId,"dy");// 1推送过未结束流程  2推送了已经结束流程  其他未推送
		
		String lgname=StringUtils.substringBefore(loginname,"%40"); 
		lgname=StringUtils.substringBefore(lgname,"@");
		lgname=StringUtils.substringBefore(lgname,"_");
		System.out.println("当前登录用户处理@，用于获取token：："+lgname);
		System.out.println("当前登录用户，用于获取token：："+loginname);
	
		//查询配置获取pdf或图片的根目录
		//  /TRS/paper/zgrtb + / +  2022-07-29  + / + 纸媒叠次 + - + 1版  + / +  267_1658979552864.png
		String sRootPath=JDBCSelectDoc.getWebsite(websiteid, IsPdf);//  /TRS/DATA/paper/zgrtb 
		
		String sCurrDaYangPath=sRootPath+"/"+PubDate;
		char cSeparator = BPMSUtil.getPathSeperator(sRootPath);
        sCurrDaYangPath = sCurrDaYangPath + cSeparator + jbdc +'-'+ jbbm + cSeparator;
		String sFileName=JDBCSelectDoc.getComposeInfo(composeId);
        
		//最终下载地址
		String dowUrl=HR_URL+sCurrDaYangPath+sFileName;
		
		//下载大样图片或pdf
		try{
			HttpFileUploadUtil.downLoadFromUrl(dowUrl,sFileName,resouse_path);
		}catch (Exception e) {
			Util.log("附件下载地址：：："+dowUrl+"===== 附件下载名称"+sFileName+"-======附件保存地址："+resouse_path, "dayang", 0);
			Map hrMap =new HashMap<String, String>();
			hrMap.put("code", "-1");	
			hrMap.put("message","大样下载失败！！!");
			JSONObject hrJson =new JSONObject(hrMap);
			System.out.println("大样可能丢失，大样下载失败！！！！！！");
    		Util.log("--------------------------------------纸媒大样同步bpms结束--------------------------------------------------","bpms",0);
    		System.out.println("--------------------------------------纸媒大样同步bpms结束--------------------------------------------------");
			return hrJson.toJSONString();
		} 
		
		//获取token
		String result=HttpController.testGetToken(bpms_userName,bpms_password,lgname,bpms_url);
		//解析返回结果
		JSONObject jsonObjectHYSE = JSON.parseObject(result);
		String token = jsonObjectHYSE.get("id").toString();//获取到token
       

		//查询附件 并进行上传
		List reslist=new ArrayList<>();
		String suijishu=BPMSUtil.getSuiJiShu();
		String fileurl=BPMSUtil.upBpmsFile(sFileName, token);
		HashMap<String , String> resfujian=new HashMap<>();
		resfujian.put("subReference",suijishu);
		resfujian.put("fileUrl", fileurl);
		resfujian.put("sort", "0");
		reslist.add(resfujian);
		
		//组装参数
		//查询到需要传给bpms接口的书文档数据
		Map  bpmsmap=new HashMap<>();
		bpmsmap.put("稿件id", composeId);
		bpmsmap.put("渠道标识", qd);
		bpmsmap.put("文章标题", PubDate+"-"+jbdc +'-'+ jbbm);
		bpmsmap.put("附件", suijishu);
		
		HashMap res=new HashMap<>();
		res.put(bpms_fromCode_zm, bpmsmap);
		res.put("thirdAttachments", reslist);
		//定义返回map  第一层data
		Map resma =new HashMap<String, String>();
		resma.put("data", res);
		resma.put("subject",PubDate+"-"+jbdc +'-'+ jbbm);
		resma.put("draft", "0");
		resma.put("templateCode", bpms_templateCode_zm);
		
		//定义返回map  最二层data
		Map resmap =new HashMap<String, String>();
		resmap.put("data", resma);
		resmap.put("appName", bpms_appName);

		//转换成json数据
		JSONObject json =new JSONObject(resmap);
		
		//调用上传表单接口
		//设置表单头
	    Map<String, String> maphead = new HashMap<String, String>();
	    maphead.put("token", token);
	    Util.log("大样bpms调用json："+json.toJSONString(),"bpms",0);
	    System.out.println("大样bpms调用json："+json.toJSONString());
	    //调用接口
//	    String post = doPost1("fuserest","609011ab-8662-4678-9280-a8eff259a288","seeyonas","http://123.121.155.161:801/seeyon/rest/token");
	    String postfrom = sendPost(bpms_from_url,json.toJSONString(),maphead);
	    Util.log("大样bpms发起表单接口返回结果："+postfrom,"bpms",0);
	    System.out.println("大样bpms发起表单接口返回结果：："+postfrom);
	    //解析结果
		JSONObject jsonform = JSON.parseObject(postfrom);
		String formcode = jsonform.get("code").toString();//获取到token
		
		// 2推送了已经结束流程   0 没有查到数据  未推送
    	Date date = new Date();
    	SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String last_time = dtf.format(date);
    	System.out.println("大样推送时的时间：："+last_time);
		String bpmslogsql="INSERT INTO xwcmmetadatabpmslog (`recid`, `channel`, `pushtime`, `pushtitle`, `pushsig`, `returnjson`, `bpmsopinion`, `bpmspushtime`) VALUES "
				+ "('"+composeId+"', '"+qd+"', '"+last_time+"', '"+bpmsmap.get("文章标题")+"', '1', '"+postfrom+"', '', NULL);";
		int resu=JDBCSelectDoc.insertBpmsLog(bpmslogsql);
		if(resu>0){
			Util.log("大样添加记录表成功："+bpmslogsql,"bpms",0);
			System.out.println("大样添加记录表成功："+bpmslogsql);
		}else{
			Util.log("大样添加记录失败："+bpmslogsql,"bpms",0);
			System.out.println("大样添加记录表失败："+bpmslogsql);
		}


		Map hrMap =new HashMap<String, String>();
		//返回海融数据
		if("0".equals(formcode)){
			
			hrMap.put("code", "0");	
			hrMap.put("message","大样推送成功!");	
			
		}else{
			hrMap.put("code", "-1");	
			hrMap.put("message", "大样推送失败！！报纸id为["+composeId+"]");	
		}
		JSONObject hrJson =new JSONObject(hrMap);
		Util.log("--------------------------------------纸媒大样同步bpms结束--------------------------------------------------","bpms",0);
		System.out.println("--------------------------------------纸媒大样同步bpms结束--------------------------------------------------");
		return hrJson.toString();
        
	}
	
	
	 /**
	  * @param url 访问地址
	  *  @param param 需要传输参数参数；对象可以通过json转换成String
	  * @param header header 参数；可以通过下面工具类将string类型转换成map
	  * @return 返回网页返回的数据
	  */
	 public static String sendPost(String url, String param, Map<String, String> header) throws UnsupportedEncodingException, IOException {
	         OutputStreamWriter out;
	         URL realUrl = new URL(url);
	         // 打开和URL之间的连接
	         HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
	         //设置超时时间
	         conn.setConnectTimeout(5000);
	         conn.setReadTimeout(15000);
	         // 设置通用的请求属性
	         if (header!=null) {
	             for (Entry<String, String> entry : header.entrySet()) {
	                 conn.setRequestProperty(entry.getKey(), entry.getValue());
	             }
	         }
	         conn.setRequestMethod("POST");
	         conn.addRequestProperty("Content-Type", "application/json");
	         conn.setRequestProperty("accept", "*/*");
	         conn.setRequestProperty("connection", "Keep-Alive");
	         conn.setRequestProperty("user-agent",
	                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	         // 发送POST请求必须设置如下两行
	         conn.setDoOutput(true);
	         conn.setDoInput(true);
	         // 获取URLConnection对象对应的输出流
	         out = new OutputStreamWriter( conn.getOutputStream(),"UTF-8");// utf-8编码
	         // 发送请求参数
	         out.write(param);

	         // flush输出流的缓冲
	         out.flush();
	         int responseCode = conn.getResponseCode();  
	         InputStream in1=null;
	         // 定义BufferedReader输入流来读取URL的响应
			 if (responseCode == 200) {  
				 in1 = new BufferedInputStream(conn.getInputStream());  
			 } else {  
				 in1 = new BufferedInputStream(conn.getErrorStream());  
			 } 
             BufferedReader rd = new BufferedReader(new InputStreamReader(in1,"utf8"));
             String tempLine = rd.readLine();
             StringBuffer tempStr = new StringBuffer();
             String crlf = System.getProperty("line.separator");
             while (tempLine != null) {
                tempStr.append(tempLine);
                tempStr.append(crlf);
                tempLine = rd.readLine();
             }
             String responseContent = tempStr.toString();
	         if(out!=null){
	             out.close();
	         }

	         return responseContent;
	     }

	 
    public static void main(String[] args) throws Exception {
	
    	HttpServletRequest request=null;
    	HttpServletResponse response=null;
//    	BpmsPaperController.dayangToBpms(request,response);
    }
}