package com.trs.count;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.kptohysdzc.DESUtil;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class test {

	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	//以下三个参数不做同步
//	private static String bmwd = ChannelidUtil.bmwd;
//	private static String zwmc = ChannelidUtil.zwmc;
//	private static String zwzsdbp = ChannelidUtil.zwzsdbp;
	
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	
    private final static String USER_AGENT = "Mozilla/5.0";

    /**
     * 请求编码
     */
     static String requestEncoding = "UTF-8";
    /**
     * 连接超时
     */
     static int connectTimeOut = 5000;
    /**
     * 读取数据超时
     */
     static int readTimeOut = 10000;
	
     
     static int count = 0;
     
     
     public static void main(String[] args) throws SQLException, Exception {
 
    	 
    	 	String sitesql="select * from hy_middleapp where kaipu_id = 'f1a8ebb3aa6d465ea88f2d30dd4f7239' AND source = 'ZD'";
    	 	String hysiteid=jdbc.JDBCDriverSelect(sitesql);
    	 	System.out.println(hysiteid);
    	 //=================================================================================================================
    	 
//			int delete = jdbc.JDBCDriver("DELETE  FROM document where source='WD' and chnlid='39180' and siteid='124'");
//			System.out.println(delete);
    	 
			//调用海云接口查询文档详细数据
//			String sServiceId="gov_webdocument";
//			String sMethodName="findDocumentById";
//			Map savemap = new HashMap();
//			savemap.put("DocId","764706");
//			savemap.put("ChannelId","4408");
//			savemap.put("CurrUserName", USER); // 当前操作的用户
//			String hy11=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
//			System.out.println(hy11);
   //============================================================================================= 	 
//    	     for(int a=0 ; a<3; a++){
//    	    	 int b = 33;
//    	    	 count=count+b; 
//    	    	 System.out.println(count);
//    	     }
//    	     System.out.println("end="+count);
// ===================================================================================================================    	 
//			//查询新增栏目的视图id
//			String viewid=zfwzUtil.selectViewid("17715");
//			//查询开普元数据集id
//			if("".equals(viewid)){
//				System.out.println("咨询视图空字符串：："+viewid);
//				viewid="5";
//			}
//			String sql="SELECT * FROM viewid WHERE viewid = '"+viewid+"'";
//			String ysjjid=JDBC.selectYsjjid(sql);
//			System.out.println(ysjjid);

	}

	public static String delHTMLTag(String htmlStr){ 
        String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式 
        String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式 
        String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式 
         
        Pattern p_script=Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE); 
        Matcher m_script=p_script.matcher(htmlStr); 
        htmlStr=m_script.replaceAll(""); //过滤script标签 
         
        Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE); 
        Matcher m_style=p_style.matcher(htmlStr); 
        htmlStr=m_style.replaceAll(""); //过滤style标签 
         
        Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE); 
        Matcher m_html=p_html.matcher(htmlStr); 
        htmlStr=m_html.replaceAll(""); //过滤html标签 

        return htmlStr.trim(); //返回文本字符串 
    } 
	
	public static  String getPath(){
		String abc="<div class='view TRS_UEDITOR trs_paper_default trs_web'><p>详见附件。</p>"
				+ "<p class='insertfileTag' style='line-height: 16px;'><img style='vertical-align: middle; margin-right: 2px;'"
				+ " src='/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_pdf.gif' />"
				+ "<a style='font-size:12px; color:#0066cc;' appendix='true' otheroperation='false' "
				+ "href='/protect/P0201901/P020190115/P020190115655681814519.pdf' title='市国资委2018年度绩效任务四季度落实情况汇总表.pdf' "
				+ "OLDSRC='/protect/P0201901/P020190115/P020190115655681814519.pdf'>市国资委2018年度绩效任务四季度落实情况汇总表.pdf</a>"
				+ "</p></div><div class='view TRS_UEDITOR trs_paper_default trs_web'><p>详见附件。</p><p class='insertfileTag' "
				+ "style='line-height: 16px;'><img style='vertical-align: middle; margin-right: 2px;' "
				+ "src='/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_pdf.gif' />"
				+ "<a style='font-size:12px; color:#0066cc;' appendix='true' otheroperation='false' "
				+ "href='/protect/P0201901/P020190115/P020190115655681814519.pdf' title='市国资委2018年度绩效任务四季度落实情况汇总表.pdf' "
				+ "OLDSRC='/protect/P0201901/P020190115/P020190115655681814519.pdf'>市国资委2018年度绩效任务四季度落实情况汇总表.pdf</a></p></div>"
				+"<p> <iframe class='edui-upload-video video-js vjs-default-skin' "
				+ "src='http://192.141.252.5/mas/openapi/pages.do?method=exPlay&appKey=gov&id=18&autoPlay=false'"
				+ " width='420' height='280' appendix='true'></iframe>测试视频库</p>";
//		
//		String abc="<p> <iframe class='edui-upload-video video-js vjs-default-skin' "
//				+ "src='http://192.141.252.5/mas/openapi/pages.do?method=exPlay&appKey=gov&id=18&autoPlay=false'"
//				+ " width='420' height='280' appendix='true'></iframe>测试视频库</p>";		
//		String aaa="iframe class='edui-upload-video video-js vjs-default-skin' "
//				+ "src='http://192.141.252.5/mas/openapi/pages.do?method=exPlay&appKey=gov&id=18&autoPlay=false'"
//				+ " width='420' height='280' appendix='true'></iframe";
//		String bb="video controls='controls' loop='loop' width='480' height='400' src='http://192.168.1.219:7002/repo-web/manager/getManagerFile?filePath=group1/M00/00/B4/wKgBxFxqgemAL1joADEBLOFwO7I159.mp4' autoplay='autoplay'></video";
//		abc=abc.replace(aaa, bb);
//		System.out.println(abc);
//		String a="a.a.a.a.a.a.a";
//		String b="bbbb";
//		
//		a.replace(".", b);
//		System.out.println(a);
//		
//		String s = "my.test.txt";
//		System.out.println(s.replace(".", "#"));
		
		//  '/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_pdf.gif'
		//  '/protect/P0201901/P020190115/P020190115655681814519.pdf'
//		String abc="    <img style='ertical-align: middle; margin-right: 2px;' src='/govapp/lib/ueditor_demo/ueditor2/dialogs/attachment/fileTypeImages/icon_jpg.gif'/><a style='font-size:12px; color:#0066cc;' appendix='true' otheroperation='false' href='/protect/P0201902/P020190223/P020190223584663980378.png' title='Q量子银座.png'>Q量子银座.png</a>";
//		String name="icon_jpg";
//		String regExImg = "("+name+").*?(.jpg|.gif|.png|.jpeg|.bmp|.JPG|.GIF|.PNG|.JPEG|.BMP)";//正文中视频
////		String regExImg = "(icon_jpg).*?(.jpg|.gif|.png|.jpeg|.bmp|.JPG|.GIF|.PNG|.JPEG|.BMP)";//正文中视频
//		
		String regExImg = "(href=).*?(.pdf|.PDF|.RAR|.rar|.zip|.ZIP)";//正文中pdf  5  改成6
		Pattern patternImg = Pattern.compile(regExImg);
		Matcher matcherImg = patternImg.matcher(abc);
		List<String> list = new ArrayList<String>();
		while (matcherImg.find()) {
			String picPath = matcherImg.group();
			picPath = picPath.substring(8, picPath.length());
//			String fullPath = getFullPath(picPath);
			System.out.println("多少个path="+picPath);
			list.add(picPath);
		}
		return null;
	}

	public static  void  getpwd(){
		String sServiceId="gov_webdocument";
		String sMethodName="findOpenDataDocumentById";
		Map savemap = new HashMap();
		savemap.put("DocId","39");
		savemap.put("ChannelId","18");
		savemap.put("CurrUserName", "dev"); // 当前操作的用户
		String hy11=null;
		try {
			hy11 = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObjectHYSave = JSON.parseObject(hy11);
		Object dataArrHYSave = jsonObjectHYSave.get("ISSUCCESS");
		System.out.println("--海云返回结果："+jsonObjectHYSave.get("MSG"));
	}
	
	public static String hyQuerySites(){
		String sServiceId = "gov_site";
		String sMethodName = "querySites";
		Map oPostData=new HashMap();
		oPostData.put("CurrUserName", "dev"); // 当前操作的用户
		oPostData.put("MEDIATYPE", 1);
//		oPostData.put("SEARCHFIELDS", "SITEDESC");
		oPostData.put("MODULEID", 10);
		String hy = null;
		try {
			hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(hy);
		return hy;
	}
	
	public static String hyuser() throws Exception{
		String sServiceIdSE = "gov_user";
		String sMethodNameSE = "checkUserExist";
		Map map=new HashMap();
		map.put("CurrUserName", "dev"); // 当前操作的用户
		map.put("SEARCHVALUE", "abbb"); // 用户名称
		String hySE=HyUtil.dataMoveDocumentHyRbj(sServiceIdSE,sMethodNameSE,map);
		return hySE;
	}
	
	public static void  hylanmu() throws Exception{
		String sServiceId = "gov_site";
		String sMethodName = "querySitesAndChnls";
		Map oPostData = new HashMap();
		oPostData.put("CurrUserName", "dev"); // 当前操作的用户
		oPostData.put("MEDIATYPE",1);
		oPostData.put("OBJTYPE",1);
		oPostData.put("OBJID",1);
		String hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		System.out.println(hy);
	}
	
	public static void  hylanmu1() throws Exception{
		String sServiceId = "gov_channel";
		String sMethodName = "queryChannels";
		Map oPostData = new HashMap();
		oPostData.put("CurrUserName", "dev"); // 当前操作的用户
		oPostData.put("SITEID",1);
//		oPostData.put("OBJTYPE",1);
//		oPostData.put("OBJID",1);
		String hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		System.out.println(hy);
	}
	
	  public static String  deleRes(JSONObject json) throws Exception {
		    
		    String encrypt = DESUtil.encrypt(json.toJSONString(), Z_APPID);
		    String url =Z_URL+ "/resource/delRes?data=" + encrypt + "&appId="+Z_APPID;
		    String post = HttpUtil.doGet(url, "UTF-8");
		    System.out.println(post); 
		    return post;
	  }
	  
	public static void test123() {
	    ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.141.7.28");
        factory.setPort(5672);
        factory.setUsername("trs_rabbitmq");
        factory.setPassword("trsadmin!@#$5678");
        Connection connection = null;
		try {
			connection = factory.newConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Channel channel = null;
		try {
			channel = connection.createChannel();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			channel.queueDeclare("site", false, false, false, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("Customer Waiting Received messages");
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Customer Received '" + message + "'");
            }
        };
        try {
			channel.basicConsume("site", true, consumer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static List<Object> selectCh(String sitId,String pId) throws Exception{
		List<Object> list=new ArrayList<>();
		// 根据站点id查询下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", USER); // 当前操作的用户
//		mSE.put("SEARCHFIELDS", userName); // 用户名称
		//设置查询多少个用户
		mSE.put("pageSize", "1500");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", sitId);
		mSE.put("ParentChannelId", pId);
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		System.out.println("结果"+hy1);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2.size()>0){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					String SITEID=dataBean.getString("SITEID");
					String CHNLNAME=dataBean.getString("CHNLNAME");
					String CHANNELID=dataBean.getString("CHANNELID");
					String HASCHILDREN=dataBean.getString("HASCHILDREN");//是否存在下级栏目
					System.out.println(SITEID+"-------------------"+CHNLNAME+"----------------"+CHANNELID);
					Map<String, Object> mapChnl=new HashMap<>();
					mapChnl.put("siteId", SITEID);
					mapChnl.put("channelName", CHNLNAME);
					mapChnl.put("channelId", CHANNELID);
					mapChnl.put("pid", pId);//上一级栏目id,0表示一级栏目
					mapChnl.put("isLeaf", 0);
					list.add(mapChnl);
					if("true".equals(HASCHILDREN)){
						List<Object> listch=selectCh(SITEID,CHANNELID);
						list.addAll(listch);
					}
					
				}
			}
		}
		return list;
	}
	
	
	@SuppressWarnings({ "unchecked", "unused" })
	public static String  pushDocument(String data) throws Exception {
		
		System.out.println("文档接收到的数据:"+data);
		Map result = new HashMap();
		Map oPostData = new HashMap();
		JSONObject jsondata = JSON.parseObject(data.replace("'", "\""));
//		JSONObject jsondata = jsonObjectSE.getJSONObject("data");
		if(data != null){
			//接收数据成功
			String siteId=jsondata.getString("siteId");//站点id
			String channelId=jsondata.getString("channelId");//栏目id
			String resID=jsondata.getString("resID");//文档资源id
			String abstracts=jsondata.getString("abstracts");//摘要
			String resTitle=jsondata.getString("resTitle");//标题
			String name=jsondata.getString("name");//名称
			String author=jsondata.getString("author");//作者
			String content=jsondata.getString("content");//正文
			String source=jsondata.getString("idPoC");//来源
			String attachments=jsondata.getString("attachments");//附件
			String type=jsondata.getString("type");//1图片类型；2网页类型;3音频类型；4视频类型;5文档类型
			String DocRelTime=jsondata.getString("createdTime");//创建时间
			
			oPostData.put("DocRelTime", DocRelTime);
			oPostData.put("ChannelId", channelId);
			oPostData.put("ObjectId", "0");
			oPostData.put("DocTitle", resTitle);
			oPostData.put("DocHtmlCon", content);
			//去除html标签
			String txtContent=delHTMLTag(content);
			System.out.println("去除html标签txtContent："+txtContent);
			oPostData.put("DocHtmlCon", txtContent);
			oPostData.put("DocAbstract", abstracts);
			oPostData.put("SUBDOCTITLE", name);
			oPostData.put("DOCAUTHOR", author);
			oPostData.put("DOCSOURCENAME", source);
			//查询文档是否已经同步
			String kpid=null;
			try {
				kpid = cR.docCheckWD(resID);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result.put("code", "1");
				result.put("data", "中间表查询失败！");
				JSONObject jsonResult =new JSONObject(result);
				return jsonResult.toString();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result.put("code", "1");
				result.put("data", "中间表查询失败！");
				JSONObject jsonResult =new JSONObject(result);
				return jsonResult.toString();
			}
			if(kpid!=null){
				System.out.println("已经同步过该文章!!!!!");
				result.put("code", "1");
				result.put("data", "已经同步过该文章!!!!!");
				JSONObject jsonResult =new JSONObject(result);
				return jsonResult.toString();
			}else{
				if("1".equals(type)){  
					oPostData.put("DOCTYPE",50);  //图片
				}else if("2".equals(type)){
					
					oPostData.put("DOCTYPE",20);  //文字
					//正文不做处理，直接处理附件
					JSONArray attach=jsondata.getJSONArray("attachments");//相附件
					if(attach.size()>0){
						//图片
						 StringBuilder docRelPic = new StringBuilder();
						 docRelPic.append("[");
						 //附件
						 StringBuilder docRelFile = new StringBuilder();
						 docRelFile.append("[");
						for(int i = 0; i < attach.size(); i++){
							  String  kuURL="http://192.141.1.10:7002/repo-web";
							  String  fjName=attach.getJSONObject(i).getString("name");
							  String  fjpath= attach.getJSONObject(i).getString("path");
							  String  fjtype=attach.getJSONObject(i).getString("type");
							  if(fjpath != null){
								  //代表存在附件，处理上传
								  //先下载再上传
								  try {
									hfl.downLoadFromUrl(kuURL+fjpath,fjName,resouse_path);
								  } catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									result.put("code", "1");
									result.put("remarks", "下载附件失败！！");
									JSONObject jsonResult =new JSONObject(result);
									return jsonResult.toString();
								  }
								  //上传海云
								  Dispatch oDispatch = WCMServiceCaller.UploadFile(resouse_path+fjName);
								  String fileName = oDispatch.getUploadShowName();
								  //图片处理
								  if("1".equals(fjtype)){
									  docRelPic.append("{\"SrcFile\":\"" + fjName+ "\",\"AppFile\":\"" + fileName + "\",\"AppDesc\":\""
													+ fjName.substring(0,fjName.lastIndexOf(".")) + "\", \"AppendixId\":\"0\" }");
								  }else if("5".equals(fjtype)||"3".equals(fjtype)||"4".equals(fjtype)){//处理附件
									  docRelFile.append("{\"SrcFile\":\"" + fjName+ "\",\"AppFile\":\"" + fileName + "\",\"AppDesc\":\""
												+ fjName.substring(0,fjName.lastIndexOf(".")) + "\", \"AppendixId\":\"0\" }");
								  }
								  
								}
						}
						docRelPic.append("]");
						docRelFile.append("]");
						
						if(!"".equals(docRelPic)&&docRelPic!=null) {
							oPostData.put("DocRelPic",docRelPic);
						}
						if(!"".equals(docRelFile)&&docRelFile!=null) {
							oPostData.put("docRelFile",docRelFile);
						}
					}else{
						System.out.println("同步文档不存在附件！！！");
					}
					oPostData.put("CurrUserName", USER); // 当前操作的用户
					String sServiceId = "gov_webdocument";
					String sMethodName = "saveDocumentInWeb";
					String hy=null;
					try {
						hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						result.put("code", "1");
						result.put("data", "添加文档失败！！！！！");
						JSONObject jsonResult =new JSONObject(result);
						return jsonResult.toString();
					}
					JSONObject jsonObjectHYSE = JSON.parseObject(hy);
					Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
					JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
					
					Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
					System.out.println("海云返回结果："+jsonObjectHYSE.get("MSG"));
					if("true".equals(dataArrHYSave)){
						
						String	docid =jsonObjectHYGPSE.getString("DOCID");//标题
						Util.log("文档新建到海云存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')","log",0);
						System.out.println("开普同步到海云存入sql："+"INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')");
						Integer jd=null;
						try {
							jd = jdbc.JDBCDriver("INSERT INTO "+TABLE+" ( "+FIELDKP+","+FIELDHY+","+SOURCE+","+PARENTID+") VALUES ('"+resID+"','"+docid+"','KP','0')");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							result.put("code", "1");
							result.put("remarks", "中间表插入失败！！");
							JSONObject jsonResult =new JSONObject(result);
							return jsonResult.toString();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							result.put("code", "1");
							result.put("remarks", "中间表插入失败！！");
							JSONObject jsonResult =new JSONObject(result);
							return jsonResult.toString();
						}
						if(jd!=-1){
							result.put("code", "0");
							result.put("data", "已成功接收推送数据");
							Util.log("开普同步到海云中间表存入成功!","log",0);
							System.out.println("开普同步到海云中间表存入成功!");
							JSONObject jsonResult =new JSONObject(result);
							return jsonResult.toString();
						}else{
							result.put("code", "1");
							result.put("remarks", "开普同步到海云中间表存入失败!");
							Util.log("开普同步到海云中间表存入失败!","log",0);
							System.out.println("开普同步到海云中间表存入失败!");
							JSONObject jsonResult =new JSONObject(result);
							return jsonResult.toString();
						}
						
					}
					
				}else if("30".equals(type)){
					oPostData.put("DOCTYPE",30);  //链接型文档
				}else if("40".equals(type)){
					oPostData.put("DOCTYPE",40);  //外部文件文档
				}
				
			}
			
			
		}else{
			result.put("code", "1");
			result.put("remarks", "接收开普数据失败!");
			System.out.println("接收开普数据失败!");
			JSONObject jsonResult =new JSONObject(result);
			return jsonResult.toString();
		}
		result.put("code", "1");
		result.put("remarks", "操作失败！！！");
		JSONObject jsonResult =new JSONObject(result);
		return jsonResult.toString();
	}
	
	
	public  static void tksoid(){
		System.out.println(coIDName);
	}
	
	
	/**
	 * 修改文档-政策文件
	 * @param
	 */
	public static void upDOcZCWJTEST(String DOCID,String CHANNELID){
		
		System.out.println("------------修改政策文件视图文档开始------------");
		//MQ消息队列数据获取
		
		String wjmc=null;//文件名称
		String zcywgjc=null;//关键词
		String zw=null;//正文
		String zy=null;//文档摘要
		String cwrq=null;//成文日期
		String ssrq=null;//实施日期
		String fzrq=null;//废止日期
		String fbrq=null;//发布日期
		String yxx=null;//文件有效性
		String zcywwh=null;//发文序号
		String zcywzrbm=null;//发文单位
		String zcywlhfw=null;//联合发文单位
		String zcywgwzl=null;//主题分类
		
		String kaipuid=null;
		try {
				kaipuid=cR.doCheck(DOCID);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(kaipuid==null){
			System.out.println("开普中不存在该文档！");
		}else{

			//调用海云接口查询文档详细数据
			String sServiceId="gov_webdocument";
			String sMethodName="findOpenDataDocumentById";
			Map savemap = new HashMap();
			savemap.put("DocId",DOCID);
			savemap.put("ChannelId",CHANNELID);
			savemap.put("CurrUserName", USER); // 当前操作的用户
			String hy11=null;
			try {
				hy11 = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,savemap);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JSONObject jsonObjectHYSE = JSON.parseObject(hy11);
			Object dataArr = jsonObjectHYSE.get("DATA");//根据json对象中数组的名字解析出其所对应的值
			JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
			
			Object dataArrHYSave = jsonObjectHYSE.get("ISSUCCESS");
			System.out.println("id:"+DOCID+"--海云返回结果："+jsonObjectHYSE.get("MSG"));
			
			if("true".equals(dataArrHYSave)){
				//处理获取结果
				wjmc =jsonObjectHYGPSE.getString("WJMC");//文件名称
				zcywgjc=jsonObjectHYGPSE.getString("GJC");//关键词
				zw=jsonObjectHYGPSE.getString("ZW");//正文
				zy=jsonObjectHYGPSE.getString("NRZY");//内容摘要
				cwrq =jsonObjectHYGPSE.getString("CWRQ");//成文日期
				ssrq =jsonObjectHYGPSE.getString("SSRQ");//实施日期
				fzrq =jsonObjectHYGPSE.getString("FZRQ");//废止日期
				fbrq =jsonObjectHYGPSE.getString("FBRQ");//发布日期
				yxx =jsonObjectHYGPSE.getString("YXX");//有效性
				zcywwh =jsonObjectHYGPSE.getString("FWXH");//发文序号
				zcywzrbm =jsonObjectHYGPSE.getString("FWDW_NAME");//发文单位
				zcywlhfw =jsonObjectHYGPSE.getString("LHFWDW_NAME");//联合发文单位
				zcywgwzl =jsonObjectHYGPSE.getString("ZTFL_NAME");//主题分类
				
				// 处理正文 内容  
				
				String pContent=null;
				String otherContent=null;
				String videoContent=null;
				//正文不为空处理
				if(zw != null){
					try {
						//图片 Content_regular_p    截取开始 src='  5个字符串
						pContent = re.contentResource(zw, 5, Content_regular_p,"标题","13141","站点名称","127");
						//处理 zip  pdf  rar   截取开始  href=' 6个字符串
						otherContent = re.contentResource(pContent, 6, Content_regular,"标题","13141","站点名称","127");
						//处理正文中的视频标签
						String videoNmae=CHANNELID+"_"+wjmc+"_"+DOCID;
						videoContent = re.contentResourceVideo(otherContent, 1, Content_regular_v,videoNmae);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				
				//处理附件
				JSONArray json = new JSONArray();// 默认附件
			    JSONArray DOCRELFILE=jsonObjectHYGPSE.getJSONArray("FJ");//相关附件
			    String docPath=null;
			    String dName=null;
			    
			    if(DOCRELFILE!=null){
			    	
			    	for(int i = 0; i < DOCRELFILE.size(); i++){
				    	String url="http://192.141.252.5/gov/file/read_file.jsp?DownName=DOCUMENT&FileName=";
				    	docPath=DOCRELFILE.getJSONObject(i).getString("APPFILE");
				    	dName=DOCRELFILE.getJSONObject(i).getString("APPDESC");
				    	String Pv=docPath.substring(docPath.lastIndexOf("."),docPath.length());
				    	//上传之前先下载
						try {
							hfl.downLoadFromUrl(url+docPath,dName+Pv,resouse_path);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
				    	String pKPpath=re.PResource(resouse_path+dName+Pv,dName+Pv);
				    	//返回的开普上传路径，需要拼接成开普需要的格式
				    	//循环添加到JSONArray中
				 	    JSONObject fpCon= new JSONObject();
				 	    fpCon.put("name", dName);//附件名称
				 	    fpCon.put("path", pKPpath);// 附件路径
				 	    json.add(fpCon);
				    }
			    }
			    
				//查询对应栏目的开普id  
			    //根据站点id 获取到开普对应的资源库id  SITEID
					
					
				// 处理正常字段同步
				JSONObject jsonOb = new JSONObject();
				//修改文档固定参数   
				jsonOb.put("name", wjmc); 
				jsonOb.put("resTranMode", "0");// 0  文本内容    1：文件资源-文件传输  
				jsonOb.put("isOrig", "1");
				jsonOb.put("resId", "103797"); //  资源id
				//默认字段
				jsonOb.put("content", videoContent);//正文
				jsonOb.put("resTitle", wjmc); 
				jsonOb.put("abstracts", zy);
//				jsonOb.put("keyword", zcywgjc); 
				//元数据集字段
				jsonOb.put("cwrq", cwrq);
				jsonOb.put("ssrq", ssrq); 
				jsonOb.put("fzrq", fzrq);
				jsonOb.put("fbrq", fbrq); 
				jsonOb.put("status", "3"); 
				if(yxx!=null){
				    if(yxx.equals("1")){
					    jsonOb.put("zcywxxyxx", "是");
				    }else if(yxx.equals("0")){
					    jsonOb.put("zcywxxyxx", "否");
					}
				  }
				
				jsonOb.put("zcywwh", zcywwh); 
			    if(zcywzrbm!=null){
				    if(!zcywzrbm.equals("[]")){
				    	jsonOb.put("zcywzrbm", zcywzrbm.replace("[\"", "").replaceAll("\",\"", ";").replaceAll("\"]", ""));
				    }	
			    }
			    if(zcywlhfw!=null){
			    	 if(!zcywlhfw.equals("[]")){
					    jsonOb.put("zcywlhfw", zcywlhfw.replace("[\"", "").replaceAll("\",\"", ";").replaceAll("\"]", "")); 
					 }
			    }
			    if(zcywgwzl!=null){
				    if(!zcywgwzl.equals("[]")){
				    	 jsonOb.put("zcywgwzl", zcywgwzl.replace("[\"", "").replaceAll("\",\"", ";").replaceAll("\"]", ""));
				    }
			    }
				jsonOb.put("zcywgjc", zcywgjc);
				jsonOb.put("wjmc", wjmc);
				
				System.out.println("存入开普数据jsonOb==:"+jsonOb);
				System.out.println("存入开普数据json==:"+json);
				String result=null;
				try {
				   result=re.updateRes0(json, jsonOb);
				} catch (Exception e) {
					// TODO Auto-generated catch block
						e.printStackTrace();
				}
				    //处理开普新增返回结果
				JSONObject jsonresult = JSON.parseObject(result);
				Integer  dataresult = (Integer) jsonresult.get("code"); 
				if(dataresult==0){
					System.out.println("修改政策文件视图资源成功！");
				}else{
					System.out.println("修改政策文件视图资源失败："+jsonresult.get("msg"));
				}
				
			}else{
				System.out.println("海云查询文档详细信息接口返回信息:"+jsonObjectHYSE.get("MSG"));
			}
			
		
		}
		System.out.println("------------修改政策文件文档结束------------");	
	
	}
	
	
	// resTranMode=2 
    public static String  createRes2(JSONArray jsonArray,JSONObject json ) throws Exception {
	    
	    json.put("attachments", jsonArray.toJSONString());//附件
	    
	    String url = "http://192.140.199.194:7302/api/res/resource/createRes";
	    Map<String, String> map = new HashMap<String, String>();
	    System.out.println("新建文档内容json："+json);
	    System.out.println("新建文档内容url："+url);
	    map.put("appId", Z_APPID);
	    map.put("data", DESUtil.encrypt(json.toJSONString(),Z_APPID));
	    System.out.println("map"+map);
	    String post = HttpUtil.doPost(url, map,json);
	    System.out.println(post); 
	    return post;
}
    
    
    
    /**
     * <pre>
     * 发送带参数的POST的HTTP请求
     * </pre>
     *
     * @param reqUrl     HTTP请求URL
     * @param parameters 参数映射表
     * @return HTTP响应的字符串
     */
    public static String doPost(String reqUrl, Map parameters) {
    	System.out.println("------reqUrl开始------"+reqUrl);
    	System.out.println("------parameters开始------"+parameters);
    	System.out.println("------dopost开始------");
        HttpURLConnection url_con = null;
        String responseContent = null;
        try {
            String params = getMapParamsToStr(parameters, requestEncoding);
            System.out.println("dopost里的params"+params);
            
            params = URLDecoder.decode(params,"UTF-8");//解码
            System.out.println("dopost里的params解码后"+params);
            URL url = new URL(reqUrl);
            System.out.println("dopost里的url"+url);
            url_con = (HttpURLConnection) url.openConnection();
            url_con.setRequestMethod("POST");
            System.setProperty("sun.net.client.defaultConnectTimeout", String.valueOf(connectTimeOut));// （单位：毫秒）jdk1.4换成这个,连接超时
            System.setProperty("sun.net.client.defaultReadTimeout", String.valueOf(readTimeOut)); // （单位：毫秒）jdk1.4换成这个,读操作超时
            url_con.setRequestProperty("User-agent","Mozilla/4.0");
            url_con.setDoOutput(true);
            
            System.out.println("url_con++++"+url_con);
            byte[] b = params.toString().getBytes();
            url_con.getOutputStream().write(b, 0, b.length);
            url_con.getOutputStream().flush();
            url_con.getOutputStream().close();
            int responseCode = url_con.getResponseCode();  
            System.out.println("responseCode:"+responseCode);
            InputStream in=null;
	    	if (responseCode == 200) {  
	    		in = new BufferedInputStream(url_con.getInputStream());  
	   	} else {  
	    		in = new BufferedInputStream(url_con.getErrorStream());  
	    	} 
            BufferedReader rd = new BufferedReader(new InputStreamReader(in, HttpUtil.requestEncoding));
            String tempLine = rd.readLine();
            StringBuffer tempStr = new StringBuffer();
            String crlf = System.getProperty("line.separator");
            while (tempLine != null) {
                tempStr.append(tempLine);
                tempStr.append(crlf);
                tempLine = rd.readLine();
            }
            responseContent = tempStr.toString();
            rd.close();
            in.close();
        } catch (IOException e) {
            System.out.println("网络故障");
            e.printStackTrace();
        } finally {
            if (url_con != null) {
                url_con.disconnect();
            }
        }
        return responseContent;
    }

    private static String getMapParamsToStr(Map paramMap, String requestEncoding) throws IOException {
        StringBuffer params = new StringBuffer();
        // 设置边界
        for (Iterator iter = paramMap.entrySet().iterator(); iter.hasNext(); ) {
            Map.Entry element = (Map.Entry) iter.next();
            params.append(element.getKey().toString());
            params.append("=");
            params.append(URLEncoder.encode(element.getValue().toString(), requestEncoding));
            params.append("&");
        }

        if (params.length() > 0) {
            params = params.deleteCharAt(params.length() - 1);
        }

        return params.toString();
    }
    
    
    
    public static void posttest(HashMap<String, String> map){  //这里没有返回，也可以返回string

    	
    	String url = "http://192.141.7.55:8080/hyids/wbjk/webaddDoc";
        OkHttpClient mOkHttpClient = new OkHttpClient();

        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        Set<String> keySet = map.keySet();
        for(String key:keySet) {
            String value = map.get(key);
            formBodyBuilder.add(key,value);
        }
        FormBody formBody = formBodyBuilder.build();

        Request request = new Request
                .Builder()
                .post(formBody)
                .url(url)
                .build();

        try (Response response = mOkHttpClient.newCall(request).execute()) {
            System.out.println(response.body().string());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static String  documentZX(String data) throws Exception {
		
		JSONObject jsonObject = JSON.parseObject(data);
		
		
		String userName=jsonObject.getString("USERNAME");
		Map result = new HashMap();
		Map oPostData = new HashMap();
		String  dataArr =jsonObject.get("DATA").toString();//根据json对象中数组的名字解析出其所对应的值
		Map maps = (Map)JSON.parse(dataArr);  
		for (Object map : maps.entrySet()){  
			
			System.out.println(((Map.Entry)map).getKey()+"   =====推送默认数据=====   " + ((Map.Entry)map).getValue()); 
			if("fj".equals(((Map.Entry)map).getKey())){
				//处理附件
				Map fjV = (Map) ((Map.Entry)map).getValue(); 
				for (Object fjVfj : fjV.entrySet()){  
					oPostData.put(((Map.Entry)fjVfj).getKey(), ((Map.Entry)fjVfj).getValue());
				}
			}else{
				oPostData.put(((Map.Entry)map).getKey(), ((Map.Entry)map).getValue());	
			}
			
	    } 
		oPostData.put("CurrUserName", userName); // 当前操作的用户
		String sServiceId = "gov_webdocument";
		String sMethodName = "saveDocumentInWeb";
		System.out.println("调用海云之前的参数："+oPostData);
		String hy=null;
		try {
			hy = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("code", "-1");
			result.put("data", "调用新增接口失败！！！！！");
			JSONObject jsonResult =new JSONObject(result);
			System.out.println("------------------开普推送文档结束---------------");
			return jsonResult.toString();
		}

		return hy;
	}
    
    /**
     * 处理默认附件 图片 方法
     */
    public static String PResource(String  pPath,String pName) {
    	

    	String PKPpath=null;
    	String urlKp=null;
    	try {
			PKPpath=uploadFile(pPath, pName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//处理返回结果
		JSONObject jsonObject = JSON.parseObject(PKPpath);
		Object dataArr = jsonObject.get("data");
		JSONObject jsonObjectHYGPSE = JSON.parseObject(dataArr.toString());
		String code=jsonObject.getString("code");
		if("0".equals(code)){
			urlKp = jsonObjectHYGPSE.getString("fileURL");
		}else{
			System.out.println("开普上传失败："+jsonObject.get("MSG"));	
		}
		
    	return urlKp;
    }
    
	/**
	 * 上传文件 视频 图片 
	 * @throws Exception
	 */
  public static String uploadFile(String path , String name) throws Exception {
	  
	  
      JSONObject  json = new JSONObject();
      File file = new File(path);
      FileInputStream fileInputStream = new FileInputStream(file);
      String encrypt = DESUtil.encrypt(json.toJSONString(), Z_APPID);
	  String url =Z_URL + "/resource/uploadFile?data=" + encrypt + "&appId="+Z_APPID;
      String s = HttpUtil.uploadFileByOkHttp(url,fileInputStream,name);

      System.out.println(s);
      return s;
  }
    
	/**
	 * 修改开普资源
	 * @throws Exception
	 */
	//  resTranMode=0
	public static String  updateRes0(JSONArray jsonArray,JSONObject json) throws Exception {
		  	
		  	json.put("attachments", jsonArray.toJSONString());//附件
		    
		    String url =Z_URL+ "/resource/updateRes";
		    System.out.println(url);
		    Map<String, String> map = new HashMap<String, String>();
		    map.put("appId",ZWBJ_APPID);
		        map.put("data", DESUtil.encrypt(json.toJSONString(),ZWBJ_STR_DEFAULT_KEY));
		    String post = HttpUtil.doPost(url, map,json);
		    System.out.println(post); 
		    return post;
	 }
    
}
