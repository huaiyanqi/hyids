package com.trs.count;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mysql.jdbc.Connection;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.HyUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.AddDocByView;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;
import com.trs.zfwz.zfwzUtil;


@Controller
@RequestMapping(value = "/siteCountDoc")
public class selectSiteDocCount {
    public static final String  jdbc_idsdriver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_idsurl= ConstantUtil.jdbc_urlids;
    
    public static final String  jdbc_idsusername=  ConstantUtil.jdbc_usernameids;
    public static final String  jdbc_idspassword= ConstantUtil.jdbc_passwordids;
	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	/**
	 * 查询站点下的  文章数量
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		//站点id
		String siteid="112"; 
		String kpsiteid="30d0477805654ee581a0784a28d9b44e"; 
		
		//栏目父id
		String pid="0";   
		//操作用户
		String userName="dev";
		
			
		//获取站点下的数据总量
//		int sitecount=docCount(siteid,pid,userName,kpsiteid,0);
//		System.out.println(sitecount);
		
//		selectCountDoc("112","1311","测试");
		
		selectSiteCountDoc("80");
		
	}
	
	
	/**
	 * 接收开普查询站点下数据接口
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/selectDocCount"},produces = "application/json; charset=utf-8")
	public  String selectDocCount(@RequestParam(value ="siteid") String siteid) throws Exception{
		
		
		System.out.println("------------------开普查询站点下栏目总数开始------------------");
		UtilCount.log("获取到的开普查询站点id："+siteid,"selectsitecount",0);
		System.out.println("获取到的开普查询站点id："+siteid);
		
		//栏目父id
		String pid="0";   
		//操作用户
		String userName="dev";
		
		//根据开普id查询海云得站点id
		String sitesql="select * from hy_middleapp where kaipu_id = '"+siteid+"' AND source = 'ZD'";
		String hysiteid=jdbc.JDBCDriverSelect(sitesql);
		
		UtilCount.log("查询海云站点id======"+sitesql,"selectsitecount"+hysiteid,0);
		UtilCount.log("查询海云站点id结果======"+hysiteid,"selectsitecount"+hysiteid,0);
		System.out.println("查询海云站点id结果======"+hysiteid);
		if(hysiteid == null){
			Map<String, Object> map=new HashMap<>();
			map.put("code", "0");
			map.put("msg", "操作失败，无法查询到该站点id，请核对参数是否为站点id");
			map.put("data", "请核对参数是否为站点id");
			JSONObject json =new JSONObject(map);
			return json.toJSONString();
		}
		
		//获取站点下的数据总量  通过接口获取到栏目数量  进行累计  接口太慢 可能会出现超时情况
//		int sitecount=docCount(hysiteid,pid,userName,siteid,0);
		
		//获取站点下数据数量  直接通过sql  查询数据库
		int sitecount=selectSiteCountDoc(hysiteid);
//					  selectSiteCountDoc("80");
		
		//组装开普需要的数据结构
		Map<String, Object> map=new HashMap<>();
		if(sitecount == -1){
			map.put("code", "0");
			map.put("msg", "操作成功,该站点下无栏目！！");
			map.put("data", "没有查询到站点下得栏目");
		}else{
			map.put("code", "0");
			map.put("msg", "操作成功");
			Map<String, Object> mapData=new HashMap<>();
			mapData.put("siteId", siteid);
			mapData.put("site_count", sitecount);
			map.put("data",mapData);
		}

		//转换成json
		JSONObject json =new JSONObject(map);
		UtilCount.log("返回给开普最终数据结果：：：："+json.toJSONString(),"selectsitecount",0);
		
		return json.toJSONString();
		
	}
	
	
	
	/**
	 * 从一级栏目开始 查询栏目下的文章数量
	 * @throws Exception
	 */
	public static  int  docCount(String sitId,String pid,String userName,String kpsiteid,int count) throws Exception {
		
		// 根据站点id查询下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", userName); // 当前操作的用户
		//设置一次查询数量
		mSE.put("pageSize", "10000");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", sitId);
		mSE.put("ParentChannelId", pid);
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		System.out.println("结果"+hy1);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2.size()>0){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					
					String SITEID=dataBean.getString("SITEID");
					String CHNLDESC=dataBean.getString("CHNLDESC");//栏目名称
					String CHANNELID=dataBean.getString("CHANNELID");//栏目id

					String HASCHILDREN=dataBean.getString("HASCHILDREN");//是否存在下级栏目
					UtilCount.log(SITEID+"-------------------栏目名称==="+CHNLDESC+"----------------栏目id===="+CHANNELID,"selectsitecount",0);
					System.out.println(SITEID+"-------------------栏目名称==="+CHNLDESC+"----------------栏目id===="+CHANNELID);
					if("true".equals(HASCHILDREN)){
						//查询栏目下的数据数量
						int acount=selectCountDoc(SITEID,CHANNELID,CHNLDESC);
						count=count+acount;
						UtilCount.log("栏目下的数据数量=="+acount,"selectsitecount",0);
						System.out.println("栏目下的数据数量=="+acount);
						UtilCount.log("当前数据总数量=="+count,"selectsitecount",0);
						System.out.println("当前数据总数量=="+count);
						
						//继续查询下级栏目
						count=docCount(SITEID,CHANNELID,userName,kpsiteid,count);
//						UtilCount.log("count下级栏目栏目=="+count,"selectsitecount",0);
//						System.out.println("count下级栏目栏目=="+count);
					}else{
						//无下级栏目 ，仅查询栏目下的数据数量
						int bcount=selectCountDoc(SITEID,CHANNELID,CHNLDESC);
						count=count+bcount;
						UtilCount.log("栏目下的数据数量"+bcount,"selectsitecount",0);
						System.out.println("栏目下的数据数量=="+bcount);
						UtilCount.log("当前数据总数量==="+count,"selectsitecount",0);
						System.out.println("当前数据总数量=="+count);
					}
					
				}
			}else{
				count = -1;
				System.out.println("没有查询到站点下得栏目");
				UtilCount.log("没有查询到站点下得栏目==父栏目id：：：："+pid,"selectsitecount",0);
				UtilCount.log("没有查询到站点下得栏目==站点id：：：："+sitId,"selectsitecount",0);
			}
		}
		System.out.println("最底层栏目查询完毕返回count=="+count);
		UtilCount.log("最底层栏目查询完毕返回count=="+count,"selectsitecount",0);
		return count;
	}


	
	
	public static int selectCountDoc(String SITEID,String CHANNELID,String CHNLNAME) throws SQLException, Exception{
		int con=0;
		//根据栏目id查询到栏目下的已发实体数据数量
		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027)";
		
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sqlcount);
		while(resultSet.next()) {
			con=resultSet.getInt("con");
//			UtilCount.log("查询到的条数："+resultSet.getInt("con"),SITEID+"文章总数", 0);
//			UtilCount.log("栏目id："+CHANNELID,SITEID+"文章总数", 0);
//			UtilCount.log("栏目名称："+CHNLNAME,SITEID+"文章总数", 0);
			System.out.println("目前查询到栏目文章数量："+con);
		}
		resultSet.close();
		stat.close();
		conn.close();
		return con;
	}

	
	/**
	 * 查询海云站点下得实体文章数量
	 * @param SITEID
	 * @param CHANNELID
	 * @param CHNLNAME
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public static int selectSiteCountDoc(String SITEID) throws SQLException, Exception{
		int con=0;
		//根据栏目id查询到栏目下的已发实体数据数量
		String sqlcount="SELECT count(*) con FROM wcmdocument WHERE SITEID = '"+SITEID+"'  AND DOCSTATUS in (10,8,12,1027) "
				+ " AND DOCCHANNEL in (SELECT CHANNELID  FROM wcmchannel  WHERE siteid = '"+SITEID+"'   AND STATUS = 0)";
		
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_idsdriver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

        // 执行查询
        stat = conn.createStatement();
        UtilCount.log("查询站点："+SITEID+"数据sql：","selectsitecount"+SITEID,0);
        UtilCount.log("sqlcount"+sqlcount,"selectsitecount"+SITEID,0);
        System.out.println("查询站点："+SITEID+"数据sql：");
        System.out.println("sqlcount"+sqlcount);
        ResultSet resultSet = stat.executeQuery(sqlcount);
		while(resultSet.next()) {
			con=resultSet.getInt("con");
//			UtilCount.log("查询到的条数："+resultSet.getInt("con"),SITEID+"文章总数", 0);
//			UtilCount.log("栏目id："+CHANNELID,SITEID+"文章总数", 0);
//			UtilCount.log("栏目名称："+CHNLNAME,SITEID+"文章总数", 0);
			UtilCount.log("目前查询到栏目文章数量："+con,"selectsitecount"+SITEID,0);
			System.out.println("目前查询到栏目文章数量："+con);
		}
		resultSet.close();
		stat.close();
		conn.close();
		return con;
	}

}
