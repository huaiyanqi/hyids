package com.trs.count;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.mysql.jdbc.Connection;
import com.trs.addhytokpsdzc.AddDocumentControllerZCWJ;
import com.trs.addhytokpzfwz.AddDocumentControllerZX;
import com.trs.hy.ChannelReceiver;
import com.trs.hy.ChannelidUtil;
import com.trs.hy.HttpFileUpload;
import com.trs.hy.HttpUtil;
import com.trs.hy.ResResource;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCIDS;
import com.trs.jdbc.JDBCIIP;
import com.trs.kafka.Util;
import com.trs.mqadddoc.GovPu;
import com.trs.oauth.ConstantUtil;


@Controller
@RequestMapping(value = "/channleDoc")
public class selectChannelDocCount {

    public static final String  jdbc_idsdriver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_idsurl= ConstantUtil.jdbc_urlids;
    
    public static final String  jdbc_idsusername=  ConstantUtil.jdbc_usernameids;
    public static final String  jdbc_idspassword= ConstantUtil.jdbc_passwordids;
	private static String HY_URL = ConstantUtil.HY_URL;
	private static String Z_URL = ConstantUtil.Z_URL;
	private static String Z_APPID = ConstantUtil.Z_APPID;
	private static String Kp_conten_url = ConstantUtil.Kp_conten_url;
	private static String resouse_path = ConstantUtil.resouse_path;	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	private static String Content_regular_v = ConstantUtil.Content_regular_v;
	private static String Z_ID = ConstantUtil.Z_ID;
	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
	private static String PARENTIDLM = ConstantUtil.PARENTID;
	private static String zcwj = ChannelidUtil.zcwj;
	private static String zcjd = ChannelidUtil.zcjd;
	private static String zx = ChannelidUtil.zx;
	private static String lsgb = ChannelidUtil.lsgb;
	private static String czxx = ChannelidUtil.czxx;
	private static String qxzm = ChannelidUtil.qxzm;
	
	//以下三个参数不做同步
//	private static String bmwd = ChannelidUtil.bmwd;
//	private static String zwmc = ChannelidUtil.zwmc;
//	private static String zwzsdbp = ChannelidUtil.zwzsdbp;
	
	
	static ResResource  re= new ResResource();
	static ChannelReceiver cR=new ChannelReceiver();
	static HttpFileUpload hfl=new HttpFileUpload();
	static JDBCIIP iip=new JDBCIIP();
	static JDBC  jdbc=new JDBC();
	static JDBCIDS  jdbcids=new JDBCIDS();
	static Map<String,String> ssoIDName=new HashMap<String,String>();
	static Map<String,String> coIDName=new HashMap<String,String>();
	static GovPu gov=new GovPu();
	static AddDocumentControllerZX addDoc=new AddDocumentControllerZX();
	static AddDocumentControllerZCWJ addDocother=new AddDocumentControllerZCWJ();
	
	static HttpUtil htu=new HttpUtil();
	static ResResource res=new ResResource();
	
	/**
	 * 历史栏目同步
	 * 历史数据同步
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		
		String aaa= "15212";
		String aaa1= "15212";
		
		String channelcount=selectChannelCount(aaa,aaa1);
		System.out.println(channelcount);

	}
	
	/**
	 * 接收开普查询栏目下数据接口
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = {"/selectDocCount"},produces = "application/json; charset=utf-8")
	public  String selectDocCount(@RequestParam(value ="channelid") String channelid) throws Exception{
		
		
		System.out.println("------------------开普查询站点下栏目总数开始------------------");
		UtilCount.log("栏目id======"+channelid,"selectchannelcount",0);
		System.out.println("获取到的开普查询栏目id："+channelid);
		
		//根据开普id查询海云得栏目id
		String sitesql="select * from hy_middleapp where kaipu_id = '"+channelid+"' AND source = 'LM'";
		String hychannelid=jdbc.JDBCDriverSelect(sitesql);
		
		UtilCount.log("查询海云栏目id===sql==="+sitesql,"selectchannelcount",0);
		UtilCount.log("查询海云栏目id结果======"+channelid,"selectchannelcount",0);
		//获取站点下的数据总量
		String channelcount=selectChannelCount(hychannelid,channelid);
		
		return channelcount;
		
	}
	
	
	/**
	 * 直接查询栏目下的实体数据数量
	 * @throws Exception
	 */

	public static String selectChannelCount(String CHANNELID,String channelidkp) {
		
		Map<String, Object> map=new HashMap<>();
		
		//根据栏目id查询到栏目下的已发实体数据数量
		String sqlcount="SELECT COUNT(*) con FROM wcmdocument WHERE DOCCHANNEL = '"+CHANNELID+"' AND DOCSTATUS in (10,8,12,1027)";
		
		UtilCount.log("查询海云栏目下数据数量sql====="+sqlcount,"selectchannelcount",0);
		try {
	    	Connection conn = null;
	        Statement stat = null;

	        // 注册驱动
	        Class.forName(jdbc_idsdriver);

	        // 创建链接
	        conn = (Connection) DriverManager.getConnection(jdbc_idsurl,jdbc_idsusername,jdbc_idspassword);

	        // 执行查询
	        stat = conn.createStatement();
	        ResultSet resultSet = stat.executeQuery(sqlcount);
			int con=0;
			while(resultSet.next()) {
				con=resultSet.getInt("con");
				UtilCount.log("查询到的条数："+con,CHANNELID+"文章总数", 0);
				System.out.println("查询到的文章数量："+con);
			}
			//关闭连接
			resultSet.close();
			stat.close();
			conn.close();
			//处理数据
			map.put("code", "0");
			map.put("msg", "操作成功");
			Map<String, Object> mapData=new HashMap<>();
			mapData.put("channelId", channelidkp);
			mapData.put("chnl_count", con);
			map.put("data",mapData);
		} catch (SQLException e) {
			map.put("code", "1");
			map.put("msg", "操作失败");
			map.put("data", "连接数据库失败！！！");
			System.out.println("查询栏目下数据数量失败：：：："+e);
			e.printStackTrace();
		} catch (Exception e) {
			map.put("code", "1");
			map.put("msg", "操作失败");
			map.put("data", "连接数据库失败！！！");
			System.out.println("查询栏目下数据数量失败：：：："+e);
			e.printStackTrace();
		}
		//转换成json
		JSONObject json =new JSONObject(map);
		UtilCount.log("查询海云栏目下数据数量最终返回结果====="+json.toJSONString(),"selectchannelcount",0);
		return json.toJSONString();
	}


}
